package com.aozhi.hugemountain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.adapter.ConsumerbillListAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StaffConsumerBillActivity extends Activity {
	LinearLayout ll_all;
	LinearLayout ll_month;
	Button btn_back,btn_ok;
	ArrayList<ConsumptionObject> list_month=new ArrayList<ConsumptionObject>();
	ArrayList<ConsumptionObject> list_all=new ArrayList<ConsumptionObject>();
	ArrayList<ConsumptionObject> list_all_bydate=new ArrayList<ConsumptionObject>();
	ConsumptionListObject mConsumerListObject;
	ConsumptionObject mConsumptionObject;
	ConsumerbillListAdapter adapter;
	TextView tv_date1, tv_date2,monthrevenue;
	ListView lv_list;
	double Month_moneyrevenue = 0;
	static final int DATE_DIALOG_ID = 0;
	String date_state="";
	private int mYear;
	private int mMonth;
	private int mDay;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staff_consumer_bill);
		initView();
		myclick();
		getconsumer_staff();
	}
	private void myclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		tv_date1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				date_state = "1";
				showDialog(DATE_DIALOG_ID);
			}
		});
		tv_date2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				date_state = "2";
				showDialog(DATE_DIALOG_ID);
			}
		});
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!tv_date1.getText().equals("年-月-日")&&!tv_date2.getText().equals("年-月-日")){
					list_all_bydate.clear();
				for(int i=0;i<list_all.size();i++){
					if(textparseint(list_all.get(i).create_time)>=textparseint(tv_date1.getText().toString())&&textparseint(list_all.get(i).create_time)<=textparseint(tv_date2.getText().toString())){
						list_all_bydate.add(list_all.get(i));
					}
				}
				adapter = new ConsumerbillListAdapter(StaffConsumerBillActivity.this,list_all_bydate);
				lv_list.setAdapter(adapter);
				}else{
					Toast.makeText(StaffConsumerBillActivity.this, "请选择开始及结束时间！", Toast.LENGTH_LONG).show();
				}
				
			}
		});
	}
	public int textparseint(String  textdate) {
		try {
		return	Integer.parseInt(new SimpleDateFormat("yyyyMMdd")
					.format(new SimpleDateFormat("yyyy-MM-dd")
							.parse(textdate)));
		} catch (NumberFormatException e) {
			return 0;
		} catch (ParseException e) {
			return 0;
		}
	}
	private void initView() {
		ll_all=(LinearLayout)findViewById(R.id.ll_all);
		ll_month=(LinearLayout)findViewById(R.id.ll_month);
		lv_list=(ListView) findViewById(R.id.lv_list);
		adapter=new ConsumerbillListAdapter(StaffConsumerBillActivity.this, list_all);
		lv_list.setAdapter(adapter);
		btn_back=(Button)findViewById(R.id.btn_back);
		tv_date1 = (TextView) findViewById(R.id.tv_date1);
		tv_date2 = (TextView) findViewById(R.id.tv_date2);
		btn_ok=(Button) findViewById(R.id.btn_ok);
		Calendar mycalendar=Calendar.getInstance(Locale.CHINA);
        Date mydate=new Date(); //获取当前日期Date对象
        monthrevenue=(TextView)findViewById(R.id.monthrevenue);
        mycalendar.setTime(mydate);////为Calendar对象设置时间为当前日期
		 mYear=mycalendar.get(Calendar.YEAR); //获取Calendar对象中的年
	        mMonth=mycalendar.get(Calendar.MONTH);//获取Calendar对象中的月
	        mDay=mycalendar.get(Calendar.DAY_OF_MONTH);//获取这个月的第几天
		if(MyApplication.StaffConsumer.toString().equals("month")){
			ll_all.setVisibility(View.GONE);
		}else{
			ll_month.setVisibility(View.GONE);
		}
	}
	private void getconsumer_staff() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffconsumption" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumerListObject = JSON.parseObject(v, ConsumptionListObject.class);
				list_all = mConsumerListObject.response;
					if (mConsumerListObject.meta.getMsg().equals("OK")) {
						if (list_all.size() > 0) {
							adapter = new ConsumerbillListAdapter(StaffConsumerBillActivity.this, list_all);
							lv_list.setAdapter(adapter);
							setrevenue();
						} else {
							Toast.makeText(StaffConsumerBillActivity.this, "无商铺",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(StaffConsumerBillActivity.this, "无商铺", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					Toast.makeText(StaffConsumerBillActivity.this, "无商铺", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	private void updateDisplay() {
		if(date_state.equals("1")){
		tv_date1.setText(new StringBuilder().append(mYear).append("-")
				.append(mMonth + 1).append("-").append(mDay).append(" "));
		}else{
			tv_date2.setText(new StringBuilder().append(mYear).append("-")
					.append(mMonth + 1).append("-").append(mDay).append(" "));
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};
	
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}
	private void setrevenue() {
		for (int i = 0; i < list_all.size(); i++) {
			if (get_year(SimpleDateFormat(list_all.get(i).create_time)) == get_year(new Date())) {
				if (get_month(SimpleDateFormat(list_all.get(i).create_time)) == get_month(new Date())) {
					Month_moneyrevenue += Double.parseDouble(list_all.get(i).money);
				}
			}
		}
		monthrevenue.setText(String.valueOf(Month_moneyrevenue));
	}
	public Date SimpleDateFormat(String str) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public int get_year(Date date) {
		return date.getYear() + 1900;
	}

	public int get_month(Date date) {
		return date.getMonth() + 1;
	}
}
