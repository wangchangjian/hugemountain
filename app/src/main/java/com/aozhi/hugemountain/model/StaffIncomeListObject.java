package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class StaffIncomeListObject {
	public Meta meta;
	public ArrayList<StaffIncomeObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<StaffIncomeObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<StaffIncomeObject> response) {
		this.response = response;
	}
}
