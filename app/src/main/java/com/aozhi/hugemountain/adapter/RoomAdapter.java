package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.model.ServiceObject;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RoomAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<RoomObject> list = new ArrayList<RoomObject>();
	public RoomAdapter(Context ctx, ArrayList<RoomObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_room, null);
			mHolder.tv_number=(TextView) convertView.findViewById(R.id.tv_number);
			mHolder.rl_room=(RelativeLayout)convertView.findViewById(R.id.rl_room);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_number.setText(list.get(position).code);
		if(list.get(position).mark.equals("0")){//空闲
			mHolder.rl_room.setBackgroundResource(R.drawable.room_green);
			mHolder.tv_number.setTextColor(mContext.getResources().getColor(R.color.room_green));
		}else if(list.get(position).mark.equals("1")){//忙碌
			mHolder.rl_room.setBackgroundResource(R.drawable.room_red);
			mHolder.tv_number.setTextColor(mContext.getResources().getColor(R.color.room_red));
		}else if(list.get(position).mark.equals("2")){//预订
			mHolder.rl_room.setBackgroundResource(R.drawable.room_yellow);
			mHolder.tv_number.setTextColor(mContext.getResources().getColor(R.color.yellow1));
		}else if(list.get(position).mark.equals("3")){//维护
			mHolder.rl_room.setBackgroundResource(R.drawable.room_hui);
			mHolder.tv_number.setTextColor(mContext.getResources().getColor(R.color.hui));
		}
		
		return convertView;
	}
	
	class Holder {
		
		TextView tv_number;
		RelativeLayout rl_room;
	}
}
