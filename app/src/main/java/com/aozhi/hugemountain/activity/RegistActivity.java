package com.aozhi.hugemountain.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.LoginActivity;
import com.aozhi.hugemountain.model.*;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.*;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.alibaba.fastjson.JSON;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegistActivity extends Activity {
	Button btn_ok, btn_yanzhengma, btn_back;
	TextView text_xieyi;
	private String num, str;
	private ProgressDialog progressDialog = null;
	private EditText et_name, et_pwd, et_surepwd, et_yanzhengma;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private CheckBox cb;
	private TextView et_photo;
	private String number = "";
	// 填写从短信SDK应用后台注册得到的APPKEY
	private static String APPKEY = "a60f6c4438e8";
	// 填写从短信SDK应用后台注册得到的APPSECRET
	private static String APPSECRET = "35b4d102dd365758244b77426d483d70";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_regist);
		initView();
		initListener();
	}

	private void initView() {
		btn_ok = (Button) findViewById(R.id.btn_ok);
		btn_back = (Button) findViewById(R.id.btn_back);
		et_photo = (TextView) findViewById(R.id.et_photo);
		et_pwd = (EditText) findViewById(R.id.et_pwd);
		et_surepwd = (EditText) findViewById(R.id.et_surepwd);
		et_name = (EditText) findViewById(R.id.et_name);
		text_xieyi = (TextView) findViewById(R.id.text_xieyi);
		btn_yanzhengma = (Button) findViewById(R.id.btn_yanzhengma);
		et_yanzhengma = (EditText) findViewById(R.id.et_yanzhengma);
		cb = (CheckBox) findViewById(R.id.cb);
	}

	private void initListener() {

		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});

		text_xieyi.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// Intent mIntent = new Intent(this,XieYiActivity.class);
				// startActivity(mIntent);
			}
		});

		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// String regEx =
				// "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
				// Pattern p = Pattern.compile(regEx);
				// Matcher m = p.matcher(et_name.getText().toString());
				if (et_pwd.getText().toString().equals("")) {
					Toast.makeText(RegistActivity.this, "请输入密码！",
							Toast.LENGTH_LONG).show();
				} else if (et_pwd.getText().toString().length() < 6) {
					Toast.makeText(RegistActivity.this, "密码不得少于6位！",
							Toast.LENGTH_LONG).show();
				} else if (!et_pwd.getText().toString()
						.equals(et_surepwd.getText().toString())) {
					Toast.makeText(RegistActivity.this, "密码与确认密码不一致！",
							Toast.LENGTH_LONG).show();
				} else if (!cb.isChecked()) {
					Toast.makeText(RegistActivity.this, "请同意巨岳注册协议！",
							Toast.LENGTH_LONG).show();
				} else {
					getuser();
				}
			}
		});

	}

	private void getuser() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "selectclientbyphoto" };
		String[] funParam3 = new String[] { "photo",
				et_photo.getText().toString() };
		params1.add(funParam1);
		params1.add(funParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(RegistActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					user_callbackListener);
		} else {
			Toast.makeText(RegistActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
					.show();
		}
		SharedPreferences mySharedPreferences = getSharedPreferences(
				"client_user", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("name", et_photo.getText().toString());
		editor.putString("pwd", et_pwd.getText().toString());
		editor.putBoolean("ok", true);
		editor.commit();
	}

	private CallbackListener user_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					Toast.makeText(RegistActivity.this, "登录账号已存在，请重新输入",
							Toast.LENGTH_LONG).show();
				} else {
					getregister();
				}

			}
		}

	};

	private void getregister() {
		ArrayList<String[]> params = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "registclient" };
		String[] funParam2 = new String[] { "photo",
				et_photo.getText().toString() };
		String[] funParam3 = new String[] { "img", "/picture/u11.png" };
		String[] funParam4 = new String[] { "pwd",
				encryption(et_pwd.getText().toString()) };
		params.add(funParam1);
		params.add(funParam2);
		params.add(funParam3);
		params.add(funParam4);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				if (mLoginListObject.getMeta().msg.equals("OK")) {
					Toast.makeText(RegistActivity.this, "注册成功",
							Toast.LENGTH_LONG).show();
					Intent mintent = new Intent(RegistActivity.this,
							LoginActivity.class);
					startActivity(mintent);
					finish();
				} else {
					Toast.makeText(RegistActivity.this, "注册失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RegistActivity.this, "注册失败", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}
}
