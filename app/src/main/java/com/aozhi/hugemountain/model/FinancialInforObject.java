package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class FinancialInforObject {
	public Meta meta;
	public ArrayList<FinancialInforBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<FinancialInforBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<FinancialInforBean> response) {
		this.response = response;
	}
	
}
