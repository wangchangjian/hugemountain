package com.aozhi.hugemountain.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

import com.aozhi.hugemountain.utils.NaiveTrustManager;


import android.util.Log;

public class UploadImageService {

	private static final String TAG = "UploadImageService";

	// private static final String TAG = "uploadFile";
	private static final int TIME_OUT = 10 * 1000; // 锟斤拷时时锟斤拷
	private static final String CHARSET = "utf-8"; // 锟斤拷锟矫憋拷锟斤拷

	/**
	 * android锟较达拷锟侥硷拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷4.0
	 * 
	 * @param file
	 * @param RequestURL
	 * @return
	 */
	public static String uploadFile4(File file, String RequestURL) {

		return "";
	}

	/**
	 * android锟较达拷锟侥硷拷锟斤拷锟斤拷锟斤拷锟斤拷 锟斤拷锟斤拷锟斤拷锟斤拷4.0
	 * 
	 * @param file
	 *            锟斤拷要锟较达拷锟斤拷锟侥硷拷
	 * @param RequestURL
	 *            锟斤拷锟斤拷锟絩ul
	 * @return 锟斤拷锟斤拷锟斤拷应锟斤拷锟斤拷锟斤拷
	 */
	public static String uploadFile(File file, String RequestURL) {
		String result = null;
		String BOUNDARY = UUID.randomUUID().toString(); // 锟竭斤拷锟绞?锟斤拷锟斤拷锟斤拷
		String PREFIX = "--", LINE_END = "\r\n";
		String CONTENT_TYPE = "multipart/form-data"; // 锟斤拷锟斤拷锟斤拷锟斤拷
		// RequestURL =
		// "https://192.168.0.101:8443/Des3Encrypt/servlet/EncryptServlet?content=3214241241241";
		try {
			URL url = new URL(RequestURL);

			if (RequestURL.indexOf("https") < 0) {
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setReadTimeout(TIME_OUT);
				conn.setConnectTimeout(TIME_OUT);
				conn.setDoInput(true); // 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
				conn.setDoOutput(true); // 锟斤拷锟斤拷锟斤拷锟斤拷锟?			
				conn.setUseCaches(false); // 锟斤拷锟斤拷锟斤拷使锟矫伙拷锟斤拷
				conn.setRequestMethod("POST"); // 锟斤拷锟斤拷式
				conn.setRequestProperty("Charset", CHARSET); // 锟斤拷锟矫憋拷锟斤拷
				conn.setRequestProperty("connection", "keep-alive");
				conn.setRequestProperty("Content-Type", CONTENT_TYPE
						+ ";boundary=" + BOUNDARY);
				//String sid = MyApplication.getInstance().getSessionId();
				//conn.setRequestProperty("sid", sid);
				//byte[] bypes = sid.getBytes();
				//conn.getOutputStream().write(bypes);
				if (file != null) {
					/**
					 * 锟斤拷锟侥硷拷锟斤拷为锟秸ｏ拷锟斤拷锟侥硷拷锟斤拷装锟斤拷锟斤拷锟较达拷
					 */
					DataOutputStream dos = new DataOutputStream(
							conn.getOutputStream());
					StringBuffer sb = new StringBuffer();
					sb.append(PREFIX);
					sb.append(BOUNDARY);
					sb.append(LINE_END);
					/**
					 * 锟斤拷锟斤拷锟截碉拷注锟解： name锟斤拷锟斤拷锟街滴拷锟斤拷锟斤拷锟斤拷锟斤拷锟揭猭ey 只锟斤拷锟斤拷锟絢ey 锟脚匡拷锟皆得碉拷锟斤拷应锟斤拷锟侥硷拷
					 * filename锟斤拷锟侥硷拷锟斤拷锟斤拷锟街ｏ拷锟斤拷锟阶猴拷锟斤拷 锟斤拷锟斤拷:abc.png
					 */

					sb.append("Content-Disposition: form-data; name=\"file1\"; filename=\""
							+ file.getName() + "\"" + LINE_END);
					sb.append("Content-Type: application/octet-stream; charset="
							+ CHARSET + LINE_END);
					sb.append(LINE_END);
					dos.write(sb.toString().getBytes());
					InputStream is = new FileInputStream(file);
					byte[] bytes = new byte[1024];
					int len = 0;
					while ((len = is.read(bytes)) != -1) {
						dos.write(bytes, 0, len);
					}
					is.close();
					dos.write(LINE_END.getBytes());
					byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END)
							.getBytes();
					dos.write(end_data);
					dos.flush();
					/**
					 * 锟斤拷取锟斤拷应锟斤拷 200=锟缴癸拷 锟斤拷锟斤拷应锟缴癸拷锟斤拷锟斤拷取锟斤拷应锟斤拷锟斤拷
					 */
					int res = conn.getResponseCode();
					Log.e(TAG, "response code:" + res);
					Log.e(TAG, "request success");
					InputStream input = conn.getInputStream();
					StringBuffer sb1 = new StringBuffer();
					int ss;
					while ((ss = input.read()) != -1) {
						sb1.append((char) ss);
					}
					result = sb1.toString();
					Log.e(TAG, "result : " + result);

				}
			} else {
				try {
					HttpsURLConnection conn = getConnection(url);
					conn.setReadTimeout(TIME_OUT);
					conn.setConnectTimeout(TIME_OUT);
					conn.setDoInput(true); // 锟斤拷锟斤拷锟斤拷锟斤拷锟斤拷
					conn.setDoOutput(true); // 锟斤拷锟斤拷锟斤拷锟斤拷锟?					conn.setUseCaches(false); // 锟斤拷锟斤拷锟斤拷使锟矫伙拷锟斤拷
					conn.setRequestMethod("POST"); // 锟斤拷锟斤拷式
					conn.setRequestProperty("Charset", CHARSET); // 锟斤拷锟矫憋拷锟斤拷
					conn.setRequestProperty("connection", "keep-alive");
					conn.setRequestProperty("Content-Type", CONTENT_TYPE
							+ ";boundary=" + BOUNDARY);
//					String sid = MyApplication.getInstance().getSessionId();
//					conn.setRequestProperty("sid", sid);
//					byte[] bypes = sid.getBytes();
//					conn.getOutputStream().write(bypes);
					if (file != null) {
						/**
						 * 锟斤拷锟侥硷拷锟斤拷为锟秸ｏ拷锟斤拷锟侥硷拷锟斤拷装锟斤拷锟斤拷锟较达拷
						 */
						DataOutputStream dos = new DataOutputStream(
								conn.getOutputStream());
						StringBuffer sb = new StringBuffer();
						sb.append(PREFIX);
						sb.append(BOUNDARY);
						sb.append(LINE_END);
						/**
						 * 锟斤拷锟斤拷锟截碉拷注锟解： name锟斤拷锟斤拷锟街滴拷锟斤拷锟斤拷锟斤拷锟斤拷锟揭猭ey 只锟斤拷锟斤拷锟絢ey 锟脚匡拷锟皆得碉拷锟斤拷应锟斤拷锟侥硷拷
						 * filename锟斤拷锟侥硷拷锟斤拷锟斤拷锟街ｏ拷锟斤拷锟阶猴拷锟斤拷 锟斤拷锟斤拷:abc.png
						 */

						sb.append("Content-Disposition: form-data; name=\"file1\"; filename=\""
								+ file.getName() + "\"" + LINE_END);
						sb.append("Content-Type: application/octet-stream; charset="
								+ CHARSET + LINE_END);
						sb.append(LINE_END);
						dos.write(sb.toString().getBytes());
						InputStream is = new FileInputStream(file);
						byte[] bytes = new byte[1024];
						int len = 0;
						while ((len = is.read(bytes)) != -1) {
							dos.write(bytes, 0, len);
						}
						is.close();
						dos.write(LINE_END.getBytes());
						byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END)
								.getBytes();
						dos.write(end_data);
						dos.flush();
						/**
						 * 锟斤拷取锟斤拷应锟斤拷 200=锟缴癸拷 锟斤拷锟斤拷应锟缴癸拷锟斤拷锟斤拷取锟斤拷应锟斤拷锟斤拷
						 */
						int res = conn.getResponseCode();
						Log.e(TAG, "response code:" + res);
						Log.e(TAG, "request success");
						InputStream input = conn.getInputStream();
						StringBuffer sb1 = new StringBuffer();
						int ss;
						while ((ss = input.read()) != -1) {
							sb1.append((char) ss);
						}
						result = sb1.toString();
						Log.e(TAG, "result : " + result);

					}
				} catch (KeyManagementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Open an URL connection. If HTTPS, accepts any certificate even if not
	 * valid, and connects to any host name.
	 * 
	 * @param url
	 *            The destination URL, HTTP or HTTPS.
	 * @return The URLConnection.
	 * @throws IOException
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	private static final int SOCKET_TIMEOUT = 3000;
	private static final TrustManager[] TRUST_MANAGER = { new NaiveTrustManager() };
	private static final AllowAllHostnameVerifier HOSTNAME_VERIFIER = new AllowAllHostnameVerifier();

	private static HttpsURLConnection getConnection(URL url)
			throws IOException, NoSuchAlgorithmException,
			KeyManagementException {
		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
		if (conn instanceof HttpsURLConnection) {
			// Trust all certificates
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(new KeyManager[0], TRUST_MANAGER, new SecureRandom());
			javax.net.ssl.SSLSocketFactory socketFactory = context
					.getSocketFactory();
			((HttpsURLConnection) conn).setSSLSocketFactory(socketFactory);

			// Allow all hostnames
			((HttpsURLConnection) conn).setHostnameVerifier(HOSTNAME_VERIFIER);

		}
		conn.setConnectTimeout(SOCKET_TIMEOUT);
		conn.setReadTimeout(SOCKET_TIMEOUT);
		return conn;
	}
}
