package com.aozhi.hugemountain.model;

import java.io.Serializable;

 public class StaffObject implements Serializable {

	 
	
	 public String id;
     public String staffphoto;
     public String staffpwd;
     public String name;
     public String sex;
     public String age;
     public String avatar;
     public String remark;
     public String create_time;
     public String update_time;
     public String mark;
     public String del_flag;
     public String store_id;
     public String service;
     public String types;
     public String phone;
     public String commission;
	public String code_id;
 	public String user_store_id;
 	public String user_staff_id;
 	public String commission1;
 	public String id1;
 	public String type_id;
 	public String contents;
 	public String workstatus;
 	public String service_id;
 	public String surchange;
 	public String service_click;
 	public String begin_time;
 	public String end_time;
 	public String service_staff;
 	public String balance;
 	public String service_name;
 	public String intermission;
 	public  String times;
 	public String datas;
 	public String  remarks;
 	public String service_time;
 	public String money;
 	public String getCode_id() {
		return code_id;
	}
	public void setCode_id(String code_id) {
		this.code_id = code_id;
	}
	public String getCommission() {
 		return commission;
 	}
 	public void setCommission(String commission) {
 		this.commission = commission;
 	}
     public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStaffphoto() {
		return staffphoto;
	}

	public void setStaffphoto(String staffphoto) {
		this.staffphoto = staffphoto;
	}

	public String getStaffpwd() {
		return staffpwd;
	}

	public void setStaffpwd(String staffpwd) {
		this.staffpwd = staffpwd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getUpdate_time() {
		return update_time;
	}

	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

	public String getDel_flag() {
		return del_flag;
	}

	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}

	public String getStore_id() {
		return store_id;
	}

	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
     public String toString() {
 		return "员工" + id +"/t"+name+ "\t" + staffphoto+ "\t" + phone + "\t" + service
 				+ "\t" + name + "\t" + phone + "\t"
 				+ remark + "\t" + create_time +  "\t" + age+"\n";
 	}
     
	public String clientphoto;//客户账号
}
