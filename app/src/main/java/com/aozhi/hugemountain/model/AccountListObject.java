package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class AccountListObject {
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<AccountObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<AccountObject> response) {
		this.response = response;
	}
	public Meta meta;
	public ArrayList<AccountObject> response;
}
