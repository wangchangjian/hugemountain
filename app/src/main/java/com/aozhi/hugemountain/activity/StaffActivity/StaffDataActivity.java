package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ConsumerActivity.BusinessActivity;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;

public class StaffDataActivity extends Activity {
	private Button btn_back;
	private ArrayList<StaffObject> list_staff = new ArrayList<StaffObject>();
	private StaffObject staff;
	private StaffObject staffinfo;
	private String typeid = "";
	private StaffListObject mStaffListObject;
	private TextView tv_code, ed_server, tv_content, tv_ok,tv_age,tv_sex,tv_name;
	private ProgressDialog progressDialog = null;
	private Spinner tv_type;
	private ArrayList<StaffObject> type_list = new ArrayList<StaffObject>();
	private ArrayList<StaffObject> service_list = new ArrayList<StaffObject>();
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private ViewPager vp_mainadv;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staffdetail);
		initView();
		myonclick();
		// getType();
		getPhoto();
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		staffinfo = (StaffObject) getIntent()
				.getSerializableExtra("staff_info");
		tv_code = (TextView) findViewById(R.id.tv_code);
		tv_type = (Spinner) findViewById(R.id.tv_type);
		tv_age= (TextView) findViewById(R.id.tv_age);
		tv_sex= (TextView) findViewById(R.id.tv_sex);
		tv_name= (TextView) findViewById(R.id.tv_name);
		ed_server = (TextView) findViewById(R.id.ed_server);
		tv_content = (TextView) findViewById(R.id.tv_content);
		tv_ok = (TextView) findViewById(R.id.tv_ok);
		tv_code.setText("编号" + staffinfo.code_id);
		tv_content.setText(staffinfo.remark);
		getServicesList(staffinfo.service_id);
		
	}

	private void myonclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						BusinessActivity.class);
				intent.putExtra("staff", "");
				setResult(505, intent);
				finish();
			}
		});
		tv_ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						BusinessActivity.class);
				intent.putExtra("staff", staffinfo);
				setResult(504, intent);
				finish();
			}
		});
	}

	private void getType() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStafftype" };
		params2.add(funParam2);

		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				type_list = mStaffListObject.getResponse();
				if (type_list.size() > 0) {
					Spinner();
				}

			} else {
				Toast.makeText(getApplicationContext(), "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 下拉框适配器
	private void Spinner() {
		ArrayList<String> timelist = new ArrayList<String>();
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		for (int i = 0; i < type_list.size(); i++) {
			timelist.add(type_list.get(i).types);
		}
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, timelist);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		tv_type.setAdapter(adapter);
		// 第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		tv_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				typeid = type_list.get(position).id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				typeid = list.get(0).types;
			}
		});
		SpinnerAdapter apsAdapter = tv_type.getAdapter(); // 得到SpinnerAdapter对象
		int k = apsAdapter.getCount();
		if (list.size() > 0) {
			for (int i = 0; i < k; i++) {
				if (list.get(0).type.equals(apsAdapter.getItem(i).toString())) {
					tv_type.setSelection(i, true);// 默认选中项
					break;
				}
			}
		}
	}

	// 图片轮播
	private void getPhoto() {
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getUserImg" };
			String[] paramName = new String[] { "user_id", staffinfo.id };
			params.add(methodParam);
			params.add(paramName);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mImageListObject = JSON.parseObject(v, ImageListObject.class);
				imglist = mImageListObject.getResponse();
				int n = 0;
				if (imglist.size() > 0) {
					for (n = 0; n < imglist.size(); n++) {
						final ImageView imgView = new ImageView(
								getApplicationContext());
						imgView.setClickable(true);
						imgView.setScaleType(ScaleType.FIT_XY);
						downloadImage.addTasks(imglist.get(n).img, imgView,
								new DownloadImage.ImageCallback() {
									public void imageLoaded(Bitmap imageBitmap,
											String imageUrl) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag(imageUrl);
										}
									}

									public void imageLoaded(Bitmap imageBitmap,
											DownloadImageMode callBackTag) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag("");
										}
									}
								});
						downloadImage.doTask();
						final String url = imglist.get(n).url;
						final String title = imglist.get(n).name;

						if (images.equals("")) {
							images = imglist.get(n).img;
						} else {
							images = images + "," + imglist.get(n).img;
						}

						imgView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent mIntent = new Intent(
										getApplicationContext(),
										ImagePagerActivity.class);
								mIntent.putExtra("images", images);
								startActivity(mIntent);
							}
						});
						imgViews.add(imgView);

					}
					vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
					vp_mainadv.setAdapter(new MyAdapter());
					// 设置监听，当ViewPager中的页面改变时调用
					vp_mainadv
							.setOnPageChangeListener(new MyPageChangeListener());
				}
			}
		}

	};

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imglist.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onPageSelected(int arg0) {
			// for(int i=0;i<imglist.size();i++){
			// if(i!=arg0){
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_normal);
			// }else{
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_focused);
			// }
			// }
		}
	}

	private void getServicesList(String service_id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getservicebyid" };
			String[] idParam = new String[] { "id",staffinfo.id };
			params.add(methodParam);
			params.add(idParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					getSellerSalesList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getSellerSalesList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffListObject = JSON.parseObject(result, StaffListObject.class);
			service_list = mStaffListObject.getResponse();
			if (mStaffListObject.meta.getMsg().equals("OK")) {
//				String sev = "";
//				if (service_list.size() > 0) {
//					for (int i = 0; i < service_list.size(); i++) {
//						sev += service_list.get(i).name + ",";
//					}
//					ed_server.setText(sev);
//				}
				
				tv_sex.setText(service_list.get(0).sex);
				tv_age.setText(service_list.get(0).age);
				tv_name.setText(service_list.get(0).name);
				tv_content.setText(service_list.get(0).remarks);
				
			}
		} else {
			Toast.makeText(getApplicationContext(), "无数据",
					Toast.LENGTH_LONG).show();
		}

	}
	};
}
