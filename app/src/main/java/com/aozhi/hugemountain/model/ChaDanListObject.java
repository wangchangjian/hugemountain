package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class ChaDanListObject {
	public Meta meta;
	public ArrayList<ChaDanBean> response;
	
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ChaDanBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ChaDanBean> response) {
		this.response = response;
	}
	
}
