package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class ServiceListObject {
	public Meta meta;
	public ArrayList<ServiceObject> response;
	public ArrayList<ServiceObject> type;
	
	public ArrayList<ServiceObject> getType() {
		return type;
	}
	public void setType(ArrayList<ServiceObject> type) {
		this.type = type;
	}
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ServiceObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ServiceObject> response) {
		this.response = response;
	}
}
