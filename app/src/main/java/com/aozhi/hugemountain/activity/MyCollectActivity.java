package com.aozhi.hugemountain.activity;


import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.CollectClientAdapter;
import com.aozhi.hugemountain.adapter.CollectStaffAdapter;
import com.aozhi.hugemountain.adapter.CollectStoreListAdapter;
import com.aozhi.hugemountain.model.ClientListObject;
import com.aozhi.hugemountain.model.ClientObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyCollectActivity extends Activity {

	Button btn_back;
	TextView t1;
	TextView t2,tv_title;
	LinearLayout item1;
	LinearLayout item2,li_staff;
	ArrayList<StoreObject> list_store=new ArrayList<StoreObject>();
	ArrayList<StaffObject> list_staff=new ArrayList<StaffObject>();
//	ArrayList<ClientObject> list_client=new ArrayList<ClientObject>();
	StaffListObject mStaffListObject;
	StoreListObject mStoreListObject;
	ClientListObject mClientListObject;
	ClientObject mClientObject;
	StoreObject mStoreObject;
	StaffObject mStaffObject;
	CollectStoreListAdapter adapter1;
	CollectStaffAdapter adapter2;
	CollectClientAdapter adapter3;
	private String client_id="";
	private ListView list_1=null;
	private ListView list_2=null;
	String Status = "";
	private TextView nodata1,nodata2;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_mycollect);
		initView();
		myonclick();
		
		if (Status.equals("我的收藏")) {
			tv_title.setText("我的收藏");
			adapter1=null;
			adapter2=null;
			getstore();
			getstaff();
		}else if (Status.equals("staff")) {
//			getstores();
			li_staff.setVisibility(View.GONE);
			getclient();
		}
	}

	private void initView() {
		Status =getIntent().getStringExtra("stats");
		t1=(TextView)findViewById(R.id.tv_service);
		t2=(TextView)findViewById(R.id.tv_staff);
		item1=(LinearLayout)findViewById(R.id.item1);
		item2=(LinearLayout)findViewById(R.id.item2);
		li_staff=(LinearLayout)findViewById(R.id.li_staff);
		nodata1=(TextView) findViewById(R.id.nodata1);
		nodata2=(TextView) findViewById(R.id.nodata2);
		list_1=(ListView) findViewById(R.id.list_1);
		list_2=(ListView) findViewById(R.id.list_2);
		tv_title=(TextView)findViewById(R.id.tv_title);
		adapter1=new CollectStoreListAdapter(MyCollectActivity.this, list_store);
		adapter2=new CollectStaffAdapter(MyCollectActivity.this, list_staff);
		list_1.setAdapter(adapter1);
		list_2.setAdapter(adapter2);
		client_id= MyApplication.Clientuser.id;
	}
	private void getstores() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getcollect_stores" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private void getclient() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getcollect_client" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	

	private void getstore() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getcollect_store" };
		String[] name1 = new String[] { "client_id", client_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private void getstaff() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getcollect_staff" };
		String[] name1 = new String[] { "client_id", client_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	
	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
					list_store = mStoreListObject.response;
					if (mStoreListObject.meta.getMsg().equals("OK")) {
						if (list_store.size() > 0) {
							item2.setVisibility(View.GONE);
							item1.setVisibility(View.VISIBLE);
							adapter1 = new CollectStoreListAdapter(MyCollectActivity.this, list_store);
							list_1.setAdapter(adapter1);
						} else {
							
							Toast.makeText(MyCollectActivity.this, "无商铺",
									Toast.LENGTH_LONG).show();
						}

					} else {
						
						Toast.makeText(MyCollectActivity.this, "无商铺", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					nodata1.setVisibility(View.VISIBLE);
					list_1.setVisibility(View.GONE);
					Toast.makeText(MyCollectActivity.this, "无商铺", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	
	
	
	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
					mStaffListObject = JSON.parseObject(v, StaffListObject.class);
					list_staff = mStaffListObject.response;
					if (mStaffListObject.meta.getMsg().equals("OK")) {
						if (list_staff.size() > 0) {
							item1.setVisibility(View.GONE);
							item2.setVisibility(View.VISIBLE);
							adapter2 = new CollectStaffAdapter(MyCollectActivity.this, list_staff);
							list_2.setAdapter(adapter2);
						} else {
							
							Toast.makeText(MyCollectActivity.this, "无员工",
									Toast.LENGTH_LONG).show();
						}

					} else {
						
						Toast.makeText(MyCollectActivity.this, "无员工", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					nodata2.setVisibility(View.VISIBLE);
					list_2.setVisibility(View.GONE);
					Toast.makeText(MyCollectActivity.this, "无员工", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list_staff = mStaffListObject.response;
				if (mStaffListObject.meta.getMsg().equals("OK")) {
					if (list_staff.size() > 0) {
							item2.setVisibility(View.VISIBLE);
							item1.setVisibility(View.GONE);
							nodata2.setVisibility(View.GONE);
							list_2.setVisibility(View.VISIBLE);
							adapter2 = new CollectStaffAdapter(MyCollectActivity.this, list_staff);
							list_2.setAdapter(adapter2);
						} else {
							nodata2.setVisibility(View.VISIBLE);
							list_2.setVisibility(View.GONE);
							Toast.makeText(MyCollectActivity.this, "无客户",
									Toast.LENGTH_LONG).show();
						}

					} else {
						nodata2.setVisibility(View.VISIBLE);
						list_2.setVisibility(View.GONE);
						Toast.makeText(MyCollectActivity.this, "无客户", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					nodata2.setVisibility(View.VISIBLE);
					list_2.setVisibility(View.GONE);
					Toast.makeText(MyCollectActivity.this, "无客户", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	private void myonclick() {
		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t1.setTextColor(getResources().getColor(R.color.y1));
				t2.setTextColor(getResources().getColor(R.color.n1));
				
				t1.setBackgroundColor(getResources().getColor(R.color.bg1));
				t2.setBackgroundColor(getResources().getColor(R.color.bg2));
				
				item1.setVisibility(View.VISIBLE);
				item2.setVisibility(View.GONE);
			}
		});
		
		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t2.setTextColor(getResources().getColor(R.color.y1));
				t1.setTextColor(getResources().getColor(R.color.n1));
				
				t2.setBackgroundColor(getResources().getColor(R.color.bg1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg2));
				
				item2.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
			}
		});
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}
}
