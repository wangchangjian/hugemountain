package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ConsumptionObject;


import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ConsumerbillListAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ConsumptionObject> invite_list = new ArrayList<ConsumptionObject>();
	
	public ConsumerbillListAdapter(Context ctx, ArrayList<ConsumptionObject> li) {
		mContext = ctx;
		invite_list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return invite_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return invite_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_consumerbill, null);
			mHolder.name=(TextView) convertView.findViewById(R.id.name);
			mHolder.num=(TextView) convertView.findViewById(R.id.num);
			mHolder.create_time=(TextView) convertView.findViewById(R.id.create_time);
			mHolder.money=(TextView) convertView.findViewById(R.id.money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(invite_list.get(position).name);
//		mHolder.num.setText("1*"+invite_list.get(position).number+"份");
		mHolder.create_time.setText(invite_list.get(position).create_time);
		mHolder.money.setText("￥"+invite_list.get(position).balance);
		
		if(!invite_list.get(position).sname.equals("")){
			mHolder.num.setText("服务项目："+invite_list.get(position).sname);
		}
		
		return convertView;
	}
	
	class Holder {
		TextView name,num,create_time,money;
	}
	
}
