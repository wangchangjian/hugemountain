package com.aozhi.hugemountain.model;

import java.util.ArrayList;

import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.model.Meta;

public class StoreListObject {
	public Meta meta;
	public ArrayList<StoreObject> response;
	public ArrayList<StoreObject> contents;
}
