package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class StaffVideoListObject {
	public Meta meta;
	public ArrayList<StaffVideoObject> response;
	
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<StaffVideoObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<StaffVideoObject> response) {
		this.response = response;
	}
	
}
