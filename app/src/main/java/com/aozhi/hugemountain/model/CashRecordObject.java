package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class CashRecordObject implements Serializable {
	public String id;
	public String username;
	public String pwd;
	public String image;
	public String location;
	public String type;
	public String areas;
	public String mark;
	public String bank;
	public String state;
	public String time;
	public String gender;
	public String brithday;
	public String idcards;
	public String create_time;
	public String modify_date;
	public String member;
	public String phone;
	public String idcards_image;
	public String audit;
	public String name;
	public String balance;
	public String lat;
	public String lon;
	public String qq;
	public String weixin;
	public String Email;
	public String photo;
	public String age;
	public String service;
	public String avatar;
	public String staffphoto;
	public String remark;
	public String commission;
	public String seller_id;
	public String storephoto;
	public String city_name;
	public String longitude;
	public String latitude;
	public String address;
	public String star;
	public String business_hours;
	public String mobile;
	public String types;
	public String code_id;
	public String sex;
	public String money;
	public String pay_status;
	public String staffname;
	
	
	
}
