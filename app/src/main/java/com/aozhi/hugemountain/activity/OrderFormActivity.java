package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.OrderFormAdapter;
import com.aozhi.hugemountain.adapter.OrderFormNAdapter;
import com.aozhi.hugemountain.adapter.OrderFormYAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Utils;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OrderFormActivity extends Activity {
	Button btn_back;
	TextView t1,t2,t3,b1,b2,b3;
	LinearLayout item1;
	LinearLayout item2;
	LinearLayout item3;
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
	private OrderFormAdapter adapter1;
	private OrderFormYAdapter adapter2;
	private OrderFormNAdapter adapter3;
	private ListView list_orderform1;
	private ListView list_orderform2;
	private ListView list_orderform3;
	private String type = "1";
	private OrderListObject mOrderListObject;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_orderform);
		initView();
		initListener();
		getorder();
		

	}


	private void initListener() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		// getorderformbyclient
		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				type = "1";
				getorder();
				t1.setTextColor(getResources().getColor(R.color.y));
				t2.setTextColor(getResources().getColor(R.color.n));
				t3.setTextColor(getResources().getColor(R.color.n));
				
				b1.setBackgroundColor(getResources().getColor(R.color.y));
				b2.setBackgroundColor(getResources().getColor(R.color.bg));
				b3.setBackgroundColor(getResources().getColor(R.color.bg));
				
				item1.setVisibility(View.VISIBLE);
				item2.setVisibility(View.GONE);
				item3.setVisibility(View.GONE);

			}
		});

		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				type = "2";
				getorder();
				t2.setTextColor(getResources().getColor(R.color.y));
				t1.setTextColor(getResources().getColor(R.color.n));
				t3.setTextColor(getResources().getColor(R.color.n));
				
				b2.setBackgroundColor(getResources().getColor(R.color.y));
				b1.setBackgroundColor(getResources().getColor(R.color.bg));
				b3.setBackgroundColor(getResources().getColor(R.color.bg));
				
				item2.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
				item3.setVisibility(View.GONE);
			}
		});

		t3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				type = "3";
				getorder();
				t3.setTextColor(getResources().getColor(R.color.y));
				t1.setTextColor(getResources().getColor(R.color.n));
				t2.setTextColor(getResources().getColor(R.color.n));
				
				b3.setBackgroundColor(getResources().getColor(R.color.y));
				b1.setBackgroundColor(getResources().getColor(R.color.bg));
				b2.setBackgroundColor(getResources().getColor(R.color.bg));
				
				item3.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
				item2.setVisibility(View.GONE);
			}
		});
	}


	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		t1 = (TextView) findViewById(R.id.tv_service);
		t2 = (TextView) findViewById(R.id.tv_staff);
		t3 = (TextView) findViewById(R.id.tv_store_detail);
		b1 = (TextView) findViewById(R.id.b1);
		b2 = (TextView) findViewById(R.id.b2);
		b3 = (TextView) findViewById(R.id.b3);
		item1 = (LinearLayout) findViewById(R.id.item1);
		item2 = (LinearLayout) findViewById(R.id.item2);
		item3 = (LinearLayout) findViewById(R.id.item3);
		list_orderform1 = (ListView) findViewById(R.id.list_orderform1);
		list_orderform2 = (ListView) findViewById(R.id.list_orderform2);
		list_orderform3 = (ListView) findViewById(R.id.list_orderform3);
		adapter1 = new OrderFormAdapter(this, list);
		adapter2 = new OrderFormYAdapter(this, list);
		adapter3 = new OrderFormNAdapter(this, list);
		list_orderform1.setAdapter(adapter1);
		list_orderform2.setAdapter(adapter2);
		list_orderform3.setAdapter(adapter3);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.order_form, menu);
		return true;
	}
	
	
	private void getorder() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getorderformbyclients" };
		String[] name1 = new String[] { "id", MyApplication.Clientuser.id };
		String[] mark1 = new String[] { "mark", "1" };
		String[] type_mark1 = new String[] { "type_mark", type };

		params2.add(funParam2);
		params2.add(name1);
		params2.add(mark1);
		params2.add(type_mark1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				if (type.equals("1")) {
					adapter1 = new OrderFormAdapter(OrderFormActivity.this,list);
					list_orderform1.setAdapter(adapter1);
				}
				if (type.equals("2")) {
					adapter2 = new OrderFormYAdapter(OrderFormActivity.this,list);
					list_orderform2.setAdapter(adapter2);
				}
				if (type.equals("3")) {
					adapter3 = new OrderFormNAdapter(OrderFormActivity.this,list);
					list_orderform3.setAdapter(adapter3);
				}
			}
		}
	};
}
