package com.aozhi.hugemountain.activity;

import java.math.BigDecimal;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.adapter.CashRecordAdapter;
import com.aozhi.hugemountain.model.AccountListObject;
import com.aozhi.hugemountain.model.AccountObject;
import com.aozhi.hugemountain.model.CashRecordListObject;
import com.aozhi.hugemountain.model.CashRecordObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CashRecordActivity extends BaseActivity implements OnClickListener {

	private TextView btn_ok, store_id, personcount;
	private Spinner sp_r;
	private EditText et_cash;
	private ListView list_p;
	private Button btn_back, btn_tx;
	private String[] sp_list = new String[] { "全部", "按员工", "按项目" };
	private ProgressDialog progressDialog = null;
	private CashRecordListObject mCashRecordListObject;
	private ArrayList<CashRecordObject> list = new ArrayList<CashRecordObject>();
	private AccountListObject mAccountListObject;
	private ArrayList<AccountObject> acclist = new ArrayList<AccountObject>();
	private CashRecordAdapter mCashRecordAdapter;

	private ArrayList<LoginBean> LoginBeanlist = new ArrayList<LoginBean>();
	private LoginListObject loginListObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record);
		initView();
		initListener();
	}

	@Override
	protected void initData() {

	}

	private void initListener() {
		store_id.setText(MyApplication.Storeuser.storephoto);
		btn_back.setOnClickListener(this);
		btn_tx.setOnClickListener(this);
		// Spinner();
		// mCashRecordAdapter = new CashRecordAdapter(CashRecordActivity.this,
		// list);
		// list_p.setAdapter(mCashRecordAdapter);
		// list_p.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long ids) {
		// final String cash_id = list.get(position).id;
		// AlertDialog.Builder builder = new Builder(
		// CashRecordActivity.this);
		// builder.setMessage("提现确认通过审核吗？");
		// builder.setPositiveButton("确认",
		// new AlertDialog.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog,
		// int which) {
		// dialog.dismiss();
		// upCashRecordStatus(cash_id, "5");
		// CashRecordActivity.this.finish();
		// }
		// });
		// builder.setNegativeButton("取消",
		// new AlertDialog.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog,
		// int which) {
		// dialog.dismiss();
		// upCashRecordStatus(cash_id, "4");
		// }
		// });
		// builder.create().show();
		// }
		// });
		// list_p.setOnItemLongClickListener(new OnItemLongClickListener() {
		//
		// @Override
		// public boolean onItemLongClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// String cash_id = list.get(position).id;
		// upCashRecordStatus(cash_id, "0");
		// return true;
		// }
		// });
	}

	public void initView() {
		btn_ok = (TextView) findViewById(R.id.btn_ok);
		btn_back = (Button) findViewById(R.id.btn_back);
		store_id = (TextView) findViewById(R.id.store_id);
		personcount = (TextView) findViewById(R.id.personcount);
		btn_tx = (Button) findViewById(R.id.btn_tx);
		et_cash = (EditText) findViewById(R.id.et_cash);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getstorebalance();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_tx:
			if (et_cash.getText().toString().equals("")) {
				Toast.makeText(CashRecordActivity.this, "请输入提现金额",
						Toast.LENGTH_SHORT).show();
			} else {

				if (Double.parseDouble(et_cash.getText().toString()) <= Double
						.parseDouble(personcount.getText().toString())) {
					getAccount();

				} else {
					Toast.makeText(CashRecordActivity.this, "提现金额不能大于可提现金额",
							Toast.LENGTH_SHORT).show();
				}
			}
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}

	}

	private void Spinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, sp_list);
		sp_r.setAdapter(adapter);
		sp_r.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (sp_r.getSelectedItem().equals("全部")) {
					getProjectList("1");
				} else if (sp_r.getSelectedItem().equals("按员工")) {
					getProjectList("a.staff_id");
				} else if (sp_r.getSelectedItem().equals("按项目")) {
					getProjectList("b.service");
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// getProjectList("1");
			}
		});
	}

	private void getProjectList(String order) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getCashReCharge" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam1 = new String[] { "order", order };
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam1);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mCashRecordListObject = JSON.parseObject(v,
						CashRecordListObject.class);
				list = mCashRecordListObject.response;
				if (list.size() > 0) {
					mCashRecordAdapter = new CashRecordAdapter(
							CashRecordActivity.this, list);
					list_p.setAdapter(mCashRecordAdapter);
				}

			} else {
				Toast.makeText(CashRecordActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getAccount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreZfb" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					upCashRecord_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener upCashRecord_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				acclist = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if (acclist.size() > 0) {
						addTixianShenqing();

					} else {
						Intent mintent = new Intent(CashRecordActivity.this,
								AddNet.class);
						mintent.putExtra("stats", "store");
						startActivity(mintent);
						// Toast.makeText(CashRecordActivity.this,"对不起,你没有添加支付账户请添加",
						// Toast.LENGTH_SHORT).show();
					}
				}
			} else {
				Toast.makeText(CashRecordActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	/**
	 * 上传提现申请
	 */
	private void addTixianShenqing() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "addApply" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam1 = new String[] { "staff_id", "0" };
		String[] nameParam2 = new String[] { "money",
				et_cash.getText().toString() };
		String[] nameParam3 = new String[] { "del_flag", "2" };
		String[] nameParam4 = new String[] { "card", acclist.get(0).zfb_name };
		String[] nameParam5 = new String[] { "zfb_name",
				acclist.get(0).zfb_username };
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					addsq_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener addsq_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				BigDecimal a1 = new BigDecimal(MyApplication.Storeuser.balance);
				BigDecimal a2 = new BigDecimal(et_cash.getText().toString());
				MyApplication.Storeuser.balance = a1.subtract(a2).toString();
				Toast.makeText(CashRecordActivity.this, "提现申请提交成功，请等待审核",
						Toast.LENGTH_SHORT).show();
				finish();
			} else {
				Toast.makeText(CashRecordActivity.this, "提现申请失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getstorebalance() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstorebalance" };
		String[] idParam2 = new String[] { "storephoto",
				MyApplication.Storeuser.storephoto };
		params2.add(funParam2);
		params2.add(idParam2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getstorebalance_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getstorebalance_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				loginListObject = JSON.parseObject(v, LoginListObject.class);
				LoginBeanlist = loginListObject.response;
				if (loginListObject.meta.getMsg().equals("OK")) {
					if (LoginBeanlist.size() > 0) {
						MyApplication.Storeuser.balance = LoginBeanlist.get(0).balance;
						personcount.setText(MyApplication.Storeuser.balance);
					}
				} else {
					Toast.makeText(CashRecordActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			}
		}
	};
}
