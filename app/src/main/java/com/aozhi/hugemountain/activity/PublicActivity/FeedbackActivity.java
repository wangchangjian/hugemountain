package com.aozhi.hugemountain.activity.PublicActivity;


import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.view.ApplicationTopView;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.aozhi.hugemountain.view.OnTopClickListener;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class FeedbackActivity extends Activity implements OnClickListener {
    private JumpProgressDialog progressDialog ;
    private EditText et_content;
    private TextView tv_submit;
    private ApplicationTopView at_app_top;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initView();
        initListnner();
    }


    private void initView() {
        et_content = (EditText) findViewById(R.id.et_content);
        tv_submit = (TextView) findViewById(R.id.tv_submit);
        at_app_top = (ApplicationTopView) findViewById(R.id.at_app_top);
    }

    private void initListnner() {
        tv_submit.setOnClickListener(this);
        at_app_top.setOnTopClickListener(new OnTopClickListener() {
            @Override
            public void onLeftbuttonClick() {
                finish();
            }

            @Override
            public void onRightbuttonClick() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.tv_submit: {
                if (et_content.getText().toString().equals("")) {
                    Toast.makeText(this, "请输入反馈内容", Toast.LENGTH_LONG).show();
                } else {

                    if (MyApplication.IS_LOGINS) {
                        setContent(MyApplication.Clientuser.clientphoto, "1");
                    } else {
                        setContent(MyApplication.Staffuser.staffphoto, "0");
                    }

                }
            }
            default:
                break;
        }
    }

    private void setContent(String uid, String types) {

        // TODO Auto-generated method stub
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] methodParam = new String[]{"fun", "setFeedBack"};
            String[] telParam = new String[]{"uid", uid};
            String[] noteParam = new String[]{"content", et_content.getText().toString()};
            String[] noteParam1 = new String[]{"types", types};
            params.add(methodParam);
            params.add(telParam);
            params.add(noteParam);
            params.add(noteParam1);
            progressDialog = new JumpProgressDialog(this);
            progressDialog.showDialog();
            new HttpConnection().get(Constant.URL, params,
                    setGoodsDianDan_callbackListener);
        } else
            Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
    }

    private CallbackListener setGoodsDianDan_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String result) {
            Log.d("返回数据", result);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (!result.equals("fail")) {
                if (result.indexOf("OK") != -1) {
                    Toast.makeText(FeedbackActivity.this, "提交成功", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(FeedbackActivity.this, "提交失败", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(FeedbackActivity.this, "提交失败", Toast.LENGTH_LONG).show();
            }
        }
    };
}
