package com.aozhi.hugemountain.anmin;

import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by ${wangchangjian} on 2015/10/27.
 */
public class JumpShakeAnmin extends Animation {

    private int width;
    private int height;
    private double beginAngle;

    public JumpShakeAnmin(double beginAngle) {
        super();
        this.beginAngle = beginAngle;
    }
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        t.getMatrix().setTranslate(0, (float) (120 * Math.sin(interpolatedTime * (Math.PI)+beginAngle)));
        super.applyTransformation(interpolatedTime, t);
    }
    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {//会获取到目标对象的宽高
        this.width = width;
        this.height = height;
        super.initialize(width, height, parentWidth, parentHeight);
    }
}
