package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import cn.beecloud.demo.ShoppingCartActivity;
import com.aozhi.hugemountain.activity.OrderFormDetailClientActivity;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.OrderFormObject;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class OrderStaffAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
	private String server="";
	public OrderStaffAdapter(Context ctx, ArrayList<OrderFormObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_orderstaff, null);
			mHolder.img = (ImageView) convertView.findViewById(R.id.img);
			mHolder.ordername = (TextView) convertView
					.findViewById(R.id.ordername);
			mHolder.servicename=(TextView)convertView.findViewById(R.id.servicename);
			mHolder.count = (TextView) convertView.findViewById(R.id.count);
			mHolder.orderstatus = (TextView) convertView.findViewById(R.id.orderstatus);
			mHolder.paystatus = (TextView) convertView.findViewById(R.id.paystatus);
			mHolder.create_time = (TextView) convertView
					.findViewById(R.id.create_time);
			mHolder.pay_manoy = (TextView) convertView
					.findViewById(R.id.pay_manoy);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.ordername.setText(list.get(position).order_id);
		mHolder.create_time.setText(list.get(position).create_time);
		mHolder.pay_manoy.setText("￥"+list.get(position).earmoney);
		String[] services=list.get(position).service.split("[,]");
		mHolder.servicename.setText(list.get(position).names);
		mHolder.count.setText("1×"+services.length+"份");
		if(list.get(position).orderstatus.equals("10")){
			mHolder.orderstatus.setText("排队中");
			mHolder.paystatus.setText("未付款");
		}else if(list.get(position).orderstatus.equals("0")){
			mHolder.orderstatus.setText("已取消");
		}else if(list.get(position).orderstatus.equals("20")){
			mHolder.orderstatus.setText("进行中");
			mHolder.paystatus.setText("已付款");
		}else if(list.get(position).orderstatus.equals("30")){
			mHolder.paystatus.setText("已付款");
		}
//		if(list.get(position).type_mark.equals("0")){
//			mHolder.paystatus.setText("已退款");
//		}else if(list.get(position).type_mark.equals("1")){
//			mHolder.paystatus.setText("未付款");
//		}else if(list.get(position).type_mark.equals("2")){
//			mHolder.paystatus.setText("已付款");
//		}
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					Intent mIntent = new Intent(mContext,OrderFormDetailClientActivity.class);
					mIntent.putExtra("order_id", list.get(position).order_id);
					mContext.startActivity(mIntent);

			}
		});
		
		mHolder.paystatus.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mintent=new Intent(mContext,ShoppingCartActivity.class);
				mintent.putExtra("cash", list.get(position).pay_manoy);
				mContext.startActivity(mintent);
			}
		});
		return convertView;
	}

	class Holder {
		ImageView img;
		TextView ordername, count, create_time, pay_manoy,servicename,orderstatus,paystatus;
		TextView tv_miaoshu;
	}
}
