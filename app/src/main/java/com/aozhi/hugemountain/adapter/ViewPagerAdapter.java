package com.aozhi.hugemountain.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.aozhi.hugemountain.view.OrderView;

import java.util.List;

/**
 * Created by ${wangchangjian} on 2016/5/8.
 */
public class ViewPagerAdapter extends PagerAdapter{
    List<OrderView> viewLists;

    public ViewPagerAdapter(List<OrderView> lists) {
        viewLists = lists;
    }

    @Override
    public int getCount() {                                                                 //获得size
        return viewLists.size();
    }

    @Override
    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == arg1;
    }

    @Override
    public void destroyItem(View view, int position, Object object)                       //销毁Item
    {
        ((ViewPager) view).removeView(viewLists.get(position));
    }

    @Override
    public Object instantiateItem(View view, int position)                                //实例化Item
    {
        ((ViewPager) view).addView(viewLists.get(position), 0);
        return viewLists.get(position);
    }
}
