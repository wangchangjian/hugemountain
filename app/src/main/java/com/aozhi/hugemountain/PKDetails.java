package com.aozhi.hugemountain;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PKDetails extends Activity  implements OnClickListener{
	
	private Button btn_back,btn_ok;
	private String name,money;
	private TextView pro_name,totalmoney;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pkhub);
		initView();
		initListener();
		setView();
	}
	private void setView() {
		name=getIntent().getStringExtra("staff_name");
		money=getIntent().getStringExtra("money");
		pro_name.setText(name);
		totalmoney.setText("￥"+money);
	}
	private void initView() {
		btn_ok=(Button) findViewById(R.id.btn_ok);
		btn_back=(Button) findViewById(R.id.btn_back);
		pro_name=(TextView) findViewById(R.id.pro_name);
		totalmoney=(TextView) findViewById(R.id.totalmoney);
	}
	private void initListener() {
		btn_back.setOnClickListener(this);
		btn_ok.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.btn_ok:
			finish();
			break;
		default:
			break;
		}
		
	}
}
