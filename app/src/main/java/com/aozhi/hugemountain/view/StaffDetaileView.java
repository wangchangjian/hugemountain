package com.aozhi.hugemountain.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;

import java.util.ArrayList;

/**
 * Created by ${wangchangjian} on 2016/4/28.
 */
public class StaffDetaileView extends LinearLayout {
    private final ViewGroup v;
    private IndexDotView dot_indicator;
    private final Dialog dialog;
    private ViewPager vp_mainadv;
    private StaffObject staffinfo;
    private ImageListObject mImageListObject;
    private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
    private DownloadImage downloadImage = new DownloadImage();
    private String images;
    private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
    private TextView jd_count1, sc_count1, pj_count1, marks, tv_ok;
    private LinearLayout collect;
    private ProgressDialog progressDialog = null;
    private Button btn_back;
    private ClickExtensionTextLayout ce_introduce;
    private TextView tv_number, tv_tel, tv_serice, jd_count, sc_count, sc_counts, tv_collect, tv_collect_name;
    private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
    private ArrayList<LoginBean> mlist = new ArrayList<LoginBean>();
    private LoginListObject mLoginListObject;
    private boolean flag = false;

    public StaffDetaileView(Context context) {
        super(context);
        v = (ViewGroup) LayoutInflater.from(MyApplication.getInstance()).inflate(R.layout.staff_detail_view, null);// 得到加载view
        jd_count = (TextView) v.findViewById(R.id.jd_count);
        sc_count = (TextView) v.findViewById(R.id.sc_count);
        sc_counts = (TextView) v.findViewById(R.id.sc_counts);
        tv_serice = (TextView) v.findViewById(R.id.tv_serice);
        collect = (LinearLayout) v.findViewById(R.id.collect);
        tv_ok = (TextView) v.findViewById(R.id.tv_ok);
        btn_back = (Button) v.findViewById(R.id.btn_back);
        tv_number = (TextView) v.findViewById(R.id.tv_number);
        tv_tel = (TextView) v.findViewById(R.id.tv_tel);
        ce_introduce = (ClickExtensionTextLayout) v.findViewById(R.id.ce_introduce);
        tv_collect = (TextView) v.findViewById(R.id.tv_collect);
        tv_collect_name = (TextView) v.findViewById(R.id.tv_collect_name);
        vp_mainadv = (ViewPager) v.findViewById(R.id.viewPager);

        dialog = new Dialog(this.getContext(), R.style.loading_dialog);
        dialog.setCancelable(true);// 不可以用“返回键”取消
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(v, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));// 设置布局

        dot_indicator = (IndexDotView) StaffDetaileView.this.v.findViewById(R.id.dot_indicator);
        dot_indicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                vp_mainadv.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                        dot_indicator.setMoveDotPercent((positionOffset + position) / vp_mainadv.getAdapter().getCount());

                    }

                    @Override
                    public void onPageSelected(int position) {

                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                dot_indicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public void setData(final StaffObject staffObjecta) {
        this.staffinfo = staffObjecta;
        tv_number.setText(staffinfo.code_id);
        // tv_tel.setText("联系电话："+staffinfo.staffphoto);
        ce_introduce.setText("自我介绍",staffinfo.remarks);
        tv_serice.setText(staffinfo.service_name);
        tv_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                onSelectChangedListener.onBookPressed();
                dialog.dismiss();
            }
        });
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
        getPhoto();
    }

    // 图片轮播
    private void getPhoto() {
        // 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
        // 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
        Constant.NET_STATUS = Utils.getCurrentNetWork(this.getContext());
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] methodParam = new String[]{"fun", "getUserImg"};
            String[] paramName = new String[]{"user_id", staffinfo.id};
            params.add(methodParam);
            params.add(paramName);
            new HttpConnection().get(Constant.URL, params, getmain_callbackListener);
        } else
            Toast.makeText(this.getContext(), "请检查网络连接！", Toast.LENGTH_LONG).show();
    }

    private OrderSelectedListener onSelectChangedListener;

    public void setOnSelectedListener(OrderSelectedListener onSelectChangedListener) {
        this.onSelectChangedListener = onSelectChangedListener;
    }


    private HttpConnection.CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mImageListObject = JSON.parseObject(v, ImageListObject.class);
                imglist = mImageListObject.getResponse();
                if (imglist.size() != 0) {
                    dot_indicator.setNums(imglist.size());

                }
                imgViews.clear();
                images = "";
                if (imglist.size() > 0) {
                    for (int n = 0; n < imglist.size(); n++) {
                        final ImageView imgView = new ImageView(StaffDetaileView.this.getContext());
                        imgView.setClickable(true);
                        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                        downloadImage.addTasks(imglist.get(n).img, imgView, new DownloadImage.ImageCallback() {
                            public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                                if (imageBitmap != null) {
                                    imgView.setImageBitmap(imageBitmap);
                                    imgView.setTag(imageUrl);
                                }
                            }

                            public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                                if (imageBitmap != null) {
                                    imgView.setImageBitmap(imageBitmap);
                                    imgView.setTag("");
                                }
                            }
                        });
                        downloadImage.doTask();
                        final String url = imglist.get(n).url;
                        final String title = imglist.get(n).name;

                        if (images.equals("")) {
                            images = imglist.get(n).img;
                        } else {
                            images = images + "," + imglist.get(n).img;
                        }
                        imgView.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent mIntent = new Intent(StaffDetaileView.this.getContext(), ImagePagerActivity.class);
                                mIntent.putExtra("images", images);
                                StaffDetaileView.this.getContext().startActivity(mIntent);
                            }
                        });
                        imgViews.add(imgView);
                    }
                }
                vp_mainadv.setAdapter(new MyAdapter());
            }
        }
    };

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return imgViews.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(View arg0, int arg1) {
            ((ViewPager) arg0).addView(imgViews.get(arg1));
            return imgViews.get(arg1);
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {

        }

        @Override
        public void finishUpdate(View arg0) {

        }
    }


}
