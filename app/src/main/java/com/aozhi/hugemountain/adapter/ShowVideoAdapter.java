package com.aozhi.hugemountain.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ShiPinActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.StaffVideoObject;

public class ShowVideoAdapter extends BaseAdapter{
	private DownloadImage downloadImage = new DownloadImage();
	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffVideoObject> list = new ArrayList<StaffVideoObject>();

	public ShowVideoAdapter(Context ctx, ArrayList<StaffVideoObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_showvideo, null);
			mHolder.iv_src=(ImageView)convertView.findViewById(R.id.iv_src);
			mHolder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_name.setText(list.get(position).name);
		if (list.get(position).src.equals("")
				|| list.get(position).src==null ) {
			
		} else {
			downloadImage.addTasks(
					list.get(position).src, mHolder.iv_src,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_src.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_src.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		downloadImage.doTask();
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				StaffVideoObject mStaffVideoObject=list.get(position);
				Intent mInten=new Intent(mContext,ShiPinActivity.class);
				mInten.putExtra("mStaffVideoObject", mStaffVideoObject);
				mContext.startActivity(mInten);
			}
		});
		return convertView;
	}
	
	
	
	class Holder {
		ImageView iv_src;
		TextView tv_name;
	}
}
