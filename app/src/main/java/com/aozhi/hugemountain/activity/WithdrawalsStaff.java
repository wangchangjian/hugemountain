package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ConsumerbillList2Adapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.model.StaffIncomeListObject;
import com.aozhi.hugemountain.model.StaffIncomeObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class WithdrawalsStaff extends Activity implements OnClickListener {
	
	private Button btn_back;
	private TextView monthmoney,totalmoney,count;
	private Spinner sp_type,sp_order;
	private String types="2";
	private ListView list_staff;
	private String[] type=new String[]{"未提现","已提现","全部"};
	private String[] order=new String[]{"按提现时间升序","按提现时间降序","按提现金额升序","按提现金额降序"};
	private ProgressDialog progressDialog=null;
	private ConsumptionListObject mConsumptionListObject;
	private ArrayList<StaffIncomeObject> list=new ArrayList<StaffIncomeObject>();
	private ArrayList<ConsumptionObject> monthlist = new ArrayList<ConsumptionObject>();
	private ArrayList<ConsumptionObject> totallist = new ArrayList<ConsumptionObject>();
	private StaffIncomeListObject mStaffIncomeListObject;
	private ConsumerbillList2Adapter adapter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staffwithdrawals);
		initView();
		initListener();
		setView();
		getMonth();
	}
	
	private void setView() {
		Spinner(sp_type,type);
//		Spinner(sp_order,order);
	}

	private void Spinner(Spinner sp,String[] sp_list) {
		ArrayAdapter<String> sp_adapter=new ArrayAdapter<String>(this,  R.layout.item_type, sp_list);
		sp.setAdapter(sp_adapter);
	}

	private void initListener() {
		btn_back.setOnClickListener(this);
		sp_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
					if(sp_type.getSelectedItem().toString().equals("全部")){
						
						getMainList("0","id");
					}else if(sp_type.getSelectedItem().toString().equals("已提现")){
						
						getMainList("0","id");
					}else if(sp_type.getSelectedItem().toString().equals("未提现")){
						
						getMainList("2","id");
					}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
//				getMainList("0","id");
			}
		});
		sp_order.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				if(sp_order.getSelectedItem().toString().equals("按提现时间升序")){
					getMainList(types,"create_time");
				}else if(sp_order.getSelectedItem().toString().equals("按提现时间降序")){
					getMainList(types,"create_time desc");
				}else if(sp_order.getSelectedItem().toString().equals("按提现金额升序")){
					getMainList(types,"money");
				}else if(sp_order.getSelectedItem().toString().equals("按提现金额降序")){
					getMainList(types,"money  desc");
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
//				getMainList(types,"id");
			}
		});
	}

	private void initView() {
		btn_back=(Button) findViewById(R.id.btn_back);
		monthmoney=(TextView) findViewById(R.id.monthmoney);
		totalmoney=(TextView) findViewById(R.id.totalmoney);
		sp_type=(Spinner) findViewById(R.id.sp_type);
		sp_order=(Spinner) findViewById(R.id.sp_order);
		list_staff=(ListView) findViewById(R.id.list_staff);
		count=(TextView) findViewById(R.id.count);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;

		default:
			break;
		}
		
	}

	//获取当月提现金额
	private void getMonth() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getMonthStaff" };
		String[] name1=new String[]{"staff_id", MyApplication.Staffuser.id};
		String[] name2=new String[]{"status","1"};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					month_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener month_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (progressDialog!= null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				monthlist = mConsumptionListObject.response;
				if(mConsumptionListObject.meta.getMsg().equals("OK")){
					if(monthlist.size()>0){
						if(!monthlist.get(0).money.equals("")){
						monthmoney.setText(monthlist.get(0).money);
						}else{
							monthmoney.setText("0");
						}
					}else{
						monthmoney.setText("0");
					}
				}
			}else{
				monthmoney.setText("0");
			}
		}
	};
	
	
	//获取总共提现的金额
		private void getTotalStaff() {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "getTotalApply" };
			String[] name1=new String[]{"staff_id",MyApplication.Staffuser.id};
			String[] name2=new String[]{"status","1"};
			params2.add(funParam2);
			params2.add(name1);
			params2.add(name2);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						total_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener total_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				// 对设备注册结果进行解析
				if (progressDialog!= null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!v.equals("fail")) {// 当请求网络返回值正常
					mConsumptionListObject = JSON.parseObject(v,
							ConsumptionListObject.class);
					totallist = mConsumptionListObject.response;
					if(mConsumptionListObject.meta.getMsg().equals("OK")){
						if(totallist.size()>0){
							if(!totallist.get(0).money.equals("")){
								totalmoney.setText(totallist.get(0).money);
								count.setText("累计提现次数"+totallist.get(0).count+"次");
							}else{
								totalmoney.setText("0");
							}
						}else{
							totalmoney.setText("0");
						}
					}
				}else{
					totalmoney.setText("0");
				}
			}
		};

	/**
	 * 获取申请列表
	 * @param order 排序方式
	 */
	private void getMainList(String stats,String order) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffincome" };
		String[] name1=new String[]{"staff_id",MyApplication.Staffuser.id};
		String[] name2=new String[]{"del_flag",stats};
		String[] name3=new String[]{"order",order};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffIncomeListObject = JSON.parseObject(v,
						StaffIncomeListObject.class);
				list = mStaffIncomeListObject.response;
				if(mStaffIncomeListObject.meta.getMsg().equals("OK")){
					if(list.size()>0){
						adapter=new ConsumerbillList2Adapter(WithdrawalsStaff.this,list);
						list_staff.setAdapter(adapter);
					}
				}
			}else{
				Toast.makeText(WithdrawalsStaff.this,"获取记录失败", Toast.LENGTH_LONG).show();
			}
		}
	};
}
