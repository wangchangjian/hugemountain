package com.aozhi.hugemountain.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.adapter.StoreListsAdapter;
import com.aozhi.hugemountain.adapter.TableAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.aozhi.hugemountain.R;

public class RevenueInquiryActivity extends Activity {
	Button btn_back;
	private TextView dayrevenue, et_peoper, monthrevenue, tv_show, et_year,
			tv_cash, tv_yue, tv_wangluo, tv_1, tv_2, tv_3, tv_4, tv_5, tv_6,
			tv_7, tv_8, tv_9, tv_10, tv_11, tv_12;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	ConsumptionObject mConsumptionObject;
	ConsumptionListObject mConsumptionListObject;
	private StoreListsAdapter adapter;
	private TableAdapter tableadapter;
	private ListView list_shopcenter;
	double Day_moneyrevenue = 0;
	double Month_moneyrevenue = 0;
	private ProgressDialog progressDialog = null;
	private Spinner sp_type, sp_orderby;
	private String leixing = "'0','1'";
	private String[] types = new String[] { "全部", "现金", "网络" };
	private String[] order = new String[] { "按时间升序", "按时间降序", "按金额升序", "按金额降序" };
	private ImageView et_choose;
	private ArrayList<ConsumptionObject> timelist = new ArrayList<ConsumptionObject>();
	private ListView list_table;
	private LinearLayout layout_table, li_query;
	private String yue = "", years = "", date = "", date2 = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_revenueinquiry);
		initView();
		myclick();
		// getDayRevenue();
		// getMonthRevenue();
		// getRevenueOrder("money_log.create_time");
		getlist(date);
	}

	public Date SimpleDateFormat(String str) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public int get_year(Date date) {
		return date.getYear() + 1900;
	}

	public int get_month(Date date) {
		return date.getMonth() + 1;
	}

	public int get_day(Date date) {
		return date.getDate();
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_show = (TextView) findViewById(R.id.tv_show);
		dayrevenue = (TextView) findViewById(R.id.dayrevenue);
		monthrevenue = (TextView) findViewById(R.id.monthrevenue);
		list_shopcenter = (ListView) findViewById(R.id.list_shopcenter);
		et_choose = (ImageView) findViewById(R.id.et_choose);
		et_year = (TextView) findViewById(R.id.et_year);
		list_table = (ListView) findViewById(R.id.list_table);
		layout_table = (LinearLayout) findViewById(R.id.layout_table);
		li_query = (LinearLayout) findViewById(R.id.li_query);
		tv_1 = (TextView) findViewById(R.id.tv_1);
		tv_2 = (TextView) findViewById(R.id.tv_2);
		tv_3 = (TextView) findViewById(R.id.tv_3);
		tv_4 = (TextView) findViewById(R.id.tv_4);
		tv_5 = (TextView) findViewById(R.id.tv_5);
		tv_6 = (TextView) findViewById(R.id.tv_6);
		tv_7 = (TextView) findViewById(R.id.tv_7);
		tv_8 = (TextView) findViewById(R.id.tv_8);
		tv_9 = (TextView) findViewById(R.id.tv_9);
		tv_10 = (TextView) findViewById(R.id.tv_10);
		tv_11 = (TextView) findViewById(R.id.tv_11);
		tv_12 = (TextView) findViewById(R.id.tv_12);
		et_peoper = (TextView) findViewById(R.id.et_peoper);
		tv_cash = (TextView) findViewById(R.id.tv_cash);
		tv_yue = (TextView) findViewById(R.id.tv_yue);
		tv_wangluo = (TextView) findViewById(R.id.tv_wangluo);
		// sp_type = (Spinner) findViewById(R.id.sp_type);
		// Spinner(sp_type,types);
		// sp_type.setOnItemSelectedListener(new OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view,
		// int position, long id) {
		// if(sp_type.getSelectedItem().toString().equals("全部")){
		// list.clear();
		// getRevenueList("2");
		// }else if(sp_type.getSelectedItem().toString().equals("现金")){
		// list.clear();
		// getRevenueList("'0'");
		// }else if(sp_type.getSelectedItem().toString().equals("网络")){
		// list.clear();
		// getRevenueList("'1'");
		// }
		// }
		// @Override
		// public void onNothingSelected(AdapterView<?> parent) {
		// list.clear();
		// getRevenueList("'0','1'");
		// }
		// });
		// sp_orderby=(Spinner) findViewById(R.id.sp_orderby);
		// Spinner(sp_orderby,order);
		// sp_orderby.setOnItemSelectedListener(new OnItemSelectedListener() {
		//
		// @Override
		// public void onItemSelected(AdapterView<?> parent, View view,
		// int position, long id) {
		// if(sp_orderby.getSelectedItem().toString().equals("按时间升序")){
		// list.clear();
		// getRevenueOrder("create_time");
		// }else if(sp_orderby.getSelectedItem().toString().equals("按时间降序")){
		// list.clear();
		// getRevenueOrder("create_time desc");
		// }else if(sp_orderby.getSelectedItem().toString().equals("按金额升序")){
		// list.clear();
		// getRevenueOrder("money");
		// }else if(sp_orderby.getSelectedItem().toString().equals("按金额降序")){
		// list.clear();
		// getRevenueOrder("money desc");
		// }
		// }
		//
		// @Override
		// public void onNothingSelected(AdapterView<?> parent) {
		//
		// }
		// });
		adapter = new StoreListsAdapter(this, list);
		list_shopcenter.setAdapter(adapter);
		tableadapter = new TableAdapter(this, timelist);
		list_table.setAdapter(tableadapter);
		// 计算当前年份和月份
		SimpleDateFormat df = new SimpleDateFormat("yyMM");// 设置日期格式
		date = df.format(new Date());
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM");// 设置日期格式
		date2 = df2.format(new Date());
		et_peoper.setText(date2);

		list_shopcenter.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent mIntent=new Intent(RevenueInquiryActivity.this,StaffOrdersActivity.class);
				mIntent.putExtra("staff_id", list.get(arg2).id);
				mIntent.putExtra("date", date);
				mIntent.putExtra("title", list.get(arg2).code_id);
				mIntent.putExtra("status", "0");
				startActivity(mIntent);
				}
		});
	}

	private void myclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		et_choose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showYearList();
			}

		});
		list_table.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				list_table.setVisibility(View.GONE);
				et_year.setText(timelist.get(arg2).year);
				years = timelist.get(arg2).year;
			}
		});

		tv_1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_1.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "1";
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_2.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "2";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_3.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "3";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_4.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "4";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_5.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_5.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "5";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_6.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_6.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "6";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_7.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "7";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_8.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_8.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "8";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_9.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_9.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "9";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_10.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_10.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "10";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_11.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_11.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "11";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_12.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		tv_12.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				tv_12.setBackgroundColor(Color.parseColor("#FF6600"));
				yue = "12";
				tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_4.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_5.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_6.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_7.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_8.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_9.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_10.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_11.setBackgroundColor(Color.parseColor("#ffffff"));
				tv_3.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		});
		et_peoper.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showMonPicker();
			}
		});

		li_query.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (et_peoper.getText().toString().equals("请选择时间(年月)")
						|| et_peoper.getText().toString().equals("")) {
					Toast.makeText(RevenueInquiryActivity.this, "请选择时间(年月)",
							Toast.LENGTH_LONG).show();
				} else {
					String s = et_peoper.getText().toString()
							.replaceAll("-", "");
					s = s.substring(2, 6);
					getlist(s);
				}
			}
		});
	}

	private void getRevenueList(String status) {
		leixing = status;
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreconsumption" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status", status }; // 收支状态：0为现金收入，1为网络收入；2为现金支出，3为网络支出
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typeList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
//					adapter = new ShopCenterAdapter(
//							RevenueInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				}
			} else {
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	public int textparseint(String textdate) {
		try {
			return Integer
					.parseInt(new SimpleDateFormat("yyyyMMdd")
							.format(new SimpleDateFormat("yyyy-MM-dd")
									.parse(textdate)));
		} catch (NumberFormatException e) {
			return 0;
		} catch (ParseException e) {
			return 0;
		}
	}

	private void getDayRevenue() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getDayIncome" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status", "0" }; // 收支状态：0为充值，1为提现；2为消费，3为收入
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					if (!list.get(0).money.equals("")) {
						dayrevenue.setText(list.get(0).money);
					} else {
						dayrevenue.setText("0");
					}
				} else {
					dayrevenue.setText("0");
				}
			} else {
				dayrevenue.setText("0");
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getMonthRevenue() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getMonthIncome" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status", "0" };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typemonth_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typemonth_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					if (!list.get(0).money.equals("")) {
						monthrevenue.setText(list.get(0).money);
					} else {
						monthrevenue.setText("0");
					}
				} else {
					monthrevenue.setText("0");
				}
			} else {
				monthrevenue.setText("0");
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void Spinner(Spinner sp, String[] data) {
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		ArrayList<String> timelist = new ArrayList<String>();
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, data);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		sp.setAdapter(adapter);
	}

	private void getRevenueOrder(String order) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreorder" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status", "0" };
		String[] name3 = new String[] { "order", order };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typeorder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeorder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
//					adapter = new ShopCenterAdapter(
//							RevenueInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				}
			} else {
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	public void showYearList() {
		for (int i = 2015; i <= 2025; i++) {
			ConsumptionObject object = new ConsumptionObject();
			object.year = String.valueOf(i);
			timelist.add(object);
		}
		layout_table.setVisibility(View.VISIBLE);
	}

	public void getlist(String date) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoremoney" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] dates = new String[] { "date", date };
		String[] status = new String[] { "status", "0" };
		params2.add(funParam2);
		params2.add(dates);
		params2.add(status);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					if (!list.get(0).m.equals("") && list.get(0).m != null) {
						monthrevenue.setText("￥" + list.get(0).m);
						if (!list.get(0).m1.equals("")
								&& list.get(0).m1 != null) {
							tv_cash.setText("￥" + list.get(0).m1);
						}
						if (!list.get(0).m2.equals("")
								&& list.get(0).m2 != null) {
							tv_yue.setText("￥" + list.get(0).m2);
						}
						if (!list.get(0).m3.equals("")
								&& list.get(0).m3 != null) {
							tv_wangluo.setText("￥" + list.get(0).m3);
						}
						getstoreorderlist();
					} else {
						monthrevenue.setText("");
						tv_cash.setText("");
						tv_yue.setText("");
						tv_wangluo.setText("");
						list.clear();
						tv_show.setVisibility(View.VISIBLE);
						list_shopcenter.setVisibility(View.GONE);
						Toast.makeText(RevenueInquiryActivity.this, "当前月没有数据",
								Toast.LENGTH_LONG).show();
					}
				}

			} else {
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// listview
	public void getstoreorderlist() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreStaffListbys" };
		String[] name1 = new String[] { "storeid", MyApplication.Storeuser.id };
		String[] dates = new String[] { "date", date };
		params2.add(funParam2);
		params2.add(dates);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getstoreorderlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getstoreorderlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
					adapter = new StoreListsAdapter(
							RevenueInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				}
			} else {
				Toast.makeText(RevenueInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}

	};

	/**
	 * 重写datePicker 1.只显示 年-月 2.title 只显示 年-月
	 * @author lmw
	 */
	public class MonPickerDialog extends DatePickerDialog {
		public MonPickerDialog(Context context, OnDateSetListener callBack,
				int year, int monthOfYear, int dayOfMonth) {
			super(context, callBack, year, monthOfYear, dayOfMonth);
			this.setTitle(year + "年" + (monthOfYear + 1) + "月");
			((ViewGroup) ((ViewGroup) this.getDatePicker().getChildAt(0))
					.getChildAt(0)).getChildAt(2).setVisibility(View.GONE);
		}

		@Override
		public void onDateChanged(DatePicker view, int year, int month, int day) {
			super.onDateChanged(view, year, month, day);
			this.setTitle(year + "年" + (month + 1) + "月");
		}

	}

	public void showMonPicker() {
		final Calendar localCalendar = Calendar.getInstance();
		localCalendar.setTime(strToDate("yyyy-MM", et_peoper.getText()
				.toString()));
		new MonPickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				localCalendar.set(1, year);
				localCalendar.set(2, monthOfYear);
				et_peoper.setText(clanderTodatetime(localCalendar, "yyyy-MM"));
			}
		}, localCalendar.get(1), localCalendar.get(2), localCalendar.get(5))
				.show();
	}

	// 字符串类型日期转化成date类型
	public static Date strToDate(String style, String date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	public static String dateToStr(String style, Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(date);
	}

	public static String clanderTodatetime(Calendar calendar, String style) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(calendar.getTime());
	}
}
