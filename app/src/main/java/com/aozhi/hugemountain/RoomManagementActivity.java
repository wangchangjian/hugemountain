package com.aozhi.hugemountain;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.activity.ShopActivity.AddRommActivity;
import com.aozhi.hugemountain.adapter.RoomAdapter;
import com.aozhi.hugemountain.model.RoomListObject;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class RoomManagementActivity extends Activity {
	private Button btn_back;
	private ArrayList<RoomObject> list = new ArrayList<RoomObject>();
	private RoomListObject mRoomListObject;
	private RoomAdapter mRoomAdapter;
	private GridView grid_view;
	private ProgressDialog progressDialog = null;
	final int RESULT_CODE = 101;
	final int REQUEST_CODE = 1;
	final int REQUEST_CODE1 = 2;
	public static final int RESULT_CODE1 = 102;
	private TextView tv_add;
	private String id="";
	private int number=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_roommanagement);
		btn_back = (Button)findViewById(R.id.btn_back);
		tv_add=(TextView)findViewById(R.id.tv_add);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		grid_view=(GridView)findViewById(R.id.grid_view);
		mRoomAdapter=new RoomAdapter(this, list);
		grid_view.setAdapter(mRoomAdapter);
		getStaffList();
		tv_add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RoomManagementActivity.this,
						AddRommActivity.class);
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		grid_view.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				id=list.get(arg2).id;
				MyApplication.mRoomObject=list.get(arg2);
				number=arg2;
				Deldialog();
				return true;
			}
		});
		
		grid_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent mintent=new Intent(RoomManagementActivity.this,RoomDetails.class);
				mintent.putExtra("id", list.get(arg2).id);
				startActivity(mintent);				
			}
		});
	}
	
	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getRoomList" };
		String[] idParam2 = new String[] { "id", MyApplication.Storeuser.getId() };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list = mRoomListObject.response;
				if (list.size() > 0) {
					mRoomAdapter=new RoomAdapter(RoomManagementActivity.this, list);
					grid_view.setAdapter(mRoomAdapter);
				}

			} else {
				Toast.makeText(RoomManagementActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("", "onActivityResult" + "requestCode" + requestCode
				+ "\n resultCode=" + resultCode);
		if (requestCode == REQUEST_CODE) {
			if (resultCode == RESULT_CODE) {
				getStaffList();
			}
		}else if (requestCode == REQUEST_CODE1) {
			if (resultCode == RESULT_CODE1) {
				getStaffList();
			}
		}
		switch (resultCode) {
		case REQUEST_CODE1:
			getStaffList();
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
	private void Deldialog() {
		AlertDialog.Builder   alertDialog= new AlertDialog.Builder(this);
		alertDialog.setTitle("房间操作");
				alertDialog.setMessage("请选择对房间修改还是删除？\n温馨提醒：删除操作不可逆，请谨慎操作");
				alertDialog.setPositiveButton("删除", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						upOrderList(id);
					}
				});
		alertDialog.setNegativeButton("修改", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						Intent intent = new Intent(RoomManagementActivity.this,
								UpdateRommActivity.class);
						intent.putExtra("id",list.get(number).id);
						startActivityForResult(intent, REQUEST_CODE1);
					}
				});
		alertDialog.create().show();
	}

	private void upOrderList(String id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upRoom" };
			String[] pageParam = new String[] { "id", id };
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					upOrderList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener upOrderList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {

				if (result.indexOf("OK") != -1) {
					Toast.makeText(RoomManagementActivity.this, "删除成功",
							Toast.LENGTH_LONG).show();
					getStaffList();

				} else {
					Toast.makeText(RoomManagementActivity.this, "删除失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomManagementActivity.this, "删除失败", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

}
