package com.aozhi.hugemountain.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ConsumptionObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StoreListsAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	private int[] colors=new int[]{0x30FF0000,0x300000FF};

	public StoreListsAdapter(Context ctx, ArrayList<ConsumptionObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_storelists, null);
			mHolder.tv_codeid=(TextView)convertView.findViewById(R.id.tv_codeid);
			mHolder.tv_cash=(TextView)convertView.findViewById(R.id.tv_cash);
			mHolder.tv_huiyuan=(TextView)convertView.findViewById(R.id.tv_huiyuan);
			mHolder.tv_wangluo=(TextView)convertView.findViewById(R.id.tv_wangluo);
			mHolder.tv_sum=(TextView)convertView.findViewById(R.id.tv_sum);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_codeid.setText(list.get(position).code_id);
		mHolder.tv_cash.setText("￥"+list.get(position).money);
		mHolder.tv_huiyuan.setText("￥"+list.get(position).member);
		mHolder.tv_wangluo.setText("￥"+list.get(position).net);
		if(list.get(position).money!=null&&list.get(position).member!=null&&list.get(position).net!=null){
			if(list.get(position).money.equals("")){
				list.get(position).money = "0";
				mHolder.tv_cash.setText("￥0");
			}
			if(list.get(position).member.equals("")){
				list.get(position).member = "0";
				mHolder.tv_huiyuan.setText("￥0");
			}
			if(list.get(position).net.equals("")){
				list.get(position).net = "0";
				mHolder.tv_wangluo.setText("￥0");
			}
		BigDecimal money = new BigDecimal(list.get(position).money); 
	    BigDecimal member = new BigDecimal(list.get(position).member);  
	    BigDecimal sum1 = money.add(member);
	    BigDecimal net = new BigDecimal(list.get(position).net); 
	    BigDecimal sum= sum1.add(net);
		mHolder.tv_sum.setText("￥"+sum);}
		
		int colorPos=position%colors.length;
        if(colorPos==1)
        	convertView.setBackgroundColor(Color.argb(250, 255, 255, 255)); //颜色设置
        else
        	convertView.setBackgroundColor(Color.argb(255, 224, 243, 250));//颜色设置
		return convertView;
	}

	class Holder {
		TextView tv_codeid,tv_cash,tv_huiyuan,tv_wangluo,tv_sum;
	}
}
