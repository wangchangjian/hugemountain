package com.aozhi.hugemountain.activity;

import java.math.BigDecimal;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.PreordainServiceAdapter;
import com.aozhi.hugemountain.adapter.PreordainStaffAdapter;
import com.aozhi.hugemountain.list.NoScrollListView;
import com.aozhi.hugemountain.model.HorizontalListView;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class OrderFormDetailClientActivity extends Activity {
	private ArrayList<ServiceObject> list_service = new ArrayList<ServiceObject>();
	private ArrayList<StaffObject> list_staff = new ArrayList<StaffObject>();
	private ArrayList<StoreObject> list_store = new ArrayList<StoreObject>();
	private ArrayList<OrderFormObject> list_order = new ArrayList<OrderFormObject>();
	private PreordainServiceAdapter adapter1;
	private PreordainStaffAdapter adapter2;
	private NoScrollListView list_1;
	private HorizontalListView list_2;
	ServiceListObject mServiceListObject;
	StaffListObject mStaffListObject;
	StoreListObject mStoreListObject;
	OrderListObject mOrderListObject;
	StoreObject mStoreObject;
	StaffObject mStaffObject;
	OrderFormObject mOrderFormObject;
	TextView order_id, storephoto, reservation_time, server_datetime, title,
			pay_time, pay_manoy;
	Button btn_back, btn_close, btn_ok;
	String orderId, staffId, serviceId, storeId,type="";
	LinearLayout ll_close;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orderformdetail_client);
		initView();
		myonclick();
		
	}

	private void initView() {
		orderId = getIntent().getStringExtra("order_id");
		type = getIntent().getStringExtra("type");
		list_1 = (NoScrollListView) findViewById(R.id.list_1);
		list_2 = (HorizontalListView) findViewById(R.id.list_2);
		adapter1 = new PreordainServiceAdapter(this, list_service);
		adapter2 = new PreordainStaffAdapter(this, list_staff);
		list_1.setAdapter(adapter2);
		list_2.setAdapter(adapter1);
		server_datetime = (TextView) findViewById(R.id.server_datetime);
		storephoto = (TextView) findViewById(R.id.storephoto);
		reservation_time = (TextView) findViewById(R.id.reservation_time);
		title = (TextView) findViewById(R.id.title);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_close = (Button) findViewById(R.id.btn_close);
		btn_ok = (Button) findViewById(R.id.btn_ok);
		pay_manoy = (TextView) findViewById(R.id.pay_manoy);
		pay_time = (TextView) findViewById(R.id.pay_time);
		order_id = (TextView) findViewById(R.id.order_id);
		order_id.setText(orderId);
		ll_close = (LinearLayout) findViewById(R.id.ll_close);
		getorder();
	}
	 @Override  
	    public boolean onKeyDown(int keyCode, KeyEvent event)  
	    {  
	        if (keyCode == KeyEvent.KEYCODE_BACK )  
	        {  
				Intent mIntent = new Intent();
				if(mOrderFormObject.orderstatus1.equals("10")){
					mIntent.putExtra("issure", "1");
					}else{
						mIntent.putExtra("issure", "2");
					}
				setResult(112, mIntent);
				finish();
	        }  
	        return false;  
	    }  
	 
	private void myonclick() {
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setorderstatus();
			}
		});

		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent mIntent = new Intent();
//				if(mOrderFormObject.orderstatus1!=null){
				if(mOrderFormObject.orderstatus1.equals("10")){
					mIntent.putExtra("issure", "1");
					}else{
						mIntent.putExtra("issure", "2");
					}
//				}
				setResult(112, mIntent);
				finish();
			}
		});
		btn_close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ArrayList<String[]> params2 = new ArrayList<String[]>();
				String[] funParam2 = new String[] { "fun",
						"updateorderform_mark" };
				String[] name1 = new String[] { "order_id", orderId };
//				Toast.makeText(OrderFormDetailClientActivity.this, orderId,
//						Toast.LENGTH_LONG).show();
				params2.add(funParam2);
				params2.add(name1);
				Constant.NET_STATUS = Utils
						.getCurrentNetWork(OrderFormDetailClientActivity.this);
				if (Constant.NET_STATUS) {
					new HttpConnection().get(Constant.URL, params2,
							type_callbackListener);
				} else {
					Toast.makeText(OrderFormDetailClientActivity.this,
							"请检查网络连接", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Toast.makeText(OrderFormDetailClientActivity.this, "订单已取消！",Toast.LENGTH_LONG).show();
//				if(type!=null){
//					if(type.equals("staff")){
//					}
//				}
				finish();
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "无数据",Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getorder() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbyorderid" };
		String[] name1 = new String[] { "order_id", orderId };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list_order.size() > 0) {
						mOrderFormObject = list_order.get(0);
						title.setText(mOrderFormObject.name);
						// phone;
						reservation_time
								.setText(mOrderFormObject.reservation_time);
//						server_datetime.setText(mOrderFormObject.service_time);
						server_datetime.setText(mOrderFormObject.begin_time+" "+mOrderFormObject.service_time);
						pay_manoy.setText("￥" + mOrderFormObject.pay_manoy);
						pay_time.setText(mOrderFormObject.pay_time);
						staffId = mOrderFormObject.staff_id;
						serviceId = mOrderFormObject.service;
						storeId = mOrderFormObject.store_id;
						storephoto.setText(mOrderFormObject.phone);
						
						if(!mOrderFormObject.orderstatus.equals("20")){
							btn_close.setVisibility(android.view.View.GONE);
							btn_ok.setVisibility(android.view.View.GONE);
						}else{
							ll_close.setVisibility(View.VISIBLE);
						}
						getservicelist(serviceId,storeId);
						getstafflist(staffId);
					} else {
						Toast.makeText(OrderFormDetailClientActivity.this,
								"无数据", Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(OrderFormDetailClientActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getservicelist(String serviceId2,String storeId) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservicelistbyid" };
//		String[] name1 = new String[] { "service_id", serviceId2 };
		String[] store = new String[] { "order_id", orderId };
		params2.add(funParam2);
		params2.add(store);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mServiceListObject = JSON.parseObject(v,
						ServiceListObject.class);
				list_service = mServiceListObject.response;
				if (mServiceListObject.meta.getMsg().equals("OK")) {
					if (list_service.size() > 0) {
						adapter1 = new PreordainServiceAdapter(
								OrderFormDetailClientActivity.this,
								list_service);
						list_2.setAdapter(adapter1);
					} else {
						Toast.makeText(OrderFormDetailClientActivity.this,
								"无服务", Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(OrderFormDetailClientActivity.this, "无服务",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "无服务",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getstafflist(String staffId2) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstafflistbyid" };
		String[] name1 = new String[] { "staff_id", staffId2 };
		String[] storeid = new String[] { "storeid",mOrderFormObject.store_id };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(storeid);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list_staff = mStaffListObject.response;
				if (mStaffListObject.meta.getMsg().equals("OK")) {
					if (list_staff.size() > 0) {
						adapter2 = new PreordainStaffAdapter(
								OrderFormDetailClientActivity.this, list_staff);
						String[] adapterData = staffId.split(",");
						list_1.setAdapter(new ArrayAdapter<String>(
								OrderFormDetailClientActivity.this,
								android.R.layout.simple_expandable_list_item_1,
								adapterData));
						list_1.setAdapter(adapter2);
					} else {
						Toast.makeText(OrderFormDetailClientActivity.this,
								"无员工", Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(OrderFormDetailClientActivity.this, "无员工",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "无员工",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	// 更新订单状态
	private void setorderstatus() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setOrderStatus" };
		String[] name1 = new String[] { "order_id", orderId };
		String[] name2 = new String[] { "orderstatus", "30" }; // 完成状态为30
		Toast.makeText(this, "正在更新中", Toast.LENGTH_LONG).show();
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					setstatus_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener setstatus_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
//				CommionOrderId();
				Intent mIntent = new Intent();
				if(mOrderFormObject.orderstatus1.equals("10")){
					mIntent.putExtra("issure", "1");
					}else{
						mIntent.putExtra("issure", "2");
					}
				setResult(112, mIntent);
				finish();
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "更新失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void CommionOrderId() {
		// http://115.28.191.58:89/juyue/json.aspx?fun=setWithdrawals&order_id=74171446513427408&store_id=&staff_id=1
		// &type=1&money=13.8&order_money=49&remarks=&recharge_money=0
		Double fencheng = Double.parseDouble(mOrderFormObject.price)
				* Double.parseDouble(list_staff.get(0).commission) / 10
				+ Double.parseDouble(list_staff.get(0).surchange);
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setWithdrawals" };
		String[] name1 = new String[] { "order_id", orderId };
		String[] name2 = new String[] { "store_id", "0" };
		String[] name3 = new String[] { "staff_id", mOrderFormObject.staff_id };
		String[] name4 = new String[] { "type", "1" };// 0为商铺分成，1为员工分成
		String[] name5 = new String[] { "money", String.valueOf(fencheng) };
		String[] name6 = new String[] { "order_money",
				mOrderFormObject.pay_manoy };
		String[] name7 = new String[] { "remarks", };
		String[] name8 = new String[] { "recharge_money", "0" };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		params2.add(name5);
		params2.add(name6);
		params2.add(name7);
		params2.add(name8);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2, comListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener comListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				if (mOrderListObject.meta.getMsg().equals("OK")) {

				}
			} else {
				Toast.makeText(OrderFormDetailClientActivity.this, "无服务",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	// update user_staff set balance=balance+{cbalance} where id='{cid}';
	// update user_store set balance=balance+{sbalance} where id='{sid}';

	private void UpUserBalance() {
		// http://115.28.191.58:89/juyue/json.aspx?fun=setWithdrawals&order_id=74171446513427408&store_id=&staff_id=1
		// &type=1&money=13.8&order_money=49&remarks=&recharge_money=0
		String sbalance = "";
		String cbalance = "0";
		if (mOrderFormObject.commission.equals("0")) {
			sbalance = mOrderFormObject.pay_manoy;
			cbalance = "0";
		} else {
			double f = Double.valueOf(mOrderFormObject.pay_manoy)
					* Double.valueOf("0." + mOrderFormObject.commission);
			BigDecimal b = new BigDecimal(f);
			double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

			cbalance = String.valueOf(f1);

			double f2 = Double.valueOf(mOrderFormObject.pay_manoy) - f1;
			BigDecimal b2 = new BigDecimal(f2);
			double f3 = b2.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			sbalance = String.valueOf(f3);
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "UpUserBalance" };
		String[] name1 = new String[] { "cid", mOrderFormObject.staff_id };
		String[] name2 = new String[] { "cbalance", cbalance };
		String[] name3 = new String[] { "sid", mOrderFormObject.store_id };
		String[] name4 = new String[] { "sbalance", sbalance };

		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);

		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					UpUserBalance_comListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener UpUserBalance_comListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				if (mOrderListObject.meta.getMsg().equals("OK")) {

				}
			}
		}
	};
}
