package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.WithDrawalsObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class WithDrawalsAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<WithDrawalsObject> list = new ArrayList<WithDrawalsObject>();

	public WithDrawalsAdapter(Context ctx, ArrayList<WithDrawalsObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_withdrawals, null);
			mHolder.tv_money = (TextView) convertView.findViewById(R.id.tv_money);
			mHolder.tv_datetime = (TextView) convertView.findViewById(R.id.tv_datetime);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_money.setText(list.get(position).money);
		mHolder.tv_datetime.setText(list.get(position).create_time);
		return convertView;
	}

	class Holder {
		TextView tv_datetime, tv_money;
	}
}
