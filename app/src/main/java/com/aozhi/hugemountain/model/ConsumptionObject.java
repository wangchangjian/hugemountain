package com.aozhi.hugemountain.model;

public class ConsumptionObject {
	 public String year;
	 public String id;
     public String services;
     public String number;
     public String client_id;
     public String create_time;
     public String del_flag;
     public String name;
     public String money;
     public String orderform_id;
     public String staffs;
     public String store_id;
     public String staff_name;
     public String service_name;
     public String commission;
     public String pay_status;
     public String usermoney;
     public String count;
     public String balance;
     public String sname;
     public String m,m1,m2,m3,code_id,paytype,order_id,member,net;
	public String getUsermoney() {
		return usermoney;
	}
	public void setUsermoney(String usermoney) {
		this.usermoney = usermoney;
	}
	public String getPay_status() {
		return pay_status;
	}
	public void setPay_status(String pay_status) {
		this.pay_status = pay_status;
	}
	public String getCommission() {
		return commission;
	}
	public void setCommission(String commission) {
		this.commission = commission;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getServices() {
		return services;
	}
	public void setServices(String services) {
		this.services = services;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getClient_id() {
		return client_id;
	}
	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getDel_flag() {
		return del_flag;
	}
	public void setDel_flag(String del_flag) {
		this.del_flag = del_flag;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getOrderform_id() {
		return orderform_id;
	}
	public void setOrderform_id(String orderform_id) {
		this.orderform_id = orderform_id;
	}
	public String getStaffs() {
		return staffs;
	}
	public void setStaffs(String staffs) {
		this.staffs = staffs;
	}
	public String getStore_id() {
		return store_id;
	}
	public void setStore_id(String store_id) {
		this.store_id = store_id;
	}
	public String getStaff_name() {
		return staff_name;
	}
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getService_name() {
		return service_name;
	}
	public void setService_name(String service_name) {
		this.service_name = service_name;
	}
     
     
}
