package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class VipCenterListObject {
	public Meta meta;
	public ArrayList<VipCenterObject> response;
	public ArrayList<VipCenterObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<VipCenterObject> response) {
		this.response = response;
	}
	
}
