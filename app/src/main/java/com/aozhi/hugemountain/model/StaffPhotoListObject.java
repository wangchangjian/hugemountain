package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class StaffPhotoListObject {
	public Meta meta;
	public ArrayList<StaffPhotoObject> response;
	
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<StaffPhotoObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<StaffPhotoObject> response) {
		this.response = response;
	}
	
}
