package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class OrderFormObject implements Serializable{

	public String id;
	public String name;
	public String client_id;
	public String staff_id;
	public String store_id;
	public String reamrk;
	public String img;
	public String price;
	public String reservation_time;
	public String pay_time;
	public String pay_manoy;
	public String earmoney;
	public String mark;
	public String type_mark;
	public String create_time;
	public String update_time;
	public String del_flag;
	public String order_id;
	public String orderstatus;
	public String timebucket;
	public String server;
	public String tel;
	public String service_time;
	public String service_id;
	public String servive_name;
	public String service_remark;
	public String service_img;
	public String service_money;
	public String service;
	public String daymoney;
	public String monthmoney;
	public String storename;
	public String msgreg_id;
	public String login_id;
	public String type;
	public String code_id;
	public String servicename;
	public String surchange;
	public String use_id;
	public String orderstatus1;
	public String sid;
	public String cid;
	public String commission;
	public String money;
	public String phone;
	public String shopphone;
	public String staffphone;
	public String names;
	public String photo;
	public String sname;
	
	public String nums;//评论数量
	public String begin_time;
	public String paytype;//支付方式
}
