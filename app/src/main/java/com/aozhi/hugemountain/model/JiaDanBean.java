package com.aozhi.hugemountain.model;

public class JiaDanBean {
	public String id;
	public String orderid;
	public String servetype;
	public String address;
	public String state;
	public String createtime;
	public String pic;
	public String img;
	
	public String uid;
	public String did;
	public String servetypes;
	public String days;
	public String cname;
	public String tel;
	
	public String sname;
	public String phone;
	public String isfloor;
	public String plies;
	public String membertype;
	public String remark;
	public String ltel;
	public String lname;
	
	public String steps;
	
	public String counts;
	public String dname;
	public String dtel;
	public String sid;
	public String yuyuedate;
	public String modificationtime;
	
	
	public String getYuyuedate() {
		return yuyuedate;
	}

	public void setYuyuedate(String yuyuedate) {
		this.yuyuedate = yuyuedate;
	}
	public String companyname;
	public String getCompanyname() {
		return companyname;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}

	public String getLogisticsid() {
		return logisticsid;
	}

	public void setLogisticsid(String logisticsid) {
		this.logisticsid = logisticsid;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getTtel() {
		return ttel;
	}

	public void setTtel(String ttel) {
		this.ttel = ttel;
	}

	public String getTaddress() {
		return taddress;
	}

	public void setTaddress(String taddress) {
		this.taddress = taddress;
	}

	public String getTnumbers() {
		return tnumbers;
	}

	public void setTnumbers(String tnumbers) {
		this.tnumbers = tnumbers;
	}

	public String getTremark() {
		return tremark;
	}

	public void setTremark(String tremark) {
		this.tremark = tremark;
	}
	public String logisticsid;
	public String pwd;
	public String ttel;
	public String taddress;
	public String tnumbers;
	public String tremark;
	
	public String getDtel() {
		return dtel;
	}

	public void setDtel(String dtel) {
		this.dtel = dtel;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}
	public String getCounts() {
		return counts;
	}

	public void setCounts(String counts) {
		this.counts = counts;
	}

	public String getSteps() {
		return steps;
	}

	public void setSteps(String steps) {
		this.steps = steps;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsfloor() {
		return isfloor;
	}

	public void setIsfloor(String isfloor) {
		this.isfloor = isfloor;
	}

	public String getPlies() {
		return plies;
	}

	public void setPlies(String plies) {
		this.plies = plies;
	}

	public String getMembertype() {
		return membertype;
	}

	public void setMembertype(String membertype) {
		this.membertype = membertype;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLtel() {
		return ltel;
	}

	public void setLtel(String ltel) {
		this.ltel = ltel;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getServetypes() {
		return servetypes;
	}
	public void setServetypes(String servetypes) {
		this.servetypes = servetypes;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getServetype() {
		return servetype;
	}
	public void setServetype(String servetype) {
		this.servetype = servetype;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	
}
