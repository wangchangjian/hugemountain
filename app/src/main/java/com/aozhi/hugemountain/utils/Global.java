package com.aozhi.hugemountain.utils;

import android.R.integer;

public class Global {
	//gender
	 public enum GENDER {
	        male(0), female(1); 
	        private final int value;
	        GENDER(int value) {
	            this.value = value;
	        }
	        public int getValue() {
	            return value;
	        }
	    }
	 public static final int REQUEST_STARTCITY = 30;
	//preference
	public static final String PREFERENCESNAME = "CodoInfo";
	public static final String PREFERENCES_USERNAME = "username";
	public static final String PREFERENCES_PASSWORD = "password";
	public static final String PREFERENCES_READEDTASKS = "readedtasks";
	public static final String PREFERENCES_LANGUAGE="language";
	public static final String PREFERENCES_GROUPREMIND="groupremind";
	public static final String PREFERENCES_GROUPTOP="grouptop";
//	public static final String PREFERENCES_REMINDIDINDEX="remindidindex";
	//preference_setting
	public static final String PREFERENCES_SETTINGPICUP="settingpicup";
	public static final String PREFERENCES_SETTINGPICLOAD="settingpicdown";
	public static final int PREFERENCES_SETTINGPIC_UPSMALL=0;
	public static final int PREFERENCES_SETTINGPIC_UPBIG=1;
	public static final int PREFERENCES_SETTINGPIC_LOADSMALL=0;
	public static final int PREFERENCES_SETTINGPIC_LOADBIG=1;
	public static final String PREFERENCES_SETTINGVOI="settingvoi";
	public static final int PREFERENCES_SETTINGVOI_VOICE=0;
	public static final int PREFERENCES_SETTINGVOI_SHAKE=1;
	public static final int PREFERENCES_SETTINGVOI_BOTH=2;
	public static final String PREFERENCES_SETTINGNOTICE="settingnotice";
	public static final int PREFERENCES_SETTINGNOTICE_NEVER=0;
	public static final int PREFERENCES_SETTINGNOTICE_DAY=1;
	public static final int PREFERENCES_SETTINGNOTICE_EVER=2;

	//database
	public final static String DATABASENAME = "cododb";
	public final static String TABLECHATNAME = "chattb";
	public final static String TABLENOTICENAME = "noticetb";
	public final static String TABLEMULTICHATNAME = "multitb";
	public final static String TABLEACCOUNT 	= "account";
	public final static String GROUP		= "grouptb";
	public final static String ORGANIZA		= "organizatb";
	public final static String MEMBER		= "membertb";
//	public static String TABLEREMINDNAME = "remindtb";
	//language
	public static final String LANGUAGE_EN = "EN";
	public static final String LANGUAGE_CN = "CN";
	public static final String LANGUAGE_VN = "VN";
	public static final String LANGUAGE_TH = "TH";
	public static final String LANGUAGE_ID = "ID";
	// requestCode
	public static final short REQUEST_REGISTER = 1;
	public static final short REQUEST_PICK_PICTURE = 11;
	public static final short REQUEST_TAKEPHOTO = 12;
	public static final short REQUEST_CHOOSEREMIND = 21;
	public static final short REQUEST_CHOOSEPUB = 22;
	public static final short REQUEST_DRAW = 23;
	public static final short REQUEST_EXERCISE = 24;
	public static final short REQUEST_ALBUM_PIC = 25;
	public static final short REQUEST_ALBUM_MY = 26;
	public static final short REQUEST_DELETEMEMBER = 27;
	public static final short REQUEST_EVALUATE = 28;
	public static final short REQUEST_FORWARD = 31;
	public static final short REQUEST_CHOOSETEAMMEMBER = 29;
	public static final short REQUEST_LOGOUT=4;
	public static final short REQUEST_CHOOSEFILE=32;


	//http
	public static String SERVER = "http://112.126.67.4/kayou";
//	public static String SERVER = "http://42.96.198.70:81/codo";
	public static String URL = SERVER+"/json.aspx";
	public static boolean NET_STATUS = false;
	public static String PHOTO_DIR = "codo/temp/";
	public static String DownLoadImageUrl = SERVER;
	public static String PIC_TYPE=".jpg";
	public static String UPLOAD_IMAGE_URL = SERVER+"/uploadface.aspx";
	public static String PHOTOS_NAME = "codophoto.jpg";
	public static String TAKE_PHOTO_DIR = "/codo/temp/" ;
	public static String DRAW_NAME = "cododraw.jpg";
	public static String UPLOAD_PIC_NAME = "codouploadpic.jpg";
	public static String SAVE_PIC_DIR = "codo/save";
	public static String CROP_PIC_TEMP = "crop_temp.jpg" ;
	
	//chat smack
	public static String XMPPSERVERHOST = "112.126.67.4";
	public static String CHAT_SERVICE_NAME = "com.aozhi.kayou.service.ChatService";
	public static String MULTICHAT_SERVICE_NAME = "com.aozhi.kayou.service.MultiChatService";
	public static String REMIND_SERVICE_NAME = "com.aozhi.kayou.service.RemindService";
	public static String AMR_DIR = "codo/temp";
	public static String SIGNAL_VOICE = "003";
	public static String SIGNAL_PIC = "002";
	public static String SIGNAL_FILE = "004";
	public static String SIGNAL_TEXT = "001";
	public static String SIGNAL_LOCA = "005";
	
	
	public static final short SENT_SIGNAL_CHAT_MESSAGE = 1;
	//multiuserchat smack
//	public static final short MULTI_JOINROOM = 101;
//	public static final short MULTI_CREATEROOM = 102;
	public static final short MULTI_SENTMESSAGE = 2;
//	public static final short MULTI_KICK = 104;
//	public static final short MULTI_INVITE = 105;
//	public static final short MULTI_INVITED = 106;
//	public static final short MULTI_RECEIVED = 107;
	public static final short MULTI_GETROOMLIST = 3;
//	public static final short MULTI_START = 109;
	public static final short MULTI_STOP = 4;
	
	//handler what
	public static final short HANDLER_COLOR = 130;
	public static final short HANDLER_BACKGROUND = 131;
	public static final short HANDLER_XMPP_CONNECT_FAILED = 132;
	public static final short HANDLER_XMPP_CONNECT_SUCCESS = 133;
	public static final short HANDLER_XMPP_LOGIN_SUCCESS = 134;
	public static final short HANDLER_XMPP_LOGIN_FAILED = 135;
	public static final short HANDLER_XMPP_REGIST_FAILED = 136;
	public static final short HANDLER_XMPP_REGIST_SUCCESS = 137;
	
	//bind setting
	public static final short BIND_PHONE = 0;
	public static final short BIND_EMAIL = 1;
	public static final short BIND_SINA = 2;
	
	//setting voice
	public static final short SETTING_PICTURE = 0;
	public static final short SETTING_VOICE = 1;
	public static final short SETTING_NOTICE = 2;
 
	
	//server back flag:
	public static final String FAIL_CONNECT = "fail";
	
	//bg image max width
	public static final int BG_MAX_WIDTH = 400 ;
	//avatar image max width
	public static final int AVATAR_MAX_WIDTH = 100 ;
	
	public static final String ADDED_GROUP = "001";
	public static final String MODIFY_GROUP = "002";
	public static final String KICKED_GROUP = "003";
	public static final String SERVER_NAME = "admin" ;
}
