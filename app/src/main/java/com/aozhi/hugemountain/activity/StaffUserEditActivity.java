package com.aozhi.hugemountain.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class StaffUserEditActivity extends Activity implements OnClickListener {

	private Button queren;
	private Button btn_back;
	private ImageView user_img;
	private TextView user_login, user_name, user_age, user_phone,jd_count,sc_count,sc_counts;
	private Spinner user_sex;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private ArrayList<LoginBean> getlist = new ArrayList<LoginBean>();
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> lists = new ArrayList<LoginBean>();
	private String serverFile = "";
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;
	public static final int PHOTOZOOM = 2;
	public static final int PHOTORESOULT = 3;
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private TextView	tv_camera,tv_photo;
	private String imgload="";
	private ProgressDialog progressDialog = null;
	private RelativeLayout editphoto;
    private EditText user_remark;
    private LinearLayout li_eva;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_useredit);
		initView();
		initListener();
		getStaffUser();
	}

	private void setView() {
		ArrayList<String> timelist = new ArrayList<String>();
//		getlist = (ArrayList<LoginBean>) getIntent().getSerializableExtra("userlist");
		getlist =lists;
		user_login.setText(getlist.get(0).staffphoto);
		user_name.setText(getlist.get(0).name);
		user_age.setText(getlist.get(0).age);
		user_phone.setText(getlist.get(0).phone);
		user_remark.setText(getlist.get(0).remarks);
		jd_count.setText(getlist.get(0).num1);
		sc_count.setText(getlist.get(0).num2);
		sc_counts.setText(getlist.get(0).num3);
		timelist.add("男");
		timelist.add("女");
//		timelist.add("其他");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_time, timelist);
		user_sex.setAdapter(adapter);
		SpinnerAdapter apsAdapter= user_sex.getAdapter(); //得到SpinnerAdapter对象
	    int k= apsAdapter.getCount();
	    for(int i=0;i<k;i++){
	        if(getlist.get(0).sex.equals(apsAdapter.getItem(i).toString())){
	        	user_sex.setSelection(i,true);// 默认选中项
	            break;
	        }
	    }
		if (getlist.get(0).avatar.equals("") || getlist.get(0).avatar == null) {

		} else {
			MyApplication.downloadImage.addTasks(getlist.get(0).avatar, user_img,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		imgload=getlist.get(0).avatar;
	}

	private void initListener() {
		queren.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		editphoto.setOnClickListener(this);
		li_eva.setOnClickListener(this);
	}

	private void initView() {
		queren = (Button) findViewById(R.id.queren);
		btn_back = (Button) findViewById(R.id.btn_back);
		user_img = (ImageView) findViewById(R.id.user_img1);
		user_login = (TextView) findViewById(R.id.user_login1);
		user_name = (TextView) findViewById(R.id.user_name1);
		user_sex = (Spinner) findViewById(R.id.user_sex1);
		user_age = (TextView) findViewById(R.id.user_age1);
		user_phone = (TextView) findViewById(R.id.user_phone1);
		editphoto=(RelativeLayout) findViewById(R.id.editphoto);
		user_remark=(EditText)findViewById(R.id.user_remark);
		jd_count = (TextView) findViewById(R.id.jd_count);
		sc_count = (TextView) findViewById(R.id.sc_count);
		sc_counts = (TextView) findViewById(R.id.sc_counts);
		li_eva = (LinearLayout) findViewById(R.id.li_eva);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.queren:
			if(!user_name.getText().equals("")||!user_age.getText().equals("")||!user_phone.getText().equals("")){
				upStaffUserInfo();
			}else{
				Toast.makeText(StaffUserEditActivity.this, "您的信息不全，不能修改，请仔细检查",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.btn_back:
			finish();
			break;
		case R.id.editphoto:
			showAtaver();
			break;
		case R.id.li_eva:
			Intent intent = new Intent(StaffUserEditActivity.this,EvaListActivity.class);
			intent.putExtra("staff_id", MyApplication.Staffuser.id);
			startActivity(intent);
			break;
		default:
			break;
		}
		
	}
	
	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.activity_xiugai);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						Environment.getExternalStorageDirectory(), "temp.jpg")));
				System.out.println("============="
						+ Environment.getExternalStorageDirectory());
				startActivityForResult(intent, PHOTOHRAPH);
				dlg.cancel();
			}
		});
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTOZOOM);
				dlg.cancel();
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode == NONE)
			return;
		// ����
		if (requestCode == PHOTOHRAPH) {
			// �����ļ�����·��������ڸ�Ŀ¼��
			File picture = new File(Environment.getExternalStorageDirectory()
					+ "/temp.jpg");
			System.out.println("------------------------" + picture.getPath());
			startPhotoZoom(Uri.fromFile(picture));
		}
		if (data == null)
			return;
		// ��ȡ�������ͼƬ
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		// ������
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 -

				new DateFormat();
				String name = DateFormat.format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA)) + ".jpg";
				Bundle bundle = data.getExtras();
				Bitmap bitmap = (Bitmap) bundle.get("data");// ��ȡ������ص����ݣ���ת��ΪBitmapͼƬ��ʽ
				FileOutputStream b = null;
				// ???????????????????????????????Ϊʲô����ֱ�ӱ�����ϵͳ���λ���أ�����������������������
				File file = new File("/sdcard/myImage/");
				file.mkdirs();// �����ļ���
				String fileName = "/sdcard/myImage/" + name;
				try {
					b = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// ������д���ļ�
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				File file1 = new File(fileName);
				user_img.setImageBitmap(photo);
				new UploadAsyncTask().execute(file1);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void startPhotoZoom(Uri uri) {
		Intent mintent = new Intent("com.android.camera.action.CROP");
		mintent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		mintent.putExtra("crop", "true");
		mintent.putExtra("aspectX", 1);
		mintent.putExtra("aspectY", 1);
		mintent.putExtra("outputX", 320);
		mintent.putExtra("outputY", 320);
		mintent.putExtra("return-data", true);
		startActivityForResult(mintent, PHOTORESOULT);
	}
	
	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			imgload=serverFile;
//			upUserPhoto();
			return serverFile;
		}
	}
	
	// 更新员工信息
		private void upStaffUserInfo() {
			String img="";
			
			if(serverFile.equals("")){
				img=getlist.get(0).avatar;
			}else{
				img=serverFile;
			}
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "upStaffUserInfo" };
			String[] name1 = new String[] { "name",user_name.getText().toString().trim()};
			String[] name2 = new String[] { "sex",user_sex.getSelectedItem().toString()};
			String[] name3 = new String[] {"age",user_age.getText().toString().trim()};
			String[] name4 = new String[] { "phone", user_phone.getText().toString().trim() };// 备注商家简介
			String[] name5 = new String[] { "login_id",MyApplication.Staffuser.getId()};
			String[] name6 = new String[] { "serverFile",img};
			String[] name7 = new String[] { "remarks",user_remark.getText().toString()};
			params2.add(funParam2);
			params2.add(name1);
			params2.add(name2);
			params2.add(name3);
			params2.add(name4);
			params2.add(name5);
			params2.add(name6);
			params2.add(name7);
			progressDialog = ProgressDialog.show(this,
					getString(R.string.app_name),
					getString(R.string.tv_dialog_context), false);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						type_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!v.equals("fail")) {
					LoginListObject mLoginListObject = JSON.parseObject(v,
							LoginListObject.class);
					list = mLoginListObject.response;
					if (mLoginListObject.meta.getMsg().equals("OK")) {
						Toast.makeText(StaffUserEditActivity.this, "个人信息更新成功！",
								Toast.LENGTH_LONG).show();
						Intent mIntent=new Intent(StaffUserEditActivity.this,StoreInfoActivity.class);
						setResult(300, mIntent);
						finish();
					} else {
						Toast.makeText(StaffUserEditActivity.this, "个人信息更新失败！",
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(StaffUserEditActivity.this, "个人信息更新失败！",
							Toast.LENGTH_LONG).show();
				}
			}
		};
		
		private void upUserPhoto() {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "setStaffImg" };
			String[] name1 = new String[] { "id",MyApplication.Staffuser.getId()};
			String[] name2 = new String[] { "path",imgload};
			params2.add(funParam2);
			params2.add(name1);
			params2.add(name2);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						upphoto_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener upphoto_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!v.equals("fail")) {
						Toast.makeText(StaffUserEditActivity.this, "图片更新成功！",
								Toast.LENGTH_LONG).show();
						
					} else {
						Toast.makeText(StaffUserEditActivity.this, "图片更新失败！",
								Toast.LENGTH_LONG).show();
					}
			}
		};
		
		// 获取用户信息并设置显示
		private void getStaffUser() {
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				// fun=getAddressList&userid=32772
				ArrayList<String[]> params = new ArrayList<String[]>();
				String[] methodParam = new String[] { "fun", "getStaffUserInfo" };
				String[] pageParam = new String[] { "login_id",
						MyApplication.Staffuser.getStaffphoto() };
				params.add(methodParam);
				params.add(pageParam);
				progressDialog = ProgressDialog.show(this, null, "正在加载", false);
				progressDialog.setCancelable(true);
				new HttpConnection().get(Constant.URL, params,
						getGoodsList_callbackListener);
			} else
				Toast.makeText(this, "请检查网络连接!", Toast.LENGTH_LONG).show();
		}

		private CallbackListener getGoodsList_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String result) {
				Log.d("返回数据", result);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!result.equals("fail")) {
					mLoginListObject = JSON.parseObject(result,
							LoginListObject.class);
					lists = mLoginListObject.response;
					if (mLoginListObject.meta.getMsg().equals("OK")) {
						if (lists.size() > 0) {
							setView();
						} else {
							Toast.makeText(StaffUserEditActivity.this, "获取用户信息失败",
									Toast.LENGTH_SHORT).show();
							finish();
						}
					}
				} else {
					Toast.makeText(StaffUserEditActivity.this, "获取用户信息失败",
							Toast.LENGTH_SHORT).show();
					finish();
				}
			}
		};

//		private void setViews() {
//			user_code.setText("编号" + list.get(0).code_id);
//			user_login.setText(list.get(0).staffphoto);
//			user_name.setText(list.get(0).name);
//			user_sex.setText(list.get(0).sex);
//			user_age.setText(list.get(0).age);
//			user_phone.setText(list.get(0).phone);
//			jd_count.setText(list.get(0).num1);
//			tv_remark.setText(list.get(0).remarks);
//			sc_count.setText(list.get(0).num2);
//			sc_counts.setText(list.get(0).num3);
//			if (list.get(0).avatar.equals("") || list.get(0).avatar == null) {
//
//			} else {
//				MyApplication.downloadImage.addTasks(list.get(0).avatar, user_img,
//						new DownloadImage.ImageCallback() {
//
//							@Override
//							public void imageLoaded(Bitmap imageBitmap,
//									String imageUrl) {
//								if (imageBitmap != null) {
//									// xHolder.tx_image.setImageBitmap(imageBitmap);
//									// xHolder.tx_image.setTag("");
//									Drawable drawable = new BitmapDrawable(
//											imageBitmap);
//									user_img.setBackgroundDrawable(drawable);
//								}
//							}
//
//							@Override
//							public void imageLoaded(Bitmap imageBitmap,
//									DownloadImageMode callBackTag) {
//								// TODO 自动生成的方法存根
//								if (imageBitmap != null) {
//									// xHolder.tx_image.setImageBitmap(imageBitmap);
//									// xHolder.tx_image.setTag("");
//									Drawable drawable = new BitmapDrawable(
//											imageBitmap);
//									user_img.setBackgroundDrawable(drawable);
//								}
//							}
//
//						});
//			}
//			MyApplication.downloadImage.doTask();
//		}

}
