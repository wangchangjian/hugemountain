package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class RoomObject implements Serializable{

	 public String id;
     public String name;
     public String code;
     public String mark;
     public String del_flag;
     public String store_id;
     public String number;
     public String staff_id;
     public String room_services;
     public String create_time;
     public String service_time;
     public String custom_time;
     public String code_id;
     public String img;
}
