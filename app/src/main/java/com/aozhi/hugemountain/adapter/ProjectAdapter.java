package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.model.ServiceObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProjectAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	
	
	public ProjectAdapter(Context ctx, ArrayList<ProjectObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_project1, null);
			mHolder.iv_img= (ImageView) convertView.findViewById(R.id.iv_img1);
			mHolder.tv_name=(TextView) convertView.findViewById(R.id.tv_name1);
			mHolder.tv_money=(TextView) convertView.findViewById(R.id.tv_money1);
			mHolder.tv_count=(TextView) convertView.findViewById(R.id.tv_count);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_name.setText(list.get(position).name);
		mHolder.tv_money.setText("￥"+list.get(position).money);
		mHolder.tv_count.setText("服务"+list.get(position).click_count+"次");
		if (list.get(position).avatar.equals("")
				|| list.get(position).avatar==null ) {
			
		} else {
			//avatar会有多张图片，用逗号分隔，取第一张显示
			String[] photos = list.get(position).avatar.split(",");
			
			MyApplication.downloadImage.addTasks(
					photos[0], mHolder.iv_img,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_img.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		return convertView;
	}
	
	class Holder {
		ImageView iv_img;
		TextView tv_name,tv_content,tv_money,tv_count,time;
	}
}
