package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.CashRecordAdapter;
import com.aozhi.hugemountain.model.CashRecordListObject;
import com.aozhi.hugemountain.model.CashRecordObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class TixianRecordActivity extends Activity implements OnClickListener {

	private TextView nodata,money;
	private Spinner sp_r;
	private ListView list_p;
	private Button btn_back;
	private String[] sp_list = new String[] { "全部", "按时间", "按金额" };
	private ProgressDialog progressDialog = null;
	private CashRecordListObject mCashRecordListObject;
	private ArrayList<CashRecordObject> list = new ArrayList<CashRecordObject>();
	private CashRecordAdapter mCashRecordAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tx_record);
		initView();
		initListener();
	}

	private void initListener() {
		btn_back.setOnClickListener(this);
		Spinner();
		mCashRecordAdapter = new CashRecordAdapter(TixianRecordActivity.this,
				list);
		list_p.setAdapter(mCashRecordAdapter);
		list_p.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long ids) {
				
			}
		});
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		nodata=(TextView) findViewById(R.id.nodata);
		sp_r=(android.widget.Spinner) findViewById(R.id.sp_r);
		list_p=(ListView) findViewById(R.id.list_p);
		money=(TextView) findViewById(R.id.money);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}

	}

	private void Spinner() {
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, sp_list);
		sp_r.setAdapter(adapter);
		sp_r.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if (sp_r.getSelectedItem().equals("全部")) {
					getProjectList("id");
				} else if (sp_r.getSelectedItem().equals("按时间")) {
					getProjectList("store_id");
				} else if (sp_r.getSelectedItem().equals("按金额")) {
					getProjectList("money");
				}

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				getProjectList("id");
			}
		});
	}

	/**
	 * 获取提现记录
	 * @param order   排序方式
	 */
	private void getProjectList(String order) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreTx" };
		String[] idParam2 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name1=new String[]{"order",order};
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(name1);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mCashRecordListObject = JSON.parseObject(v,
						CashRecordListObject.class);
				list = mCashRecordListObject.response;
				if (list.size() > 0) {
					nodata.setVisibility(View.GONE);
					list_p.setVisibility(View.VISIBLE);
					mCashRecordAdapter = new CashRecordAdapter(
							TixianRecordActivity.this, list);
					list_p.setAdapter(mCashRecordAdapter);
					Double sum=0.00;
					for(int i=0;i<list.size();i++){
						sum+=Double.parseDouble(list.get(i).money);
					}
					money.setText(String.valueOf(sum));
				}else{
					nodata.setVisibility(View.VISIBLE);
					list_p.setVisibility(View.GONE);
				}

			} else {
				Toast.makeText(TixianRecordActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};
}
