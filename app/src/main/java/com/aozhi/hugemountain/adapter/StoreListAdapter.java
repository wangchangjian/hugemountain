package com.aozhi.hugemountain.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.aozhi.hugemountain.activity.ConsumerActivity.BusinessActivity;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.*;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StoreListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StoreObject> store_list = new ArrayList<StoreObject>();

	public StoreListAdapter(Context ctx, ArrayList<StoreObject> li) {
		mContext = ctx;
		store_list = li;
		inflater = LayoutInflater.from(mContext);}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return store_list.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return store_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public static String getDistanceFromXtoY(double lat_a, double lng_a, double lat_b, double lng_b) {
		LatLng start = new LatLng(lat_a,lng_a);
		LatLng end = new LatLng(lat_b,lng_b);
		DistanceUtil.getDistance(start, end);
		double f = DistanceUtil.getDistance(start, end)/1000;
		BigDecimal b = new BigDecimal(f);
		double f1 = b.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		return String.valueOf(f1);}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_storelist, null);
			mHolder.phone = (ImageView) convertView.findViewById(R.id.phone);
			mHolder.name = (TextView) convertView.findViewById(R.id.name);
			mHolder.photo = (TextView) convertView.findViewById(R.id.photo);
			mHolder.tv_juli=(TextView)convertView.findViewById(R.id.juli);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(store_list.get(position).name);
//		mHolder.photo.setText(store_list.get(position).storephoto);
		mHolder.photo.setText(store_list.get(position).mobile);
		MyApplication.client_storeName = store_list.get(position).name;

		String str = store_list.get(position).location;
		String[] sourceStrArray = str.substring(1, str.length() - 1).split(",");
//		LatLng start = new LatLng(MyApplication.app_latitude,
//				MyApplication.app_longitude);
//		LatLng end = new LatLng(Double.parseDouble(sourceStrArray[1]),
//				Double.parseDouble(sourceStrArray[0]);
		String jili=getDistanceFromXtoY(MyApplication.app_latitude,MyApplication.app_longitude,
				Double.parseDouble(sourceStrArray[1]),
				Double.parseDouble(sourceStrArray[0]));
		mHolder.tv_juli.setText(jili + "km");

		if (store_list.get(position).phone.equals("") || store_list.get(position).phone == null) {

		} else {
			MyApplication.downloadImage.addTask(store_list.get(position).phone, mHolder.phone, new DownloadImage.ImageCallback() {
						@Override
						public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}
						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}
					});
		}
		MyApplication.downloadImage.doTask();
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(mContext, BusinessActivity.class);
				mIntent.putExtra("store_id", store_list.get(position).store_id);
				mContext.startActivity(mIntent);
			}
		});
//		mHolder.moible.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
//						+ store_list.get(position).storephoto));
//				mContext.startActivity(intent);
//			}
//		});
//		mHolder.addr.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(mContext, MapActivity.class);
//				intent.putExtra("longitude", store_list.get(position).longitude); // 经度
//				intent.putExtra("latitude", store_list.get(position).latitude);// 纬度
//				intent.putExtra("storename", store_list.get(position).name);
//				mContext.startActivity(intent);
//			}
//		});
		return convertView;
	}
	class Holder {
		ImageView phone;
		TextView name, photo;
		TextView tv_juli;
	}
}
