package com.aozhi.hugemountain.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ${wangchangjian} on 2016/4/23.
 */
public class LinedTextView extends TextView{
    private Paint mPaint;

    public LinedTextView(Context context) {
        this(context,null);
    }

    public LinedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTextColor(Color.GREEN);
        mPaint=new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(2f);
        mPaint.setColor(this.getTextColors().getDefaultColor());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawLine(0,getHeight()/2-2,getWidth(),getHeight()/2+2,mPaint);
    }
}
