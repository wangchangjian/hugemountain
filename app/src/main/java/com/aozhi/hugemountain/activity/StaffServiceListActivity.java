package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StaffServiceAdapter;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class StaffServiceListActivity extends Activity {
	Button btn_back;
	ListView list;
	
	
	ArrayList<ServiceObject> list_ServiceObject=new ArrayList<ServiceObject>();
	ServiceListObject mServiceListObject;
	ServiceObject mServiceObject;
	StaffServiceAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staff_service_list);
		initView();
		myclick();
		initListener();
	}

	private void initView() {
		btn_back=(Button)findViewById(R.id.btn_back);
		list=(ListView)findViewById(R.id.list);
		adapter=new StaffServiceAdapter(StaffServiceListActivity.this, list_ServiceObject);
		list.setAdapter(adapter);
	}

	private void myclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	private void initListener() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getProjectList" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };


		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mServiceListObject = JSON.parseObject(v, ServiceListObject.class);
				list_ServiceObject = mServiceListObject.response;
				adapter = new StaffServiceAdapter(StaffServiceListActivity.this,list_ServiceObject);
				list.setAdapter(adapter);
			}
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.staff_service_list, menu);
		return true;
	}

}
