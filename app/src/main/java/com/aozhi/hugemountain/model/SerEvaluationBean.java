package com.aozhi.hugemountain.model;

public class SerEvaluationBean {
	public String id;
	public String uid;
	public String did;
	public String oid;
	public String content;
	public String numbers;
	public String numbers1;
	public String getNumbers1() {
		return numbers1;
	}
	public void setNumbers1(String numbers1) {
		this.numbers1 = numbers1;
	}
	public String getNumbers2() {
		return numbers2;
	}
	public void setNumbers2(String numbers2) {
		this.numbers2 = numbers2;
	}
	public String numbers2;
	public String createtime;
	public String name;
	public String image;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getNumbers() {
		return numbers;
	}
	public void setNumbers(String numbers) {
		this.numbers = numbers;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
}
