package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.activity.PreordainPayActivity;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.OrderFormObject;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class OrderFormNAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();

	public OrderFormNAdapter(Context ctx, ArrayList<OrderFormObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_orderform_n, null);
			mHolder.img= (ImageView) convertView.findViewById(R.id.img);
			mHolder.ordername=(TextView) convertView.findViewById(R.id.ordername);
			mHolder.remark=(TextView) convertView.findViewById(R.id.remark);
			mHolder.create_time=(TextView) convertView.findViewById(R.id.create_time);
			mHolder.pay_manoy=(TextView) convertView.findViewById(R.id.pay_manoy);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.ordername.setText("订单号："+list.get(position).order_id);
		mHolder.remark.setText(list.get(position).reamrk);
		mHolder.create_time.setText(list.get(position).create_time);
		mHolder.pay_manoy.setText("￥"+list.get(position).pay_manoy);
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
					Intent mIntent = new Intent(mContext,PreordainPayActivity.class);
					mIntent.putExtra("order_id", list.get(position).order_id);
					mContext.startActivity(mIntent);
			}
		});
		return convertView;
	}
	
	class Holder {
		ImageView img;
		TextView ordername,remark,create_time,pay_manoy;
		TextView tv_miaoshu;
	}
}
