package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;
import java.util.List;
import com.aozhi.hugemountain.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAddAdapter extends BaseAdapter {
	private Context mContext;
	public List<Bitmap> list=new ArrayList<Bitmap>();
	private LayoutInflater inflater;
	public ImageAddAdapter(Context c,List<Bitmap> bm) {
		mContext = c;
		list = bm;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_addimg, null);
			mHolder.img= (ImageView) convertView.findViewById(R.id.imgadd);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.img.setImageBitmap((Bitmap) list.get(position));
		return convertView;
	}

	class Holder {
		ImageView img;
	}
	
	public static Bitmap drawableToBitmap(Drawable drawable) {    
        
        Bitmap bitmap = Bitmap    
                        .createBitmap(    
                                        drawable.getIntrinsicWidth(),    
                                        drawable.getIntrinsicHeight(),    
                                        drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888    
                                                        : Bitmap.Config.RGB_565);    
        Canvas canvas = new Canvas(bitmap);    
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());    
        drawable.draw(canvas);    
        return bitmap;    
}    
}
