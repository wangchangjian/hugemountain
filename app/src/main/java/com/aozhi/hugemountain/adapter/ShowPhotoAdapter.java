package com.aozhi.hugemountain.adapter;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.StaffPhotoObject;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

public class ShowPhotoAdapter extends BaseAdapter {
	private LoginListObject mLoginListObject;
	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffPhotoObject> list = new ArrayList<StaffPhotoObject>();
	public boolean del_flag = true;

	public ShowPhotoAdapter(Context ctx, ArrayList<StaffPhotoObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_showphoto, null);
			mHolder.iv_src = (ImageView) convertView.findViewById(R.id.iv_src);
			mHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_name.setText(list.get(position).name);

		if (list.get(position).src.equals("") || list.get(position).src == null) {

		} else {
			MyApplication.downloadImage.addTasks(list.get(position).src,
					mHolder.iv_src, new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_src.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.iv_src.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(mContext, ImagePagerActivity.class);
				mIntent.putExtra("images", list.get(position).src);
				mContext.startActivity(mIntent);
			}
		});
		// 删除图片
		convertView.setOnLongClickListener(new View.OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				AlertDialog.Builder builder = new Builder(mContext);
				builder.setMessage("确认删除吗？\n注意删除不可恢复！");
				builder.setPositiveButton("确认",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								upCollctions(list.get(position).id);
								if(del_flag){
									list.remove(position);
									notifyDataSetInvalidated();
//									notifyDataSetChanged();
								}
								arg0.dismiss();
							}
						});
				builder.setNegativeButton("取消",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.dismiss();
							}
						});
				builder.create().show();
				return true;
			}
		});
		return convertView;
		
		

	}
	
	private void upCollctions(String id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(mContext);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "delstaffphoto" };
			String[] telParam = new String[] { "id", id };
			params.add(methodParam);
			params.add(telParam);
			new HttpConnection().get(Constant.URL, params,
					upCollctions_callbackListener);
		} else
			Toast.makeText(mContext, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener upCollctions_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (!result.equals("fail")) {
				mLoginListObject = JSON.parseObject(result,
						LoginListObject.class);
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(mContext, "删除成功",
							Toast.LENGTH_LONG).show();
					del_flag=true;
				} else {
					Toast.makeText(mContext, "删除失败",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(mContext, "删除失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	class Holder {
		ImageView iv_src;
		TextView tv_name;
	}
		
	
}
