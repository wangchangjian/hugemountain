package com.aozhi.hugemountain;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.activity.ConsumerActivity.ChooseDatetimeActivity;
import com.aozhi.hugemountain.adapter.BusinessServiceAdapter;
import com.aozhi.hugemountain.adapter.BusinessStaffAdapter;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WorkStoreActivity extends Activity {

	Button btn_back, btn_staff_ok, btn_service_ok;
	TextView t1;
	TextView t2;
	TextView t3;
	LinearLayout item1;
	LinearLayout item2;
	LinearLayout item3;
	private ArrayList<ServiceObject> list_service = new ArrayList<ServiceObject>();
	private ArrayList<StaffObject> list_staff = new ArrayList<StaffObject>();
	private ArrayList<StoreObject> list_store = new ArrayList<StoreObject>();
	private BusinessServiceAdapter adapter1;
	private BusinessStaffAdapter adapter2;
	private ListView list_busness1;
	private ListView list_busness2;
	private String store_id = "";
	ServiceListObject mServiceListObject;
	StaffListObject mStaffListObject;
	StoreListObject mStoreListObject;
	StoreObject mStoreObject;
	ImageView phone;
	TextView name, title, business_houre, remark, address, photo, service;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_business);
		initView();
		myonclick();
		getbusinessservicelist();
		getbusinessstafflist();
		getbusinesss();
		if(!MyApplication.Status.equals("client")){
			item1.setVisibility(View.GONE);
			item2.setVisibility(View.GONE);
			t1.setVisibility(View.GONE);
			t2.setVisibility(View.GONE);
			list_busness1.setVisibility(View.GONE);
			list_busness2.setVisibility(View.GONE);
			item3.setVisibility(View.GONE);
			t3.setVisibility(View.GONE);
			t3.setTextColor(getResources().getColor(R.color.y1));
			t1.setTextColor(getResources().getColor(R.color.n1));
			t2.setTextColor(getResources().getColor(R.color.n1));

			t3.setBackgroundColor(getResources().getColor(R.color.bg1));
			t1.setBackgroundColor(getResources().getColor(R.color.bg2));
			t2.setBackgroundColor(getResources().getColor(R.color.bg2));

			item3.setVisibility(View.VISIBLE);
			item1.setVisibility(View.GONE);
			item2.setVisibility(View.GONE);
		}
	}

	private void initView() {
		t1 = (TextView) findViewById(R.id.tv_service);
		t2 = (TextView) findViewById(R.id.tv_staff);
		t3 = (TextView) findViewById(R.id.tv_store_detail);
		item1 = (LinearLayout) findViewById(R.id.item1);
		item2 = (LinearLayout) findViewById(R.id.item2);
		item3 = (LinearLayout) findViewById(R.id.item3);
		store_id = getIntent().getStringExtra("store_id");
		MyApplication.store_id=store_id;
//		list_busness1 = (ListView) findViewById(R.id.list_busness1);
//		list_busness2 = (ListView) findViewById(R.id.list_busness2);
		adapter1 = new BusinessServiceAdapter(this, list_service);
		adapter2 = new BusinessStaffAdapter(this, list_staff);
		list_busness1.setAdapter(adapter1);
		list_busness2.setAdapter(adapter2);
		phone = (ImageView) findViewById(R.id.photo);
		business_houre = (TextView) findViewById(R.id.business_houre);
		remark = (TextView) findViewById(R.id.remark);
		address = (TextView) findViewById(R.id.address_name);
		photo = (TextView) findViewById(R.id.photo);
		service = (TextView) findViewById(R.id.service);
		name = (TextView) findViewById(R.id.name);
		title = (TextView) findViewById(R.id.title);
		
		MyApplication.list_business_service.clear();
		MyApplication.list_business_staff.clear();
	}

	private void getbusinessservicelist() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservicebystore" };
		String[] name1 = new String[] { "store_id", store_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private void getbusinessstafflist() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffbystore" };
		String[] name1 = new String[] { "store_id", store_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private void getbusinesss() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstore" };
		String[] name1 = new String[] { "store_id", store_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mServiceListObject = JSON.parseObject(v,
						ServiceListObject.class);
				list_service = mServiceListObject.response;
				if (mServiceListObject.meta.getMsg().equals("OK")) {
					if (list_service.size() > 0) {
						adapter1 = new BusinessServiceAdapter(
								WorkStoreActivity.this, list_service);
						list_busness1.setAdapter(adapter1);
					} else {
						Toast.makeText(WorkStoreActivity.this, "无服务",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(WorkStoreActivity.this, "无服务",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(WorkStoreActivity.this, "无服务", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list_staff = mStaffListObject.response;
				if (mStaffListObject.meta.getMsg().equals("OK")) {
					if (list_staff.size() > 0) {
						adapter2 = new BusinessStaffAdapter(
								WorkStoreActivity.this, list_staff);
						list_busness2.setAdapter(adapter2);
					} else {
						Toast.makeText(WorkStoreActivity.this, "无员工",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(WorkStoreActivity.this, "无员工",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(WorkStoreActivity.this, "无员工", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list_store = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					if (list_store.size() > 0) {
						mStoreObject = list_store.get(0);
						MyApplication.this_store=mStoreObject;
						// phone;
						business_houre.setText(mStoreObject.business_hours);
						remark.setText(mStoreObject.remark);
						name.setText(mStoreObject.name);
						title.setText(mStoreObject.name);
						address.setText("地址："+mStoreObject.address);
						photo.setText("电话："+mStoreObject.storephoto);
						service.setText(mStoreObject.service);
					} else {
						Toast.makeText(WorkStoreActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(WorkStoreActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(WorkStoreActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	private void myonclick() {
		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t1.setTextColor(getResources().getColor(R.color.y1));
				t2.setTextColor(getResources().getColor(R.color.n1));
				t3.setTextColor(getResources().getColor(R.color.n1));

				t1.setBackgroundColor(getResources().getColor(R.color.bg1));
				t2.setBackgroundColor(getResources().getColor(R.color.bg2));
				t3.setBackgroundColor(getResources().getColor(R.color.bg2));

				item1.setVisibility(View.VISIBLE);
				item2.setVisibility(View.GONE);
				item3.setVisibility(View.GONE);
			}
		});

		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t2.setTextColor(getResources().getColor(R.color.y1));
				t1.setTextColor(getResources().getColor(R.color.n1));
				t3.setTextColor(getResources().getColor(R.color.n1));

				t2.setBackgroundColor(getResources().getColor(R.color.bg1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg2));
				t3.setBackgroundColor(getResources().getColor(R.color.bg2));

				item2.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
				item3.setVisibility(View.GONE);
			}
		});

		t3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t3.setTextColor(getResources().getColor(R.color.y1));
				t1.setTextColor(getResources().getColor(R.color.n1));
				t2.setTextColor(getResources().getColor(R.color.n1));

				t3.setBackgroundColor(getResources().getColor(R.color.bg1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg2));
				t2.setBackgroundColor(getResources().getColor(R.color.bg2));

				item3.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
				item2.setVisibility(View.GONE);
			}
		});
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		btn_service_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(MyApplication.list_business_service.size()==0){
					Toast.makeText(WorkStoreActivity.this, "请选择服务！",Toast.LENGTH_LONG).show();
				}else if(MyApplication.list_business_staff.size()==0){
					Toast.makeText(WorkStoreActivity.this, "请选择员工！",Toast.LENGTH_LONG).show();
				}else{
					startActivity(new Intent(WorkStoreActivity.this,ChooseDatetimeActivity.class));
				}
			}
		});
		btn_staff_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(MyApplication.list_business_service.size()==0){
					Toast.makeText(WorkStoreActivity.this, "请选择服务！",Toast.LENGTH_LONG).show();
				}else if(MyApplication.list_business_staff.size()==0){
					Toast.makeText(WorkStoreActivity.this, "请选择员工！",Toast.LENGTH_LONG).show();
				}else{
					startActivity(new Intent(WorkStoreActivity.this,ChooseDatetimeActivity.class));
				}
			}
		});
	}

}
