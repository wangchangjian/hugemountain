package com.aozhi.hugemountain.activity.ConsumerActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.OrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.view.AlreadyFinishedOrderView;
import com.aozhi.hugemountain.view.AlreadyPaidOrderView;
import com.aozhi.hugemountain.view.OrderView;
import com.aozhi.hugemountain.view.ProcessingOrderView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ConsumerOrderActivity extends Activity implements OnClickListener {
    private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
    private TextView tv_order1, tv_order2, tv_order3;
    private Double sum;
    private ProgressDialog progressDialog = null;
    private ViewPager vp_order;
    private List<OrderView> vp_item_views;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_clientorder);
        initView();
        initListener();
    }

    private void initView() {
        vp_order = (ViewPager) findViewById(R.id.vp_order);

        tv_order1 = (TextView) findViewById(R.id.tv_today_order);
        tv_order2 = (TextView) findViewById(R.id.tv_thismonth_order);
        tv_order3 = (TextView) findViewById(R.id.tv_order3);

        vp_item_views = new ArrayList<OrderView>();
        vp_item_views.add(new ProcessingOrderView(this));
        vp_item_views.add(new AlreadyPaidOrderView(this));
        vp_item_views.add(new AlreadyFinishedOrderView(this));
        vp_order.setAdapter(new ViewPagerAdapter(vp_item_views));
        vp_order.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                refshOrderState(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        refshOrderState(0);
    }

    private boolean needGetData1 = true;
    private boolean needGetData2 = true;
    private boolean needGetData0 = true;
    private static final int LOADING_DELAY_TIME = 30000;

    private void refshOrderState(int position) {
        switch (position) {
            case 0:
                if (needGetData0) {
                    TimerTask task0 = new TimerTask() {
                        @Override
                        public void run() {
                            needGetData0 = true;
                        }
                    };
                    timer.schedule(task0, LOADING_DELAY_TIME);
                    vp_item_views.get(0).jumpProgressDialog.showDialog();
                    needGetData0 = false;
                    vp_item_views.get(0).getData("10");
                }
                tv_order1.setTextColor(Color.parseColor("#18B1B1"));
                tv_order2.setTextColor(Color.parseColor("#000000"));
                tv_order3.setTextColor(Color.parseColor("#000000"));
                vp_order.setCurrentItem(0);
                break;
            case 1:
                if (needGetData1) {
                    TimerTask task1 = new TimerTask() {
                        @Override
                        public void run() {
                            needGetData1 = true;
                        }
                    };
                    timer.schedule(task1, LOADING_DELAY_TIME);
                    vp_item_views.get(0).jumpProgressDialog.showDialog();
                    needGetData1 = false;
                    vp_item_views.get(1).getData("20");
                }
                vp_order.setCurrentItem(1);
                tv_order1.setTextColor(Color.parseColor("#000000"));
                tv_order2.setTextColor(Color.parseColor("#18B1B1"));
                tv_order3.setTextColor(Color.parseColor("#000000"));
                break;
            case 2:
                if (needGetData2) {
                    TimerTask task2 = new TimerTask() {
                        @Override
                        public void run() {
                            needGetData2 = true;
                        }
                    };
                    timer.schedule(task2, LOADING_DELAY_TIME);
                    vp_item_views.get(0).jumpProgressDialog.showDialog();
                    needGetData2 = false;
                    vp_item_views.get(2).getData("30");
                }
                vp_order.setCurrentItem(2);
                tv_order1.setTextColor(Color.parseColor("#000000"));
                tv_order2.setTextColor(Color.parseColor("#000000"));
                tv_order3.setTextColor(Color.parseColor("#18B1B1"));
                break;
            default:
                break;
        }
    }

    private void initListener() {
        tv_order1.setOnClickListener(this);
        tv_order2.setOnClickListener(this);
        tv_order3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_today_order:
                tv_order1.setTextColor(Color.parseColor("#18B1B1"));
                tv_order2.setTextColor(Color.parseColor("#000000"));
                tv_order3.setTextColor(Color.parseColor("#000000"));
                vp_order.setCurrentItem(0);
                break;
            case R.id.tv_thismonth_order:
                vp_order.setCurrentItem(1);
                tv_order1.setTextColor(Color.parseColor("#000000"));
                tv_order2.setTextColor(Color.parseColor("#18B1B1"));
                tv_order3.setTextColor(Color.parseColor("#000000"));
                break;
            case R.id.tv_order3:
                vp_order.setCurrentItem(2);
                tv_order1.setTextColor(Color.parseColor("#000000"));
                tv_order2.setTextColor(Color.parseColor("#000000"));
                tv_order3.setTextColor(Color.parseColor("#18B1B1"));
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case 156:
                tv_order1.setTextColor(Color.parseColor("#18B1B1"));
                tv_order2.setTextColor(Color.parseColor("#000000"));
                tv_order3.setTextColor(Color.parseColor("#000000"));
                break;
            default:
                break;
        }
    }

    private static Boolean isQuit = false;
    private Timer timer = new Timer();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isQuit == false) {
                isQuit = true;
                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
                TimerTask task = null;
                task = new TimerTask() {
                    @Override
                    public void run() {
                        isQuit = false;
                    }
                };
                timer.schedule(task, 2000);
            } else {
                finish();
                MyApplication.IS_LOGINS = false;
                System.exit(0);
            }
        }
        return false;
    }

    private class ViewPagerAdapter extends PagerAdapter {

        List<OrderView> viewLists;

        public ViewPagerAdapter(List<OrderView> lists) {
            viewLists = lists;
        }

        @Override
        public int getCount() {                                                                 //获得size
            return viewLists.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(View view, int position, Object object)                       //销毁Item
        {
            ((ViewPager) view).removeView(viewLists.get(position));
        }

        @Override
        public Object instantiateItem(View view, int position)                                //实例化Item
        {
            ((ViewPager) view).addView(viewLists.get(position), 0);
            return viewLists.get(position);
        }
    }
}
