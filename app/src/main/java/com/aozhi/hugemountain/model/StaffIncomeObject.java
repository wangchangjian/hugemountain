package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class StaffIncomeObject implements Serializable{
	public String no;
	public String staff_name;
	public String money;
	public String id;
	public String create_time;
	public String staff_id;
	public String del_flag;
	public String remarks;
	public String code_id;
	public String name;
}
