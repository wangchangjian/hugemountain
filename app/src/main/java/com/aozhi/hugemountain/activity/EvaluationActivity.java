package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class EvaluationActivity extends Activity implements OnClickListener{
	
	private Button back;
	private TextView tv_quality1,tv_quality2,tv_quality3,tv_quality4,tv_quality5;
	private TextView tv_service1,tv_service2,tv_service3,tv_service4,tv_service5;
	private TextView tv_distribution1,tv_distribution2,tv_distribution3,tv_distribution4,tv_distribution5;
	private TextView service_name,staff_code;
	private EditText et_content;
	private LinearLayout layout_quality,layout_service,layout_distribution;
	private String quality="0",service="0",distribution="0";
	private TextView tv_submit;
	private OrderFormObject orderobject;
	private OrderListObject mOrderListObject;
	private ArrayList<OrderFormObject> list_order = new ArrayList<OrderFormObject>();
	
	private String code_id,serviecs;
	public static boolean eva = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pingjia);
		initView();
		initListener();
		
	}
	private void initListener() {
		orderobject=(OrderFormObject) getIntent().getSerializableExtra("OrderFormObject");
		
		back.setOnClickListener(this);
		tv_quality1.setOnClickListener(this);
		tv_quality2.setOnClickListener(this);
		tv_quality3.setOnClickListener(this);
		tv_quality4.setOnClickListener(this);
		tv_quality5.setOnClickListener(this);
		tv_service1.setOnClickListener(this);
		tv_service2.setOnClickListener(this);
		tv_service3.setOnClickListener(this);
		tv_service4.setOnClickListener(this);
		tv_service5.setOnClickListener(this);
		tv_distribution1.setOnClickListener(this);
		tv_distribution2.setOnClickListener(this);
		tv_distribution3.setOnClickListener(this);
		tv_distribution4.setOnClickListener(this);
		tv_distribution5.setOnClickListener(this);
		tv_submit.setOnClickListener(this);
	}
	
	private void initView() {
		back=(Button) findViewById(R.id.back);
		service_name=(TextView) findViewById(R.id.service_name);
		staff_code=(TextView) findViewById(R.id.staff_code);
		tv_quality1=(TextView) findViewById(R.id.tv_quality1);
		tv_quality2=(TextView) findViewById(R.id.tv_quality2);
		tv_quality3=(TextView) findViewById(R.id.tv_quality3);
		tv_quality4=(TextView) findViewById(R.id.tv_quality4);
		tv_quality5=(TextView) findViewById(R.id.tv_quality5);
		tv_service1=(TextView) findViewById(R.id.tv_service1);
		tv_service2=(TextView) findViewById(R.id.tv_service2);
		tv_service3=(TextView) findViewById(R.id.tv_service3);
		tv_service4=(TextView) findViewById(R.id.tv_service4);
		tv_service5=(TextView) findViewById(R.id.tv_service5);
		tv_distribution1=(TextView) findViewById(R.id.tv_distribution1);
		tv_distribution2=(TextView) findViewById(R.id.tv_distribution2);
		tv_distribution3=(TextView) findViewById(R.id.tv_distribution3);
		tv_distribution4=(TextView) findViewById(R.id.tv_distribution4);
		tv_distribution5=(TextView) findViewById(R.id.tv_distribution5);
		et_content=(EditText) findViewById(R.id.et_content);
		layout_quality=(LinearLayout) findViewById(R.id.layout_quality);
		layout_service=(LinearLayout) findViewById(R.id.layout_service);
		layout_distribution=(LinearLayout) findViewById(R.id.layout_distribution);
		tv_submit=(TextView) findViewById(R.id.tv_submit);
		
		code_id=getIntent().getStringExtra("code_id");
		serviecs=getIntent().getStringExtra("serviecs");
		service_name.setText(serviecs);
		staff_code.setText(code_id);
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.back:
			finish();
			break;
		case R.id.tv_quality1:
			quality="1";
			layout_quality.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
			break;
		case R.id.tv_quality2:
			quality="2";
			layout_quality.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
			break;
		case R.id.tv_quality3:
			quality="3";
			layout_quality.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
			break;
		case R.id.tv_quality4:
			quality="4";
			layout_quality.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
			break;
		case R.id.tv_quality5:
			quality="5";
			layout_quality.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
			break;
		case R.id.tv_service1:
			service="1";
			layout_service.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
			break;
		case R.id.tv_service2:
			service="2";
			layout_service.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
			break;
		case R.id.tv_service3:
			service="3";
			layout_service.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
			break;
		case R.id.tv_service4:
			service="4";
			layout_service.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
			break;
		case R.id.tv_service5:
			service="5";
			layout_service.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
			break;
		case R.id.tv_distribution1:
			distribution="1";
			layout_distribution.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
			break;
		case R.id.tv_distribution2:
			distribution="2";
			layout_distribution.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
			break;
		case R.id.tv_distribution3:
			distribution="3";
			layout_distribution.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
			break;
		case R.id.tv_distribution4:
			distribution="4";
			layout_distribution.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
			break;
		case R.id.tv_distribution5:
			distribution="5";
			layout_distribution.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
			break;
		case R.id.tv_submit:
			setColler();
			break;
		default:
			break;
		}
		
	}
	
	private void setColler() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setCollectForClient" };
		String[] name1 = new String[] { "store_id", orderobject.store_id };
		String[] name2 =new String[] {"staff_id",orderobject.staff_id};
		String[] name3 =new String[] {"client_id",orderobject.client_id};
		String[] name4 =new String[] {"of_id",orderobject.id};
		String[] orderid =new String[] {"order_id",orderobject.order_id};
		String[] name5 =new String[] {"quality",quality};
		String[] name6 =new String[] {"service",service};
		String[] name7 =new String[] {"distribution",distribution};
		String[] name8 =new String[] {"contents",et_content.getText().toString()};
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		params1.add(name3);
		params1.add(name4);
		params1.add(name5);
		params1.add(name6);
		params1.add(name7);
		params1.add(name8);
		params1.add(orderid);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(getApplicationContext(), "评价成功,感谢你的评价", Toast.LENGTH_SHORT).show();
					eva = true;
					finish();
				} 
			} else {
				Toast.makeText(getApplicationContext(), "评价失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	
	
}
