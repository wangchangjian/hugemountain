package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class LoginListObject {
	public Meta meta;
	public ArrayList<LoginBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<LoginBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<LoginBean> response) {
		this.response = response;
	}
}
