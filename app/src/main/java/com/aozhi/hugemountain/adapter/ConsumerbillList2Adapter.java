package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.model.StaffIncomeObject;

import android.R.integer;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ConsumerbillList2Adapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffIncomeObject> invite_list = new ArrayList<StaffIncomeObject>();

	public ConsumerbillList2Adapter(Context ctx, ArrayList<StaffIncomeObject> list) {
		mContext = ctx;
		invite_list = list;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return invite_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return invite_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_staffwith, null);
			mHolder.create_time = (TextView) convertView
					.findViewById(R.id.create_time);
			mHolder.money = (TextView) convertView.findViewById(R.id.money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.create_time.setText(invite_list.get(position).create_time);
		mHolder.money.setText("￥"+ Integer.parseInt(invite_list.get(position).money));
		return convertView;
	}

	class Holder {
		TextView create_time, money;
	}

}
