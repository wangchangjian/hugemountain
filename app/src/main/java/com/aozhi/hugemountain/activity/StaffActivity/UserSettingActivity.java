package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.MyCollectActivity;
import com.aozhi.hugemountain.activity.PublicActivity.LoginsActivity;
import com.aozhi.hugemountain.activity.ShowActivity;
import com.aozhi.hugemountain.activity.StaffMoneyActivity;
import com.aozhi.hugemountain.activity.StaffUserEditActivity;
import com.aozhi.hugemountain.activity.WithdrawalsActivity;
import com.aozhi.hugemountain.activity.WithdrawalsStaff;
import com.aozhi.hugemountain.adapter.ShowPhotoAdapter;
import com.aozhi.hugemountain.adapter.ShowVideoAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.list.NoScrollGridView;
import com.aozhi.hugemountain.model.StaffPhotoListObject;
import com.aozhi.hugemountain.model.StaffPhotoObject;
import com.aozhi.hugemountain.model.StaffVideoListObject;
import com.aozhi.hugemountain.model.StaffVideoObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.view.ApplicationTopView;
import com.aozhi.hugemountain.view.ConfirmDialog;
import com.aozhi.hugemountain.view.OnTopClickListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class UserSettingActivity extends Activity implements OnClickListener {

	private TextView t1;
	private TextView t2;
	private ImageView user_img;
	private ProgressDialog progressDialog = null;
	private NoScrollGridView grid_view1;
	private ShowPhotoAdapter mShowphotoAdapter;
	private StaffPhotoObject mStaffPhotoObject;
	private StaffPhotoListObject mStaffPhotoListObject;
	private ArrayList<StaffPhotoObject> list_photo = new ArrayList<StaffPhotoObject>();
	private NoScrollGridView grid_view2;
	private ShowVideoAdapter mShowVideoAdapter;
	private StaffVideoObject mStaffVideoObject;
	private StaffVideoListObject mStaffVideoListObject;
	private ArrayList<StaffVideoObject> list_video = new ArrayList<StaffVideoObject>();
	private LinearLayout user_info, user_tixian, user_zhanghu, user_huizong,
			user_setting, user_show, user_time, collect;
	private TextView login_id,tv_bianhao,tv_yue;
    private LinearLayout layout_shouru;
	private ApplicationTopView at_app_top;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_usersetting);
		getstaffbalance();
		initView();
		initListener();
	}

	private void initListener() {
		// t1.setOnClickListener(this);
		// t2.setOnClickListener(this);
		user_info.setOnClickListener(this);
		user_tixian.setOnClickListener(this);
		user_zhanghu.setOnClickListener(this);
		user_huizong.setOnClickListener(this);
		user_show.setOnClickListener(this);
		user_setting.setOnClickListener(this);
		user_time.setOnClickListener(this);
		collect.setOnClickListener(this);
		layout_shouru.setOnClickListener(this);
		at_app_top.setOnTopClickListener(new OnTopClickListener() {
			@Override
			public void onLeftbuttonClick() {
			}
			@Override
			public void onRightbuttonClick() {
				showQuitDialog();
			}
		});


	}

	private void initView() {
		// grid_view1 = (NoScrollGridView) findViewById(R.id.grid_view1);
		// grid_view2 = (NoScrollGridView) findViewById(R.id.grid_view2);
		mShowphotoAdapter = new ShowPhotoAdapter(this, list_photo);
		// grid_view1.setAdapter(mShowphotoAdapter);
		// grid_view2.setAdapter(mShowVideoAdapter);
		at_app_top= (ApplicationTopView) findViewById(R.id.at_app_top);
		layout_shouru=(LinearLayout)findViewById(R.id.layout_shouru);
		user_img = (ImageView) findViewById(R.id.user_img);
		login_id = (TextView) findViewById(R.id.login_id);
		tv_bianhao = (TextView) findViewById(R.id.tv_bianhao);
		tv_yue = (TextView) findViewById(R.id.tv_yue);
		user_info = (LinearLayout) findViewById(R.id.user_info);
		user_tixian = (LinearLayout) findViewById(R.id.user_tixian);
		user_zhanghu = (LinearLayout) findViewById(R.id.user_zhanghu);
		user_huizong = (LinearLayout) findViewById(R.id.user_huizong);
		user_setting = (LinearLayout) findViewById(R.id.user_setting);
		user_show = (LinearLayout) findViewById(R.id.user_show);
		user_time = (LinearLayout) findViewById(R.id.user_time);
		collect=(LinearLayout) findViewById(R.id.collect);
		login_id.setText("账号："+ MyApplication.Staffuser.staffphoto);
		tv_bianhao.setText("编号："+MyApplication.Staffuser.code_id);
		tv_yue.setText("余额："+MyApplication.Staffuser.balance+"元");
		SetAvatar();
		// GetPhotoList();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.user_info:
//			startActivity(new Intent(UserSettingActivity.this,
//					StaffUserInfoActivity.class));
			startActivity(new Intent(UserSettingActivity.this,
					StaffUserEditActivity.class));
			break;
		case R.id.user_tixian:
			startActivity(new Intent(UserSettingActivity.this,
					WithdrawalsActivity.class));
			break;
		case R.id.user_zhanghu:
			Intent mintent = new Intent(UserSettingActivity.this,
					StaffAccountActivity.class);
			mintent.putExtra("stats", "staff");
			startActivity(mintent);
			break;
		case R.id.user_huizong:
			Intent mIntent = new Intent(UserSettingActivity.this,
					WithdrawalsStaff.class);
			startActivity(mIntent);
			break;
		case R.id.user_show:
			startActivity(new Intent(UserSettingActivity.this,
					ShowActivity.class));
			break;
		case R.id.user_setting:
			startActivity(new Intent(UserSettingActivity.this, SettingActivity.class));
			break;
		case R.id.user_time:
			startActivity(new Intent(UserSettingActivity.this,
					StaffStoreListActivity.class));
			break;
		case R.id.collect:
			Intent intent = new Intent(UserSettingActivity.this,
					MyCollectActivity.class);
			intent.putExtra("stats", "staff");
			startActivity(intent);
			break;
		case R.id.layout_shouru:{
			Intent mIntent1=new Intent(UserSettingActivity.this,StaffMoneyActivity.class);
			startActivity(mIntent1);
		}
		default:
			break;
		}
	}
	protected void showQuitDialog() {
		final ConfirmDialog confirmDialog=new ConfirmDialog(this,"确实要退出吗");
		confirmDialog.confirmListener(new ConfirmDialog.ConfirmListener() {
			@Override
			public void confirm() {
				confirmDialog.dismiss();
				SharedPreferences mySharedPreferences = getSharedPreferences("staff_user", Activity.MODE_PRIVATE);
				SharedPreferences.Editor editor = mySharedPreferences.edit();
				editor.putString("name", "");
				editor.putString("pwd", "");
				editor.putBoolean("ok", false);
				editor.commit();
				JPushInterface.setAlias(getApplicationContext(), "", mTagsCallback);
				Intent mIntent = new Intent(UserSettingActivity.this,LoginsActivity.class);
				startActivity(mIntent);
				finish();
			}
		});
		confirmDialog.showConfirmAndCancel();
	}
	
	private final TagAliasCallback mTagsCallback = new TagAliasCallback() {

        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs ;
            switch (code) {
            case 0:
                logs = "Set tag and alias success";
//                Log.i(TAG, logs);
                break;
                
            case 6002:
                logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
//                Log.i(TAG, logs);
//                if (ExampleUtil.isConnected(getApplicationContext())) {
//                	mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
//                } else {
////                	Log.i(TAG, "No network");
//                }
                break;
            
            default:
                logs = "Failed with errorCode = " + code;
//                Log.e(TAG, logs);
            }
            
//            ExampleUtil.showToast(logs, getApplicationContext());
        }
        
    };
    
	private void GetPhotoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getstaffphoto" };
			String[] name = new String[] { "staff_id",
					MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener1);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffPhotoListObject = JSON.parseObject(result,
						StaffPhotoListObject.class);
				list_photo = mStaffPhotoListObject.response;
				if (mStaffPhotoListObject.meta.getMsg().equals("OK")) {
					if (list_photo.size() > 0) {
						mShowphotoAdapter = new ShowPhotoAdapter(
								UserSettingActivity.this, list_photo);
						grid_view1.setAdapter(mShowphotoAdapter);
					} else {
						Toast.makeText(UserSettingActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(UserSettingActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(UserSettingActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void GetVideoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getstaffvideo" };
			String[] name = new String[] { "staff_id",
					MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener2);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffVideoListObject = JSON.parseObject(result,
						StaffVideoListObject.class);
				list_video = mStaffVideoListObject.response;
				if (mStaffVideoListObject.meta.getMsg().equals("OK")) {
					if (list_video.size() > 0) {
						mShowVideoAdapter = new ShowVideoAdapter(
								UserSettingActivity.this, list_video);
						grid_view2.setAdapter(mShowVideoAdapter);
					} else {
						Toast.makeText(UserSettingActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(UserSettingActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(UserSettingActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void SetAvatar() {
		if (MyApplication.Staffuser.avatar.equals("")|| MyApplication.Staffuser.avatar.equals("null")) {

		} else {
			MyApplication.downloadImage.addTasks(
					MyApplication.Staffuser.avatar, user_img,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");

								// Bitmap bmBitmap = Utils
								// .toRoundBitmap(imageBitmap);
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								// Bitmap bmBitmap = Utils
								// .toRoundBitmap(imageBitmap);
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
	}
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                System.exit(0);
	            }
	        }
	        return false;
	}
	
	private void getstaffbalance() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffbalance" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffPhotoListObject = JSON.parseObject(v,
						StaffPhotoListObject.class);
				list_photo = mStaffPhotoListObject.response;
					if (list_photo.size() > 0) {
						MyApplication.Staffuser.balance=list_photo.get(0).balance;
						
					}
				}
		}
	};
}
