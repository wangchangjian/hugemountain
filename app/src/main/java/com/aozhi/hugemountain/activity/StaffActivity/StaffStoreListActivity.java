package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.SearchStoreActivity;
import com.aozhi.hugemountain.adapter.StaffStoreListAdapter;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StaffStoreListActivity extends Activity implements OnClickListener {
	private TextView staff_search;
	private ListView list_store;
	private ArrayList<StoreObject> list = new ArrayList<StoreObject>();
	private StaffStoreListAdapter mStoreListAdapter;
	private StoreListObject mStoreListObject;
	private JumpProgressDialog progressDialog ;
	private LocationClient mLocationClient;
	private TextView startwork,endwork,rest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_office);
		initView();
		initListnner();
		initLoc();
	}

	private void initView() {
		progressDialog = new JumpProgressDialog(this);
		staff_search = (TextView) findViewById(R.id.search);
		startwork=(TextView) findViewById(R.id.startwork);
		endwork=(TextView) findViewById(R.id.endwork);
		rest=(TextView) findViewById(R.id.rest);
		list_store = (ListView) findViewById(R.id.list_store);
		mStoreListAdapter = new StaffStoreListAdapter(StaffStoreListActivity.this, list);
		list_store.setAdapter(mStoreListAdapter);
		getorder();
	}

	private void initListnner() {
		staff_search.setOnClickListener(this);
		startwork.setOnClickListener(this);
		endwork.setOnClickListener(this);
		rest.setOnClickListener(this);
		list_store.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Intent mIntent = new Intent(StaffStoreListActivity.this,StaffStoreDetailsActivity.class);
				mIntent.putExtra("store_id", list.get(position).id);
				startActivityForResult(mIntent, 133);
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		
		case R.id.search:
			Intent mIntent = new Intent(this, SearchStoreActivity.class);
			startActivity(mIntent);
			break;
		case R.id.startwork:
			startwork.setTextColor(Color.parseColor("#ffffff"));
			startwork.setBackgroundColor(Color.parseColor("#FF9201"));
			endwork.setTextColor(Color.parseColor("#000000"));
			endwork.setBackgroundColor(Color.parseColor("#ffffff"));
			rest.setTextColor(Color.parseColor("#000000"));
			rest.setBackgroundColor(Color.parseColor("#ffffff"));
			setWorkStatus("1");
			break;
		case R.id.endwork:
			startwork.setTextColor(Color.parseColor("#000000"));
			startwork.setBackgroundColor(Color.parseColor("#ffffff"));
			endwork.setTextColor(Color.parseColor("#ffffff"));
			endwork.setBackgroundColor(Color.parseColor("#FF9201"));
			rest.setTextColor(Color.parseColor("#000000"));
			rest.setBackgroundColor(Color.parseColor("#ffffff"));
			setWorkStatus("2");
			break;
		case R.id.rest:
			startwork.setTextColor(Color.parseColor("#000000"));
			startwork.setBackgroundColor(Color.parseColor("#ffffff"));
			endwork.setTextColor(Color.parseColor("#000000"));
			endwork.setBackgroundColor(Color.parseColor("#ffffff"));
			rest.setTextColor(Color.parseColor("#ffffff"));
			rest.setBackgroundColor(Color.parseColor("#FF9201"));
			setWorkStatus("3");
			break;
		default:
			break;
		}
	}

	public  void getorder() {

		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "getstaffoffice" };
			String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id};
			params2.add(funParam2);
			params2.add(name1);
			progressDialog.showDialog();
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						type_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dimissDialog();
			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						mStoreListAdapter = new StaffStoreListAdapter(
								StaffStoreListActivity.this,list);
						list_store.setAdapter(mStoreListAdapter);
						if(list.get(0).workstatus.equals("0")){
							startwork.setTextColor(Color.parseColor("#000000"));
							startwork.setBackgroundColor(Color.parseColor("#ffffff"));
							endwork.setTextColor(Color.parseColor("#000000"));
							endwork.setBackgroundColor(Color.parseColor("#ffffff"));
							rest.setTextColor(Color.parseColor("#000000"));
							rest.setBackgroundColor(Color.parseColor("#ffffff"));
						}else if(list.get(0).workstatus.equals("1")){
							startwork.setTextColor(Color.parseColor("#ffffff"));
							startwork.setBackgroundColor(Color.parseColor("#FF9201"));
							endwork.setTextColor(Color.parseColor("#000000"));
							endwork.setBackgroundColor(Color.parseColor("#ffffff"));
							rest.setTextColor(Color.parseColor("#000000"));
							rest.setBackgroundColor(Color.parseColor("#ffffff"));
						}else if(list.get(0).workstatus.equals("2")){
							startwork.setTextColor(Color.parseColor("#000000"));
							startwork.setBackgroundColor(Color.parseColor("#ffffff"));
							endwork.setTextColor(Color.parseColor("#ffffff"));
							endwork.setBackgroundColor(Color.parseColor("#FF9201"));
							rest.setTextColor(Color.parseColor("#000000"));
							rest.setBackgroundColor(Color.parseColor("#ffffff"));
						}else if(list.get(0).workstatus.equals("3")){
							startwork.setTextColor(Color.parseColor("#000000"));
							startwork.setBackgroundColor(Color.parseColor("#ffffff"));
							endwork.setTextColor(Color.parseColor("#000000"));
							endwork.setBackgroundColor(Color.parseColor("#ffffff"));
							rest.setTextColor(Color.parseColor("#ffffff"));
							rest.setBackgroundColor(Color.parseColor("#FF9201"));
						}
					}

				} else {
					Toast.makeText(StaffStoreListActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(StaffStoreListActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 133:
			getorder();
			break;

		default:
			break;
		}
		
	};
	
	
	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		int span = 1000;
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	private void initLoc() {
		mLocationClient = ((MyApplication) getApplication()).mLocationClient;
//		tv_city = (TextView) findViewById(R.id.tv_city);
//		((MyApplication) getApplication()).mLocationResult = tv_city;
		InitLocation();
		mLocationClient.start();
//		System.out.println("获取到的定位信息是：" + tv_city.getText().toString());
	}
	
	public  void setWorkStatus(String status) {

		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "setWorkstatus" };
			String[] name1 = new String[] { "staff_id",MyApplication.Staffuser.id};
			String[] name2=new String[]{"status",status};
			params2.add(funParam2);
			params2.add(name1);
			params2.add(name2);
			progressDialog .showDialog();
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						setwork_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}
	}

	private CallbackListener setwork_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dimissDialog();
			}
			if (v.equals("fail")) {
				Toast.makeText(StaffStoreListActivity.this, "状态更新失败", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                System.exit(0);
	            }
	        }
	        return false;
	}
}
