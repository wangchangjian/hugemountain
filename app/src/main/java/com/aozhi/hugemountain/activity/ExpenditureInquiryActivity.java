package com.aozhi.hugemountain.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StoreListsAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

//import com.aozhi.hugemountain.adapter.ShopCenterAdapter;
//import com.aozhi.hugemountain.model.ShopCenterObject;

import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ExpenditureInquiryActivity extends Activity {
	Button btn_back;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	ConsumptionObject mConsumptionObject;
	ConsumptionListObject mConsumptionListObject;
	private StoreListsAdapter adapter;
	double Day_moneyrevenue = 0;
	double Month_moneyrevenue = 0;
	private ListView list_shopcenter;
	private TextView dayrevenue, monthrevenue,tv_show,et_peoper,tv_cash,tv_yue,tv_wangluo;
	private ProgressDialog progressDialog = null;
	private Double money;
	private Spinner sp_type,sp_orderby;
	private String leixing="'2','3'",date="",date2="";
	private String[] types = new String[] { "全部", "现金", "网络" };
	private String[] order = new String[] { "按时间升序", "按时间降序", "按金额升序","按金额降序" };
	
	private LinearLayout li_query;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expenditureinquiry);
		initView();
		myclick();
		getlist(date);
	}

	private void myclick() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		et_peoper.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showMonPicker();
			}
		});
		li_query.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if(et_peoper.getText().toString().equals("请选择时间(年月)")||et_peoper.getText().toString().equals("")){
					Toast.makeText(ExpenditureInquiryActivity.this, "请选择时间(年月)", Toast.LENGTH_LONG).show();
				}else{
				String s = et_peoper.getText().toString().replaceAll("-", "");
				s = s.substring(2,6);
				getlist(s);
				}
			}
		});
	}

	private void getExpend(String status) {
		leixing=status;
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getconsumptionstaff" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status", status }; // 收支状态：0为现金收入，1为网络收入；2为现金支出，3为网络支出
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list=mConsumptionListObject.response;
				if(list.size()>0){
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
//					adapter = new ShopCenterAdapter(
//							ExpenditureInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				}else{
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				}
				} else {
					Toast.makeText(ExpenditureInquiryActivity.this, "数据获取失败",
							Toast.LENGTH_SHORT).show();
				}
		}
	};


	public Date SimpleDateFormat(String str) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public int get_year(Date date) {
		return date.getYear() + 1900;
	}

	public int get_month(Date date) {
		return date.getMonth() + 1;
	}

	public int get_day(Date date) {
		return date.getDate();
	}

	private void initView() {
		tv_show=(TextView) findViewById(R.id.tv_show);
		list_shopcenter = (ListView) findViewById(R.id.list_shopcenter);
		adapter = new StoreListsAdapter(this, list);
		list_shopcenter.setAdapter(adapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		dayrevenue = (TextView) findViewById(R.id.dayrevenue);
		monthrevenue = (TextView) findViewById(R.id.monthrevenue);
		et_peoper = (TextView) findViewById(R.id.et_peoper);
		tv_cash = (TextView) findViewById(R.id.tv_cash);
		tv_yue = (TextView) findViewById(R.id.tv_yue);
		tv_wangluo = (TextView) findViewById(R.id.tv_wangluo);
		li_query=  (LinearLayout) findViewById(R.id.li_query);
		
		//计算当前年份和月份
		SimpleDateFormat df = new SimpleDateFormat("yyMM");//设置日期格式
		date = df.format(new Date());
		
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM");//设置日期格式
		date2 = df2.format(new Date());
		et_peoper.setText(date2);
		
		list_shopcenter.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent mIntent=new Intent(ExpenditureInquiryActivity.this,StaffOrdersActivity.class);
				mIntent.putExtra("staff_id", list.get(arg2).id);
				mIntent.putExtra("date", date);
				mIntent.putExtra("title", list.get(arg2).code_id);
				mIntent.putExtra("status", "1");
				
				startActivity(mIntent);
				}
		});
	}

	private void getDayRevenue() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getDayIncome " };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2=  new String[] {"status","1"};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typeday_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
					if (list.size()> 0) {
						if(!list.get(0).money.equals("")&&!list.get(0).money.equals(null)){
						dayrevenue.setText(list.get(0).money);
						}else{
							dayrevenue.setText("0");
						}
					}else{
						dayrevenue.setText("0");
					}
				}else{
					dayrevenue.setText("0");
//					Toast.makeText(ExpenditureInquiryActivity.this,"获取数据失败", Toast.LENGTH_LONG).show();
				}
			}
	};
	
	private void getMonthRevenue() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getMonthIncome" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2=  new String[] {"status","1"};  //收支状态：0为现金收入，1为网络收入；2为现金支出，3为网络支出
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typemonth_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typemonth_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
					if (list.size() > 0) {
						if(!list.get(0).money.equals("")){
						monthrevenue.setText(list.get(0).money);
						}else{
							monthrevenue.setText("0");
						}
					}else{
						monthrevenue.setText("0");
					}
				}else{
					monthrevenue.setText("0");
//					Toast.makeText(ExpenditureInquiryActivity.this,"获取数据失败", Toast.LENGTH_LONG).show();
				}
			}
	};
	
	private void Spinner(Spinner sp,String[] data) {
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		ArrayList<String> timelist = new ArrayList<String>();
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type,data);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		sp.setAdapter(adapter);
	}
	
	private void getRevenueOrder(String order) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreorder" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] name2 = new String[] { "status","1"}; 
		String[] name3 = new String[] {"order",order};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					typeorder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeorder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
					adapter = new StoreListsAdapter(
							ExpenditureInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				}
			}else{
				Toast.makeText(ExpenditureInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	public void getlist(String date) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoremoney" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] dates = new String[] { "date", date };
		String[] status = new String[] { "status", "1" };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(dates);
		params2.add(status);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					if(!list.get(0).m.equals("")&&list.get(0).m!=null){
						monthrevenue.setText("￥"+list.get(0).m);
						if(!list.get(0).m1.equals("")&&list.get(0).m1!=null){
							tv_cash.setText("￥"+list.get(0).m1);
						}
						if(!list.get(0).m2.equals("")&&list.get(0).m2!=null){
							tv_yue.setText("￥"+list.get(0).m2);
						}
						if(!list.get(0).m3.equals("")&&list.get(0).m3!=null){
							tv_wangluo.setText("￥"+list.get(0).m3);
						}
						getstoreorderlist();
					}else{
						monthrevenue.setText("");
						tv_cash.setText("");
						tv_yue.setText("");
						tv_wangluo.setText("");
						list.clear();
						tv_show.setVisibility(View.VISIBLE);
						list_shopcenter.setVisibility(View.GONE);
						Toast.makeText(ExpenditureInquiryActivity.this, "当前月没有数据",
								Toast.LENGTH_LONG).show();
					}
				} 
			}else{
				Toast.makeText(ExpenditureInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	//获取员工列表
	public void getstoreorderlist() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreStaffListbyz" };
		String[] name1 = new String[] { "storeid", MyApplication.Storeuser.id };
		String[] dates = new String[] { "date", date };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(dates);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getstoreorderlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getstoreorderlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
					adapter = new StoreListsAdapter(ExpenditureInquiryActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				} 
			}else{
				Toast.makeText(ExpenditureInquiryActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	/**
	 * 重写datePicker 1.只显示 年-月 2.title 只显示 年-月
	 * @author lmw
	 */
	public class MonPickerDialog extends DatePickerDialog {
		public MonPickerDialog(Context context, OnDateSetListener callBack,
				int year, int monthOfYear, int dayOfMonth) {
			super(context, callBack, year, monthOfYear, dayOfMonth);
			this.setTitle(year + "年" + (monthOfYear + 1) + "月");
			((ViewGroup) ((ViewGroup) this.getDatePicker().getChildAt(0))
					.getChildAt(0)).getChildAt(2).setVisibility(View.GONE);
		}

		@Override
		public void onDateChanged(DatePicker view, int year, int month, int day) {
			super.onDateChanged(view, year, month, day);
			this.setTitle(year + "年" + (month + 1) + "月");
		}

	}

	public void showMonPicker() {
		final Calendar localCalendar = Calendar.getInstance();
		localCalendar.setTime(strToDate("yyyy-MM", et_peoper.getText()
				.toString()));
		new MonPickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				localCalendar.set(1, year);
				localCalendar.set(2, monthOfYear);
				et_peoper.setText(clanderTodatetime(localCalendar, "yyyy-MM"));
			}
		}, localCalendar.get(1), localCalendar.get(2), localCalendar.get(5))
				.show();
	}

	// 字符串类型日期转化成date类型
	public static Date strToDate(String style, String date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	public static String dateToStr(String style, Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(date);
	}

	public static String clanderTodatetime(Calendar calendar, String style) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(calendar.getTime());
	}
}
