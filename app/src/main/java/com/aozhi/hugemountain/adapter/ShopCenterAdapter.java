package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ConsumptionObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ShopCenterAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();

	public ShopCenterAdapter(Context ctx, ArrayList<ConsumptionObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_shopcenter, null);
			mHolder.tv_money=(TextView)convertView.findViewById(R.id.tv_money);
			mHolder.tv_orderid=(TextView)convertView.findViewById(R.id.tv_orderid);
			mHolder.tv_times=(TextView)convertView.findViewById(R.id.tv_times);
			mHolder.tv_isclasses=(TextView)convertView.findViewById(R.id.tv_isclasses);
			mHolder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
//		id,create_time,money,code_id
		mHolder.tv_money.setText("￥"+list.get(position).money);
		mHolder.tv_orderid.setText("订单号："+list.get(position).order_id);
		mHolder.tv_times.setText("订单时间："+list.get(position).create_time);
		String type="";
		if(list.get(position).paytype!=null&&!list.get(position).paytype.equals("")){
		if(list.get(position).paytype.equals("0")){
			type= "余额";
		}else if(list.get(position).paytype.equals("1")){
			type= "支付宝";
		}else if(list.get(position).paytype.equals("2")){
			type= "微信";
		}else{
			type= "现金";
		}}
		mHolder.tv_isclasses.setText("支付方式："+type);
		mHolder.tv_name.setText("员工编号："+list.get(position).code_id);
//		
//		mHolder.tv_name.setText(list.get(position).name );
//		if(list.get(position).pay_status.equals("0")){
//			mHolder.tv_status.setText("现金收入");
//		}else if(list.get(position).pay_status.equals("1")){
//			mHolder.tv_status.setText("网络收入");
//		}else if(list.get(position).pay_status.equals("2")){
//			mHolder.tv_status.setText("现金支出");
//		}else if(list.get(position).pay_status.equals("3")){
//			mHolder.tv_status.setText("网络支出");
//		}
		return convertView;
	}

	class Holder {
		TextView tv_name,tv_money,tv_status,tv_orderid,tv_times,tv_isclasses;
	}
}
