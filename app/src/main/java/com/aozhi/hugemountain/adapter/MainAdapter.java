package com.aozhi.hugemountain.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.MainObject;

public class MainAdapter extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<MainObject> list = new ArrayList<MainObject>();

	public MainAdapter(Context ctx, ArrayList<MainObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_main, null);
			mHolder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
			mHolder.img_main=(ImageView)convertView.findViewById(R.id.img_main);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_name.setText(list.get(position).name);
		
		
		if (list.get(position).ico_url.equals("")
				|| list.get(position).ico_url==null ) {
			
		} else {
			MyApplication.downloadImage.addTasks(
					list.get(position).ico_url, mHolder.img_main,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_main.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_main.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
//		convertView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent mIntent = new Intent(mContext,StoreListActivity.class);
//				mIntent.putExtra("seller_id", list.get(position).id);
//				mContext.startActivity(mIntent);
//			}
//		});
		return convertView;
	}
	
	
	
	class Holder {
		TextView tv_name;
		
		ImageView img_main;
	}
}
