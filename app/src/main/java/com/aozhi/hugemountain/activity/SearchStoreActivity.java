package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.StaffStoreDetailsActivity;
import com.aozhi.hugemountain.adapter.SearchAdapter;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class SearchStoreActivity extends Activity implements OnClickListener {

	private String contents;
	private ListView list_searchstore;
	private Button sure, drop, back;
	private ProgressDialog progressDialog = null;
	private ArrayList<StoreObject> list = new ArrayList<StoreObject>();
	private SearchAdapter mSearchAdapter;
	private StoreListObject mStoreListObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_searchstore);
		initView();
		initListener();
		setView();
	}

	private void initView() {
		sure = (Button) findViewById(R.id.sure);
		drop = (Button) findViewById(R.id.drop);
		back = (Button) findViewById(R.id.back);
		list_searchstore = (ListView) findViewById(R.id.list_searchstore);
	}

	private void initListener() {
		sure.setOnClickListener(this);
		drop.setOnClickListener(this);
		back.setOnClickListener(this);
	}

	private void setView() {
		contents = getIntent().getStringExtra("contents");
		mSearchAdapter = new SearchAdapter(SearchStoreActivity.this, list);
		list_searchstore.setAdapter(mSearchAdapter);

		list_searchstore.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				Intent mIntent = new Intent(SearchStoreActivity.this,
						StaffStoreDetailsActivity.class);
//				mIntent.putExtra("store_id", list.get(position).id);
				mIntent.putExtra("store_id", list.get(position).store_id);
				startActivity(mIntent);
			}
		});
		getorder();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.sure:
				break;
			case R.id.drop:
				finish();
				break;
			case R.id.back:
				finish();
				break;
			default:
				break;
		}
	}

	/*
	private void getorder() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "getstorelist1" };
			String[] name1 = new String[] { "city",
					MyApplication.app_current_city };
			params2.add(funParam2);
			params2.add(name1);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						type_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						mSearchAdapter = new SearchAdapter(
								SearchStoreActivity.this, list);
						list_searchstore.setAdapter(mSearchAdapter);
					}

				} else {
					Toast.makeText(SearchStoreActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(SearchStoreActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};
*/

	private void getorder() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
//			String[] methodParam = new String[] { "ak","cHuUTMwABwmzf3HQ8BKtB8Uh" };
			String[] methodParam = new String[] { "ak","GM3NqGyZDhXqUExqGNeU8lMc" };
//			String[] geotable_idParam = new String[] { "geotable_id", "125660" };
			String[] geotable_idParam = new String[] { "geotable_id", "134516" };
			String[] geo_position1 = new String[] { "region", MyApplication.app_current_city };
			String[] sortbyParam = new String[] { "sortby","distance:1" };
			// http://api.map.baidu.com/geosearch/v3/local?ak=cHuUTMwABwmzf3HQ8BKtB8Uh&geotable_id=125660&sortby=distance:1
			String[] locationParam = new String[] {"location", MyApplication.app_longitude + ","+ MyApplication.app_latitude };
			params.add(locationParam);
			String[] radiusss = new String[] { "radius", "1000000" };
			params.add(radiusss);
			params.add(methodParam);
			params.add(geotable_idParam);
			params.add(geo_position1);
			params.add(sortbyParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.BAIDUURL, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list = mStoreListObject.contents;
				if(list!=null){
					if (list.size() > 0) {
						mSearchAdapter = new SearchAdapter(
								SearchStoreActivity.this, list);
						list_searchstore.setAdapter(mSearchAdapter);
						MyApplication.search=true;
					}
				} else {
					Toast.makeText(SearchStoreActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(SearchStoreActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void IntoStore() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "getstorelist1" };
			String[] name1 = new String[] { "city",
					MyApplication.app_current_city };
			params2.add(funParam2);
			params2.add(name1);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						into_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}
	}

	private CallbackListener into_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						mSearchAdapter = new SearchAdapter(
								SearchStoreActivity.this, list);
						list_searchstore.setAdapter(mSearchAdapter);
					}

				} else {
					Toast.makeText(SearchStoreActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(SearchStoreActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};
}
