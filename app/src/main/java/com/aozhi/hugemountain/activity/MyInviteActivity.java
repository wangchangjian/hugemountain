package com.aozhi.hugemountain.activity;


import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.InviteListAdapter;
import com.aozhi.hugemountain.adapter.InviteListStaffAdapter;
import com.aozhi.hugemountain.model.ClientObject;
import com.aozhi.hugemountain.model.InviteListObject;
import com.aozhi.hugemountain.model.InviteListObjectStaff;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MyInviteActivity extends Activity {
	Button btn_back;
	TextView t1;
	TextView t2;

	LinearLayout item1;
	LinearLayout item2;
	
	
	ArrayList<ClientObject> list_client=new ArrayList<ClientObject>();
	ArrayList<ClientObject> list_toclient=new ArrayList<ClientObject>();
	InviteListObject mClientListObject;
	InviteListObject mToClientListObject;
	ClientObject mClientObject;
	ClientObject mToClientObject;
	ArrayList<StaffObject> list_staff=new ArrayList<StaffObject>();
	ArrayList<StaffObject> list_tostaff=new ArrayList<StaffObject>();
	InviteListObjectStaff mStaffListObject;
	InviteListObjectStaff mToStaffListObject;
	StaffObject mStaffObject;
	StaffObject mToStaffObject;
	InviteListAdapter adapter1;
	InviteListAdapter adapter2;
	InviteListStaffAdapter adapter3;
	InviteListStaffAdapter adapter4;
	private String client_id="",Status="";
	private ListView list_1;
	private ListView list_2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_myinvite);
		Status= MyApplication.Status;
		initView();
		myonclick();
		if(Status.equals("client")){
			getclient();
			gettoclient();
		}
		if(Status.equals("staff")){
			getstaff();
			gettostaff();
		}
		
	}
	private void initView() {
		t1=(TextView)findViewById(R.id.tv_service);
		t2=(TextView)findViewById(R.id.tv_staff);
		item1=(LinearLayout)findViewById(R.id.item1);
		item2=(LinearLayout)findViewById(R.id.item2);
		
		list_1=(ListView) findViewById(R.id.list_1);
		list_2=(ListView) findViewById(R.id.list_2);
		adapter1=new InviteListAdapter(MyInviteActivity.this, list_client);
		adapter2=new InviteListAdapter(MyInviteActivity.this, list_toclient);
		list_1.setAdapter(adapter1);
		list_2.setAdapter(adapter2);
		client_id=MyApplication.Clientuser.id;
		
		
		
		
		
	}
	private void gettostaff() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getinvite_staff" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private void getstaff() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "gettoinvite_staff" };
		String[] name1 = new String[] { "tostaff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener4);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	
	private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, InviteListObjectStaff.class);
				list_staff = mStaffListObject.response;
					if (mStaffListObject.meta.getMsg().equals("OK")) {
						if (list_staff.size() > 0) {
							adapter3 = new InviteListStaffAdapter(MyInviteActivity.this, list_staff);
							list_1.setAdapter(adapter3);
						} else {
							Toast.makeText(MyInviteActivity.this, "无邀请记录",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(MyInviteActivity.this, "无邀请记录", Toast.LENGTH_LONG)
								.show();
					}
					
				} else {
					Toast.makeText(MyInviteActivity.this, "无邀请记录", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	
	
	
	private CallbackListener type_callbackListener4 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
					mToStaffListObject = JSON.parseObject(v, InviteListObjectStaff.class);
					list_tostaff = mToStaffListObject.response;
					if (mToStaffListObject.meta.getMsg().equals("OK")) {
						if (list_tostaff.size() > 0) {
							adapter4 = new InviteListStaffAdapter(MyInviteActivity.this, list_tostaff);
							list_2.setAdapter(adapter4);
						} else {
							Toast.makeText(MyInviteActivity.this, "无被邀请记录",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(MyInviteActivity.this, "无被邀请记录", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					Toast.makeText(MyInviteActivity.this, "无被邀请记录", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	

	
	private void gettoclient() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getinvite" };
		String[] name1 = new String[] { "client_id", client_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private void getclient() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "gettoinvite" };
		String[] name1 = new String[] { "toclient_id", client_id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	
	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mClientListObject = JSON.parseObject(v, InviteListObject.class);
				list_client = mClientListObject.response;
					if (mClientListObject.meta.getMsg().equals("OK")) {
						if (list_client.size() > 0) {
							adapter1 = new InviteListAdapter(MyInviteActivity.this, list_client);
							list_1.setAdapter(adapter1);
						} else {
							Toast.makeText(MyInviteActivity.this, "无邀请记录",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(MyInviteActivity.this, "无邀请记录", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					Toast.makeText(MyInviteActivity.this, "无邀请记录", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	
	
	
	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
					mToClientListObject = JSON.parseObject(v, InviteListObject.class);
					list_toclient = mToClientListObject.response;
					if (mToClientListObject.meta.getMsg().equals("OK")) {
						if (list_toclient.size() > 0) {
							adapter2 = new InviteListAdapter(MyInviteActivity.this, list_toclient);
							list_2.setAdapter(adapter2);
						} else {
							Toast.makeText(MyInviteActivity.this, "无被邀请记录",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(MyInviteActivity.this, "无被邀请记录", Toast.LENGTH_LONG)
								.show();
					}
				} else {
					Toast.makeText(MyInviteActivity.this, "无被邀请记录", Toast.LENGTH_LONG)
							.show();
				}
		}
	};
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_invite, menu);
		return true;
	}
	private void myonclick() {
		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t1.setTextColor(getResources().getColor(R.color.y1));
				t2.setTextColor(getResources().getColor(R.color.n1));
				
				t1.setBackgroundColor(getResources().getColor(R.color.bg1));
				t2.setBackgroundColor(getResources().getColor(R.color.bg2));
				
				item1.setVisibility(View.VISIBLE);
				item2.setVisibility(View.GONE);
			}
		});
		
		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t2.setTextColor(getResources().getColor(R.color.y1));
				t1.setTextColor(getResources().getColor(R.color.n1));
				
				t2.setBackgroundColor(getResources().getColor(R.color.bg1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg2));
				
				item2.setVisibility(View.VISIBLE);
				item1.setVisibility(View.GONE);
			}
		});
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}
}
