package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.StaffObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BusinessStaffAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();
	
	
	public BusinessStaffAdapter(Context ctx, ArrayList<StaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_managestaff3, null);
			mHolder.phone= (ImageView) convertView.findViewById(R.id.img_avatar);
			mHolder.name=(TextView) convertView.findViewById(R.id.tv_name);
			mHolder.tv_service12=(TextView) convertView.findViewById(R.id.tv_service12);
			mHolder.surchange=(TextView) convertView.findViewById(R.id.surchange);
			mHolder.origin_money1= (TextView) convertView.findViewById(R.id.origin_money1);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		MyApplication.intermission=list.get(position).intermission;
		mHolder.name.setText("编号"+list.get(position).code_id);
		mHolder.tv_service12.setText(" 服务"+list.get(position).service_click+"次");		
		mHolder.surchange.setText("￥"+list.get(position).surchange);
		mHolder.origin_money1.setText("￥"+list.get(position).surchange);
		if (list.get(position).avatar.equals("")|| list.get(position).avatar==null ) {
			
		} else {
			MyApplication.downloadImage.addTask(
					list.get(position).avatar, mHolder.phone,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		return convertView;
	}
	
	class Holder {
		ImageView phone;
		TextView name,age,tv_service12,surchange;
		 TextView origin_money1;
	}
}
