package com.aozhi.hugemountain.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.FuwuAdapter;
import com.aozhi.hugemountain.adapter.ImageAddAdapter;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ProjectReleasesActivity extends Activity {

	private Button btn_back;
	private Button btn_ok;
	private ImageView img_add;
	private GridView photo_add;
	private EditText et_name, et_money, et_content, et_time;
	private ProgressDialog progressDialog = null;
	public static final int RESULT_CODE = 101;
	private Bitmap bitmap;
	private TextView tv_camera, tv_photo;
	private LinearLayout img_liner;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// 拍照
	public static final int PHOTOZOOM = 2; // 缩放
	public static final int PHOTORESOULT = 3;// 结果
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String serverFile = "";
	private ImageAddAdapter adapter;
	private List<Bitmap> list = new ArrayList<Bitmap>();
	private ArrayList<String> pt = new ArrayList<String>();
	private ArrayList<ProjectObject> plist = new ArrayList<ProjectObject>();
	private ArrayList<ProjectObject> servlist = new ArrayList<ProjectObject>();
	private ArrayList<ProjectObject> servlist1 = new ArrayList<ProjectObject>();
	private ArrayList<ProjectObject> pathlist = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private ProjectListObject mProjectListObject1;
	private String project_id = "";
	private Button chack;
	private ArrayList<ProjectObject> fuwulist = new ArrayList<ProjectObject>();
	private ListView list_fuwu;
	private FuwuAdapter mFuwuAdapter;
	private AutoCompleteTextView at_tv;
	private List<String> sl = new ArrayList<String>();
	private ArrayAdapter<String> sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_projectreleases);

		img_add = (ImageView) findViewById(R.id.img_add1);
		et_name = (EditText) findViewById(R.id.et_name);
		et_money = (EditText) findViewById(R.id.et_money);
		et_content = (EditText) findViewById(R.id.et_content);
		btn_back = (Button) findViewById(R.id.btn_back);
		img_liner = (LinearLayout) findViewById(R.id.img_liner);
		photo_add = (GridView) findViewById(R.id.photo_add);
		et_time = (EditText) findViewById(R.id.et_time);
		getServices();
		chack = (Button) findViewById(R.id.chack);
		chack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// InputChacked();
				showFuwu();
			}
		});
		Bitmap bmp = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_addpic_focused);
		list.add(bmp);
		adapter = new ImageAddAdapter(this, list);
		photo_add.setAdapter(adapter);
		img_add.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				//添加图片
				showAtaver();
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		btn_ok = (Button) findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (et_name.getText().toString().equals("")) {
					Toast.makeText(ProjectReleasesActivity.this, "请输入要发布的服务名称",
							Toast.LENGTH_LONG).show();
				} else if (et_money.getText().toString().equals("")) {
					Toast.makeText(ProjectReleasesActivity.this, "请输入服务价格",
							Toast.LENGTH_LONG).show();
				} else if (et_content.getText().toString().equals("")) {
					Toast.makeText(ProjectReleasesActivity.this, "请输入服务的介绍内容",
							Toast.LENGTH_LONG).show();
				} else if (et_time.getText().toString().equals("")) {
					Toast.makeText(ProjectReleasesActivity.this, "请输入服务时长",
							Toast.LENGTH_LONG).show();
				} else {
					AddProject();
				}
			}
		});
		photo_add.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showAtaver();
			}
		});
	}

	// 用户输入的项目名称和数据库进行匹配
	private void InputChacked() {
		if (!et_name.getText().toString().equals("")) {
			for (int i = 0; i < servlist.size(); i++) {
				if (et_name.getText().toString().equals(servlist.get(i).name)) {
					Toast.makeText(getApplicationContext(), "匹配成功",
							Toast.LENGTH_SHORT).show();
					project_id = servlist.get(i).id;
					getStoreServices();
				} else {
					if (i == servlist.size()) {
						Toast.makeText(getApplicationContext(),
								"未找到匹配项，你输入的项目未审核，如需审核请联系管理员",
								Toast.LENGTH_SHORT).show();
					}
				}
			}
		} else {
			Toast.makeText(getApplicationContext(), "您没有输入内容，请检查输入",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void AddProject() {
		if (project_id.equals("")) {
			return;
		} else {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			// {name}','{img}','{content}',now(),'{money}','{sid}')
			String[] funParam2 = new String[] { "fun", "AddProject" };
			String[] nameParam2 = new String[] { "store_id",
					MyApplication.Storeuser.id };
			String[] pwdParam2 = new String[] { "service_id", project_id };
			String[] typesParam2 = new String[] { "money",
					et_money.getText().toString().trim() };
			String[] store_idParam2 = new String[] { "click_count", "0" };
			String[] nameParam3 = new String[] { "time",
					et_time.getText().toString() };
			String[] nameParam=new String[]{"contents",et_content.getText().toString()};
			//此处应该将多个图片的路径上传到服务器，使用逗号分隔图片
			String photos = "";
			for (String phot : pt) {
				if(photos != ""){
					photos += ",";
				}
				photos += phot;
			}
			String[] nameParam6=new String[]{"avatar",photos};
			params2.add(funParam2);
			params2.add(nameParam2);
			params2.add(pwdParam2);
			params2.add(typesParam2);
			params2.add(store_idParam2);
			params2.add(nameParam3);
			params2.add(nameParam);
			params2.add(nameParam6);
			progressDialog = ProgressDialog.show(this,
					getString(R.string.app_name),
					getString(R.string.tv_dialog_context), false);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						AddStaffInfo_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}
	}

	private CallbackListener AddStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				plist = mProjectListObject.response;
				if (mProjectListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(ProjectReleasesActivity.this, "项目发布成功",
							Toast.LENGTH_LONG).show();
					//照片已经在选择后上传了，此处不需要重复上传了。by fgrass
					/*for (int i = 0; i < pt.size(); i++) {
						UpProjectPhoto(pt.get(i),plist.get(0).id);
					}*/
					Intent intent = new Intent();
					setResult(RESULT_CODE, intent);
					finish();
				} else {
					Toast.makeText(ProjectReleasesActivity.this, "项目发布失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(ProjectReleasesActivity.this, "项目发布失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.renwufabu_shooting);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, 11);
				dlg.cancel();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(new File(Environment
								.getExternalStorageDirectory(), "juyue.jpg")));
				startActivityForResult(intent, 12);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode != RESULT_OK) {
			return;
		}
		if (data != null) {
			switch (requestCode) {
			case 11:
				startPhotoZoom(data.getData());
				break;
			case 12:
				Log.i("OnActivityResult", "**********************case camera!");
				File temp = new File(Environment.getExternalStorageDirectory()
						+ "/juyue.jpg");
				startPhotoZoom(Uri.fromFile(temp));
				break;

			case 13: {
				setPicToView(data);
			}
				break;

			default:
				break;
			}

		} else if (Build.MODEL.equals("MI 2C")) {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			startPhotoZoom(Uri.fromFile(temp));
		} else {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			if (temp.exists()) {
				startPhotoZoom(Uri.fromFile(temp));
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			pt.add(serverFile);
			return serverFile;
		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 13);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	@SuppressWarnings("deprecation")
	@SuppressLint({ "NewApi", "SdCardPath" })
	private void setPicToView(Intent picdata) {
		Bundle extras = picdata.getExtras();
		if (extras != null) {
			bitmap = extras.getParcelable("data");
			String sdStatus = Environment.getExternalStorageState();
			if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
				// Log.i("TestFile"
				// "SD card is not avaiable/writeable right now.");
				return;
			}
			@SuppressWarnings("static-access")
			String fileName = new DateFormat().format("yyyyMMdd_hhmmss",
					Calendar.getInstance(Locale.CHINA)) + ".jpg";
			File dir = new File("/sdcard/juyue/cache/");
			dir.mkdirs();// 创建文件夹

			fileName = "/sdcard/juyue/cache/" + fileName;
			String result = Utils.saveBitmapToFile(bitmap, fileName);
			if (result == null || result.trim().equals("")) {
				Utils.DisplayToast(this, "存储空间不足");
			}
			// @SuppressWarnings("unused")
			File file = new File(fileName);
			// img_add.setImageBitmap(bitmap);
			new UploadAsyncTask().execute(file);
			if (list.size() < 9) {
				list.add(bitmap);
				adapter.notifyDataSetChanged();
			}

		}
	}

	private void UpProjectPhoto(String path,String preject_id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		// {name}','{img}','{content}',now(),'{money}','{sid}')
		String[] funParam2 = new String[] { "fun", "upProjectPhoto" };
		String[] nameParam2 = new String[] { "project_id", preject_id };
		String[] nameParam1 = new String[] { "path", path };
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddProject_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddProject_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {
					Intent mintent = new Intent(ProjectReleasesActivity.this,
							ProjectActivity.class);
					Toast.makeText(ProjectReleasesActivity.this, "图片上传成功",
							Toast.LENGTH_LONG).show();
					startActivityForResult(mintent, 152);
					finish();
				} else {
					Toast.makeText(ProjectReleasesActivity.this, "图片上传失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(ProjectReleasesActivity.this, "图片上传失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 获取项目库里的项目
	private void getServices() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservices" };
		// String[] nameParam = new String[] { "which", which };
		params2.add(funParam2);
		// params2.add(nameParam);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getserv_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getserv_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				servlist = mProjectListObject.response;
				if (mProjectListObject.meta.getMsg().equals("OK")) {

				}
			} else {
				
			}

		}
	};

	// 获取店铺的项目
	private void getStoreServices() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservices1" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "service_id", project_id };
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getserv1_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getserv1_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject1 = JSON.parseObject(v,
						ProjectListObject.class);
				servlist1 = mProjectListObject1.response;
				if (mProjectListObject1.meta.getMsg().equals("OK")) {
					if (servlist1.size() > 0) {
						Toast.makeText(getApplicationContext(),
								"该项目已经添加，不能重复添加", Toast.LENGTH_SHORT).show();
						et_name.setText("");
					} else {
						Toast.makeText(getApplicationContext(), "检查通过",
								Toast.LENGTH_SHORT).show();
					}
				}
			} else {
				Toast.makeText(ProjectReleasesActivity.this, "获取店铺项目失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	private void showFuwu() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.activity_item);
		list_fuwu = (ListView) window.findViewById(R.id.list_fuwu);
		mFuwuAdapter = new FuwuAdapter(this, fuwulist);
		list_fuwu.setAdapter(mFuwuAdapter);
		if (!et_name.getText().toString().equals("")) {
			getServiceList(et_name.getText().toString());
		} else {
			getServiceList("");
			Toast.makeText(ProjectReleasesActivity.this, "输入名称可做精确查找",
					Toast.LENGTH_SHORT).show();
		}
		list_fuwu.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String ns=fuwulist.get(arg2).name;
				project_id=fuwulist.get(arg2).id;
				et_name.setText(ns);	
				dlg.cancel();
			}
		});
	}

	// 获取店铺的项目
	private void getServiceList(String text) {
		ArrayList<String[]> params3 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getServicesList" };
		String[] nameParam = new String[] { "name", text };
		String[] nameParam1 = new String[] { "store_id", MyApplication.Storeuser.id };
		
		params3.add(funParam2);
		params3.add(nameParam);
		params3.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params3,
					getserv2_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getserv2_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject1 = JSON.parseObject(v,
						ProjectListObject.class);
				fuwulist = mProjectListObject1.response;
				if (mProjectListObject1.meta.getMsg().equals("OK")) {
					if (fuwulist.size() > 0) {
						mFuwuAdapter = new FuwuAdapter(
								ProjectReleasesActivity.this, fuwulist);
						list_fuwu.setAdapter(mFuwuAdapter);
					}
				}
			}

		}
	};
}
