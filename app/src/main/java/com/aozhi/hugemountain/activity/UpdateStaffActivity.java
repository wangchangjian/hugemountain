package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.adapter.TypeAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.StaffsListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView.ScaleType;

public class UpdateStaffActivity extends Activity {
	private Button btn_back;
	private TextView tv_edit, upimg;
	private Spinner tv_type;
	private TextView et_tel, et_code;
	private TextView et_content, et_commission;
	private LinearLayout pager;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LoginListObject mLoginListObject;
	private ProgressDialog progressDialog = null;
	private String id = "";
	private TextView et_server;
	private ListView list_type;
	private TypeAdapter mTypeAdapter;
	private StaffsListObject mStaffsListObject;
	private ArrayList<ManageStaffObject> type_list = new ArrayList<ManageStaffObject>();
	private ArrayList<ManageStaffObject> service_list = new ArrayList<ManageStaffObject>();
	private ArrayList<Integer> myChose = new ArrayList<Integer>();
	private String typeid = "";
	private TextView tv_camera, tv_photo;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// ����
	public static final int PHOTOZOOM = 2; // ����
	public static final int PHOTORESOULT = 3;// ���
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String serverFile = "";
	private String imgload = "";
	private ViewPager vp_mainadv;
	private int currentItem = 0;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
	private TextView status1, status2, status3;
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			vp_mainadv.setCurrentItem(currentItem);// �л���ǰ��ʾͼƬ
		}
	};

	private Handler updateHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// setText();
		};
	};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_updatestaff);
		initView();
		getType();
		myclick();

	}

	private void initView() {
		// TODO Auto-generated method stub
		tv_type = (Spinner) findViewById(R.id.tv_type);
		tv_edit = (TextView) findViewById(R.id.tv_edit);
		btn_back = (Button) findViewById(R.id.btn_back);
		id = getIntent().getStringExtra("id");
		imgload = getIntent().getStringExtra("img_path");
		et_tel = (TextView) findViewById(R.id.et_tel);
		et_code = (TextView) findViewById(R.id.et_code);
		et_content = (TextView) findViewById(R.id.et_content);
		et_commission = (TextView) findViewById(R.id.et_commission);
		et_server = (TextView) findViewById(R.id.ed_server);
		pager = (LinearLayout) findViewById(R.id.pager);
		upimg = (TextView) findViewById(R.id.upimg);
		status1 = (TextView) findViewById(R.id.status1);
		status2 = (TextView) findViewById(R.id.status2);
		status3 = (TextView) findViewById(R.id.status3);
		getPhoto();
		getStaffList();
	}

	private void myclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				setResult(25);
				finish();
			}
		});

		tv_edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(UpdateStaffActivity.this,
						EditStaffActivity.class);
				intent.putExtra("id", id);
				startActivityForResult(intent, 403);
			}
		});
	}

	private void getType() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStafftype" };
		params2.add(funParam2);

		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				type_list = mStaffsListObject.getResponse();
				if (type_list.size() > 0) {
					Spinner();
				}

			} else {
				Toast.makeText(UpdateStaffActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffDetails" };
		String[] idParam2 = new String[] { "id", id };
		String[] idParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam1);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					et_code.setText("编号" + list.get(0).code_id);
					et_tel.setText(list.get(0).staffphoto);
					Spinner();
					et_content.setText(list.get(0).remark);
					et_commission.setText(list.get(0).commission);
					et_server.setText(list.get(0).service_name);
//					getServicesList();
					switch (Integer.parseInt(list.get(0).workstatus)) {
					case 0:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 1:
						status1.setBackgroundColor(Color.parseColor("#FF7900"));
						status1.setTextColor(Color.parseColor("#ffffff"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 2:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#FF7900"));
						status2.setTextColor(Color.parseColor("#ffffff"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 3:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#FF7900"));
						status3.setTextColor(Color.parseColor("#ffffff"));
						break;
					default:

						break;
					}
				}
			} else {
				Toast.makeText(UpdateStaffActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 下拉框适配器
	private void Spinner() {
		ArrayList<String> timelist = new ArrayList<String>();
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		for (int i = 0; i < type_list.size(); i++) {
			timelist.add(type_list.get(i).types);
		}
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, timelist);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		tv_type.setAdapter(adapter);
		// 第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		tv_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				typeid = type_list.get(position).id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				typeid = list.get(0).types;
			}
		});
		SpinnerAdapter apsAdapter = tv_type.getAdapter(); // 得到SpinnerAdapter对象
		int k = apsAdapter.getCount();
		if (list.size() > 0) {
			for (int i = 0; i < k; i++) {
				if (list.get(0).type.equals(apsAdapter.getItem(i).toString())) {
					tv_type.setSelection(i, true);// 默认选中项
					break;
				}
			}
		}
	}

	// 图片轮播
	private void getPhoto() {
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getStaffPhotos" };
			String[] paramName = new String[] { "user_id", id };
			params.add(methodParam);
			params.add(paramName);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mImageListObject = JSON.parseObject(v, ImageListObject.class);
				imglist = mImageListObject.getResponse();
				int n = 0;
				if (imglist.size() > 0) {
					for (n = 0; n < imglist.size(); n++) {
						final ImageView imgView = new ImageView(
								UpdateStaffActivity.this);
						imgView.setClickable(true);
						imgView.setScaleType(ScaleType.FIT_XY);
						downloadImage.addTasks(imglist.get(n).img, imgView,
								new DownloadImage.ImageCallback() {
									public void imageLoaded(Bitmap imageBitmap,
											String imageUrl) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag(imageUrl);
										}
									}

									public void imageLoaded(Bitmap imageBitmap,
											DownloadImageMode callBackTag) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag("");
										}
									}
								});
						downloadImage.doTask();
						final String url = imglist.get(n).url;
						final String title = imglist.get(n).name;

						if (images.equals("")) {
							images = imglist.get(n).img;
						} else {
							images = images + "," + imglist.get(n).img;
						}

						imgView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent mIntent = new Intent(
										UpdateStaffActivity.this,
										ImagePagerActivity.class);
								mIntent.putExtra("images", images);
								startActivity(mIntent);
							}
						});
						imgViews.add(imgView);

					}
					vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
					vp_mainadv.setAdapter(new MyAdapter());
					// 设置监听，当ViewPager中的页面改变时调用
					vp_mainadv
							.setOnPageChangeListener(new MyPageChangeListener());
				}
			}
		}

	};

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imglist.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub

		}
	}

	public void onDestory() {
		vp_mainadv = null;
		handler.removeCallbacks(new ScrollTask());
		list = null;
		handler = null;
		imgViews = null;
		System.gc();
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		scheduleExecutorService = Executors.newSingleThreadScheduledExecutor();
		// ��Activity��ʾ������ÿ�����л�һ��ͼƬ��ʾ
		scheduleExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 7,
				TimeUnit.SECONDS);
		super.onStart();
	}

	@Override
	protected void onStop() {
		scheduleExecutorService.shutdown();
		super.onStop();
	}

	private ScheduledExecutorService scheduleExecutorService;

	/*
	 * 
	 * �����л�����ʱ�䣺2013.4.22���ߣ�guido
	 */
	private class ScrollTask implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			synchronized (vp_mainadv) {
				System.out.println("currentItem: " + currentItem);
				currentItem = (currentItem + 1) /* % imageViews.size() */;
				if (currentItem >= imgViews.size()) {
					currentItem = 0;
				}
				handler.obtainMessage().sendToTarget(); // ͨ��Handler�л�ͼƬ
			}
		}

	}

	private void getServicesList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffServices" };
		String[] idParam2 = new String[] { "staff_id", id};
		String[] nameParam=new String[]{"store_id",MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getServices_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getServices_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				service_list = mStaffsListObject.getResponse();
				if (mStaffsListObject.meta.getMsg().equals("OK")) {
					String sev = "";
					if (service_list.size() > 0) {
						for (int i = 0; i < service_list.size(); i++) {
							sev += service_list.get(i).name + ",";
						}
						et_server.setText(sev);
					}
				}
			} else {
				Toast.makeText(UpdateStaffActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 403:
			Intent intent=new Intent(getApplicationContext(),ManagementStaffActivity.class);
			setResult(25,intent);
			finish();
			break;

		default:
			break;
		}
	}
}
