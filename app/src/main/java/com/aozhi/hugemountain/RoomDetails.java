package com.aozhi.hugemountain;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.model.OrderObject;
import com.aozhi.hugemountain.model.RoomListObject;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class RoomDetails extends Activity implements OnClickListener {

	private ProgressDialog progressDialog = null;
	private ArrayList<RoomObject> list = new ArrayList<RoomObject>();
	private ArrayList<RoomObject> list1 = new ArrayList<RoomObject>();
	private ArrayList<OrderFormObject> orderlist = new ArrayList<OrderFormObject>();
	private RoomListObject mRoomListObject;
	private TextView roomcode, roomname, roomstatus, roomstaff, roomservice,
			roomtime;
	private Button btn_back;
	private String id = "";
	private LinearLayout item1,item2;
	private TextView order_id,mobile,create_time,code_id,staff_service,room_service,other;
	private ImageView staff_img;
	private Button order_ok,order_del;
	private OrderListObject mOrderListObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_roomdetails);
		initView();
		initListener();
		getRoomDetails();
	}

	private void initView() {
		roomcode = (TextView) findViewById(R.id.roomcode);
		roomname = (TextView) findViewById(R.id.roomname);
		roomstatus = (TextView) findViewById(R.id.roomstatus);
		roomstaff = (TextView) findViewById(R.id.roomstaff);
		roomservice = (TextView) findViewById(R.id.roomservice);
		roomtime = (TextView) findViewById(R.id.roomtime);
		btn_back = (Button) findViewById(R.id.img1);
		id = getIntent().getStringExtra("id");
		item1=(LinearLayout) findViewById(R.id.item1);
		item2=(LinearLayout) findViewById(R.id.item2);
		order_id=(TextView) findViewById(R.id.order_ids);
		mobile=(TextView) findViewById(R.id.mobile);
		create_time=(TextView) findViewById(R.id.create_time);
		staff_img=(ImageView) findViewById(R.id.staff_img);
		code_id=(TextView) findViewById(R.id.code_id1);
		staff_service=(TextView) findViewById(R.id.staff_service);
		room_service=(TextView) findViewById(R.id.room_service);
		other=(TextView) findViewById(R.id.other);
		order_del=(Button) findViewById(R.id.order_del);
		order_ok=(Button) findViewById(R.id.order_ok);
		
	}

	private void initListener() {
		order_del.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		order_ok.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			finish();
			break;
		case R.id.order_ok:
			setRoomOrderOk();
			break;
		case R.id.order_del:
			AlertDialog.Builder builder=new Builder(RoomDetails.this);
			builder.setMessage("确认取消订单吗？");
			builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					setRoomOrderQvxiao();
					arg0.dismiss();
					finish();
				}
			});
			builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					arg0.dismiss();
				}
			});
			builder.create().show();
			break;
		default:
			break;
		}
	}

	private void getRoomDetails() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getRoomDetail" };
		String[] nameParam2 = new String[] { "room_id", id };
		params2.add(funParam2);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddStaffInfo_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list = mRoomListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						roomcode.setText(list.get(0).code);
						roomname.setText(list.get(0).name);
						if (list.get(0).mark.equals("0")) {
							item1.setVisibility(View.VISIBLE);
							item2.setVisibility(View.GONE);
							roomstatus.setText("空闲中");
							roomservice.setText("等待中");
							
							roomtime.setText("");
						} else if (list.get(0).mark.equals("1")) {
//							roomstatus.setText("客已满");
//							roomservice.setText(list.get(0).room_services);
//							roomtime.setText(list.get(0).service_time);
							item1.setVisibility(View.GONE);
							item2.setVisibility(View.VISIBLE);
							getRoomOrder();
						} else if (list.get(0).mark.equals("2")) {
							item1.setVisibility(View.GONE);
							item2.setVisibility(View.VISIBLE);
							getRoomOrder();
//							roomstatus.setText("已预订");
//							roomservice.setText(list.get(0).room_services);
//							roomtime.setText(list.get(0).service_time);
						} else if (list.get(0).mark.equals("3")) {
							item1.setVisibility(View.VISIBLE);
							item2.setVisibility(View.GONE);
							roomstatus.setText("维修中");
							roomservice.setText("");
							roomtime.setText(list.get(0).service_time);
						}
					}
//					getRoomServices();
				} else {
					Toast.makeText(RoomDetails.this, "获取信息失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "获取信心失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void getRoomServices() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getRoomDetail1" };
		String[] nameParam2 = new String[] { "room_id", id };
		params2.add(funParam2);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getInfo_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list1 = mRoomListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					if (list1.size() > 0) {
						if (list1.get(0).mark.equals("0")) {
							roomstatus.setText("空闲中");
							roomservice.setText("等待中");
							roomtime.setText("");
						} else if (list1.get(0).mark.equals("1")) {
							roomstatus.setText("客已满");
							roomservice.setText(list1.get(0).room_services);
							roomtime.setText(list1.get(0).service_time);
						} else if (list1.get(0).mark.equals("2")) {
							roomstatus.setText("已预订");
							roomservice.setText(list1.get(0).room_services);
							roomtime.setText(list1.get(0).service_time);
						} else if (list1.get(0).mark.equals("3")) {
							roomstatus.setText("维修中");
							roomservice.setText("");
							roomtime.setText(list1.get(0).service_time);
						}

					} else {
						roomstaff.setText("");
						roomstatus.setText("");
						roomservice.setText("");
						roomtime.setText("");
					}

					Toast.makeText(RoomDetails.this, "获取信息成功",
							Toast.LENGTH_LONG).show();

				} else {
					Toast.makeText(RoomDetails.this, "获取信息失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "获取信息失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void getRoomOrder() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getRoomOrder" };
		String[] nameParam2 = new String[] { "room_id", id };
		params2.add(funParam2);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getOrder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getOrder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				orderlist = mOrderListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					if(orderlist.size()>0){
					order_id.setText(orderlist.get(0).order_id);
					mobile.setText(orderlist.get(0).tel);
					create_time.setText(orderlist.get(0).create_time);
					code_id.setText(orderlist.get(0).code_id);
					staff_service.setText(orderlist.get(0).servicename);
					room_service.setText(orderlist.get(0).servicename);
					other.setText("￥"+orderlist.get(0).surchange);
					getStaffimg(orderlist.get(0).use_id);
					}
				} else {
					Toast.makeText(RoomDetails.this, "获取信息失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "获取信息失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	private void setRoomOrderOk() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "orderform_ok" };
		String[] nameParam2 = new String[] { "order_id", order_id.getText().toString()};
		params2.add(funParam2);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					setOrderOk_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener setOrderOk_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list1 = mRoomListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(RoomDetails.this, "成功",
							Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(RoomDetails.this, "获取信息失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "获取信息失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	
	private void setRoomOrderQvxiao() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setOrderStatus" };
		String[] nameParam1 = new String[] { "order_id", order_id.getText().toString()};
		String[] nameParam2 = new String[] {"orderstatus","0"};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					setOrderde_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener setOrderde_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list1 = mRoomListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(RoomDetails.this, "成功",
							Toast.LENGTH_LONG).show();

				} else {
					Toast.makeText(RoomDetails.this, "失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	
	private void getStaffimg(String user_id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getUserImg" };
		String[] nameParam1 = new String[] { "user_id",user_id};
		params2.add(funParam2);
		params2.add(nameParam1);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					setOrderde_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener setOrderde_callbackListener1= new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list1 = mRoomListObject.response;
				if (mRoomListObject.meta.getMsg().equals("OK")) {
					setImg(list1.get(0).img,staff_img);
				} else {
					Toast.makeText(RoomDetails.this, "失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RoomDetails.this, "失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	private void setImg(String path,final ImageView iv){
		if (path.equals("") || path == null) {
			iv.setBackgroundResource(R.drawable.img_add);
		} else {
			MyApplication.downloadImage.addTasks(path,
					iv, new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								iv.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								iv.setBackgroundDrawable(drawable);
							}
						}
					});
		}
		MyApplication.downloadImage.doTask();
	}
}
