package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class OrderListObject {
	public Meta meta;
	public ArrayList<OrderFormObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<OrderFormObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<OrderFormObject> response) {
		this.response = response;
	}
}
