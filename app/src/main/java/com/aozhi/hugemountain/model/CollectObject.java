package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class CollectObject  implements Serializable{
	 public String id;
	 public String store_id;
	 public String staff_id;
	 public String client_id;
	 public String mark;
	 public String create_time;
	 public String update_time;
	 public String fel_flag;
	 public String of_id;
	 public String quality;
	 public String service;
	 public String distribution;
	 public String contents;
	 public String clientname;
	 
	 public String clientphoto;//客户账号
}