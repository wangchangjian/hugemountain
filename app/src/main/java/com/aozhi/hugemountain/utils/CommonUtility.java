package com.aozhi.hugemountain.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore.MediaColumns;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

/**
 * @author albert albert@buyantech.com 2012
 * 
 */
public class CommonUtility {
	 private static final String TAG = "CommonUtility";

	public static String md5(String in) {
	        MessageDigest digest;
	        try {
	            digest = MessageDigest.getInstance("MD5");
	            digest.reset();
	            digest.update(in.getBytes());
	            byte[] a = digest.digest();
	            int len = a.length;
	            StringBuilder sb = new StringBuilder(len << 1);
	            for (int i = 0; i < len; i++) {
	                sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
	                sb.append(Character.forDigit(a[i] & 0x0f, 16));
	            }
//	           return Base64.encodeToString(a,Base64.NO_WRAP);
	           return sb.toString();
	        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
	        return null;
	    }
	 public static String HashEncryptString(String originalPwd, String lastTwochar)
     {
         String Key = lastTwochar;

        String base64Result = md5(originalPwd+Key);
        if(base64Result.endsWith("=="))
         return base64Result.replace("==", Key);
        return base64Result+Key;
     }

	 
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        // if no network is available networkInfo will be null, otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
	    
 // And to convert the image URI to the direct file system path of the image file
    public static String getRealPathFromURI(Context c,Uri contentUri) {

            // can post image
//            String [] proj={MediaStore.Images.Media.DATA};
            Cursor cursor = c.getContentResolver().query( contentUri,
                            null, // Which columns to return
                            null,       // WHERE clause; which rows to return (all rows)
                            null,       // WHERE clause selection arguments (none)
                            null); // Order-by clause (ascending by name)
            int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
            cursor.moveToFirst();

            return cursor.getString(column_index);
    }
    
    /**
     * Given a media filename, returns it's id in the media content provider
     *
     * @param providerUri
     * @param appContext
     * @param fileName
     * @return
     */
    public static long getMediaItemIdFromProvider(Uri providerUri, Context appContext, String fileName) {
        //find id of the media provider item based on filename
        String[] projection = { BaseColumns._ID, MediaColumns.DATA };
        Cursor cursor = appContext.getContentResolver().query(
                providerUri, projection,
                MediaColumns.DATA + "=?", new String[] { fileName },
                null);
        if (null == cursor) {
            Log.d(TAG, "Null cursor for file " + fileName);
            return 0;
        }
        long id = 0;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getLong(cursor.getColumnIndexOrThrow(BaseColumns._ID));
        }
        cursor.close();
        return id;
    }
    /**
     * Force a refresh of media content provider for specific item
     * 
     * @param fileName
     */
    public static void refreshMediaProvider(Context appContext, String fileName) {
        MediaScannerConnection scanner = null;
        try {
            scanner = new MediaScannerConnection(appContext, null);
            scanner.connect();
            try {
                Thread.sleep(200);
            } catch (Exception e) {
            }
            if (scanner.isConnected()) {
                Log.d(TAG, "Requesting scan for file " + fileName);
                scanner.scanFile(fileName, null);
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot to scan file", e);
        } finally {
            if (scanner != null) {
                scanner.disconnect();
            }
        }
    } 
    
    
    public static Bitmap fileToBitmap(String path){
    	BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// 获取这个图片的宽和高
		Bitmap bitmap = BitmapFactory.decodeFile(path, options); // 此时返回bm为空
    	return bitmap;
    }
    
    /**
	 * @param f
	 * @return
	 */
	public static Bitmap CompressImage(File f,int width,int height) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		// 获取这个图片的宽和高
		Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), options); // 此时返回bm为空
		options.inJustDecodeBounds = false;
		// 计算缩放比
		int beHeight = (int) (options.outHeight / (float) height);
		int beWidth = (int) (options.outWidth / (float) width);
		int be = beHeight>beWidth?beHeight:beWidth;
		if (be <= 0)
			be = 1;
		options.inSampleSize = be;
		// 重新读入图片，注意这次要把options.inJustDecodeBounds 设为 false哦
		bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), options);
		if(bitmap==null) return null;
		int w = bitmap.getWidth();
		int h = bitmap.getHeight();
		System.out.println(w + "   " + h);
		return bitmap;
	}
	public static String parserAddress(String fullname){
		if(fullname==null)
			return null;
		int index = fullname.lastIndexOf("/");
		if(index<=0) return fullname;
		return fullname.substring(0,index);
	}
	/**
	 * 判断openfire用户的状态
	 *     strUrl : url格式 - http://my.openfire.com:9090/plugins/presence/status?jid=user1@my.openfire.com&type=xml
	 *    返回值 : 0 - 用户不存在; 1 - 用户在线; 2 - 用户离线
	 *  说明   ：必须要求 openfire加载 presence 插件，同时设置任何人都可以访问
	 */   
	public  static short IsUserOnLine(String strUrl)
	{
	    short            shOnLineState    = 0;    //-不存在-
	   
	    try
	    {
	        URL             oUrl     = new URL(strUrl);
	    URLConnection     oConn     = oUrl.openConnection();
	    if(oConn!=null)
	    {
	        BufferedReader     oIn = new BufferedReader(new InputStreamReader(oConn.getInputStream()));
	        if(null!=oIn)
	        {
	            String strFlag = oIn.readLine();
	            oIn.close();
	           
	            if(strFlag.toLowerCase().indexOf("type=\"unavailable\"")>=0)
	            {
	                shOnLineState = 2;
	            }
	            if(strFlag.toLowerCase().indexOf("type=\"error\"")>=0)
	            {
	                shOnLineState = 0;
	            }
	            else if(strFlag.toLowerCase().indexOf("priority")>=0 || strFlag.indexOf("id=\"")>=0)
	            {
	                shOnLineState = 1;
	            }
	        }
	    }
	    }
	    catch(Exception e)
	    {           
	    	e.printStackTrace();
	    }
	   
	    return     shOnLineState;
	} 
	public static String downloadFile(String folder,String url) throws IOException{
		if(url==null || url.startsWith("http:")==false) return null;
		
		String filename = CommonUtility.getFilename(url);
	    File file = new File(folder);
	    if(file.exists()==false)
	    	file.mkdirs();
	    
	    String filepath = folder+filename;
	    File todownload = new File(filepath);
	    if(todownload.exists())
	    	return todownload.getAbsolutePath();
	    
		URL u = null;
		try{
			u = new URL(url);
		}catch(MalformedURLException ex){
			ex.printStackTrace();
			return null;
		}
	    HttpURLConnection c = (HttpURLConnection) u.openConnection();
	    c.setRequestMethod("GET");
	    c.setDoOutput(true);
	    c.connect();
	    int cc =  c.getResponseCode();
	    if(cc!=200) return null;
	    
	    Log.d(TAG, filepath);
	    FileOutputStream f = new FileOutputStream(filepath);
	   
	    InputStream in = c.getInputStream();

	    byte[] buffer = new byte[1024];
	    int len1 = 0;
	    while ( (len1 = in.read(buffer)) > 0 ) {
	        f.write(buffer,0, len1);
	    }

	    f.close();
	    
	    return filepath;
		}
public static String downloadNoteattachFile(String folder,String url) throws IOException{
		
		URL u = null;
		try{
			u = new URL(url);
		}catch(MalformedURLException ex){
			ex.printStackTrace();
			return null;
		}
	    HttpURLConnection c = (HttpURLConnection) u.openConnection();
	    c.setRequestMethod("GET");
	    c.setDoOutput(true);
	    c.connect();
	    int cc =  c.getResponseCode();
	    if(cc!=200) return null;
	    String filename = CommonUtility.getAttachFilename(url);
	    File file = new File(folder);
	    if(file.exists()==false)
	    	file.mkdirs();
	    
	    String filepath = folder+filename;
	    Log.d(TAG, filepath);
	    FileOutputStream f = new FileOutputStream(filepath);
	   
	    InputStream in = c.getInputStream();

	    byte[] buffer = new byte[1024];
	    int len1 = 0;
	    while ( (len1 = in.read(buffer)) > 0 ) {
	        f.write(buffer,0, len1);
	    }

	    f.close();
	    
	    return filepath;
		}
public static String getAttachFilename(String url){
	String parts[] = url.split("name=");
	String filename = parts[parts.length-1];//.lastIndexOf("/")
	return filename;
	}

	public static String getFilename(String url){
		String parts[] = url.split("/");
		String filename = parts[parts.length-1];//.lastIndexOf("/")
		return filename;
		}
	
	  public static void move(Context cxt,String src,String dst) throws IOException{
			 //Path to the just created empty db, which I carefully closed so  have safe write access
			           OutputStream databaseOutputStream = new FileOutputStream(dst);
			           InputStream databaseInputStream = new FileInputStream(src);

			           byte[] buffer = new byte[1000];
			           int length;
			           while ( (length = databaseInputStream.read(buffer)) > 0 ) {
			                   databaseOutputStream.write(buffer);
			                   Log.w("Bytes: ", ((Integer)length).toString());
			                   Log.w("value", buffer.toString());
			           }

			           //Close the streams
			           databaseOutputStream.flush();
			           databaseOutputStream.close();
			           databaseInputStream.close(); 
		   }
	  
	  public static String getLocation(Context context){
		  int cid = 0;
			int lac = 0;
			int mcc = 0;
			int mnc = 0;
			TelephonyManager mTelephonyManager = (TelephonyManager) context
					.getSystemService(Context.TELEPHONY_SERVICE);
			if (mTelephonyManager == null) {
				return null;
			}
			GsmCellLocation mGsmCellLocation = (GsmCellLocation) mTelephonyManager.getCellLocation();
			if (mGsmCellLocation == null) {
				 cid = 7272;
				 lac = 28709;
			} else {
				cid = mGsmCellLocation.getCid();
				lac = mGsmCellLocation.getLac();
				
				mcc = Integer.valueOf(mTelephonyManager.getNetworkOperator().substring(0,3));
				mnc = Integer.valueOf(mTelephonyManager.getNetworkOperator().substring(3,5));
			}
			try {
				JSONObject holder = new JSONObject();
				holder.put("version", "1.1.0");
				holder.put("host", "maps.google.com");
				holder.put("request_address", true);
				JSONArray array = new JSONArray();
				JSONObject data = new JSONObject();
				data.put("cell_id", cid); // 25070
				data.put("location_area_code", lac);// 4474
				data.put("mobile_country_code", mcc);// 460
				data.put("mobile_network_code", mnc);// 0
				array.put(data);
				holder.put("cell_towers", array);
				DefaultHttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost("http://www.google.com/loc/json");
				StringEntity se = new StringEntity(holder.toString());
				post.setEntity(se);
				HttpResponse resp = client.execute(post);
				HttpEntity entity = resp.getEntity();
				BufferedReader br = new BufferedReader(new InputStreamReader(entity.getContent()));
				StringBuffer sb = new StringBuffer();
				String result = br.readLine();
				while (result != null) {
					sb.append(result);
					result = br.readLine();
				}
				String message=sb.toString();
				Log.v("ddddd=====", message);
				JSONObject jsonObject = new JSONObject(message.toString()); 
				JSONObject location = jsonObject.getJSONObject("location");
				double latitude = new Double(location.getString("latitude")).doubleValue();
				double longitude = new Double(location.getString("longitude")).doubleValue();
				Log.v("fffff=====", latitude+"");
				Log.v("ggggg=====", longitude+"");
				return latitude +","+longitude;
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
	  }
}
