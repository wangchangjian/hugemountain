package com.aozhi.hugemountain.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.aozhi.hugemountain.R;

/**
 * Created by ${wangchangjian} on 2016/5/4.
 */
public class ClickExtensionTextLayout extends FrameLayout {
    private final TextView tv_content;
    private final TextView tv_arror;
    private final TextView tv_title;
    private ViewGroup des_layout;

    public ClickExtensionTextLayout(Context context) {
        this(context, null);
    }

    public ClickExtensionTextLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        des_layout = (ViewGroup) LayoutInflater.from(this.getContext()).inflate(R.layout.click_extension_layout, this);
        tv_content = (TextView) des_layout.findViewById(R.id.dest_content);
        tv_arror = (TextView) des_layout.findViewById(R.id.tv_arrow);
        tv_title = (TextView) des_layout.findViewById(R.id.tv_title);
        tv_arror.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                expand();
            }
        });
    }

    public void setText(CharSequence title, CharSequence content) {
        if (title != null)
            tv_title.setText(title);
        if (content != null)
            tv_content.setText(content);
    }


    /**
     * 获取到界面的ScollView
     */
    public ScrollView getScrollView(View view) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) parent;
            if (group instanceof ScrollView) {
                return (ScrollView) group;
            } else {
                return getScrollView(group);
            }

        } else {
            return null;
        }

    }


    /**
     * 获取7行的高度
     *
     * @return
     */
    public int getShortMeasureHeight() {
        // 复制一个新的TextView 用来测量,最好不要在之前的TextView测量 有可能影响其它代码执行
        TextView textView = new TextView(this.getContext());
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);//设置字体大小14dp
        textView.setMaxLines(7);
        textView.setLines(0);// 强制有7行
        int width = tv_content.getMeasuredWidth(); // 开始宽度

        int widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(1000, MeasureSpec.AT_MOST);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }

    /**
     * 获取TextView 自己本身的高度
     *
     * @return
     */
    public int getLongMeasureHeight() {
        int width = tv_content.getMeasuredWidth(); // 开始宽度
        tv_content.getLayoutParams().height = RelativeLayout.LayoutParams.WRAP_CONTENT;// 高度包裹内容

        int widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY);
        int heightMeasureSpec = MeasureSpec.makeMeasureSpec(1000, MeasureSpec.AT_MOST);
        tv_content.measure(widthMeasureSpec, heightMeasureSpec);//
        return tv_content.getMeasuredHeight();
    }

    boolean flag=true;// true展开了 false 没有展开

    ScrollView scrollView;
//	scrollView.scrollTo(0, scrollView.getMeasuredHeight())


    private void expand() {
        scrollView = getScrollView(des_layout);
        int startHeight;
        int targetHeight;
        if (!flag) {
            flag = true;
            startHeight = getShortMeasureHeight();
            targetHeight = getLongMeasureHeight();
        } else {
            flag = false;
            startHeight = getLongMeasureHeight();
            targetHeight = getShortMeasureHeight();
        }
        final RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) tv_content.getLayoutParams();
        ValueAnimator animator = ValueAnimator.ofInt(startHeight, targetHeight);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                layoutParams.height = value;
                tv_content.setLayoutParams(layoutParams);
                scrollView.scrollTo(0, scrollView.getMeasuredHeight());// 让scrollView 移动到最下面
            }
        });
        animator.addListener(new Animator.AnimatorListener() {  // 监听动画执行
            //当动画开始执行的时候调用
            @Override
            public void onAnimationStart(Animator arg0) {

            }

            @Override
            public void onAnimationRepeat(Animator arg0) {

            }

            @Override
            public void onAnimationEnd(Animator arg0) {
                if (flag) {
                    tv_arror.setBackgroundResource(R.drawable.extension_text_arrow_up);
                } else {
                    tv_arror.setBackgroundResource(R.drawable.extension_text_arrow_down);
                }
            }

            @Override
            public void onAnimationCancel(Animator arg0) {

            }
        });
        animator.setDuration(500);//设置动画持续时间
        animator.start();
    }
}
