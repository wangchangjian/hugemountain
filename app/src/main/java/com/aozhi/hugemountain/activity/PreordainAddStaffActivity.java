package com.aozhi.hugemountain.activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import com.aozhi.hugemountain.R;

public class PreordainAddStaffActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preordainaddstaff);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.preordain_add_staff, menu);
		return true;
	}

}
