package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.adapter.OrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

public class AddOrderActivity extends BaseActivity implements OnClickListener{
	
	private Button addorder,btn_back;
	private ListView orderaddlist;
	private Spinner sp_order;
	private String[] order=new String[]{"全部","可追加订单","已付款订单"};
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
	private OrderAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addorder);
		initView();
		list=(ArrayList<OrderFormObject>) getIntent().getSerializableExtra("list");
		initListener();
	}

	@Override
	protected void initData() {

	}

	private void Spinner() {
		ArrayAdapter sp_adapter=new ArrayAdapter(this,R.layout.item_time, order);
		sp_order.setAdapter(sp_adapter);
		sp_order.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(sp_order.getSelectedItem().equals("全部")){
					
				}else if(sp_order.getSelectedItem().equals("可追加订单")){
					
				}else if(sp_order.getSelectedItem().equals("已付款订单")){
					
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private void initListener() {
		addorder.setOnClickListener(this);
		btn_back.setOnClickListener(this);
		Spinner();
		adapter = new OrderAdapter(AddOrderActivity.this,list);
		orderaddlist.setAdapter(adapter);
	}

	public void initView() {
		addorder=(Button) findViewById(R.id.addorder);
		orderaddlist=(ListView) findViewById(R.id.orderaddlist);
		sp_order=(Spinner) findViewById(R.id.sp_order);
		btn_back=(Button) findViewById(R.id.btn_back);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addorder:
			
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}
		
	}
}
