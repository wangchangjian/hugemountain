package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class ClientListObject {
	public Meta meta;
	public ArrayList<ClientObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ClientObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ClientObject> response) {
		this.response = response;
	}
}
