package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class InviteListObjectStaff {
	public Meta meta;
	public ArrayList<StaffObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<StaffObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<StaffObject> response) {
		this.response = response;
	}
}
