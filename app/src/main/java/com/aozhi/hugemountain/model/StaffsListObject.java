package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class StaffsListObject {
	public Meta meta;
	public ArrayList<ManageStaffObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ManageStaffObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ManageStaffObject> response) {
		this.response = response;
	}
}
