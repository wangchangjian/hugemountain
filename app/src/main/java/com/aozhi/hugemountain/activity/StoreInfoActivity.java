package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class StoreInfoActivity extends Activity implements OnClickListener {

	private TextView eidt, store_name, addr, worktime, servertype, store_phone,
			star;
	private TextView store_id;
	private ImageView store_img,viewPager;
	private ProgressDialog progressDialog = null;
	private LoginListObject mLoginListObject;
	private ImageView img1,img_avatar;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private ViewPager vp_mainadv;
	private int currentItem = 0;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_storeinfo);
		initView();
		initListener();
		getStoreInfo();
	}

	private void initListener() {
		eidt.setOnClickListener(this);
		img1.setOnClickListener(this);
		servertype.setOnClickListener(this);
	}

	private void initView() {
		img1 = (ImageView) findViewById(R.id.img1);
		store_img = (ImageView) findViewById(R.id.store_img);
		eidt = (TextView) findViewById(R.id.edit);
		store_name = (TextView) findViewById(R.id.store_name);
		addr = (TextView) findViewById(R.id.addr);
		worktime = (TextView) findViewById(R.id.worktime);
		servertype = (TextView) findViewById(R.id.servertype);
		store_phone = (TextView) findViewById(R.id.store_phone);
		star = (TextView) findViewById(R.id.star);
		store_id=(TextView) findViewById(R.id.store_id);
		img_avatar=(ImageView)findViewById(R.id.img_avatar);
		setImg(MyApplication.Storeuser.phone);
	}
	private void setImg(String path){
		if (path.equals("") || path == null) {
		} else {
			MyApplication.downloadImage.addTasks(path,
					img_avatar, new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								img_avatar.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								img_avatar.setBackgroundDrawable(drawable);
							}
						}
					});
		}
		MyApplication.downloadImage.doTask();
		
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			finish();
			break;
		case R.id.edit:
			Intent mIntent = new Intent(StoreInfoActivity.this,
					StoreEditActivity.class);
			mIntent.putExtra("list", list);
			startActivityForResult(mIntent, 20);
			break;
		default:
			break;
		}

	}

	// 获取商铺信息
	private void getStoreInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreInfo" };
		String[] name2 = new String[] { "store_id", MyApplication.Storeuser.id }; // 用户id
		params2.add(funParam2);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					getPhoto();
					setInfo();
				} else {
					Toast.makeText(StoreInfoActivity.this, "设置失败！",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(StoreInfoActivity.this, "获取失败！",
						Toast.LENGTH_LONG).show();
			}
		}

		private void setInfo() {
			store_id.setText(MyApplication.Storeuser.storephoto);
			store_name.setText(list.get(0).name);
			addr.setText(list.get(0).address);
			worktime.setText(list.get(0).business_hours);
			servertype.setText(list.get(0).service);
			store_phone.setText(list.get(0).mobile);
			switch (Integer.parseInt(list.get(0).star)) {
			case 0:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate0));
				break;
			case 1:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
				break;
			case 2:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
				break;
			case 3:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
				break;
			case 4:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
				break;
			case 5:
				star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
				break;
			default:
				break;
			}
			if (list.get(0).phone.equals("") || list.get(0).phone == null) {

			} else {
				MyApplication.downloadImage.addTasks(list.get(0).phone,
						store_img, new DownloadImage.ImageCallback() {

							@Override
							public void imageLoaded(Bitmap imageBitmap,
									String imageUrl) {
								if (imageBitmap != null) {
									// xHolder.tx_image.setImageBitmap(imageBitmap);
									// xHolder.tx_image.setTag("");
									Drawable drawable = new BitmapDrawable(
											imageBitmap);
									store_img.setBackgroundDrawable(drawable);
								}
							}

							@Override
							public void imageLoaded(Bitmap imageBitmap,
									DownloadImageMode callBackTag) {
								// TODO 自动生成的方法存根
								if (imageBitmap != null) {
									// xHolder.tx_image.setImageBitmap(imageBitmap);
									// xHolder.tx_image.setTag("");
									Drawable drawable = new BitmapDrawable(
											imageBitmap);
									store_img.setBackgroundDrawable(drawable);
								}
							}

						});
			}
			MyApplication.downloadImage.doTask();
		}

	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 20:
			Intent mIntent=new Intent(StoreInfoActivity.this,HomeStoreActivity.class);
			setResult(964, mIntent);
			finish();
			break;
		case RESULT_OK:

			break;
		default:
			break;
		}
	};

	// 图片轮播
	private void getPhoto() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getStoreImg" };
			String[] paramName = new String[] { "store_id", list.get(0).id };
			params.add(methodParam);
			params.add(paramName);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mImageListObject = JSON.parseObject(v, ImageListObject.class);
				imglist = mImageListObject.getResponse();
				if (imglist.size() > 0) {
					for (int n = 0; n < imglist.size(); n++) {
						final ImageView imgView = new ImageView(
								StoreInfoActivity.this);
						imgView.setClickable(true);
						imgView.setScaleType(ScaleType.FIT_XY);
						downloadImage.addTasks(imglist.get(n).img, imgView,
								new DownloadImage.ImageCallback() {
									public void imageLoaded(Bitmap imageBitmap,
											String imageUrl) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag(imageUrl);
										}
									}

									public void imageLoaded(Bitmap imageBitmap,
											DownloadImageMode callBackTag) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag("");
										}
									}
								});
						downloadImage.doTask();
						final String url = imglist.get(n).url;
						final String title = imglist.get(n).name;
						final String path=imglist.get(n).img;
						if (images.equals("")) {
							images = imglist.get(n).img;
						} else {
							images = images + "," + imglist.get(n).img;
						}

						imgView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent mIntent = new Intent(
										StoreInfoActivity.this,
										ImagePagerActivity.class);
								mIntent.putExtra("images", images);
								startActivity(mIntent);
							}
						});
						imgView.setOnLongClickListener(new OnLongClickListener() {
							
							@Override
							public boolean onLongClick(View v) {
								AlertDialog.Builder builder = new Builder(
										StoreInfoActivity.this);
								builder.setMessage("设置该图片为首页显示图片吗？");
								builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Intent mintent=new Intent(StoreInfoActivity.this,HomeStoreActivity.class);
										setStoreImg(path);
										setResult(964, mintent);
										dialog.dismiss();	
										StoreInfoActivity.this.finish();
									}
									
								});
								builder.setNegativeButton("取消",
										new DialogInterface.OnClickListener() {
	
											@Override
											public void onClick(
													DialogInterface dialog,
													int which) {
												dialog.dismiss();
	
											}
										});
								builder.create().show();
//								setStoreImg(path);
								
								return true;
							}
						});
						imgViews.add(imgView);

					}
					vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
					vp_mainadv.setAdapter(new MyAdapter());
					// 设置监听，当ViewPager中的页面改变时调用
					vp_mainadv
							.setOnPageChangeListener(new MyPageChangeListener());

				}
			}
		}

	};

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imglist.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, final int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {

		}

		@Override
		public void onPageSelected(int arg0) {
			// for(int i=0;i<imglist.size();i++){
			// if(i!=arg0){
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_normal);
			// }else{
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_focused);
			// }
			// }
		}
	}

	private void setStoreImg(String path) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "setImgPath" };
			String[] paramName = new String[] { "id",list.get(0).id};  //店铺id
			String[] paramName1=new String[] {"path",path};
			params.add(methodParam);
			params.add(paramName);
			params.add(paramName1);
			new HttpConnection().get(Constant.URL, params,
					getpath_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getpath_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Toast.makeText(StoreInfoActivity.this, "设置成功", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(StoreInfoActivity.this, "设置失败", Toast.LENGTH_SHORT).show();
			}
		}
	};
}
