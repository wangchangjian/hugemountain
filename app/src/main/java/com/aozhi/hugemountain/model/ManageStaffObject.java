package com.aozhi.hugemountain.model;

public class ManageStaffObject {
	public String id;
	public String types;
	public String name;
	public String remark;
	public String money;
	public String store_id;
	public String staff_id;
	public String create_time;
	public String photo;
	public String del_flag;
	public String update_time;
	public String mark;	
	public String code_id;
	public String parent_id;
	public String icoUrl;
	public String service;
	public String phone;
	public String commission;
	public String service_time;
	public String click_count;
	public String seller_id;
	public String store_ser;
}
