package com.aozhi.hugemountain.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import cn.smssdk.gui.RegisterPage;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Utils;

public class LoginStoreActivity extends Activity {
	Button btn_login,btn_regist,btn_back;
	View tv_findpwd;
//	private boolean ok;
	private EditText login_edit_account, login_edit_pwd;
//	private CheckBox cb;
	static String name, pwd;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private ProgressDialog progressDialog = null;
	private Dialog pd;
	// 填写从短信SDK应用后台注册得到的APPKEY
	private static String APPKEY = "a60f6c4438e8";
	// 填写从短信SDK应用后台注册得到的APPSECRET
	private static String APPSECRET = "35b4d102dd365758244b77426d483d70";
	// 短信注册，随机产生头像
	private static final String[] AVATARS = {
			"http://tupian.qqjay.com/u/2011/0729/e755c434c91fed9f6f73152731788cb3.jpg",
			"http://99touxiang.com/public/upload/nvsheng/125/27-011820_433.jpg",
			"http://img1.touxiang.cn/uploads/allimg/111029/2330264224-36.png",
			"http://img1.2345.com/duoteimg/qqTxImg/2012/04/09/13339485237265.jpg",
			"http://diy.qqjay.com/u/files/2012/0523/f466c38e1c6c99ee2d6cd7746207a97a.jpg",
			"http://img1.touxiang.cn/uploads/20121224/24-054837_708.jpg",
			"http://img1.touxiang.cn/uploads/20121212/12-060125_658.jpg",
			"http://img1.touxiang.cn/uploads/20130608/08-054059_703.jpg",
			"http://diy.qqjay.com/u2/2013/0422/fadc08459b1ef5fc1ea6b5b8d22e44b4.jpg",
			"http://img1.2345.com/duoteimg/qqTxImg/2012/04/09/13339510584349.jpg",
			"http://img1.touxiang.cn/uploads/20130515/15-080722_514.jpg",
			"http://diy.qqjay.com/u2/2013/0401/4355c29b30d295b26da6f242a65bcaad.jpg" };
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_loginstore);
		SMSSDK.initSDK(this, APPKEY, APPSECRET);  //设置短息验证的应用
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
		btn_login = (Button)findViewById(R.id.btn_login);
		btn_login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (login_edit_account.getText().toString().equals("")) {
					Toast.makeText(LoginStoreActivity.this, "请输入手机号！", Toast.LENGTH_LONG).show();
				} else if (login_edit_account.getText().toString().trim().length()!=11) {
					Toast.makeText(LoginStoreActivity.this, "请输入正确格式的电话号码！", Toast.LENGTH_LONG).show();
				} else if (login_edit_pwd.getText().toString().equals("")) {
					Toast.makeText(LoginStoreActivity.this, "请输入密码！", Toast.LENGTH_LONG).show();
				} else if(!MyApplication.ifreg){
					Toast.makeText(LoginStoreActivity.this,
							"对不起，功能受限，请联系服务商", Toast.LENGTH_LONG).show();
				}else {
					name = login_edit_account.getText().toString();
					pwd = login_edit_pwd.getText().toString();
					getlogin();
				}
			}
		});
		tv_findpwd = (View)findViewById(R.id.tv_findpwd);
		tv_findpwd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(LoginStoreActivity.this, FindPwdStore1Activity.class);
				startActivity(intent);
			}
		});
		btn_regist = (Button) findViewById(R.id.btn_regist);
		btn_regist.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// 打开注册页面
				RegisterPage registerPage = new RegisterPage();
				registerPage.setRegisterCallback(new EventHandler() {
					public void afterEvent(int event, int result,
							Object data) {
						// 解析注册结果
						if (result == SMSSDK.RESULT_COMPLETE) {
							@SuppressWarnings("unchecked")
							HashMap<String, Object> phoneMap = (HashMap<String, Object>) data;
							String country = (String) phoneMap
									.get("country");
							String phone = (String) phoneMap.get("phone");
							// 提交用户信息
							registerUser(country, phone);
						}
					}
				});
				MyApplication.Status="store";
				registerPage.show(getBaseContext());
/*			Intent intent = new Intent(LoginStoreActivity.this,
//						RegiststoreActivity.class);
//				startActivity(intent); */
				finish();
			}
		});
//		cb=(CheckBox)findViewById(R.id.cb);
		login_edit_account=(EditText)findViewById(R.id.login_edit_account);
		login_edit_pwd=(EditText)findViewById(R.id.login_edit_pwd);
		
		SharedPreferences sharedPreferences = getSharedPreferences("store_user",Activity.MODE_PRIVATE);
		String name = sharedPreferences.getString("name", "");
		String pwd = sharedPreferences.getString("pwd", "");
		if(!name.equals("")&&!name.equals(null)){
			login_edit_account.setText(name);
			login_edit_pwd.setText(pwd);
		}
//		ok = sharedPreferences.getBoolean("ok", ok);
//		cb.setChecked(ok);
//		if (!name.equals("") || !name.equals(null)) {
//			login_edit_account.setText(name);
//			login_edit_pwd.setText(pwd);
//		}
	}
	private void getlogin() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstore_login" };
		String[] name = new String[] { "name",login_edit_account.getText().toString() };
		String[] pwd = new String[] { "pwd",encryption(login_edit_pwd.getText().toString()) };

		params2.add(funParam2);
		params2.add(name);
		params2.add(pwd);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();

				if (list.size() > 0) {
//					if (cb.isChecked()) {
						SharedPreferences mySharedPreferences = getSharedPreferences(
								"store_user", Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = mySharedPreferences
								.edit();
						editor.putString("name", login_edit_account.getText()
								.toString());
						editor.putString("pwd", login_edit_pwd.getText()
								.toString());
//						editor.putBoolean("ok", true);
						editor.commit();
//					} else {
//						SharedPreferences mySharedPreferences = getSharedPreferences("store_user", Activity.MODE_PRIVATE);
//						SharedPreferences.Editor editor = mySharedPreferences.edit();
//						editor.putString("name", "");
//						editor.putString("pwd", "");
////						editor.putBoolean("ok", false);
//						editor.commit();
//					}
						MyApplication.isstaff=true;
					MyApplication.Storeuser = list.get(0);
					MyApplication.Status="store";
					Intent mintent = new Intent(LoginStoreActivity.this,HomeStoreActivity.class);
					
					startActivity(mintent);
					finish();
				} else {
					Toast.makeText(LoginStoreActivity.this, "登录失败！",Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(LoginStoreActivity.this, "登录失败！", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}

	// 提交用户信息
	private void registerUser(String country, String phone) {
		Random rnd = new Random();
		int id = Math.abs(rnd.nextInt());
		String uid = String.valueOf(id);
		String nickName = "SmsSDK_User_" + uid;
		String avatar = AVATARS[id % 12];
		SMSSDK.submitUserInfo(uid, nickName, avatar, country, phone);
	}
}
