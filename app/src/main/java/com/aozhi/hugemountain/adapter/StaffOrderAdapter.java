package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.OrderFormObject;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StaffOrderAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
	public StaffOrderAdapter(Context ctx, ArrayList<OrderFormObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_orderform, null);
//			mHolder.img= (ImageView) convertView.findViewById(R.id.img);
			mHolder.remark=(TextView) convertView.findViewById(R.id.remark);
			mHolder.create_time=(TextView) convertView.findViewById(R.id.create_time);
			mHolder.pay_manoy=(TextView) convertView.findViewById(R.id.pay_manoy);
			mHolder.tv_states=(TextView) convertView.findViewById(R.id.tv_states);
			
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.remark.setText(list.get(position).order_id);
		mHolder.create_time.setText(list.get(position).create_time);
		mHolder.pay_manoy.setText("￥"+list.get(position).pay_manoy);
//		convertView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//			}
//		});
		
		if(list.get(position).orderstatus.equals("10")){
			if(list.get(position).orderstatus1.equals("40")){
				mHolder.tv_states.setText("待确认收现");
			}else{
				mHolder.tv_states.setText("待支付");
			}
		}
		return convertView;
	}
	
	class Holder {
		ImageView img;
		TextView remark,create_time,pay_manoy,tv_states;
		TextView tv_miaoshu;
	}
}
