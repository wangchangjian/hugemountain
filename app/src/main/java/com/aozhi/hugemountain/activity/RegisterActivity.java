package com.aozhi.hugemountain.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

import com.aozhi.hugemountain.model.*;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.text.TextWatcher;

public class RegisterActivity extends Activity {
    Button btn_ok, btn_yanzhengma, btn_back;
    TextView text_xieyi, tv_type, tv_kehu, tv_shanghu, tv_yuangong, tv_ts;
    private ProgressDialog progressDialog = null;
    private String types = "1";
    private EditText et_username, et_pwd, et_storename, et_yanzhengma, et_staffname, et_staffage;
    private LoginListObject mLoginListObject;
    private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
    private LoginListObject userListObject;
    private ArrayList<LoginBean> userlist = new ArrayList<LoginBean>();
    private CheckBox cb;
    private LinearLayout layout_name, layout_staff;
    // 填写从短信SDK应用后台注册得到的APPKEY
    private static String APPKEY = "a60f6c4438e8";
    // 填写从短信SDK应用后台注册得到的APPSECRET
    private static String APPSECRET = "35b4d102dd365758244b77426d483d70";
    private TimeCount time;
    private RadioGroup sex;
    private RadioButton sex1, sex2;
    private String number = "";
    private String xingb = "女";
    private int resultCode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_regist);
        initView();
        initListener();
        time = new TimeCount(60000, 1000);// 构造CountDownTimer对象
        cn.smssdk.SMSSDK.initSDK(RegisterActivity.this, APPKEY, APPSECRET);
        EventHandler eh = new EventHandler() {

            @Override
            public void afterEvent(int event, int result, Object data) {

                Message msg = new Message();
                msg.arg1 = event;
                msg.arg2 = result;
                msg.obj = data;
                handler.sendMessage(msg);
            }

        };
        SMSSDK.registerEventHandler(eh);
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
            int event = msg.arg1;
            int result = msg.arg2;
            Object data = msg.obj;
            Log.e("event", "event=" + event);
            if (result == SMSSDK.RESULT_COMPLETE) {
                // 短信注册成功后，返回MainActivity,然后提示新好友
                if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {// 提交验证码成功
                    Toast.makeText(getApplicationContext(), "提交验证码成功",
                            Toast.LENGTH_SHORT).show();

                } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                    Toast.makeText(getApplicationContext(), "验证码已经发送",
                            Toast.LENGTH_SHORT).show();

                } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {// 返回支持发送验证码的国家列表
                    Toast.makeText(getApplicationContext(), "获取国家列表成功",
                            Toast.LENGTH_SHORT).show();

                }
            } else {
                ((Throwable) data).printStackTrace();
                // int resId = getStringRes(RegistActivity.this,
                // "smssdk_network_error");
                // Toast.makeText(RegistActivity.this, "验证码错误",
                // Toast.LENGTH_SHORT).show();
                // if (resId > 0) {
                // Toast.makeText(RegistActivity.this, resId,
                // Toast.LENGTH_SHORT).show();
                // }
            }

        }

    };

    private void initView() {
        btn_ok = (Button) findViewById(R.id.btn_ok);
        btn_back = (Button) findViewById(R.id.btn_back);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        et_storename = (EditText) findViewById(R.id.et_storename);
        et_username = (EditText) findViewById(R.id.et_username);
        text_xieyi = (TextView) findViewById(R.id.text_xieyi);
        btn_yanzhengma = (Button) findViewById(R.id.btn_yanzhengma);
        et_yanzhengma = (EditText) findViewById(R.id.et_yanzhengma);
        cb = (CheckBox) findViewById(R.id.cb);
        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_ts = (TextView) findViewById(R.id.tv_ts);
        layout_name = (LinearLayout) findViewById(R.id.layout_name);
        layout_staff = (LinearLayout) findViewById(R.id.layout_staff);
        et_staffname = (EditText) findViewById(R.id.et_staffname);
        et_staffage = (EditText) findViewById(R.id.et_staffage);
        sex = (RadioGroup) findViewById(R.id.sex);
        sex1 = (RadioButton) findViewById(R.id.sex1);
        sex2 = (RadioButton) findViewById(R.id.sex2);
        sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup arg0, int arg1) {
                switch (arg1) {
                    case R.id.sex1:
                        xingb = "男";
                        break;
                    case R.id.sex2:
                        xingb = "女";
                        break;
                    default:
                        break;
                }
            }
        });

        et_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                getuserbyphone();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    /* 定义一个倒计时的内部类 */
    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
        }

        public void onFinish() {// 计时完毕时触发
            btn_yanzhengma.setText("重新验证");
            btn_yanzhengma.setClickable(true);
        }

        public void onTick(long millisUntilFinished) {// 计时过程显示
            btn_yanzhengma.setClickable(false);
            btn_yanzhengma.setText(millisUntilFinished / 1000 + "秒");
        }
    }

    private void initListener() {
        tv_type.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                showAtaver();
                tv_ts.setVisibility(View.GONE);
            }
        });
        btn_yanzhengma.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if (et_username.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "请输入电话号码",
                            Toast.LENGTH_SHORT).show();
                } else if (et_username.getText().toString().length() > 11
                        || et_username.getText().toString().length() < 11) {

                    Toast.makeText(RegisterActivity.this, "电话号码输入有误，请重新输入",
                            Toast.LENGTH_SHORT).show();
                } else {
                    getuser();
                }
            }
        });
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                backToLogin();
                finish();
            }
        });

        text_xieyi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Intent mIntent = new Intent(this,XieYiActivity.class);
                // startActivity(mIntent);
            }
        });

        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (et_username.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "请输入手机号码！",
                            Toast.LENGTH_LONG).show();
                } else if (et_yanzhengma.getText().toString().equals("")) {
                    Toast.makeText(RegisterActivity.this, "请输入验证码！",
                            Toast.LENGTH_LONG).show();
                } else if (et_pwd.getText().toString().length() < 6) {
                    Toast.makeText(RegisterActivity.this, "密码不得少于6位！",
                            Toast.LENGTH_LONG).show();
                } else if (!cb.isChecked()) {
                    Toast.makeText(RegisterActivity.this, "请同意巨岳注册协议！",
                            Toast.LENGTH_LONG).show();
                } else {

                    getregister();
                }
            }
        });

    }

    private void backToLogin() {
        Intent mIntent = new Intent();
        mIntent.putExtra("types", "");
        setResult(0, mIntent);
    }

    private void yangzhengma() {
        if (!et_username.getText().toString().equals("")) {
            if (et_username.getText().toString().length() > 11
                    || et_username.getText().toString().length() < 11) {

                Toast.makeText(RegisterActivity.this,
                        getString(R.string.tel_error), Toast.LENGTH_SHORT)
                        .show();
            } else {
                time.start();// 开始计时
                // num = String.valueOf((int) ((Math.random() * 9 + 1) *
                // 100000));
                // Toast.makeText(RegistActivity.this, "验证码" + num,
                // Toast.LENGTH_SHORT).show();

                EventHandler eh = new EventHandler() {

                    @Override
                    public void afterEvent(int event, int result, Object data) {

                        if (result == SMSSDK.RESULT_COMPLETE) {
                            // 回调完成
                            if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
                                // 提交验证码成功
                            } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                                // 获取验证码成功
                            } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
                                // 返回支持发送验证码的国家列表
                            }
                        } else {
                            ((Throwable) data).printStackTrace();
                        }
                    }
                };
                SMSSDK.registerEventHandler(eh);

                SMSSDK.getVerificationCode("86", et_username.getText()
                        .toString());
            }
        } else {
            Toast.makeText(RegisterActivity.this, getString(R.string.tel),
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void start_check_yanzhengma() {
        SMSSDK.submitVerificationCode("86", et_username.getText().toString(),
                et_yanzhengma.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SMSSDK.unregisterAllEventHandler();
    }

    @Override
    public void onBackPressed() {
        backToLogin();
        super.onBackPressed();
    }

    private void showAtaver() {
        final AlertDialog dlg = new AlertDialog.Builder(this).create();
        dlg.show();
        Window window = dlg.getWindow();
        window.setContentView(R.layout.item_typess);
        tv_kehu = (TextView) window.findViewById(R.id.tv_kehu);
        tv_shanghu = (TextView) window.findViewById(R.id.tv_shanghu);
        tv_yuangong = (TextView) window.findViewById(R.id.tv_yuangong);
        tv_kehu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                types = "1";
                layout_name.setVisibility(View.GONE);
                layout_staff.setVisibility(View.GONE);
                tv_type.setText("客户注册");
                dlg.cancel();
            }
        });
        tv_shanghu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                types = "2";
                layout_name.setVisibility(View.VISIBLE);
                layout_staff.setVisibility(View.GONE);
                tv_type.setText("商户注册");
                dlg.cancel();
            }
        });
        tv_yuangong.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                types = "3";
                layout_staff.setVisibility(View.VISIBLE);
                layout_name.setVisibility(View.GONE);
                tv_type.setText("员工注册");
                dlg.cancel();
            }
        });
    }

    String fun = "";

    private void getuser() {
        ArrayList<String[]> params1 = new ArrayList<String[]>();
//		if (types.equals("1")) {
//			fun = "selectclientbyphoto";
//		} else {
//			fun = "selectstorebyphoto";
//		}
        String[] funParam2 = null;
        String[] name1 = null;
        if (types.equals("1")) {
            funParam2 = new String[]{"fun", "getuserclientbyphone"};
            name1 = new String[]{"clientphoto", et_username.getText().toString()};
        } else if (types.equals("2")) {
            funParam2 = new String[]{"fun", "getuserstorebyphone"};
            name1 = new String[]{"storephoto", et_username.getText().toString()};
        } else {
            funParam2 = new String[]{"fun", "getuserstaffbyphone"};
            name1 = new String[]{"staffphoto", et_username.getText().toString()};
        }
        params1.add(funParam2);
        params1.add(name1);
//		String[] funParam1 = new String[] { "fun", fun };
//		String[] funParam3 = new String[] { "photo",
//				et_username.getText().toString() };
//		params1.add(funParam1);
//		params1.add(funParam3);
        Constant.NET_STATUS = Utils.getCurrentNetWork(RegisterActivity.this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params1,
                    user_callbackListener);
        } else {
            Toast.makeText(RegisterActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private CallbackListener user_callbackListener = new HttpConnection.CallbackListener() {

        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mLoginListObject = JSON.parseObject(v, LoginListObject.class);
                list = mLoginListObject.getResponse();
                if (list.size() > 0) {
                    Toast.makeText(RegisterActivity.this, "登录账号已存在，请重新输入",
                            Toast.LENGTH_LONG).show();
                } else {
                    yangzhengma();
                }

            }
        }

    };

    private void getregister() {

        if (types.equals("1")) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] funParam1 = new String[]{"fun", "registclient"};
            String[] funParam2 = new String[]{"photo",
                    et_username.getText().toString()};
            String[] funParam3 = new String[]{"img", "/picture/u11.png"};
            String[] funParam4 = new String[]{"pwd",
                    encryption(et_pwd.getText().toString())};
            params.add(funParam1);
            params.add(funParam2);
            params.add(funParam3);
            params.add(funParam4);
            progressDialog = ProgressDialog.show(this,
                    getString(R.string.app_name),
                    getString(R.string.tv_dialog_context), false);
            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params,
                        type_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        } else if (types.equals("2")) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] funParam1 = new String[]{"fun", "registstore"};
            String[] funParam2 = new String[]{"photo",
                    et_username.getText().toString()};
            String[] funParam3 = new String[]{"phone", "/picture/store11.png"}; // 系统默认头像
            String[] funParam4 = new String[]{"pwd",
                    encryption(et_pwd.getText().toString())};
            String[] funParam5 = new String[]{"name",
                    et_storename.getText().toString().trim()};
            params.add(funParam1);
            params.add(funParam2);
            params.add(funParam3);
            params.add(funParam4);
            params.add(funParam5);
            progressDialog = ProgressDialog.show(this,
                    getString(R.string.app_name),
                    getString(R.string.tv_dialog_context), false);
            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params,
                        type_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        } else {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] funParam1 = new String[]{"fun", "registstaff"};
            String[] funParam2 = new String[]{"photo",
                    et_username.getText().toString()};
            String[] funParam3 = new String[]{"img", "/picture/u11.png"};
            String[] funParam4 = new String[]{"pwd",
                    encryption(et_pwd.getText().toString())};
            String[] funParam5 = new String[]{"name",
                    et_staffname.getText().toString().trim()};
            String[] funParam6 = new String[]{"sex", xingb};
            String[] funParam7 = new String[]{"age",
                    et_staffage.getText().toString().trim()};
            params.add(funParam1);
            params.add(funParam2);
            params.add(funParam3);
            params.add(funParam4);
            params.add(funParam5);
            params.add(funParam6);
            params.add(funParam7);
            progressDialog = ProgressDialog.show(this,
                    getString(R.string.app_name),
                    getString(R.string.tv_dialog_context), false);
            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params,
                        type_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        }


    }

    private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mLoginListObject = JSON.parseObject(v, LoginListObject.class);
                if (mLoginListObject.getMeta().msg.equals("OK")) {
                    Toast.makeText(RegisterActivity.this, "注册成功",
                            Toast.LENGTH_LONG).show();
                    if (types.equals("1")) {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "client_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", et_username.getText()
                                .toString());
                        editor.putString("pwd", et_pwd.getText().toString());
                        editor.putBoolean("ok", true);
                        editor.commit();
                    } else if (types.equals("2")) {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "store_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", et_username.getText()
                                .toString());
                        editor.putString("pwd", et_pwd.getText().toString());
                        editor.putBoolean("ok", true);
                        editor.commit();
                    } else {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "staff_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", et_username.getText()
                                .toString());
                        editor.putString("pwd", et_pwd.getText().toString());
                        editor.putBoolean("ok", true);
                        editor.commit();
                    }
//					Intent mintent = new Intent(RegisterActivity.this,
//							LoginsActivity.class);
//					mintent.putExtra("types", types);
//					startActivity(mintent);
//				


                    Intent mIntent = new Intent();
                    mIntent.putExtra("types", types);
                    // 设置结果，并进行传送
                    setResult(resultCode, mIntent);
                    finish();
                } else {
                    Toast.makeText(RegisterActivity.this, "注册失败",
                            Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(RegisterActivity.this, "注册失败", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    public static String encryption(String plainText) {
        String re_md5 = new String();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }

            re_md5 = buf.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return re_md5;
    }

    //判断是否已注册

    /**
     * SELECT * FROM user_store WHERE storephoto='{storephoto}';
     * SELECT * FROM user_client WHERE clientphoto='{clientphoto}';
     * SELECT * FROM user_staff WHERE staffphoto='{staffphoto}';
     */
    private void getuserbyphone() {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = null;
        String[] name1 = null;
        if (types.equals("1")) {
            funParam2 = new String[]{"fun", "getuserclientbyphone"};
            name1 = new String[]{"clientphoto", et_username.getText().toString()};
        } else if (types.equals("2")) {
            funParam2 = new String[]{"fun", "getuserstorebyphone"};
            name1 = new String[]{"storephoto", et_username.getText().toString()};
        } else {
            funParam2 = new String[]{"fun", "getuserstaffbyphone"};
            name1 = new String[]{"staffphoto", et_username.getText().toString()};
        }
        params2.add(funParam2);
        params2.add(name1);
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    getuserbyphone_callbackListener2);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener getuserbyphone_callbackListener2 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            if (!v.equals("fail")) {
                userListObject = JSON.parseObject(v, LoginListObject.class);
                userlist = userListObject.response;
                if (userlist.size() > 0) {
                    Toast.makeText(RegisterActivity.this, "该手机号已被注册，请重新输入", Toast.LENGTH_LONG).show();
                }
            } else {
            }
        }
    };

}
