package com.aozhi.hugemountain.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ShowPhotoAdapter;
import com.aozhi.hugemountain.adapter.ShowVideoAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.list.NoScrollGridView;
import com.aozhi.hugemountain.model.StaffPhotoListObject;
import com.aozhi.hugemountain.model.StaffPhotoObject;
import com.aozhi.hugemountain.model.StaffVideoListObject;
import com.aozhi.hugemountain.model.StaffVideoObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowActivity extends Activity  implements OnClickListener{
	private Button btn_back;
	private TextView t1;
	private TextView t2,tv_camera,tv_photo;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;
	public static final int PHOTOZOOM = 2; 
	public static final int PHOTORESOULT = 3;
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String imgload="";
	private String serverFile = "";
	
	private ProgressDialog progressDialog = null;
	private NoScrollGridView grid_view1;
	private ShowPhotoAdapter mShowphotoAdapter;
	private StaffPhotoObject mStaffPhotoObject;
	private StaffPhotoListObject mStaffPhotoListObject;
	private ArrayList<StaffPhotoObject> list_photo = new ArrayList<StaffPhotoObject>();
	private NoScrollGridView grid_view2;
	private ShowVideoAdapter mShowVideoAdapter;
	private StaffVideoObject mStaffVideoObject;
	private StaffVideoListObject mStaffVideoListObject;
	private ArrayList<StaffVideoObject> list_video = new ArrayList<StaffVideoObject>();

	private ImageView img_avatar;
	private TextView tv_name, tv_remark,show_add;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_show);
		initView();
		initListener();
		myclievk();

	}

	private void initView() {
		grid_view1 = (NoScrollGridView) findViewById(R.id.grid_view1);
		grid_view2 = (NoScrollGridView) findViewById(R.id.grid_view2);
		mShowphotoAdapter = new ShowPhotoAdapter(this, list_photo);
		grid_view1.setAdapter(mShowphotoAdapter);
		grid_view2.setAdapter(mShowVideoAdapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		t1 = (TextView) findViewById(R.id.tv_service);
		t2 = (TextView) findViewById(R.id.tv_staff);
		show_add=(TextView) findViewById(R.id.show_add);
		img_avatar = (ImageView) findViewById(R.id.img_avatar);
		tv_name = (TextView) findViewById(R.id.tv_name);
		tv_remark = (TextView) findViewById(R.id.tv_remark);
		tv_name.setText(MyApplication.Staffuser.name);
		tv_remark.setText(MyApplication.Staffuser.remark);
		SetAvatar();
	}

	private void SetAvatar() {
		if (MyApplication.Staffuser.avatar.equals("")
				|| MyApplication.Staffuser.avatar == null) {

		} else {
			MyApplication.downloadImage.addTasks(
					MyApplication.Staffuser.avatar, img_avatar,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");

//								Bitmap bmBitmap = Utils
//										.toRoundBitmap(imageBitmap);
								Drawable drawable = new BitmapDrawable(imageBitmap);
								img_avatar.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
//								Bitmap bmBitmap = Utils
//										.toRoundBitmap(imageBitmap);
								Drawable drawable = new BitmapDrawable(imageBitmap);
								img_avatar.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
	}

	private void initListener() {
		show_add.setOnClickListener(this);
		GetPhotoList();
	}

	private void GetPhotoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getstaffphoto" };
			String[] name = new String[] { "staff_id",
					MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name);
//			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
//			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener1);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
			if (!result.equals("fail")) {
				mStaffPhotoListObject = JSON.parseObject(result,
						StaffPhotoListObject.class);
				list_photo = mStaffPhotoListObject.response;
				if (mStaffPhotoListObject.meta.getMsg().equals("OK")) {
					if (list_photo.size() > 0) {
						mShowphotoAdapter = new ShowPhotoAdapter(
								ShowActivity.this, list_photo);
						grid_view1.setAdapter(mShowphotoAdapter);
					} else {
						Toast.makeText(ShowActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	private void GetVideoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getstaffvideo" };
			String[] name = new String[] { "staff_id",
					MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener2);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffVideoListObject = JSON.parseObject(result,
						StaffVideoListObject.class);
				list_video = mStaffVideoListObject.response;
				if (mStaffVideoListObject.meta.getMsg().equals("OK")) {
					if (list_video.size() > 0) {
						mShowVideoAdapter = new ShowVideoAdapter(
								ShowActivity.this, list_video);
						grid_view2.setAdapter(mShowVideoAdapter);
					} else {
						Toast.makeText(ShowActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	private void myclievk() {
		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t1.setTextColor(getResources().getColor(R.color.y1));
				t2.setTextColor(getResources().getColor(R.color.n1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg1));
				t2.setBackgroundColor(getResources().getColor(R.color.bg2));
				grid_view1.setVisibility(View.VISIBLE);
				grid_view2.setVisibility(View.GONE);
			}
		});

		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				GetVideoList();
				t2.setTextColor(getResources().getColor(R.color.y1));
				t1.setTextColor(getResources().getColor(R.color.n1));
				t2.setBackgroundColor(getResources().getColor(R.color.bg1));
				t1.setBackgroundColor(getResources().getColor(R.color.bg2));
				grid_view2.setVisibility(View.VISIBLE);
				grid_view1.setVisibility(View.GONE);
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		
	
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.show_add:
//			Intent mIntent=new Intent(ShowActivity.this,ShowEditActivity.class);
//			startActivityForResult(mIntent, 136);
			showAtaver();
			break;
		default:
			break;
		}
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		
		//-------------\
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode == NONE)
			return;
		if (requestCode == PHOTOHRAPH) {
			File picture = new File(Environment.getExternalStorageDirectory()
					+ "/temp.jpg");
			System.out.println("------------------------" + picture.getPath());
			startPhotoZoom(Uri.fromFile(picture));
		}
		if (data == null)
			return;
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 -

				new DateFormat();
				String name = DateFormat.format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA)) + ".jpg";
				Bundle bundle = data.getExtras();
				Bitmap bitmap = (Bitmap) bundle.get("data");
				FileOutputStream b = null;
				File file = new File("/sdcard/myImage/");
				file.mkdirs();
				String fileName = "/sdcard/myImage/" + name;
				try {
					b = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				File file1 = new File(fileName);
				
//				grid_view1.setImageBitmap(photo);

				new UploadAsyncTask().execute(file1);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
		//------------------
		
//		super.onActivityResult(requestCode, resultCode, data);
//		switch (resultCode) {
//		case 136:
//			GetPhotoList();
//			break;
//
//		default:
//			break;
//		}
	}
	
	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.activity_xiugai);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						Environment.getExternalStorageDirectory(), "temp.jpg")));
				System.out.println("============="
						+ Environment.getExternalStorageDirectory());
				startActivityForResult(intent, PHOTOHRAPH);
				dlg.cancel();
			}
		});
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
//				intent.setDataAndType(
//						MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//						IMAGE_UNSPECIFIED);
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTOZOOM);
				dlg.cancel();
			}
		});
	}
	
	public void startPhotoZoom(Uri uri) {
		Intent mintent = new Intent("com.android.camera.action.CROP");
		mintent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		mintent.putExtra("crop", "true");
		mintent.putExtra("aspectX", 1);
		mintent.putExtra("aspectY", 1);
		mintent.putExtra("outputX", 320);
		mintent.putExtra("outputY", 320);
		mintent.putExtra("return-data", true);
		startActivityForResult(mintent, PHOTORESOULT);
	}
	
	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			imgload=serverFile;
			upPhotoList();
			return serverFile;
		}
	}
	
	private void upPhotoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upStaffPhoto" };
			String[] name0=new String[] {"name",""};
			String[] name1=new String[] {"path",serverFile};
			String[] name2 = new String[] { "staff_id",MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name0);
			params.add(name1);
			params.add(name2);
//			progressDialog = ProgressDialog.show(this, null, "正在加载。。。", false);
//			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainLists_callbackListener1);
		} else
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainLists_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
			if (!result.equals("fail")) {
				mStaffPhotoListObject = JSON.parseObject(result,
						StaffPhotoListObject.class);
				list_photo = mStaffPhotoListObject.response;
				if (mStaffPhotoListObject.meta.getMsg().equals("OK")) {
//						Intent mintent=new Intent(ShowActivity.this,ShowActivity.class);
//						setResult(136,mintent);
//						finish();
					GetPhotoList();
				} else {
					Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(ShowActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
}
