package com.aozhi.hugemountain.activity.ConsumerActivity;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.activity.PreordainPayActivity;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.activity.RoomActivity;
import com.aozhi.hugemountain.adapter.DateAdapter;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.SpecialCalendar;
import com.aozhi.hugemountain.adapter.TimesAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.model.RoomListObject;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.ApplicationTopView;
import com.aozhi.hugemountain.view.OnTopClickListener;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.GestureDetector.OnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.TextView;
import android.widget.ViewFlipper;

public class ChooseDatetimeActivity extends BaseActivity implements OnGestureListener {
	CheckBox cb_choosetime1, cb_choosetime2, cb_choosetime3, cb_choosetime4;
	private String storeid;
	Button btn_dateafter, btn_datebefor, btn_next, chooseroom;
	ApplicationTopView applicationTopView;
	TextView tv_year, tv_month, tv_day, tv_time;
	Date date_show, date;
	private ProgressDialog progressDialog2 = null;
	OrderFormObject mOrderFormObject;
	OrderListObject mOrderListObject;
	ArrayList<OrderFormObject> list_order;
	private ArrayList<OrderFormObject> msglist = new ArrayList<OrderFormObject>();
	private String client_id = "", service = "", staff_id = "", store_id = "",
			pay_manoy = null, order_id = null, service_time = null;
	private static int jumpWeek = 0;
	double price = 0;
	double money = 0;
	int num = 0;
	String servicetime = "";
	private TextView tvDate;
	private int year_c = 0;
	private int month_c = 0;
	private int day_c = 0;
	private int week_c = 0;
	private int week_num = 0;
	private Spinner sp_room;
	private GridView gv_times;
	private String room_id;
	private TimesAdapter madapter;
	private ArrayList<String> timelist = new ArrayList<String>();
	private ArrayList<Boolean> bl = new ArrayList<Boolean>();
	private ArrayList<RoomObject> list = new ArrayList<RoomObject>();
	private RoomListObject mRoomListObject;
	private String currentDate = "";
	private int currentYear;
	private int currentMonth;
	private int currentWeek;
	private int currentDay;
	private int currentNum;
	private boolean isStart;
	private SpecialCalendar sc = null;
	private String timess = "";
	private DateAdapter dateAdapter;
	private int daysOfMonth = 0; 
	private int dayOfWeek = 0; 
	private int weeksOfMonth = 0;
	private boolean isLeapyear = false; 
	// private GridView gridView = null;
	private GestureDetector gestureDetector = null;
	private static String TAG = "ZzL";
	private String dayNumbers[] = new String[7];
	private int selectPostion = 0;
	private ViewFlipper flipper1 = null;
	private String type;
	LinearLayout container;
	private LinearLayout date_choose;
	private String datatime;
	private StoreListObject mStoreListObject;
	private ArrayList<StoreObject> list_times = new ArrayList<StoreObject>();
	private LinearLayout rooms;
	private ArrayList<StaffObject> staffs = MyApplication.list_business_staff;
	private ArrayList<ServiceObject> services = MyApplication.list_business_service;
	public TextView roomname, starttime, endtime;
	private Timer timer;
	private GridView gridView;
	private BaseAdapter adapter;
	private JSONArray datas = new JSONArray();// 时间段data
	private JSONArray headDatas = new JSONArray();// 头部时间
	private int selectHead;
	Calendar calendar;
	String datass = "";
	ArrayList<String> arr_time = new ArrayList<String>();
	SimpleDateFormat formatter;
	private String tag = "";
	JSONObject item;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choose_datetime);
		Log.i("setContentView","       ");

		selectHead=0;
		initView();
		myonclick();
		if (type.equals("现场")) {
			rooms.setVisibility(View.GONE);
			date_choose.setVisibility(View.GONE);
			addorderform();
		} else if (type.equals("预订")) {
			rooms.setVisibility(View.GONE);
			date_choose.setVisibility(View.VISIBLE);
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
			getStaffSWorkTime(df.format(new Date()));
		}
		// getBusyTime(staffs.get(0).service_staff);
		// new Thread(new ThreadShow()).start();
		gridView = (GridView) findViewById(R.id.gridView);
	// System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
	}

	@Override
	protected void initData() {

	}

	class ViewHolder {
		public TextView timeTextView;
		}

	public void initView() {
		storeid = getIntent().getStringExtra("store_id");
		type = getIntent().getStringExtra("type");
		date_choose = (LinearLayout) findViewById(R.id.date_choose);
		applicationTopView = (ApplicationTopView) findViewById(R.id.at_top_view);
		btn_next = (Button) findViewById(R.id.btn_next);
		chooseroom = (Button) findViewById(R.id.chooseroom);
		tv_time = (TextView) findViewById(R.id.tv_time);
		tvDate = (TextView) findViewById(R.id.tv_date);
		container = (LinearLayout) findViewById(R.id.week_days);
		gv_times = (GridView) findViewById(R.id.gv_times);
		gv_times.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
		roomname = (TextView) findViewById(R.id.roomname);
		starttime = (TextView) findViewById(R.id.starttime);
		endtime = (TextView) findViewById(R.id.endtime);
		rooms = (LinearLayout) findViewById(R.id.rooms);
		timelist.add("00:00");
		madapter = new TimesAdapter(this, timelist);
		gv_times.setAdapter(madapter);
		tvDate.setText(year_c + "-" + month_c + "-" + day_c);
		gestureDetector = new GestureDetector(this);
		flipper1 = (ViewFlipper) findViewById(R.id.flipper1);
		dateAdapter = new DateAdapter(this, getResources(), currentYear,
				currentMonth, currentWeek, currentNum, selectPostion,
				currentWeek == 1 ? true : false);
		addGridView();
		dayNumbers = dateAdapter.getDayNumbers();
		gridView.setAdapter(dateAdapter);
		selectPostion = dateAdapter.getTodayPosition();
		gridView.setSelection(selectPostion);
		flipper1.addView(gridView, 0);

	}

	private void myonclick() {
		applicationTopView.setOnTopClickListener(new OnTopClickListener() {
			@Override
			public void onLeftbuttonClick() {
				finish();
			}

			@Override
			public void onRightbuttonClick() {

			}
		});
		btn_next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// AlertDialog.Builder builder = new Builder(
				// ChooseDatetimeActivity.this);
				// builder.setMessage("ȷ���µ���\n\t��ʼʱ��"
				// + tvDate.getText().toString() + MyApplication.begintime
				// + "\n\t����ʱ��" + tvDate.getText().toString()
				// + MyApplication.endtime);
				// builder.setPositiveButton("ȷ��",
				// new DialogInterface.OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface arg0, int arg1) {
				// addorderform();
				// arg0.dismiss();
				// }
				// });
				// builder.setNegativeButton("ȡ��",
				// new DialogInterface.OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface arg0, int arg1) {
				// arg0.dismiss();
				// }
				// });
				// builder.create().show();
				if (servicetime.equals("")) {
					Toast.makeText(ChooseDatetimeActivity.this, "请选择预定时间", Toast.LENGTH_LONG).show();
				}else {
				addorderform();
				}
			}
		});
		
		chooseroom.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						RoomActivity.class);
				intent.putExtra("store_id", storeid);
				startActivityForResult(intent, 126);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 126:
			Bundle MarsBuddle = data.getExtras();
			room_id = MarsBuddle.getString("room_id");
			roomname.setText(MarsBuddle.getString("name"));
			chooseroom.setVisibility(View.GONE);
			roomname.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}

	}

	private void addorderform() {
		// staff_id=1&store_id=1&pay_manoy=49.0&order_id=36111446521572024&service_time=2015-10-3++11%3A32%3A52&
		// price=44.0&client_id=4&service=2&name=����3jjhjhhggg&room_id=20&begin_time=2015-10-3++11%3A32%3A52&end_time=
		client_id = MyApplication.Clientuser.id;
		ArrayList<StaffObject> staffs = MyApplication.list_business_staff;
		ArrayList<ServiceObject> services = MyApplication.list_business_service;
		staff_id = staffs.get(0).id;
		money = Double.parseDouble(services.get(0).money);
		service = services.get(0).id;
		pay_manoy = String.valueOf(money
				+ Double.parseDouble(staffs.get(0).surchange));
		int n1 = 0;
		while (n1 == 0) {
			n1 = (int) (Math.random() * (10));
		}
		int n2 = (int) (Math.random() * (10));
		int n3 = (int) (Math.random() * (10));
		int n4 = (int) (Math.random() * (10));
		String n = String.valueOf(n1) + String.valueOf(n2) + String.valueOf(n3)
				+ String.valueOf(n4);
		if (type.equals("预订")) {
			String month1=String.valueOf(month_c);
			if(month_c<10){
				month1="0"+month1;
			}
			datatime = String.valueOf(year_c) + "-" + month1
					+ "-" + String.valueOf(day_c) ;
					//+ MyApplication.begintime;
			room_id = "0";
		} else if (type.equals("现场")) {
			Time curTime = new Time();
			curTime.setToNow();
			datatime = curTime.year + "-" + curTime.month + "-"
					+ curTime.monthDay + "  " + curTime.hour + ":"
					+ curTime.minute + ":" + curTime.second;
			room_id = "0";
		}
		order_id = n + String.valueOf(System.currentTimeMillis());
		
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "addorderform" };
		String[] name1 = new String[] { "staff_id", staff_id };
		String[] name2 = new String[] { "store_id", storeid };
		String[] name3 = new String[] { "pay_manoy", pay_manoy };
		String[] name4 = new String[] { "order_id", order_id };
		String[] name5 = new String[] { "service_time", servicetime };
		String[] name7 = new String[] { "client_id", client_id };
		String[] name6 = new String[] { "price", String.valueOf(money) };
		String[] name8 = new String[] { "service", service };
//		String[] name9 = new String[] { "name", MyApplication.client_storeName };
		String[] name9 = new String[] { "name", "" };
		String[] name10 = new String[] { "room_id", room_id };
//		String[] name11 = new String[] { "begin_time", datatime };
		String[] name11 = new String[] { "begin_time", datass };
		String[] name12 = new String[] { "end_time",
				tvDate.getText().toString() + "  " + MyApplication.endtime };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		params2.add(name5);
		params2.add(name6);
		params2.add(name7);
		params2.add(name8);
		params2.add(name9);
		params2.add(name10);
		params2.add(name11);
		params2.add(name12);
		progressDialog2 = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog2.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (progressDialog2 != null) {
				progressDialog2.dismiss();
				progressDialog2 = null;
			}
			if (!v.equals("fail")) {
				saveorderId();
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "下单失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void saveorderId() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getorderformbyorderid" };
		String[] name1 = new String[] { "order_id", order_id };
		params2.add(funParam2);
		params2.add(name1);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list_order.size() > 0) {
						mOrderFormObject = list_order.get(0);
						setRoom();
					 
						
						setJpushUpload(list_order.get(0).staffphone);
						setJpushUploadShop(list_order.get(0).shopphone);
						Intent intent = new Intent();
						intent.setClass(ChooseDatetimeActivity.this,
								PreordainPayActivity.class);
						intent.putExtra("order_id", mOrderFormObject.order_id);
						intent.putExtra("status", "0");
						intent.putExtra("type", type); 
						intent.putExtra("orderstatus1", mOrderFormObject.orderstatus1);
						startActivity(intent);
						finish();
					} else {
						Toast.makeText(ChooseDatetimeActivity.this, "失败",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ChooseDatetimeActivity.this, "失败",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "失败",
						Toast.LENGTH_LONG).show();
			}
		}

	};

 
 
 
	
	private void setJpushUploadShop(String shopphone) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		// http://115.28.191.58/jpushAction?registrationId=080cdfbfdbd&alert=234234&title=127398132
		//tag  要推送用户的手机号
		String[] name1 = new String[] { "title", "您有新的订单" }; 
		String[] name2 = new String[] { "alert",
//				"客户" + msglist.get(0).login_id + "需要服务" }; 
				"您有新的订单，客户" + mOrderFormObject.phone + "需要服务" }; 
		String[] name3 = new String[] { "tag",		
				shopphone};
//				JPushInterface.getRegistrationID(getApplicationContext()) }; 
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL3, params2,
					jpush_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener jpush_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (!v.equals("0")) {
				Toast.makeText(ChooseDatetimeActivity.this, "成功",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	
	
	private void setJpushUpload(String userphone) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		// http://115.28.191.58/jpushAction?registrationId=080cdfbfdbd&alert=234234&title=127398132
		//tag  要推送用户的手机号
		String[] name1 = new String[] { "title", "您有新的订单" }; 
		String[] name2 = new String[] { "alert",
//				"客户" + msglist.get(0).login_id + "需要服务" }; 
				"您有新的订单，客户" + mOrderFormObject.phone + "需要服务" }; 
		String[] name3 = new String[] { "tag",		
				userphone};
//				JPushInterface.getRegistrationID(getApplicationContext()) }; 
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL3, params2,
					jpush_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener jpush_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (!v.equals("0")) {
				Toast.makeText(ChooseDatetimeActivity.this, "成功",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void addGridView() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		gridView = new GridView(this);
		gridView.setNumColumns(7);
		gridView.setGravity(Gravity.CENTER_VERTICAL);
		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		gridView.setVerticalSpacing(1);
		gridView.setHorizontalSpacing(1);
		gridView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return ChooseDatetimeActivity.this.gestureDetector
						.onTouchEvent(event);
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Log.i(TAG, "day:" + dayNumbers[position]);
				selectPostion = position;
				dateAdapter.setSeclection(position);
				dateAdapter.notifyDataSetChanged();
				tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "-"
						+ dateAdapter.getCurrentMonth(selectPostion) + "-"
						+ dayNumbers[position]);
			}
		});
		gridView.setLayoutParams(params);
	}

	@Override
	protected void onPause() {
		super.onPause();
		jumpWeek = 0;
	}

	@Override
	public boolean onDown(MotionEvent e) {

		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {

	}

	public void getCurrent() {
		if (currentWeek > currentNum) {
			if (currentMonth + 1 <= 12) {
				currentMonth++;
			} else {
				currentMonth = 1;
				currentYear++;
			}
			currentWeek = 1;
			currentNum = getWeeksOfMonth(currentYear, currentMonth);
		} else if (currentWeek == currentNum) {
			if (getLastDayOfWeek(currentYear, currentMonth) == 6) {
			} else {
				if (currentMonth + 1 <= 12) {
					currentMonth++;
				} else {
					currentMonth = 1;
					currentYear++;
				}
				currentWeek = 1;
				currentNum = getWeeksOfMonth(currentYear, currentMonth);
			}

		} else if (currentWeek < 1) {
			if (currentMonth - 1 >= 1) {
				currentMonth--;
			} else {
				currentMonth = 12;
				currentYear--;
			}
			currentNum = getWeeksOfMonth(currentYear, currentMonth);
			currentWeek = currentNum - 1;
		}
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		int gvFlag = 0;
		if (e1.getX() - e2.getX() > 80) {
			addGridView();
			currentWeek++;
			getCurrent();
			dateAdapter = new DateAdapter(this, getResources(), currentYear,
					currentMonth, currentWeek, currentNum, selectPostion,
					currentWeek == 1 ? true : false);
			dayNumbers = dateAdapter.getDayNumbers();
			gridView.setAdapter(dateAdapter);
			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "-"
					+ dateAdapter.getCurrentMonth(selectPostion) + "-"
					+ dayNumbers[selectPostion]);
			gvFlag++;
			flipper1.addView(gridView, gvFlag);
			dateAdapter.setSeclection(selectPostion);
			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_in));
			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_left_out));
			this.flipper1.showNext();
			flipper1.removeViewAt(0);
			return true;

		} else if (e1.getX() - e2.getX() < -80) {
			addGridView();
			currentWeek--;
			getCurrent();
			dateAdapter = new DateAdapter(this, getResources(), currentYear,
					currentMonth, currentWeek, currentNum, selectPostion,
					currentWeek == 1 ? true : false);
			dayNumbers = dateAdapter.getDayNumbers();
			gridView.setAdapter(dateAdapter);
			tvDate.setText(dateAdapter.getCurrentYear(selectPostion) + "-"
					+ dateAdapter.getCurrentMonth(selectPostion) + "-"
					+ dayNumbers[selectPostion]);
			gvFlag++;
			flipper1.addView(gridView, gvFlag);
			dateAdapter.setSeclection(selectPostion);
			this.flipper1.setInAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_in));
			this.flipper1.setOutAnimation(AnimationUtils.loadAnimation(this,
					R.anim.push_right_out));
			this.flipper1.showPrevious();
			flipper1.removeViewAt(0);
			return true;
			// }
		}
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return this.gestureDetector.onTouchEvent(event);
	}

	public ChooseDatetimeActivity() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
		currentDate = sdf.format(date);
		year_c = Integer.parseInt(currentDate.split("-")[0]);
		month_c = Integer.parseInt(currentDate.split("-")[1]);
		day_c = Integer.parseInt(currentDate.split("-")[2]);
		currentYear = year_c;
		currentMonth = month_c;
		currentDay = day_c;
		sc = new SpecialCalendar();
		getCalendar(year_c, month_c);
		week_num = getWeeksOfMonth();
		currentNum = week_num;
		if (dayOfWeek == 7) {
			week_c = day_c / 7 + 1;
		} else {
			if (day_c <= (7 - dayOfWeek)) {
				week_c = 1;
			} else {
				if ((day_c - (7 - dayOfWeek)) % 7 == 0) {
					week_c = (day_c - (7 - dayOfWeek)) / 7 + 1;
				} else {
					week_c = (day_c - (7 - dayOfWeek)) / 7 + 2;
				}
			}
		}
		currentWeek = week_c;
		getCurrent();

	}

	/** 
	 * @param year
	 * @param month
	 */
	public int getWeeksOfMonth(int year, int month) {
		int preMonthRelax = 0;
		int dayFirst = getWhichDayOfWeek(year, month);
		int days = sc.getDaysOfMonth(sc.isLeapYear(year), month);
		if (dayFirst != 7) {
			preMonthRelax = dayFirst;
		}
		if ((days + preMonthRelax) % 7 == 0) {
			weeksOfMonth = (days + preMonthRelax) / 7;
		} else {
			weeksOfMonth = (days + preMonthRelax) / 7 + 1;
		}
		return weeksOfMonth;

	}

	/**
	 * @param year
	 * @param month
	 * @return
	 */
	public int getWhichDayOfWeek(int year, int month) {
		return sc.getWeekdayOfMonth(year, month);

	}

	/**
	 * 
	 * @param year
	 * @param month
	 */
	public int getLastDayOfWeek(int year, int month) {
		return sc.getWeekDayOfLastMonth(year, month,
				sc.getDaysOfMonth(isLeapyear, month));
	}

	public void getCalendar(int year, int month) {
		isLeapyear = sc.isLeapYear(year);
		daysOfMonth = sc.getDaysOfMonth(isLeapyear, month); 
		dayOfWeek = sc.getWeekdayOfMonth(year, month); 
	}

	public int getWeeksOfMonth() {
		// getCalendar(year, month);
		int preMonthRelax = 0;
		if (dayOfWeek != 7) {
			preMonthRelax = dayOfWeek;
		}
		if ((daysOfMonth + preMonthRelax) % 7 == 0) {
			weeksOfMonth = (daysOfMonth + preMonthRelax) / 7;
		} else {
			weeksOfMonth = (daysOfMonth + preMonthRelax) / 7 + 1;
		}
		return weeksOfMonth;
	}

	private void getBusyTime(String user_id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffTime1" };
		String[] name1 = new String[] { "staff_id", user_id };
		params2.add(funParam2);
		params2.add(name1);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					gettime_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener gettime_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list_times = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					if (list_times.size() > 0) {
						timelist.clear();
						fenge(list_times.get(0).begin_time + "-"
								+ list_times.get(0).end_time);
					} else {
						Toast.makeText(ChooseDatetimeActivity.this, "失败",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ChooseDatetimeActivity.this, "失败",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "失败",
						Toast.LENGTH_LONG).show();
			}
		}

	};

	private void fenge(String content) {
		String[] work = content.split("-");
		String[] start = work[0].split(":");
		String[] end = work[1].split(":");
		for (int i = Integer.parseInt(start[0]); i < Integer.parseInt(end[0]); i++) {
			if (i < 10) {
				timelist.add("0" + i + ":00:00");
			} else {
				timelist.add(i + ":00:00");
			}
		}
		madapter = new TimesAdapter(this, timelist);
		gv_times.setAdapter(madapter);
	}

	private void Split(String begintime) {
		String[] start = begintime.split("[:]");
		if (Integer.parseInt(start[0]) < 9) {
			MyApplication.endtime = "0"
					+ String.valueOf(Integer.parseInt(start[0]) + 1) + ":00:00";
		} else {
			MyApplication.endtime = String
					.valueOf(Integer.parseInt(start[0]) + 1) + ":00:00";
		}
		this.starttime.setText(tvDate.getText().toString() + "  "
				+ MyApplication.begintime);
		this.endtime.setText(tvDate.getText().toString() + " "
				+ MyApplication.endtime);
	};

	Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				if (!MyApplication.begintime.equals("")) {
					Split(MyApplication.begintime);
				}
			}
		}

	};

	class ThreadShow implements Runnable {
		@Override
		public void run() {
			while (true) {
				try {
					Thread.sleep(1000);
					Message msg = new Message();
					msg.what = 1;
					handler.sendMessage(msg);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setRoom() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setRoomRecord" };
		String[] name1 = new String[] { "order_id", order_id };
		String[] name2 = new String[] { "store_id", storeid };
		String[] name3 = new String[] { "room_id", room_id };
		String[] name4 = new String[] { "begin_time", datatime };
		String[] name5 = new String[] { "end_time",
				tvDate.getText().toString() + "  " + MyApplication.endtime };
		String[] name6 = new String[] { "client_id",
				MyApplication.Clientuser.id };
		String[] name7 = new String[] { "remarks", "0" };
		String[] name8 = new String[] { "status", "1" };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		params2.add(name5);
		params2.add(name6);
		params2.add(name7);
		params2.add(name8);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					room_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener room_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
			}
		}
	};

	private ProgressDialog progressDialog = null;
	private StaffListObject mStaffListObject;
	private ArrayList<StaffObject> list11 = new ArrayList<StaffObject>();
	
	//获取员工工作时间
	private void getStaffSWorkTime(String datas) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getOrderTimes" };
		String[] name1 = new String[] { "staff_id",
				MyApplication.list_business_staff.get(0).id };
		String[] name2 = new String[] { "datas", datas };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener11);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener11 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list11 = mStaffListObject.response;
				if (mStaffListObject.meta.getMsg().equals("OK")) {
					if (list11.size() > 0) {
						timess = list11.get(0).times+","+list11.get(0).service_time;
						Settime();
					} else {
						timess = "";
						Settime();
					}
				}
			} else {
				Toast.makeText(ChooseDatetimeActivity.this, "获取失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	OnClickListener clickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			for (int i = 0; i < 7; i++) {
				View view = container.getChildAt(i);
				view.setBackgroundColor(getResources().getColor(R.color.white));
			}
			View view = container.getChildAt(v.getId());
			view.setBackgroundColor(getResources().getColor(R.color.red));
			v.setTag(view);
			TextView txtv = (TextView) v;
			String timmeString = txtv.getText().toString();
			list.clear();
			DisplayMetrics dm = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(dm);
			int mScreenWidth = dm.widthPixels;
			for (int i = 0; i < headDatas.length(); i++) {
				JSONObject item = headDatas.optJSONObject(i);
				try {
					Calendar ca = (Calendar) item.get("time");
					TextView text = new TextView(ChooseDatetimeActivity.this);
					text.setClickable(true);
					text.setId(i);
					text.setOnClickListener(clickListener);
					text.setGravity(Gravity.CENTER);
					text.setBackgroundColor(getResources().getColor(
							R.color.white));
					text.setLayoutParams(new LayoutParams(mScreenWidth / 7,
							LayoutParams.MATCH_PARENT));
					calendar = (Calendar) item.get("time");
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					String ttString = formatter.format(ca.getTime());
					String weekStr = getWeek(ca);
					if (weekStr.equals(timmeString)) {
						datass = ttString;
						
						selectHead=i;
						getStaffSWorkTime(ttString);
						break;
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	};

	// 头部构造31天时间
	public void getHeadDates() throws JSONException {
		headDatas = new JSONArray();
		for (int i = 0; i < 7; i++) {
			JSONObject item = new JSONObject();
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, i);
			item.put("state", 0);
			item.put("time", calendar);
			headDatas.put(item);
		}
	}

	// 构造时间段
	public void getOtherDay() throws JSONException {
		int hour = 8;
		datas = new JSONArray();
		for (int i = 0; i < 30; i++) {
			JSONObject item = new JSONObject();
			Calendar c = Calendar.getInstance();
			
			int gridMin=0;
			if (i % 2 == 0) {
				hour++;
				c.set(Calendar.MINUTE, 00);
			} else {
				gridMin=30;
				c.set(Calendar.MINUTE, 30);
			}
			c.set(Calendar.HOUR_OF_DAY, hour);
			item.putOpt("time", c);
			Date dt=new Date();
			int nowday=dt.getDay();
			
		    int nowHour=dt.getHours();
		    int nowMin=dt.getMinutes();
		    
		    int nowNum=nowHour*100+nowMin;
		    int gridNum=hour*100+gridMin;
		    
		    JSONObject itemS = headDatas.optJSONObject(this.selectHead);
		    
			calendar = (Calendar) itemS.get("time");
			formatter = new SimpleDateFormat("yyyy-MM-dd");
			String ttString1 = formatter.format(calendar.getTime());
			String ttString2 = formatter.format(dt);
		    if(gridNum<nowNum && ttString1.equals(ttString2)){
		    	item.put("state", 2);
		    }
			
			// if (i<40&&i>14) {
			// item.put("state", 0);
			// } else {
			// item.put("state", 1);// 0 normal 1 choose 2 disable
			// }
			calendar = (Calendar) item.get("time");
			formatter = new SimpleDateFormat("HH:mm");
			if (!timess.equals("")) {
				String str = timess;
				String[] strarray = str.split(",");
				for (int j = 0; j < strarray.length; j++) {
					if (formatter.format(calendar.getTime())
							.equals(strarray[j])) {
						item.put("state", 2);
					}
				}
			} else {
				item.put("state", 0);
			}
			datas.put(item);
		}
	}

	// 根据claendat获取星期几
	private String getWeek(Calendar c) {
		String Week = "周";
		if (c.get(Calendar.DAY_OF_WEEK) == 1) {
			Week += "天";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 2) {
			Week += "一";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 3) {
			Week += "二";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 4) {
			Week += "三";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 5) {
			Week += "四";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 6) {
			Week += "五";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 7) {
			Week += "六";
		}
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
		String result = Week + "\n" + formatter.format(c.getTime());
		return result;
	}

	private void Settime() {
		try {
			getHeadDates();
			getOtherDay();
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int mScreenWidth = dm.widthPixels;
		for (int i = 0; i < headDatas.length(); i++) {
			JSONObject item = headDatas.optJSONObject(i);
			try {
				Calendar ca = (Calendar) item.get("time");
				TextView text = new TextView(this);
				text.setClickable(true);
				text.setId(i);
				text.setOnClickListener(clickListener);

				text.setGravity(Gravity.CENTER);
				text.setBackgroundColor(getResources().getColor(R.color.white));
				text.setLayoutParams(new LayoutParams(mScreenWidth / 7,
						LayoutParams.MATCH_PARENT));
				calendar = (Calendar) item.get("time");
				formatter = new SimpleDateFormat("yyyy-MM-dd");
				String ttString = formatter.format(ca.getTime());
				text.setTag(i);
				String weekStr = getWeek(ca);
				  JSONObject itemS = headDatas.optJSONObject(this.selectHead);
				    
					calendar = (Calendar) itemS.get("time");
					formatter = new SimpleDateFormat("yyyy-MM-dd");
					Calendar  calendar1 = (Calendar) itemS.get("time");
					
					String timmeString=getWeek(calendar1);
				if (weekStr.equals(timmeString)) {
					datass = ttString;
					
			
//					break;
				}
				
				if(i==0){
					text.setBackgroundColor(getResources().getColor(R.color.red));
					text.setClickable(true);
				}
				container.addView(text);
				text.setText(getWeek(ca));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		adapter = new BaseAdapter() {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ViewHolder holder = null;
				item = datas.optJSONObject(position);
				if (convertView == null) {
					holder = new ViewHolder();
					convertView = LayoutInflater.from(
							ChooseDatetimeActivity.this).inflate(
							R.layout.cell_time, null);
					holder.timeTextView = (TextView) convertView
							.findViewById(R.id.timeTextView);
					convertView.setTag(holder);
				} else {
					holder = (ViewHolder) convertView.getTag();
				}
				try {
					calendar = (Calendar) item.get("time");
					formatter = new SimpleDateFormat("HH:mm");
					holder.timeTextView.setText(formatter.format(calendar
							.getTime()));
				} catch (JSONException e) {
					e.printStackTrace();
				}

				int state = item.optInt("state");
				switch (state) {
				case 0:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.white));
					break;
				case 1:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.red));
					break;
				case 2:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.gray));
					break;

				default:
					break;
				}
				return convertView;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return position;
			}

			@Override
			public int getCount() {
				return datas.length();
			}
		};
		
		gridView.setAdapter(adapter);
		
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				JSONObject item = datas.optJSONObject(position);
				if (item.optInt("state") == 2) {
					return;
				}
				for (int i = 0; i < datas.length(); i++) {
					JSONObject child = datas.optJSONObject(i);

					if (child.optInt("state") == 1) {
						try {
							child.put("state", 0);
							calendar = (Calendar) item.get("time");
							formatter = new SimpleDateFormat("HH:mm");
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}
				try {
					item.put("state", 1);
				} catch (JSONException e) {
					e.printStackTrace();
				}

				JSONObject child = datas.optJSONObject(position);
				try {
					child.put("state", 1);
					calendar = (Calendar) item.get("time");
					formatter = new SimpleDateFormat("HH:mm");
					servicetime = formatter.format(calendar.getTime());
				} catch (JSONException e) {
					e.printStackTrace();
				}
				// if (item.optInt("state") == 1) {
				// try {
				// item.put("state", 0);
				// calendar = (Calendar) item.get("time");
				// formatter = new SimpleDateFormat("HH:mm");
				//
				// tag = formatter.format(calendar.getTime());
				//
				// for (int i = 0; i < arr_time.size(); i++) {
				//
				// if (arr_time.get(i).equals(tag)) {
				// arr_time.remove(i);
				// break;
				// }
				//
				// }
				// } catch (JSONException e) {
				// e.printStackTrace();
				// }
				//
				// } else {
				// try {
				// item.put("state", 1);
				// calendar = (Calendar) item.get("time");
				// formatter = new SimpleDateFormat("HH:mm");
				//
				// tag = formatter.format(calendar.getTime());
				// arr_time.add(tag);
				// } catch (JSONException e) {
				// e.printStackTrace();
				// }
				// }
				adapter.notifyDataSetChanged();
			}
		});
	}
}