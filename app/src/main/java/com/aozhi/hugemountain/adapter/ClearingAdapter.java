package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ClearingObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.model.ManageStaffObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ClearingAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();

	public ClearingAdapter(Context ctx, ArrayList<ConsumptionObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_clearing, null);
			mHolder.date=(TextView)convertView.findViewById(R.id.date);
			mHolder.name=(TextView)convertView.findViewById(R.id.name);
			mHolder.money=(TextView)convertView.findViewById(R.id.money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}

		mHolder.date.setText(list.get(position).create_time.substring(0,list.get(position).create_time.indexOf(" ")));
		mHolder.money.setText("￥"+Integer.parseInt(list.get(position).money));
		mHolder.name.setText(list.get(position).name );
		return convertView;
	}

	class Holder {
		TextView name,money,date;
	}
}
