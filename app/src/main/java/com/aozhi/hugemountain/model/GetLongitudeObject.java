package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class GetLongitudeObject {
	public ArrayList<GetLongitudeObject> location;
	public String precise;
	public String confidence;
	public String level;
	public String lng;
	public String lat;
}
