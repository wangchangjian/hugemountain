package com.aozhi.hugemountain.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aozhi.hugemountain.R;

/**
 * Created by L on 2015/11/16.
 */
public class ConfirmDialog {
    private Dialog loadingDialog;
    private Button confirmButtn;
    private Button cancelButton;
    private TextView textView;

    public ConfirmDialog(Context context, String string) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_confirm, null);// 得到加载view
        // main.xml中的ImageView
        confirmButtn = (Button) v.findViewById(R.id.ibtn_confirm);
        cancelButton = (Button) v.findViewById(R.id.ibtn_cancel);
        textView = (TextView) v.findViewById(R.id.tv_msg);
        textView.setText(string);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.dismiss();
            }
        });
        loadingDialog = new Dialog(context, R.style.loading_dialog);// 创建自定义样式dialog

        loadingDialog.setCancelable(false);// 不可以用“返回键”取消
        loadingDialog.setContentView(v, new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));// 设置布局
    }

    /**
     * 弹出只有确认按钮的Dialog
     */
    public void showOnlyConfirm() {
        show();
        cancelButton.setVisibility(View.GONE);
    }

    /**
     * 弹出有确认和取消按钮的dialog
     */
    public  void showConfirmAndCancel() {
        show();
    }

    public void confirmListener(final ConfirmListener confirmListener){
        confirmButtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                confirmListener.confirm();
            }
        });
    }

   public interface ConfirmListener {
        void confirm();
    }

    public void show() {
        loadingDialog.show();
    }

    public void dismiss() {
        loadingDialog.dismiss();
    }

    public boolean isShowing(){
        return  loadingDialog.isShowing();
    }

}
