package com.aozhi.hugemountain.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.adapter.StoreListAdapter;
import com.aozhi.hugemountain.model.CodoUser;
import com.aozhi.hugemountain.model.FriendObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StatFs;
import android.os.Build.VERSION;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.webkit.WebView;
import android.widget.Toast;

public class Utils {
	
	public static int dipToPixels(Context context, int dip) {  
	    final float SCALE = context.getResources().getDisplayMetrics().density;  
	  
	    float valueDips = dip;  
	    int valuePixels = (int) (valueDips * SCALE + 0.5f);  
	  
	    return valuePixels;  
	  
	}
	/**
	 * 转换图片成圆形
	 * 
	 * @param bitmap
	 *            传入Bitmap对象
	 * @return
	 */
	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height) {
			roundPx = width / 2;

			left = 0;
			top = 0;
			right = width;
			bottom = width;

			height = width;

			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;

			float clip = (width - height) / 2;

			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;

			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right,
				(int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top,
				(int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);// 设置画笔无锯齿

		canvas.drawARGB(0, 0, 0, 0); // 填充整个Canvas

		// 以下有两种方法画圆,drawRounRect和drawCircle
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);// 画圆角矩形，第一个参数为图形显示区域，第二个参数和第三个参数分别是水平圆角半径和垂直圆角半径。
		// canvas.drawCircle(roundPx, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));// 设置两张图片相交时的模式,参考http://trylovecatch.iteye.com/blog/1189452
		canvas.drawBitmap(bitmap, src, dst, paint); // 以Mode.SRC_IN模式合并bitmap和已经draw了的Circle

		return output;
	}
	
	public static Double sub(Double v1, Double v2) {
		   BigDecimal b1 = new BigDecimal(v1.toString());
		   BigDecimal b2 = new BigDecimal(v2.toString());
		   return new Double(b1.subtract(b2).doubleValue());
		}
	public static String getTimeShow1(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date tmpDate;
		Calendar current = Calendar.getInstance();
		try {
			tmpDate = format.parse(time);
			Calendar cal = format.getCalendar();
			int y = current.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
			if (y > -1) {
				if (y < 1) {
					int d = current.get(Calendar.DAY_OF_YEAR)
							- cal.get(Calendar.DAY_OF_YEAR);
					if (d > -1) {
						if (d < 1) {
							int h = current.get(Calendar.HOUR_OF_DAY)
									- cal.get(Calendar.HOUR_OF_DAY);
							if (h > -1) {
								if (h < 1) {
									int m = current.get(Calendar.MINUTE)
											- cal.get(Calendar.MINUTE);
									if (m > -1) {
										if (m < 1) {
											return "刚刚";
										} else {
											return m + "分钟前";
										}
									} else {
										return time;
									}
								} else {
									return h + "小时前";
								}
							} else {
								return time;
							}
						} else {
							return d + "天前";
						}
					} else {
						return time;
					}
				} else {
					return y + "年前";
				}
			} else {
				return time;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}
	public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd HH:mm" ;
	public static boolean isNumeric(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		return pattern.matcher(str).matches();
	}
	public static boolean isPhoneNumberValid(String phoneNumber) {
		boolean isValid = false;
		String expression = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{5})$";
		String expression2 = "^\\(?(\\d{3})\\)?[- ]?(\\d{4})[- ]?(\\d{4})$";
		CharSequence inputStr = phoneNumber;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		Pattern pattern2 = Pattern.compile(expression2);
		Matcher matcher2 = pattern2.matcher(inputStr);
		if (matcher.matches() || matcher2.matches()) {
			isValid = true;
		}
		return isValid;
	}
	public static String getScreenDPI(Activity mActivity) {
		DisplayMetrics displaysMetrics = new DisplayMetrics();
		mActivity.getWindowManager().getDefaultDisplay()
				.getMetrics(displaysMetrics);
		int densityDpi = displaysMetrics.densityDpi; // ��Ļ�ܶ�DPI��120 / 160 / 240��
		if (densityDpi == 120)
			return "ldpi:" + densityDpi;
		else if (densityDpi == 160)
			return "mdpi:" + densityDpi;
		else if (densityDpi == 240)
			return "hdpi:" + densityDpi;
		else if (densityDpi == 320)
			return "xhdpi:" + densityDpi;
		else
			return "unknow" + densityDpi;
	}
	public static Bitmap getImageThumbnail(String photoPath, int i, int j) {
		// TODO Auto-generated method stub
		return null;
	}
	public static boolean sdcardisstorage() {
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			File sdcardDir = Environment.getExternalStorageDirectory();
			StatFs sf = new StatFs(sdcardDir.getPath());
			long blockSize = sf.getBlockSize();
			long availCount = sf.getAvailableBlocks();
			Long avail = availCount * blockSize / 1024;
			if (avail >= 2 * 1024) {
				return true;
			}
		}
		return false;
	}
	
	public static String saveBitmapToFile(Bitmap mSrcBitmap, String fileName) {
		// String
		// fileName=Constant.QUESTION_SHOW_IMAGE+System.currentTimeMillis()+".png";
		FileOutputStream fos = null;
		try {
			if (!sdcardisstorage()) {
				return "";
			}
			File imageFile = new File(fileName);
			imageFile.createNewFile();
			fos = new FileOutputStream(imageFile);
			mSrcBitmap.compress(CompressFormat.PNG, 100, fos);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return fileName;
	}
	
	public static String getTimeShow(String time){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date tmpDate;
		Calendar current = Calendar.getInstance();
		try {
			tmpDate = format.parse(time);
			Calendar cal = format.getCalendar();
			int y=current.get(Calendar.YEAR)-cal.get(Calendar.YEAR);
			if (y>-1) {
				if (y<1) {
					int d = current.get(Calendar.DAY_OF_YEAR)-cal.get(Calendar.DAY_OF_YEAR);
					if (d>-1) {
					if (d<1) {
						int h = current.get(Calendar.HOUR_OF_DAY)-cal.get(Calendar.HOUR_OF_DAY);
						if (h>-1) {
							if (h<1) {
								int m = current.get(Calendar.MINUTE)-cal.get(Calendar.MINUTE);
								if (m>-1) {
									if (m<1) {
										return "刚刚";
									}else {
										return m+"分钟前";
									}
								}else {
									return time;
								}
							}else {
								return h+"小时前";
							}
						}else {
							return time;
						}
					}else {
						return d+"天前";
					}
					}else {
						return time;
					}
				}else {
					return y+"年前";
				}
			}else {
				return time;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			return "";
		}
	}
	/**
	 * save decimal
	 * 
	 * @param pDouble
	 * @return
	 */
	public static double saveDecimal(double pDouble) {
		BigDecimal bd = new BigDecimal(pDouble);
		BigDecimal bd1 = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		pDouble = bd1.doubleValue();
		// long ll = Double.doubleToLongBits(pDouble);

		return pDouble;
	}

	/**
	 * using to replace String to star
	 * 
	 * @param str
	 * @return
	 */
	public static String getCurrentGlTime() {
		return System.currentTimeMillis()+"";
	}
	public static long getCurrentGlTimeByLong() {
		return System.currentTimeMillis();
	}
	private static Handler chatHandler;
	public static void setChatHandler(Handler h){
		chatHandler = h;
	}
	private static Handler remindHandler;
	public static void setRemindHandler(Handler h){
		remindHandler = h;
	}
	public static void systemLogout(){
		if (chatHandler!=null) {
			android.os.Message msg = new android.os.Message();
			msg.what = Global.MULTI_STOP;
			chatHandler.sendMessage(msg);
		}
		if (remindHandler!=null) {
			android.os.Message msg = new android.os.Message();
			msg.what = Global.MULTI_STOP;
			remindHandler.sendMessage(msg);
		}
//		MyApplication.exit();
//		Intent intent = new Intent();
//		intent.setAction("ExitApp");
//		System.exit(0);
	}
	public static String replaceByStar(String str) {

		StringBuffer sb = new StringBuffer(str);
		int length = sb.length();
		sb.replace(length - 4, length, "****");
		return sb.toString();
	}
	public static String getFormatString(long milliSeconds, String pattern){
		if(pattern == null) {
			pattern = DEFAULT_DATE_PATTERN ;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		Date date = new Date(milliSeconds);
		return dateFormat.format(date);
	}
	public static Date getFormatDate(long milliSeconds, String pattern){
		if(pattern == null) {
			pattern = DEFAULT_DATE_PATTERN ;
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		return new Date(milliSeconds);
	}
	/**
	 * 
	 * @param flag
	 */
	private static long start;

	public static void TimeConsume(boolean flag) {
		// long result =
		if (flag)
			start = System.currentTimeMillis();
		else {
			Log.d("ʱ�����", "" + ((System.currentTimeMillis() - start) / 1000)
					+ " s");
		}

	}

	/*
	 * 
	 * �������ַ�ת��Ϊʮ�����Unicode�����ַ�
	 */

	public static String stringToUnicode(String s) {
		String str = "";
		for (int i = 0; i < s.length(); i++) {
			int ch = (int) s.charAt(i);
			str += "\\u" + Integer.toHexString(ch);
		}
		return str;

	}

	public static String unicodeToString(String str) {
		Pattern pattern = Pattern.compile("(\\\\u(\\p{XDigit}{4}))");
		Matcher matcher = pattern.matcher(str);
		char ch;
		while (matcher.find()) {
			ch = (char) Integer.parseInt(matcher.group(2), 16);
			str = str.replace(matcher.group(1), ch + "");
		}
		return str;
	}

	public static String code2Utf8(String str) {
		String result = null;
		if (!str.equals("")) {
			try {
				result = new String(str.getBytes("ISO8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			result = "";
		}
		return result;
	}

	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;

	}

	/**
	 * @param email
	 * @return
	 */
	public static boolean checkEmail(String email) {
		boolean flag = false;
		try {
			String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
			Pattern regex = Pattern.compile(check);
			Matcher matcher = regex.matcher(email);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	/**
	 * user java reg to check phone number and replace 86 or +86 only check
	 * start with "+86" or "86" ex +8615911119999 13100009999 replace +86 or 86
	 * with ""
	 * 
	 * @param phoneNum
	 * @return
	 * @throws Exception
	 */
	public static String checkPhoneNum(String phoneNum) {

		Pattern p1 = Pattern.compile("^((\\+{0,1}86){0,1})1[0-9]{10}");
		Matcher m1 = p1.matcher(phoneNum);
		if (m1.matches()) {
			Pattern p2 = Pattern.compile("^((\\+{0,1}86){0,1})");
			Matcher m2 = p2.matcher(phoneNum);
			StringBuffer sb = new StringBuffer();
			while (m2.find()) {
				m2.appendReplacement(sb, "");
			}
			m2.appendTail(sb);
			return sb.toString();

		} else {
			return "�ֻ������������,����������";

		}
	}

	/**
	 * @param mobiles
	 * @return [0-9]{5,9}
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		// logger.info(m.matches()+"---");
		return m.matches();
	}

	public static Bitmap getBitmap(Bitmap bitmp) {
		int width = bitmp.getWidth();
		int height = bitmp.getHeight();
		int newWidth = 320;
		int newHeight = 320;
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		Matrix matrix = new Matrix();
		// ���ͼƬ����
		matrix.postScale(scaleWidth, scaleHeight);
		// ��תͼƬ ����

		bitmp = Bitmap.createBitmap(bitmp, 0, 0, width, height, matrix, true);
		return bitmp;
	}

//	/**
//	 * �����ļ�
//	 * 
//	 * @param bm
//	 * @param fileName
//	 * @throws IOException
//	 */
//	public static void saveFile(Bitmap bm, String fileName) throws IOException {
//		String path = Environment.getExternalStorageDirectory() + "/"
//				+ Constant.PHOTO_DIR;
//		File dirFile = new File(path);
//		if (!dirFile.exists()) {
//			dirFile.mkdir();
//		}
//		File myCaptureFile = new File(path + fileName);
//		BufferedOutputStream bos = new BufferedOutputStream(
//				new FileOutputStream(myCaptureFile));
//		bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
//		bos.flush();
//		bos.close();
//	}
//
//	public static void appendSaveFile(Context context, String fileName,
//			String content) {
//		if (Environment.MEDIA_MOUNTED.equals(Environment
//				.getExternalStorageState())) {
//
//			String foldername = Environment.getExternalStorageDirectory()
//					.getPath() + "/" + Constant.DDesignatedDriver_DIR;
//			
//			File folder = new File(foldername);
//
//			if (folder == null || !folder.exists()) {
//				folder.mkdir();
//			}
//
//			fileName = new String(foldername + fileName);
//			try {
//				// ��һ��д�ļ�����캯���еĵڶ������true��ʾ��׷����ʽд�ļ�
//				FileWriter writer = new FileWriter(fileName, true);
//				writer.write(content);
//				writer.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		} else {
//			// Toast.makeText(context,"δ����SD����",Toast.LENGTH_LONG).show();
//			Log.e("appendSaveFile δ����SD��", "δ����SD��");
//		}
//	}

	public static void DisplayToastTop(Context context, String str) {
		Toast toast = Toast.makeText(context, str, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.TOP, 0, 200);
		toast.show();
	}

	public static void DisplayToast(Context context, String str) {
		Toast toast = Toast.makeText(context, str, Toast.LENGTH_LONG);
		// toast.setGravity(Gravity.TOP, 0, 200);
		toast.show();
	}

	/**
	 * Check the format of the email.
	 * @return true if the email is valid.
	 */
	public static boolean isEmail(String email) {

		return Pattern.matches(
				"[a-zA-Z0-9._%+-]+@(?:[a-zA-Z0-9-]+.)+[a-zA-Z]{2,4}", email);
	}

	/**
	 * @return
	 */
	public static String getFileName() {

		Calendar cal = Calendar.getInstance();
		String result = "" + cal.get(Calendar.YEAR) + "_"
				+ (cal.get(Calendar.MONTH) + 1) + "_"
				+ cal.get(Calendar.DAY_OF_MONTH) + "_"
				+ cal.get(Calendar.HOUR_OF_DAY) + "_"
				+ cal.get(Calendar.MINUTE) + "_" + cal.get(Calendar.SECOND);
		// System.out.println(result);
		return result;
	}

	public static long getMSByString(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long millionSeconds = 0l;
		try {
			millionSeconds = sdf.parse(str).getTime();// ����
		} catch (Exception e) {

		}
		return millionSeconds;
	}

	/**
	 * @param str
	 * @return
	 */
	public static String getDateTimeByMillisecond(String str) {// 1361159855187
		String time = null;
		if (str != null && !str.equals("")) {
			long CurrentTime = System.currentTimeMillis();
			long oneHour = 60 * 60 * 1000;// һСʱ�ĺ�����
			long oneDay = 24 * 60 * 60 * 1000;// һ��ĺ�����
			Date date = new Date(Long.valueOf(str));

			SimpleDateFormat format;// 1361159427865
			/*
			 * if(CurrentTime<(Long.valueOf(str)+oneHour)){//1Сʱ���� format = new
			 * SimpleDateFormat("mm"); Date currentDate = new Date(CurrentTime);
			 * String currentTime = format.format(currentDate); time =
			 * format.format(date); time =
			 * (Integer.parseInt(currentTime)-Integer.parseInt(time))+"������ǰ "; }
			 * else if(CurrentTime<(Long.valueOf(str)+oneDay)){//һ������ format =
			 * new SimpleDateFormat("HH:mm:ss"); time = format.format(date); }
			 * else{
			 */
			SimpleDateFormat formatDate = new SimpleDateFormat(
					"yyyy-MM-dd");
			time = formatDate.format(date);
			// }
		} else {
			time = "";
		}

		return time;
	}

	/**
	 * @return
	 */
	public static String getCurrentTime() {
		// Date now = new Date();
		Calendar cal = Calendar.getInstance();

		return (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE)
				+ " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
				+ cal.get(Calendar.MINUTE);
	}

	public static boolean isServiceRunning(Context ctx) {
		ActivityManager manager = (ActivityManager) ctx
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if ("com.example.MyService".equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
	
	protected static String execRootCmd(String paramString) {
		String result = "result : ";
		try {
			Process localProcess = Runtime.getRuntime().exec("su ");// ����Root�����androidϵͳ����su����
			OutputStream localOutputStream = localProcess.getOutputStream();
			DataOutputStream localDataOutputStream = new DataOutputStream(
					localOutputStream);
			InputStream localInputStream = localProcess.getInputStream();
			DataInputStream localDataInputStream = new DataInputStream(
					localInputStream);
			String str1 = String.valueOf(paramString);
			String str2 = str1 + "\n";
			localDataOutputStream.writeBytes(str2);
			localDataOutputStream.flush();
			String str3 = null;
			// while ((str3 = localDataInputStream.readLine()) != null) {
			// Log.d("result", str3);
			// }
			localDataOutputStream.writeBytes("exit\n");
			localDataOutputStream.flush();
			localProcess.waitFor();
			return result;
		} catch (Exception localException) {
			localException.printStackTrace();
			return result;
		}
	}

	public static int execRootCmdSilent(String paramString) {
		try {
			Process localProcess = Runtime.getRuntime().exec("su");
			Object localObject = localProcess.getOutputStream();
			DataOutputStream localDataOutputStream = new DataOutputStream(
					(OutputStream) localObject);
			String str = String.valueOf(paramString);
			localObject = str + "\n";
			localDataOutputStream.writeBytes((String) localObject);
			localDataOutputStream.flush();
			localDataOutputStream.writeBytes("exit\n");
			localDataOutputStream.flush();
			localProcess.waitFor();
			int result = localProcess.exitValue();
			return (Integer) result;
		} catch (Exception localException) {
			localException.printStackTrace();
			return -1;
		}
	}

	public static boolean haveRoot() {
		int i = execRootCmdSilent("echo test"); // ͨ��ִ�в�������4���
		if (i != -1) {
			return true;
		}
		return false;
	}

	/**
	 * get screen array
	 * @param act
	 * @param displaysMetrics
	 * @return
	 */
	public static int[] getScreenWidth(Activity act,
			DisplayMetrics displaysMetrics) {
		act.getWindowManager().getDefaultDisplay().getMetrics(displaysMetrics);
		int[] result = new int[2];
		result[0] = displaysMetrics.widthPixels;
		result[1] = displaysMetrics.heightPixels;
		return result;
	}

	public static boolean getCurrentNetWork(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activityNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		// Constant.isConnection = true;
		if (activityNetworkInfo == null || !activityNetworkInfo.isAvailable()) {
			Constant.NET_STATUS = false;
		} else {
			Constant.NET_STATUS = true;
		}
		return Constant.NET_STATUS;
	}


	/**
	 * get this app version name
	 * 
	 * @param context
	 * @return
	 * @throws Exception
	 */
	public static String getVersionName(Context context) throws Exception {
		// ��ȡpackagemanager��ʵ��
		PackageManager packageManager = context.getPackageManager();
		// getPackageName()���㵱ǰ��İ���0����ǻ�ȡ�汾��Ϣ
		PackageInfo packInfo = packageManager.getPackageInfo(
				context.getPackageName(), 0);
		String version = packInfo.versionName;
		return version;
	}

	/**
	 * ��ȡ�������
	 * 
	 * @return
	 */
	public static String getPhoneNumber(Context context) {
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return mTelephonyMgr.getLine1Number();
	}

	/**
	 * @param context
	 * @return
	 */
	public static String getPhoneSIM(Context context) {
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return mTelephonyMgr.getSimSerialNumber();
	}

	/**
	 * ��ȡ����IMEI����
	 * 
	 * @param context
	 * @return
	 */
	public static String getPhoneIMEI(Context context) {
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return mTelephonyMgr.getDeviceId();
	}

	/**
	 * ��ȡ����IMSI����
	 * 
	 * @param ctx
	 * @return
	 */
	public static String getPhoneIMSI(Context ctx) {
		TelephonyManager mTelephonyMgr;
		mTelephonyMgr = (TelephonyManager) ctx
				.getSystemService(Context.TELEPHONY_SERVICE);
		return mTelephonyMgr.getSubscriberId();
	}

	/**
	 * ��ȡϵͳ�汾 2012-11-15 Thu.
	 * 
	 * @return
	 */
	public static int getSystemSdkVersion() {
		int sysVersion = Integer.parseInt(VERSION.SDK);
		return sysVersion;
	}

	/**
	 * ��������l��״̬
	 * 
	 * @param strurl
	 * @return
	 */
	public static boolean checkUrlStatus(String strurl) {
		URL url = null;
		HttpURLConnection conn = null;
		try {
			url = new URL(strurl);
			try {
				// InputStream in = url.openStream();
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
				conn.setRequestProperty("Content-Type",
						"application/x-www-form-urlencoded");
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(Constant.REQUEST_TIMEOUT);
				conn.setReadTimeout(Constant.REQUEST_TIMEOUT);
				conn.connect();
				// in.close();
			} catch (SocketTimeoutException e) {

				return false;
			} catch (IOException e) {
				return false;
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		conn.disconnect();
		return true;
	}

	/**
	 * ������ͨ�Ի���
	 * 
	 * @param ctx
	 *            ������ ����
	 * @param iconId
	 *            ͼ�꣬�磺R.drawable.icon ����
	 * @param title
	 *            ���� ����
	 * @param message
	 *            ��ʾ���� ����
	 * @param btnName
	 *            ��ť��� ����
	 * @param listener
	 *            ��������ʵ��android.content.DialogInterface.OnClickListener�ӿ� ����
	 * @return
	 */
	public static Dialog createNormalDialog(Context ctx,
			// int iconId,
			String title, String message, String btnName,
			OnClickListener listener) {
		Dialog dialog = null;
		android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(
				ctx);
		// ���öԻ����ͼ��
		// builder.setIcon(iconId);
		// ���öԻ���ı���
		builder.setTitle(title);
		// ���öԻ������ʾ����
		builder.setMessage(message);
		// ��Ӱ�ť��android.content.DialogInterface.OnClickListener.OnClickListener
		builder.setPositiveButton(btnName, listener);
		// ����һ����ͨ�Ի���
		dialog = builder.create();
		return dialog;

	}

	public static Dialog create2ButtonDialog(
			Context ctx,
			// int iconId,
			String title, String message, String btn1Name,
			OnClickListener listener1, String btn2Name,
			OnClickListener listener2) {
		Dialog dialog = null;
		android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(
				ctx);
		// ���öԻ����ͼ��
		// builder.setIcon(iconId);
		// ���öԻ���ı���
		builder.setTitle(title);
		// ���öԻ������ʾ����
		builder.setMessage(message);
		// ��Ӱ�ť��android.content.DialogInterface.OnClickListener.OnClickListener
		builder.setNegativeButton(btn1Name, listener1);
		builder.setPositiveButton(btn2Name, listener2);
		// ����һ����ͨ�Ի���
		dialog = builder.create();
		return dialog;

	}

	public static String readRawFile(Context ctx, int resId) {
		Resources res = ctx.getResources();
		InputStream in = null;
		BufferedReader br = null;
		StringBuffer sb = new StringBuffer();
		sb.append("");
		try {
			in = res.openRawResource(resId);
			String str;
			br = new BufferedReader(new InputStreamReader(in, "GBK"));
			while ((str = br.readLine()) != null) {
				sb.append(str);
				sb.append("\n");
			}
		} catch (NotFoundException e) {
			// Toast.makeText(this, "�ı��ļ�������", 100).show();
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// Toast.makeText(this, "�ı���������쳣", 100).show();
			e.printStackTrace();
		} catch (IOException e) {
			// Toast.makeText(this, "�ļ���ȡ����", 100).show();
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * check url then handler webview error
	 * 
	 * @param webView
	 * @param url
	 */

	public static void checkWebViewUrl(final WebView webView, final String url) {
		if (url == null || url.equals("")) {
			return;
		}
		new AsyncTask<String, Void, Integer>() {

			@Override
			protected Integer doInBackground(String... params) {
				int responseCode = -1;
				try {
					URL url = new URL(params[0]);
					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					responseCode = connection.getResponseCode();
				} catch (Exception e) {
					if (Constant.DEBUG_SWITCH)
						Log.e("Loading webView error:", e.getMessage());
					// Toast.makeText(ctx, "��������l��״��.",
					// Toast.LENGTH_SHORT).show();
				}
				return responseCode;
			}

			@Override
			protected void onPostExecute(Integer result) {
				if (result != 200) {
					// webView.setVisibility(View.GONE);
					webView.getSettings().setDefaultTextEncodingName("utf-8");
					if (!Constant.NET_STATUS)
						webView.loadData("����l��״���,�޷���������ַ.", "text/html",
								"utf-8");
				} else {
					webView.loadUrl(url);
				}
			}
		}.execute(url);
	}

	public static void saveParameter(Context context, String key, Object value) {
		SharedPreferences settings = context.getSharedPreferences("cooldriver",
				0);
		if (value instanceof Boolean) {
			settings.edit().putBoolean(key, (Boolean) value).commit();
		} else if (value instanceof Integer) {
			settings.edit().putInt(key, (Integer) value).commit();
		} else if (value instanceof Float) {
			settings.edit().putFloat(key, (Float) value).commit();
		} else if (value instanceof Long) {
			settings.edit().putLong(key, (Long) value).commit();
		} else {
			settings.edit().putString(key, value.toString()).commit();
		}
	}

	public static Object getParameter(Context context, String key,
			Object defValue) {
		SharedPreferences settings = context.getSharedPreferences("cooldriver",
				0);
		if (defValue instanceof Boolean) {
			return settings.getBoolean(key, (Boolean) defValue);
		} else if (defValue instanceof Float) {
			return settings.getFloat(key, (Float) defValue);
		} else if (defValue instanceof Long) {
			return settings.getLong(key, (Long) defValue);
		} else if (defValue == null) {
			return settings.getString(key, null);
		} else {
			return settings.getString(key, (String) defValue);
		}
	}
	public static String getRealPath(Uri fileUrl, Context mContext) {
		Uri uri = fileUrl;
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor actualimagecursor = ((Activity) mContext).managedQuery(uri,
				proj, null, null, null);
		int actual_image_column_index = actualimagecursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		actualimagecursor.moveToFirst();
		String img_path = actualimagecursor
				.getString(actual_image_column_index);
		return img_path;
	}
	public static FriendObject userToFriend(CodoUser user){
		FriendObject friendObject = new FriendObject();
		friendObject.face = user.face;
		friendObject.uid = user.id;
		friendObject.mycolor = user.mycolor;
		friendObject.name = user.name;
		friendObject.phone = user.phone;
		friendObject.singMessage = user.singMessage;
		friendObject.title = user.title;
		friendObject.username = user.username;
		return friendObject;
	}
	public static int getUserColor(String cols) {
		String color = cols;
		if (color != null && !color.equals("")) {
			int r = Integer.parseInt(color.substring(1, 3), 16);
			int g = Integer.parseInt(color.substring(3, 5), 16);
			int b = Integer.parseInt(color.substring(5), 16);
			int col = Color.rgb(r, g, b);
			return col;
		}
		return 0xff5e808c;
	}
	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();
			int i;
			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}
}
