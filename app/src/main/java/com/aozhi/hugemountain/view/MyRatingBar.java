package com.aozhi.hugemountain.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;


/**
 * Created by ${wangchangjian} on 2016/4/28.
 */
public class MyRatingBar extends LinearLayout {
    private ViewGroup view;
    private int numrRatings;

    public MyRatingBar(Context context) {
        this(context, null);
    }

    public MyRatingBar(Context context, AttributeSet attrs) {

        super(context, attrs);
        view = (ViewGroup) ((ViewGroup) LayoutInflater.from(MyApplication.getInstance()).inflate(R.layout.rating_bar_layout, this)).getChildAt(0);
    }

    public void setNumRatings(int ratings) {
        this.numrRatings = ratings;
        int nums = view.getChildCount();
        if (ratings > nums) {
        } else {
            for (int i = 0; i < nums; i++) {
                if (i > ratings - 1)
                    view.getChildAt(i).setVisibility(GONE);
            }
        }
    }
    public void setRating(int ratings) {
        for (int i = 0; i < numrRatings; i++) {
            ((RadioButton) view.getChildAt(i)).setChecked(false);//先清除上一题选中的状态
        }
        for (int i = 0; i < ratings; i++) {
            Log.i("setRating", ratings + "");

            ((RadioButton) view.getChildAt(i)).setChecked(true);
        }
    }
}
