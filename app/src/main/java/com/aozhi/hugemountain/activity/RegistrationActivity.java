package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationActivity extends Activity {
	Button btn_back,btn_refresh,btn_1,btn_2,btn_3,btn_ok;
	TextView tv_name;
	String mark="0";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_registration);
		initView();
		myonclick();
		tv_name.setText(MyApplication.Staffuser.name);
	}

	private void initView() {
		btn_back = (Button)findViewById(R.id.btn_back);
		tv_name=(TextView)findViewById(R.id.tv_name);
		btn_refresh = (Button)findViewById(R.id.btn_refresh);
		btn_1 = (Button)findViewById(R.id.btn_1);
		btn_2 = (Button)findViewById(R.id.btn_2);
		btn_3 = (Button)findViewById(R.id.btn_3);
		btn_ok = (Button)findViewById(R.id.btn_ok);
	}

	private void myonclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		btn_refresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
			}
		});
		btn_1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mark="1";
				btn_1.setBackgroundColor(getResources().getColor(R.color.n1));
				btn_2.setBackgroundColor(getResources().getColor(R.color.bg1));
				btn_3.setBackgroundColor(getResources().getColor(R.color.bg1));
			}
		});
		btn_2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mark="2";
				btn_1.setBackgroundColor(getResources().getColor(R.color.bg1));
				btn_2.setBackgroundColor(getResources().getColor(R.color.n1));
				btn_3.setBackgroundColor(getResources().getColor(R.color.bg1));
			}
		});
		btn_3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				mark="3";
				btn_1.setBackgroundColor(getResources().getColor(R.color.bg1));
				btn_2.setBackgroundColor(getResources().getColor(R.color.bg1));
				btn_3.setBackgroundColor(getResources().getColor(R.color.n1));
			}
		});
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(mark.equals("0")){
					Toast.makeText(RegistrationActivity.this, "请选择签到方式！", Toast.LENGTH_LONG).show();
				}else{
					ArrayList<String[]> params1 = new ArrayList<String[]>();
					String[] funParam1 = new String[] { "fun", "updatestaff" };
					String[] name1 = new String[] { "staff_id",MyApplication.Staffuser.id };
					String[] name2 = new String[] { "mark",mark };
					params1.add(funParam1);
					params1.add(name1);
					params1.add(name2);
					Constant.NET_STATUS = Utils.getCurrentNetWork(RegistrationActivity.this);
					if (Constant.NET_STATUS) {
						new HttpConnection().get(Constant.URL, params1,
								type_callbackListener);
					} else {
						Toast.makeText(RegistrationActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
								.show();
					}
				}
			}
		});
	}
	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Toast.makeText(RegistrationActivity.this, "签到已完成！", Toast.LENGTH_LONG)
				.show();
			}
		}
	};
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

}
