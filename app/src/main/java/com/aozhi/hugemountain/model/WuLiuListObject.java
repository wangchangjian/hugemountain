package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class WuLiuListObject {
	public Meta meta;
	public ArrayList<WuLiuBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<WuLiuBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<WuLiuBean> response) {
		this.response = response;
	}
}
