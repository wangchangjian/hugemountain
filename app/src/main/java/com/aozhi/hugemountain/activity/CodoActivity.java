package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CodoActivity extends Activity implements OnClickListener {

	private Button btn_back, btn_yz, btn_next;
	private TextView tv_tel;
	private String tel = "",types="";
	private String num = "";
	private TimeCount time;
	private EditText et_yanzhengma;
	// 短信验证
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LoginListObject mMemberListObject;
	private ProgressDialog progressDialog = null;
	String APPKEY = "a60f6c4438e8";
	String APPSECRET = "35b4d102dd365758244b77426d483d70";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_codo);
		initView();
		initListnner();
		time = new TimeCount(60000, 1000);// 构造CountDownTimer对象
		tel = getIntent().getStringExtra("tel");
		types = getIntent().getStringExtra("types");
		SMSSDK.initSDK(this,APPKEY,APPSECRET);
		EventHandler eh=new EventHandler(){
			@Override
			public void afterEvent(int event, int result, Object data) {
				Message msg = new Message();
				msg.arg1 = event;
				msg.arg2 = result;
				msg.obj = data;
				handler.sendMessage(msg);
			}
		};
		SMSSDK.registerEventHandler(eh);
		SMSSDK.getVerificationCode("86",tel);
	}

	/* 定义一个倒计时的内部类 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
		}

		public void onFinish() {// 计时完毕时触发
			btn_yz.setText("重新验证");
			btn_yz.setClickable(true);
		}

		public void onTick(long millisUntilFinished) {// 计时过程显示
			btn_yz.setClickable(false);
			btn_yz.setText(millisUntilFinished / 1000 + "秒");
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		tel = getIntent().getStringExtra("tel");
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_next = (Button) findViewById(R.id.btn_next);
		btn_yz = (Button) findViewById(R.id.btn_yz);
		tv_tel = (TextView) findViewById(R.id.tv_tel);
		et_yanzhengma = (EditText) findViewById(R.id.et_yanzhengma);
		 tv_tel.setText(tel.substring(0,
		 tel.length() - (tel.substring(3)).length())
		 + "******" + tel.substring(9)+"发送数字短信验证码");
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		btn_yz.setOnClickListener(this);
		btn_next.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
//			Intent mIntent = new Intent(CodoActivity.this,
//					FindPwd1Activity.class);
//			startActivity(mIntent);
			finish();
		}
			break;
		case R.id.btn_yz: {
			time.start();// 开始计时
			if (tel.equals("")) {
				Toast.makeText(CodoActivity.this, getString(R.string.tel),
						Toast.LENGTH_SHORT).show();
			} else if (tel.length() > 11 || tel.length() < 11) {
				Toast.makeText(CodoActivity.this,
						getString(R.string.tel_error), Toast.LENGTH_SHORT)
						.show();
			} else {
				SMSSDK.getVerificationCode("86",tel);
				GetUsername();
			}
		}
			break;
		case R.id.btn_next: {
			if (TextUtils.isEmpty(et_yanzhengma.getText().toString())) {
				Toast.makeText(CodoActivity.this,
						getString(R.string.yanzhengma), Toast.LENGTH_SHORT)
						.show();
			} else if (!TextUtils.isEmpty(et_yanzhengma.getText().toString())) {
				SMSSDK.submitVerificationCode("86", tel, et_yanzhengma.getText().toString());
			} 
		}
			break;
		default:
			break;
		}
	}

	Handler handler=new Handler(){
		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			int event = msg.arg1;
			int result = msg.arg2;
			Object data = msg.obj;
			Log.e("event", "event="+event);
			if (result == SMSSDK.RESULT_COMPLETE) {
				//短信注册成功后，返回MainActivity,然后提示新好友
				if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {//提交验证码成功
					Toast.makeText(getApplicationContext(), "提交验证码成功", Toast.LENGTH_SHORT).show();
					Intent mIntent = new Intent(CodoActivity.this,FindPwd3Activity.class);
					mIntent.putExtra("tel", tel);
					mIntent.putExtra("types", types);
					startActivity(mIntent);
					finish();
				} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE){
					Toast.makeText(getApplicationContext(), "验证码已经发送", Toast.LENGTH_SHORT).show();
				}else if (event ==SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES){//返回支持发送验证码的国家列表
					Toast.makeText(getApplicationContext(), "获取国家列表成功", Toast.LENGTH_SHORT).show();
				}
			} else {
				((Throwable) data).printStackTrace();
			}
			
		}
		
	};
	private void GetUsername() {
		// TODO Auto-generated method stub
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam=null;
			if(types.equals("1")){
				methodParam = new String[] { "fun", "findClientname" };
			}else if(types.equals("2")){
				methodParam = new String[] { "fun", "findStorename" };
			}else{
				methodParam = new String[] { "fun", "findStaffname" };
			}
			String[] pageParam = new String[] { "username", tel };
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetInFo_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetInFo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mMemberListObject = JSON.parseObject(result,
						LoginListObject.class);
				list = mMemberListObject.response;
				if (mMemberListObject.meta.getMsg().equals("OK")) {
					if (Integer.valueOf(list.get(0).count) <= 0) {
						Toast.makeText(CodoActivity.this, "该手机号未注册，请重新输入手机号码",
								Toast.LENGTH_LONG).show();
					} else {
						yangzhengma();
					}

				}

			}
		}
	};

	private void yangzhengma() {
		if (!tel.equals("")) {
			if (tel.length() > 11 || tel.length() < 11) {
				Toast.makeText(CodoActivity.this,
						getString(R.string.tel_error), Toast.LENGTH_SHORT)
						.show();
			} else {
				time.start();// 开始计时
				SMSSDK.getVerificationCode("86", tel);
				Handler handler = new Handler() {
					@Override
					public void handleMessage(Message msg) {
						// TODO Auto-generated method stub
						super.handleMessage(msg);
						int event = msg.arg1;
						int result = msg.arg2;
						Object data = msg.obj;
						Log.e("event", "event=" + event);
						if (result == SMSSDK.RESULT_COMPLETE) {
							// 短信注册成功后，返回MainActivity,然后提示新好友
							if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {// 提交验证码成功
								
								Toast.makeText(getApplicationContext(),
										"提交验证码成功", Toast.LENGTH_SHORT).show();
							} else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
								Toast.makeText(getApplicationContext(),
										"验证码已经发送", Toast.LENGTH_SHORT).show();
							} else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {// 返回支持发送验证码的国家列表
								Toast.makeText(getApplicationContext(),
										"获取国家列表成功", Toast.LENGTH_SHORT).show();
							}
						} else {
							((Throwable) data).printStackTrace();
							Toast.makeText(CodoActivity.this, "验证码错误",
									Toast.LENGTH_SHORT).show();
						}
					}
				};
			}
		} else {
			Toast.makeText(CodoActivity.this, getString(R.string.tel),
					Toast.LENGTH_SHORT).show();
		}
	}
}
