package com.aozhi.hugemountain.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.adapter.OrderStaffAdapter;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;

import java.util.ArrayList;

/**
 * Created by ${wangchangjian} on 2016/4/25.
 */
public class StaffTodayOrderView extends OrderView {
    private OrderListObject mOrderListObject;
    public StaffTodayOrderView(Context context) {
        this(context, null);
    }
    public StaffTodayOrderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initListener_staffofday();
    }



    private void initListener_staffofday() {
        ArrayList<String[]> params1 = new ArrayList<String[]>();
        String[] funParam1 = new String[]{"fun", "getorderformbystaff1"};
        String[] name1 = new String[]{"id", MyApplication.Staffuser.id};
        params1.add(funParam1);
        params1.add(name1);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this.getContext());
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params1, typeday_callbackListener);
        } else {
            Toast.makeText(this.getContext(), "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private OrderStaffAdapter adapter;
    private HttpConnection.CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (jumpProgressDialog != null) {
                jumpProgressDialog.dimissDialog();
            }
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mOrderListObject = JSON.parseObject(v, OrderListObject.class);
                list = mOrderListObject.response;
                if (mOrderListObject.meta.getMsg().equals("OK")) {
                    list_order.setVisibility(View.VISIBLE);
                    adapter = new OrderStaffAdapter(StaffTodayOrderView.this.getContext(), list);
                    list_order.setAdapter(adapter);
                } else {
                    list_order.setVisibility(View.GONE);
                }
            }
        }
    };

}
