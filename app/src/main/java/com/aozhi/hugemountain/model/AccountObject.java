package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class AccountObject implements Serializable{
	public String store_id;
	public String staff_id;
	public String del_flag;
	public String card_number;
	public String card_address;
	public String zfb_name;
	public String create_time;
	public String remarks;
	public String card_type;
	public String zfb_username;
}
