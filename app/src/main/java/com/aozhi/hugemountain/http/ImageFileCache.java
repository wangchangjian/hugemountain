package com.aozhi.hugemountain.http;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Comparator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;
import android.widget.Toast;

public class ImageFileCache {
	
	private static final String CACHDIR = "kayou/cache";
	private static final int FREE_SD_SPACE_NEEDED_TO_CACHE = 10;
	private int MB = 1024 * 1024;
	private static final String WHOLESALE_CONV = ".cache";
	private static final int CACHE_SIZE = 10;
	
	public ImageFileCache() {
		removeCache(getDirectory());
	}

	private boolean removeCache(String dirPath) {
		final File dir = new File(dirPath);
		final File[] files = dir.listFiles();
		if (files == null) {
			return true;
		}

		if (!android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			return false;
		}

		int dirSize = 0;
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().contains(WHOLESALE_CONV)) {
				dirSize += files[i].length();
			}
		}

		if (dirSize > CACHE_SIZE * MB || FREE_SD_SPACE_NEEDED_TO_CACHE > freeSpaceOnSd()) {
			final int removeFactor = (int) ((0.4 * files.length) + 1);

			Arrays.sort(files, new FileLastModifSort());

			for (int i = 0; i < removeFactor; i++) {

				if (files[i].getName().contains(WHOLESALE_CONV)) {
					files[i].delete();
				}
			}
		}
		if (freeSpaceOnSd() <= CACHE_SIZE) {
			return false;
		}
		return true;
	}
	private class FileLastModifSort implements Comparator<File> {
		public int compare(File arg0, File arg1) {
			if (arg0.lastModified() > arg1.lastModified()) {
				return 1;
			} else if (arg0.lastModified() == arg1.lastModified()) {
				return 0;
			} else {
				return -1;
			}
		}
	}
	
	public Bitmap getImage(final String url) {
		
		final String path = getDirectory() + "/" + convertUrlToFileName(url);
		final File file = new File(path);
		if (file.exists()) {
			try {
				final Bitmap bmp = BitmapFactory.decodeFile(path);
				if (bmp == null) {
					file.delete();
				} else {
					updateFileTime(path);
					return bmp;
				}
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	private String getDirectory() {
		String dir = getSDPath() ;
		Log.e("ImageFileCache", "dir ===== " + dir);
		return dir;
	}
	
	
    public void getSdCardPath(String url,Bitmap bitmap) {
    	try {
            if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            	 	String fileDirectory = getSDPath();
            	 	Log.v("", "fileDirectory === " + fileDirectory);
                    //Ŀ¼
                    File dir = new File(fileDirectory);
                    if (!dir.exists()) {
                    	dir.mkdirs();
                    }
                    Log.v("", "path.exists() === " + dir.exists());
                    //�ļ�
                    final String fileName = convertUrlToFileName(url);
                    File file = new File(fileDirectory + "/" + fileName);
                    if (!file.exists()) {
                    	try {
                        	
	                       	 file.createNewFile();
	                   		 final OutputStream outStream = new FileOutputStream(file);
                       	 
	                   		 bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
	                   		 outStream.flush();
	                   		 outStream.close();
                           
                       } catch (FileNotFoundException e) {
                           e.printStackTrace();
                       } catch (IOException e) {
                           e.printStackTrace();
                       } catch (OutOfMemoryError e) {
                           e.printStackTrace();
                       }
                    }
                
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	public String getSDPath() {
		File sdcardDir = Environment.getExternalStorageDirectory();
        String path = sdcardDir.getParent() + "/" + sdcardDir.getName();
        final String fileDirectory = path + "/" + CACHDIR;
		return fileDirectory;
	}
	
	private String convertUrlToFileName(String url) {
		 final String[] strs = url.split("/");
		 String fileName = strs[strs.length - 1];
		return fileName/*getMD5Str(url) + WHOLESALE_CONV*/;
	}
	public void updateFileTime(String path) {
		final File file = new File(path);
		final long newModifiedTime = System.currentTimeMillis();
		file.setLastModified(newModifiedTime);
	}
	
    public void saveBmpToSd(Bitmap bitmap, String url) {
        if (bitmap == null) {
            return;
        }
        
        if (FREE_SD_SPACE_NEEDED_TO_CACHE > freeSpaceOnSd()) {
            return;
        }
        
        getSdCardPath(url, bitmap);
        
    }
	
	private int freeSpaceOnSd() {
		final StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
		final double sdFreeMB = ((double) stat.getAvailableBlocks() * (double) stat.getBlockSize())/ MB;
				
		return (int) sdFreeMB;
	}

    public boolean deleteImage(final String url) {
        final String path = getDirectory() + "/" + convertUrlToFileName(url);
        final File file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }
}
