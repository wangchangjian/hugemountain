package com.aozhi.hugemountain.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ClearingAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ListView;

public class ClearingActivity extends Activity {
	Button btn_back,btn_ok;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	private ArrayList<ConsumptionObject> list_bydate = new ArrayList<ConsumptionObject>();
	private ClearingAdapter adapter;
	private ListView list_clearing;
	TextView tv_date1, tv_date2;
	private int mYear;
	private int mMonth;
	private int mDay;
	static final int DATE_DIALOG_ID = 0;
	String date_state="";
	ConsumptionListObject mConsumptionListObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_clearing);
		initView();
		myclick();
		getclearing();
	}

	public int textparseint(String  textdate) {
		try {
		return	Integer.parseInt(new SimpleDateFormat("yyyyMMdd")
					.format(new SimpleDateFormat("yyyy-MM-dd")
							.parse(textdate)));
		} catch (NumberFormatException e) {
			return 0;
		} catch (ParseException e) {
			return 0;
		}
	}

	private void initView() {
		list_clearing = (ListView) findViewById(R.id.list_clearing);
		adapter = new ClearingAdapter(this, list);
		list_clearing.setAdapter(adapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_date1 = (TextView) findViewById(R.id.tv_date1);
		tv_date2 = (TextView) findViewById(R.id.tv_date2);
		btn_ok=(Button) findViewById(R.id.btn_ok);
		Calendar mycalendar=Calendar.getInstance(Locale.CHINA);
        Date mydate=new Date(); //获取当前日期Date对象
        mycalendar.setTime(mydate);////为Calendar对象设置时间为当前日期
        mYear=mycalendar.get(Calendar.YEAR); //获取Calendar对象中的年
        mMonth=mycalendar.get(Calendar.MONTH);//获取Calendar对象中的月
        mDay=mycalendar.get(Calendar.DAY_OF_MONTH);//获取这个月的第几天
	}

	private void myclick() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		tv_date1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				date_state = "1";
				showDialog(DATE_DIALOG_ID);
			}
		});
		tv_date2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				date_state = "2";
				showDialog(DATE_DIALOG_ID);
			}
		});
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!tv_date1.getText().equals("年-月-日")&&!tv_date2.getText().equals("年-月-日")){
					list_bydate.clear();
				for(int i=0;i<list.size();i++){
					if(textparseint(list.get(i).create_time)>=textparseint(tv_date1.getText().toString())&&textparseint(list.get(i).create_time)<=textparseint(tv_date2.getText().toString())){
						list_bydate.add(list.get(i));
					}
				}
				
				adapter = new ClearingAdapter(ClearingActivity.this,list_bydate);
				list_clearing.setAdapter(adapter);
				}else{
					Toast.makeText(ClearingActivity.this, "请选择开始及结束时间！", Toast.LENGTH_LONG).show();
				}
				
			}
		});
	}

	private void updateDisplay() {
		if(date_state.equals("1")){
		tv_date1.setText(new StringBuilder().append(mYear).append("-").append(mMonth + 1).append("-").append(mDay).append(" "));
		}else{
			tv_date2.setText(new StringBuilder().append(mYear).append("-")
					.append(mMonth + 1).append("-").append(mDay).append(" "));
		}
	}

	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			mYear = year;
			mMonth = monthOfYear;
			mDay = dayOfMonth;
			updateDisplay();
		}
	};
	
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
					mDay);
		}
		return null;
	}
	private void getclearing() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreconsumption" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };

		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v, ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				adapter = new ClearingAdapter(ClearingActivity.this,list);
				list_clearing.setAdapter(adapter);
			}
		}
	};
}
