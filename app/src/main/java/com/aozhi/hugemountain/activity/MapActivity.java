package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.model.LatLng;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class MapActivity extends Activity {
	private ArrayList<StoreObject> slist = new ArrayList<StoreObject>();
	private StoreListObject mStoreListObject;
	private ProgressDialog progressDialog = null;
	private Button btn_back;
	private int i = 0;
	private int index;
	MapView mMapView;
	private BaiduMap mBaiduMap;
	private String seller_id = "";
	private String store_name, latitude, longitude, city;
	private TextView title;
	 public MyLocationListenner myListener = new MyLocationListenner();
	 private LocationClient mLocClient;
		private LocationMode mCurrentMode;
		BitmapDescriptor mCurrentMarker;
		HorizontalScrollView horizontalScrollView;
		DisplayMetrics dm;
		private int NUM = 5; // 每行显示个数
		private int hSpacing = 20;// 水平间距
		// UI相关
		OnCheckedChangeListener radioButtonListener;
		Button requestLocButton;
		boolean isFirstLoc = true;// 是否首次定位
	    
	    
	    
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_map);
		city = getIntent().getStringExtra("city");
		seller_id = getIntent().getStringExtra("seller_id");
		store_name = getIntent().getStringExtra("storename");
		latitude = getIntent().getStringExtra("latitude");
		longitude = getIntent().getStringExtra("longitude");
		title = (TextView) findViewById(R.id.title);
		title.setText(store_name);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
//		mMapView = (MapView) findViewById(R.id.bmapView);
//		mBaiduMap = mMapView.getMap();
//		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
//		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(14.0f);// 设置地图的缩放比例
//		mBaiduMap.setMapStatus(msu);// 将前面的参数交给BaiduMap类
		
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(18.0f);// 设置地图的缩放比例
		mBaiduMap.setMapStatus(msu);// 将前面的参数交给BaiduMap类

//		View child = mMapView.getChildAt(1);
		// 隐藏百度logo和缩放控件ZoomControl
//		mMapView.showZoomControls(false);
//		Location();
		getMapList(city);
		
		
	}
	/**
	 * 定位SDK监听函数
	 */
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
			mBaiduMap.setMyLocationData(locData);

			if (isFirstLoc) {
				isFirstLoc = false;
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
			}
		}

		public void onReceivePoi(BDLocation poiLocation) {
		}
	}

	private void Location() {

		// 地图初始化
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
	}
	@Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
    }

 
	private void getMapList(String city) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			// String[] funParam2 = new String[] { "fun", "getstorelist"};
			// String[] name1 = new String[] { "seller_id", seller_id };
			// params2.add(funParam2);
			// params2.add(name1);
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "ak",
					"cHuUTMwABwmzf3HQ8BKtB8Uh" };
			String[] geotable_idParam = new String[] { "geotable_id", "125660" };

			String[] geo_position1 = new String[] { "region", city };

			String[] sortbyParam = new String[] { "sortby", "distance:1" };
			// http://api.map.baidu.com/geosearch/v3/local?ak=cHuUTMwABwmzf3HQ8BKtB8Uh&geotable_id=125660&sortby=distance:1

			params.add(methodParam);
			params.add(geotable_idParam);
			params.add(geo_position1);

			params.add(sortbyParam);

			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);

			new HttpConnection().get(Constant.BAIDUURL1, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

	}


	
	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStoreListObject = JSON.parseObject(result,
						StoreListObject.class);
				slist = mStoreListObject.contents;

				if (slist.size() > 0) {
					for (i = 0; i < slist.size(); i++) {

						String str = slist.get(i).location;
						String[] sourceStrArray = str.substring(1,
								str.length() - 1).split(",");

						initOverLay(Double.valueOf(sourceStrArray[1]),
								Double.valueOf(sourceStrArray[0]), i);
						index = i;
					}
					Locations();
				}
				mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
					public boolean onMarkerClick(final Marker marker) {
						// 创建InfoWindow展示的view
						Button button = new Button(getApplicationContext());
						button.setBackgroundResource(R.drawable.popup);
						button.setTextColor(getResources().getColor(
								R.color.black));
						index = marker.getZIndex();
						button.setText(slist.get(index).name);
						// 定义用于显示该InfoWindow的坐标点
						String str = slist.get(index).location;
						String[] sourceStrArray = str.substring(1,
								str.length() - 1).split(",");

						LatLng pt = new LatLng(Double
								.valueOf(sourceStrArray[1]), Double
								.valueOf(sourceStrArray[0]));
						// 创建InfoWindow , 传入 view， 地理坐标， y
						// 轴偏移量
						InfoWindow mInfoWindow = new InfoWindow(button, pt, -47);
						// 显示InfoWindow
						mBaiduMap.showInfoWindow(mInfoWindow);
						button.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View arg0) {
								// TODO Auto-generated
								// method stub

							}
						});
						return true;
					}
				});

			}
		}
	};
	private InfoWindow mInfoWindow;

	public void InfoWindow(int index, double latitude, double longitude) {

		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			public boolean onMarkerClick(final Marker marker) {
				Button button = new Button(getApplicationContext());
				button.setBackgroundResource(R.drawable.popup);
				OnInfoWindowClickListener listener = null;
				int index = marker.getZIndex();
				String btnname = slist.get(index).name;
				button.setText(btnname);
				button.setTextColor(getResources().getColor(R.color.black));
				listener = new OnInfoWindowClickListener() {
					public void onInfoWindowClick() {
						LatLng ll = marker.getPosition();
						LatLng llNew = new LatLng(ll.latitude, ll.longitude);
						marker.setPosition(llNew);
						mBaiduMap.hideInfoWindow();
					}

				};
				LatLng ll = marker.getPosition();
				mInfoWindow = new InfoWindow(BitmapDescriptorFactory
						.fromView(button), ll, -47, listener);
				mBaiduMap.showInfoWindow(mInfoWindow);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Au`to-generated method stub
						// Intent mIntent = new Intent(HomeActivity.this,
						// MerchantdetailsActivity.class);
						// mIntent.putExtra("mMerchantObject", mMerchantObject);
						// startActivity(mIntent);
					}
				});
				return true;
			}

		});

	}

	public void initOverLay(double latitude, double longitude, int i) {
		// 定义Maker坐标点
		LatLng ll = new LatLng(latitude, longitude);
		// 构建Marker图标
		if (slist.get(i).name.equals("我的位置")) {

			BitmapDescriptor bd1 = BitmapDescriptorFactory
					.fromResource(R.drawable.location);
			MarkerOptions option = new MarkerOptions().position(ll).icon(bd1);
			option.zIndex(i);
			option.title("uiuiu");
			// 在地图上添加Marker，并显示
			mBaiduMap.addOverlay(option);// 添加当前分店信息图层
			MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
			mBaiduMap.setMapStatus(u);
		} else {
			BitmapDescriptor bd1 = BitmapDescriptorFactory
					.fromResource(R.drawable.icon_gcoding);
			MarkerOptions option = new MarkerOptions().position(ll).icon(bd1);
			option.zIndex(i);
			option.title("uiuiu");
			// 在地图上添加Marker，并显示
			mBaiduMap.addOverlay(option);// 添加当前分店信息图层
			MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
			mBaiduMap.setMapStatus(u);
		}

		// 构建MarkerOption，用于在地图上添加Marker\

	}
	private void Locations() {
		requestLocButton = (Button) findViewById(R.id.button1);
		mCurrentMode = LocationMode.NORMAL;
		requestLocButton.setText("普通");
		OnClickListener btnClickListener = new OnClickListener() {
			public void onClick(View v) {
				switch (mCurrentMode) {
				case NORMAL:
					requestLocButton.setText("跟随");
					mCurrentMode = LocationMode.FOLLOWING;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				case COMPASS:
					requestLocButton.setText("普通");
					mCurrentMode = LocationMode.NORMAL;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				case FOLLOWING:
					requestLocButton.setText("罗盘");
					mCurrentMode = LocationMode.COMPASS;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				}
			}
		};
		requestLocButton.setOnClickListener(btnClickListener);

		RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
		radioButtonListener = new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.defaulticon) {
					// 传入null则，恢复默认图标
					mCurrentMarker = null;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, null));
				}
				if (checkedId == R.id.customicon) {
					// 修改为自定义marker
					mCurrentMarker = BitmapDescriptorFactory
							.fromResource(R.drawable.icon_geo);
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
				}
			}
		};
		group.setOnCheckedChangeListener(radioButtonListener);

		// 地图初始化
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		// 定位初始化
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(10000);
		option.setIsNeedAddress(true);

		mLocClient.setLocOption(option);
		mLocClient.start();
	}
}
