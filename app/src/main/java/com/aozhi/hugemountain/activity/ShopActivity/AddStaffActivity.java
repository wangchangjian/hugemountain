package com.aozhi.hugemountain.activity.ShopActivity;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ManagementStaffActivity;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.adapter.ImageAddAdapter;
import com.aozhi.hugemountain.adapter.StaffCodeAdapter;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.StaffsListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Spinner;

public class AddStaffActivity extends BaseActivity {
	private Button btn_back;
	private Button btn_sure;
	private Button chack;
	private ArrayList<ManageStaffObject> list = new ArrayList<ManageStaffObject>();
	private ArrayList<ManageStaffObject> type_list = new ArrayList<ManageStaffObject>();
	private ArrayList<ManageStaffObject> codelist = new ArrayList<ManageStaffObject>();
	private ArrayList<ManageStaffObject> backlist = new ArrayList<ManageStaffObject>();
	private ArrayList<ManageStaffObject> fuwulist = new ArrayList<ManageStaffObject>();
	private ArrayList<String> timelist = new ArrayList<String>();
	private ArrayList<String> codes = new ArrayList<String>();
	private StaffsListObject mStaffsListObject;
	private Spinner tv_type;
	private ProgressDialog progressDialog = null;
	private EditText et_number;
	private EditText et_pwd, et_commission, et_phone, et_context;
	private TextView et_server, tv_service;
	final int RESULT_CODE = 101;
	private String typeid = "";
	private String user_id = "";
	private GridView photo_add;
	private List<Bitmap> listphoto = new ArrayList<Bitmap>();
	private ImageAddAdapter adapter;
	private Bitmap bitmap;
	private TextView tv_camera, tv_photo;
	private LinearLayout img_liner;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// 拍照
	public static final int PHOTOZOOM = 2; // 缩放
	public static final int PHOTORESOULT = 3;// 结果
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String serverFile = "";
	private ArrayList<String> pt = new ArrayList<String>();
	private String service_ids = "";
	private ListView list_fuwu;
	private StaffCodeAdapter mStaffCodeAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_addstaff);
		chack = (Button) findViewById(R.id.chack);
		chack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				showFuwu();
			}
		});
		photo_add = (GridView) findViewById(R.id.photo_add);
		Bitmap bmp = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_addpic_focused);
		listphoto.add(bmp);
		adapter = new ImageAddAdapter(this, listphoto);
		photo_add.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showAtaver();
			}
		});
		photo_add.setAdapter(adapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		btn_sure = (Button) findViewById(R.id.btn_sure);
		btn_sure.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (et_commission.getText().toString().equals("")) {
					Toast.makeText(AddStaffActivity.this, "请输入员工分成",
							Toast.LENGTH_LONG).show();
				} else {
					getInfo();

				}
			}
		});
		et_pwd = (EditText) findViewById(R.id.et_pwd);
		et_number = (EditText) findViewById(R.id.et_number);
		tv_type = (Spinner) findViewById(R.id.tv_type);
		et_commission = (EditText) findViewById(R.id.et_commission);
		et_context = (EditText) findViewById(R.id.et_context);
		tv_service = (TextView) findViewById(R.id.tv_service);
		tv_service.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getService();
			}
		});
		getType();
	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {

	}

	private void getInfo() {

		if (et_number.getText().toString().trim().equals("")) {
			Toast.makeText(getApplicationContext(), "请输入员工编号",
					Toast.LENGTH_SHORT).show();
		} else {
			for (int i = 0; i < codes.size(); i++) {
				if (et_number.getText().toString().trim().equals(codes.get(i))) {
					user_id = codelist.get(i).id;
					break;
				}
				if (i == codes.size()) {
					Toast.makeText(getApplicationContext(), "您输入员工编号不正确",
							Toast.LENGTH_SHORT).show();
					return;
				}
			}

		}
		if (et_commission.getText().toString().trim().equals("")) {
			Toast.makeText(getApplicationContext(), "请输入员工分成",
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (tv_service.getText().toString().trim().equals("")) {
			Toast.makeText(getApplicationContext(), "请选择员工服务项目",
					Toast.LENGTH_SHORT).show();
			return;
		}
		AddStaffInfo();
	}

	private void getType() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStafftype" };
		params2.add(funParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				list = mStaffsListObject.getResponse();
				if (list.size() > 0) {
					Spinner(list);
				}

			} else {
				Toast.makeText(AddStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void AddStaffInfo() {
		if (user_id.equals("")) {
			Toast.makeText(getApplicationContext(), "员工不存在或已添加，请仔细检查",
					Toast.LENGTH_SHORT).show();
			return;
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		// fun=addStaffofStore&type_id=2&store_id=20151030225703&commission=2&contents=hxhcbbvjcn-&staff_id=30
		String[] funParam2 = new String[] { "fun", "addStaffofStore" };
		String[] nameParam = new String[] { "type_id", typeid }; // 职位状态
		String[] nameParam1 = new String[] { "store_id", MyApplication.Storeuser.id};
		String[] nameParam2 = new String[] { "commission",
				et_commission.getText().toString() };
		String[] nameParam3 = new String[] { "contents",
				et_context.getText().toString().trim() };
		String[] nameParam4 = new String[] { "staff_id", user_id };
		String[] nameParam5 = new String[] { "service_id", service_ids };
		String[] nameParam6 = new String[] { "service_name", tv_service.getText().toString() };
		params2.add(funParam2);
		params2.add(nameParam);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddStaffInfo_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				backlist = mStaffsListObject.response;
				Toast.makeText(AddStaffActivity.this, "添加成功", Toast.LENGTH_LONG)
						.show();
				delStaffService();
				for (int i = 0; i < pt.size(); i++) {
					UpStaffPhoto(pt.get(i));
				}
				Intent mintent = new Intent(getApplicationContext(),
						ManagementStaffActivity.class);
				startActivityForResult(mintent, 30);
				finish();
			} else {
				Toast.makeText(AddStaffActivity.this, "添加失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}

	private void Spinner(final ArrayList<ManageStaffObject> list) {

		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		for (int i = 0; i < list.size(); i++) {
			timelist.add(list.get(i).types);
		}
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, timelist);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		tv_type.setAdapter(adapter);
		// 第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		tv_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				typeid = list.get(position).id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				typeid = list.get(0).types;
			}
		});
		// 设置默认显示
		SpinnerAdapter apsAdapter = tv_type.getAdapter(); // 得到SpinnerAdapter对象
		int k = apsAdapter.getCount();
		if (list.size() > 0) {
			for (int i = 0; i < k; i++) {
				if (list.get(0).types.equals(apsAdapter.getItem(i).toString())) {
					tv_type.setSelection(i, true);// 默认选中项
					break;
				}
			}
		}
	}

	// 服务选项
	private void showMultiChosDia(final ArrayList<ManageStaffObject> list) {
		final String[] mList = new String[list.size()];
		final boolean mChoseSts[] = new boolean[list.size()];
		String serverStr=this.tv_service.getText().toString();
		for (int i = 0; i < list.size(); i++) {
			mList[i] = list.get(i).name;
			if(serverStr.indexOf(list.get(i).name)>-1){
				mChoseSts[i] = true;
			}else{
			mChoseSts[i] = false;}
		}
		AlertDialog.Builder multiChosDia = new AlertDialog.Builder(
				AddStaffActivity.this);
		multiChosDia.setMultiChoiceItems(mList, mChoseSts,
				new DialogInterface.OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						mChoseSts[which] = isChecked;
					}
				});
		multiChosDia.setPositiveButton("确定",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String str = "";
						service_ids="";
						for (int i = 0; i < mChoseSts.length; i++) {
							if (mChoseSts[i]) {
								str += mList[i] + ",";
								service_ids += list.get(i).store_ser + ",";
							}
						}
//						tv_service.setText(str);
						
//						String str = "";
//						serv_id="";
//						for (int i = 0; i <mChoseSts.length; i++) {
//							if(mChoseSts[i]){
//							str += mList[i] + ",";
//							serv_id+=list.get(i).id+",";
//							}
//						}
//						
						if(mChoseSts.length>0){
							if(!str.equals("")){
						str=str.substring(0,str.length()-1);
						service_ids=","+service_ids;
						tv_service.setText(str);}
						}
					}
				});
		multiChosDia.create().show();
	}

	// 获取店铺服务列表
	private void getService() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservicebystore" };
		String[] nameParam = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(nameParam);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					service_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener service_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				type_list = mStaffsListObject.getResponse();
				if (type_list.size() > 0) {
					showMultiChosDia(type_list);
				}

			} else {
				Toast.makeText(AddStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	// 获取员工编号
	private void getUserCode(String code) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getAddUserStaffs" };
		String[] param = new String[] { "store_id",
				MyApplication.Storeuser.getId() };
		String[] nameParam = new String[] { "code", code };
		params2.add(funParam2);
		params2.add(param);
		params2.add(nameParam);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					usercode_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener usercode_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				codelist = mStaffsListObject.response;
				if (mStaffsListObject.meta.getMsg().equals("OK")) {
					mStaffCodeAdapter = new StaffCodeAdapter(
							AddStaffActivity.this, codelist);
					list_fuwu.setAdapter(mStaffCodeAdapter);
					if (codelist.size() > 0) {
						for (int i = 0; i < codelist.size(); i++) {
							codes.add(codelist.get(i).code_id);
						}
					}
				} else {
					Toast.makeText(AddStaffActivity.this,
							"没有可以增加的员工哦，请新增员工后在添加", Toast.LENGTH_SHORT).show();
				}

			} else {
				Toast.makeText(AddStaffActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.renwufabu_shooting);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, 11);
				dlg.cancel();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(new File(Environment
								.getExternalStorageDirectory(), "juyue.jpg")));
				startActivityForResult(intent, 12);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode != RESULT_OK) {
			return;
		}
		if (data != null) {
			switch (requestCode) {
			case 11:
				startPhotoZoom(data.getData());
				break;
			case 12:
				Log.i("OnActivityResult", "**********************case camera!");
				File temp = new File(Environment.getExternalStorageDirectory()
						+ "/juyue.jpg");
				startPhotoZoom(Uri.fromFile(temp));
				break;

			case 13: {
				setPicToView(data);
			}
				break;

			default:
				break;
			}

		} else if (Build.MODEL.equals("MI 2C")) {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			startPhotoZoom(Uri.fromFile(temp));
		} else {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			if (temp.exists()) {
				startPhotoZoom(Uri.fromFile(temp));
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			pt.add(serverFile);
			return serverFile;
		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 320);
		intent.putExtra("outputY", 320);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 13);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	@SuppressLint({ "NewApi", "SdCardPath" })
	private void setPicToView(Intent picdata) {
		Bundle extras = picdata.getExtras();
		if (extras != null) {
			bitmap = extras.getParcelable("data");
			String sdStatus = Environment.getExternalStorageState();
			if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
				// Log.i("TestFile"
				// "SD card is not avaiable/writeable right now.");
				return;
			}
			@SuppressWarnings("static-access")
			String fileName = new DateFormat().format("yyyyMMdd_hhmmss",
					Calendar.getInstance(Locale.CHINA)) + ".jpg";
			File dir = new File("/sdcard/juyue/cache/");
			dir.mkdirs();// 创建文件夹

			fileName = "/sdcard/juyue/cache/" + fileName;
			String result = Utils.saveBitmapToFile(bitmap, fileName);
			if (result == null || result.trim().equals("")) {
				Utils.DisplayToast(this, "存储空间不足");
			}
			// @SuppressWarnings("unused")
			File file = new File(fileName);
			// img_add.setImageBitmap(bitmap);
			new UploadAsyncTask().execute(file);
			if (listphoto.size() < 9) {
				listphoto.add(bitmap);
				adapter.notifyDataSetChanged();
			}

		}
	}

	private void UpStaffPhoto(String path) {
		if (path.equals("") || path.equals(null)) {
			return;
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upStaffPhoto1" };
		String[] nameParam2 = new String[] { "staff_id", user_id };
		String[] nameParam1 = new String[] { "path", path };
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddService_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddService_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {

				} else {
					Toast.makeText(getApplicationContext(), "服务更新失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getApplicationContext(), "服务更新失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	private void showFuwu() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.activity_item);
		list_fuwu = (ListView) window.findViewById(R.id.list_fuwu);
		mStaffCodeAdapter = new StaffCodeAdapter(this, codelist);
		list_fuwu.setAdapter(mStaffCodeAdapter);
		if (!et_number.getText().toString().equals("")) {
			getUserCode(et_number.getText().toString());
		} else {
			getUserCode("");
			Toast.makeText(AddStaffActivity.this, "输入编号可做精确查找",
					Toast.LENGTH_SHORT).show();
		}
		list_fuwu.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				String ns = codelist.get(arg2).code_id;
				user_id = codelist.get(arg2).id;
				et_number.setText(ns);
				dlg.cancel();
			}
		});
	}

	//删除原有服务项目
	private void delStaffService() {		
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delstaffclass" };
		String[] nameParam2 = new String[] { "staff_id", user_id };
		String[] nameParam1 = new String[] { "store_id", MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddProject_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddProject_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {
					String[] sv=service_ids.split("[,]");
					for(int i=0;i<sv.length;i++){
						setStaffService(sv[i]);
					}
				} else {
					Toast.makeText(getApplicationContext(), "图片上传失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getApplicationContext(), "图片上传失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};
	
	
	private void setStaffService(String service_id) {
		if (service_id.equals("") || service_id.equals(null)) {
			return;
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setStaffservices" };
		String[] nameParam1 = new String[] { "user_staff_id", user_id };
		String[] nameParam2 = new String[] { "store_service_id", service_id };
		String[] nameParam3 = new String[] {"user_store_id",MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(nameParam1);
		params2.add(nameParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddService_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddService_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {

				} else {
					Toast.makeText(getApplicationContext(), "服务更新失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getApplicationContext(), "服务更新失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};
}
