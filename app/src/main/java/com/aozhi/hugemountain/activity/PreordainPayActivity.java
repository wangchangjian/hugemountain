package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import cn.beecloud.demo.ShoppingCartActivity;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.PreordainServiceAdapter;
import com.aozhi.hugemountain.adapter.PreordainStaffAdapter;
import com.aozhi.hugemountain.list.NoScrollListView;
import com.aozhi.hugemountain.model.HorizontalListView;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class PreordainPayActivity extends Activity implements OnClickListener {
	private final String TAG = "PreordainPayActivity";
	private String tradeStatus = null;
	private ArrayList<ServiceObject> list_service = new ArrayList<ServiceObject>();
	private ArrayList<StaffObject> list_staff = new ArrayList<StaffObject>();
	private ArrayList<StaffObject> list_staff1 = new ArrayList<StaffObject>();
	private ArrayList<StoreObject> list_store = new ArrayList<StoreObject>();
	private ArrayList<OrderFormObject> list_order = new ArrayList<OrderFormObject>();
	private PreordainServiceAdapter adapter1;
	private PreordainStaffAdapter adapter2;
	private NoScrollListView list_1;
	private HorizontalListView list_2;
	ServiceListObject mServiceListObject;
	StaffListObject mStaffListObject;
	StoreListObject mStoreListObject;
	OrderListObject mOrderListObject;
	StoreObject mStoreObject;
	StaffObject mStaffObject;
	private OrderFormObject mOrderFormObject;
	TextView order_id, storephoto, reservation_time, server_datetime, title,
			pay_manoy,
			btn_ok,btn_delorder,btn_evaluation,btn_collection,btn_sure,btn_yue,btn_qrcash;
	CheckBox cb_wechat, cb_alipay;
	Button btn_back;
	String orderId, staffId, serviceId, storeId;
	private String recharge_amount = "0.01", yue = "";
	private ProgressDialog mProgress = null;
	private LinearLayout foot;
	private String status;
	private String type, paytype, orderstatus1;
	private int resultCode = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preordain_pay);
		initView();
		myonclick();
		showbtn();
		getorder();
	}

	private void initView() {
		orderId = getIntent().getStringExtra("order_id");
		status = getIntent().getStringExtra("status");
		type = getIntent().getStringExtra("type");
		paytype = getIntent().getStringExtra("paytype");
		orderstatus1 = getIntent().getStringExtra("orderstatus1");
		list_1 = (NoScrollListView) findViewById(R.id.list_1);
		list_2 = (HorizontalListView) findViewById(R.id.list_2);
		adapter1 = new PreordainServiceAdapter(this, list_service);
		adapter2 = new PreordainStaffAdapter(this, list_staff);
		list_2.setAdapter(adapter2);
		server_datetime = (TextView) findViewById(R.id.server_datetime);
		storephoto = (TextView) findViewById(R.id.storephoto);
		reservation_time = (TextView) findViewById(R.id.reservation_time);
		title = (TextView) findViewById(R.id.title);
		cb_wechat = (CheckBox) findViewById(R.id.cb_wechat);
		cb_alipay = (CheckBox) findViewById(R.id.cb_alipay);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_ok = (TextView) findViewById(R.id.btn_ok);
		btn_yue = (TextView) findViewById(R.id.btn_yue);
		btn_qrcash = (TextView) findViewById(R.id.btn_qrcash);
		btn_delorder = (TextView) findViewById(R.id.btn_delorder);
		btn_evaluation = (TextView) findViewById(R.id.btn_evaluation);
		btn_collection = (TextView) findViewById(R.id.btn_collection);
		pay_manoy = (TextView) findViewById(R.id.pay_manoy);
		order_id = (TextView) findViewById(R.id.order_id);
		btn_sure = (TextView) findViewById(R.id.btn_sure);
		order_id.setText(orderId);
		foot = (LinearLayout) findViewById(R.id.foot);
	}
	
	private void myonclick() {
		btn_back.setOnClickListener(this);
		cb_wechat.setOnClickListener(this);
		cb_alipay.setOnClickListener(this);
		btn_ok.setOnClickListener(this);
		btn_sure.setOnClickListener(this);
		btn_delorder.setOnClickListener(this);
		btn_evaluation.setOnClickListener(this);
		btn_collection.setOnClickListener(this);
		btn_yue.setOnClickListener(this);
		btn_qrcash.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			setResult(156);
			finish();
			break;
		case R.id.cb_wechat:
			cb_wechat.setChecked(true);
			cb_alipay.setChecked(false);
			break;
		case R.id.cb_alipay:
			cb_wechat.setChecked(false);
			cb_alipay.setChecked(true);
			break;
		case R.id.btn_ok:
//			if (MyApplication.isstaff) {
//				if (list_order.get(0).orderstatus1 != null) {
//					if (list_order.get(0).orderstatus1.equals("40")) {
//						Deldialog();
//					}
//				}
//			} else {
				Intent mIntent = new Intent(PreordainPayActivity.this,
						ShoppingCartActivity.class);
				mIntent.putExtra("orderid", orderId);
				mIntent.putExtra("storename", mOrderFormObject.storename);
				mIntent.putExtra("cash", mOrderFormObject.pay_manoy);
				mIntent.putExtra("services", mOrderFormObject.service);
				mIntent.putExtra("store_id", mOrderFormObject.store_id);
				mIntent.putExtra("yue", yue);
				startActivity(mIntent);
				finish();
//			}
			break;
		case R.id.btn_sure:
			setOrderStatus("30", "");
			break;
		// 取消订单
		case R.id.btn_delorder:
			delDialog();
			break;
//			------------------
		case R.id.btn_evaluation:
			Intent intent = new Intent(getApplicationContext(),
					EvaluationActivity.class);
			intent.putExtra("OrderFormObject", mOrderFormObject);
			intent.putExtra("code_id", list_staff.get(0).code_id);
			intent.putExtra("serviecs", list_service.get(0).name);
//			startActivity(intent);
			startActivityForResult(intent, 1); 
			break;
		case R.id.btn_collection:
			setCollection();
			break;
		case R.id.btn_yue:
			if (Double.valueOf(mOrderFormObject.pay_manoy) > Double
					.valueOf(list_staff1.get(0).balance)) {
				Toast.makeText(PreordainPayActivity.this, "余额不足",
						Toast.LENGTH_LONG).show();
			} else {
				Yue();
			}
			break;
		// 现金确认
		case R.id.btn_qrcash:
			setOrderStatus("20", "");
			break;
		default:
			break;
		}
	}
	
	private void showbtn(){
//		 订单状态 10生成订单 20用户付款完成 30完成订单
		if (Integer.parseInt(status) == 10 && type.equals("store")) {
			btn_delorder.setVisibility(View.VISIBLE);// 商户可以取消订单
			btn_ok.setVisibility(View.GONE);
			btn_evaluation.setVisibility(View.GONE);
			btn_collection.setVisibility(View.GONE);
			btn_sure.setVisibility(View.GONE);
			if (orderstatus1 != null) {
				if (orderstatus1.equals("40")) {
					btn_qrcash.setVisibility(View.VISIBLE);
				}
			}
		}
		if (Integer.parseInt(status) == 20 && type.equals("store")) {
			btn_sure.setVisibility(View.VISIBLE);
			btn_delorder.setVisibility(View.GONE);
			btn_ok.setVisibility(View.GONE);
			btn_evaluation.setVisibility(View.GONE);
			btn_collection.setVisibility(View.GONE);
		}
		if (Integer.parseInt(status) == 30 && type.equals("store")) {
			foot.setVisibility(View.GONE);
		}
		
		if (orderstatus1.equals("40")) {
			btn_ok.setVisibility(View.GONE);
		}
		
		if (!type.equals("store")) {
			if (Integer.parseInt(status) < 20) {
				btn_ok.setVisibility(View.VISIBLE);
				btn_delorder.setVisibility(View.VISIBLE);
				btn_evaluation.setVisibility(View.GONE);
				btn_collection.setVisibility(View.GONE);
			} else if (Integer.parseInt(status) >= 20
					&& Integer.parseInt(status) < 30) {
				btn_ok.setVisibility(View.GONE);
				btn_delorder.setVisibility(View.GONE);
				btn_evaluation.setVisibility(View.GONE);
				btn_collection.setVisibility(View.VISIBLE);
			} else if (Integer.parseInt(status) == 30) {
				btn_ok.setVisibility(View.GONE);
				btn_delorder.setVisibility(View.GONE);
				btn_evaluation.setVisibility(View.VISIBLE);
				btn_collection.setVisibility(View.VISIBLE);
			}
		}
		if (type.equals("预订")) {
			setRoomStatus("2");
		} else if (type.equals("现场")) {
			setRoomStatus("1");
		}

		if (MyApplication.isstaff) {
			btn_collection.setVisibility(View.GONE);
			if (Integer.parseInt(status) == 20) {
				btn_sure.setVisibility(View.VISIBLE);
			}
		} 
//		else {
//			if (Integer.parseInt(status) == 10) {
//				btn_yue.setVisibility(View.GONE);
//			}
//		}
		if (orderstatus1.equals("40")) {
			btn_ok.setVisibility(View.GONE);
		}
	}
	
	private void getorder() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbyorderid" };
		String[] name1 = new String[] { "order_id", orderId };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list_order.size() > 0) {
						mOrderFormObject = list_order.get(0);
						title.setText(mOrderFormObject.storename);
						// phone;
						reservation_time
								.setText(mOrderFormObject.reservation_time);
						// server_datetime.setText(mOrderFormObject.service_time);
						server_datetime.setText(mOrderFormObject.begin_time
								+ " " + mOrderFormObject.service_time);
						pay_manoy.setText("￥" + mOrderFormObject.pay_manoy);
						staffId = mOrderFormObject.cid;
						serviceId = mOrderFormObject.service;
						storeId = mOrderFormObject.store_id;
						storephoto.setText(mOrderFormObject.phone);
						getservicelist(serviceId);
						getstafflist(staffId);
						getClientAmount();
					
					} else {
						Toast.makeText(PreordainPayActivity.this, "无数据",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(PreordainPayActivity.this, "无数据",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(PreordainPayActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getservicelist(String serviceId2) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservicelistbyid" };
		String[] store = new String[] { "order_id", orderId };
		params2.add(funParam2);
		params2.add(store);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mServiceListObject = JSON.parseObject(v,
						ServiceListObject.class);
				list_service = mServiceListObject.response;
				if (mServiceListObject.meta.getMsg().equals("OK")) {
					if (list_service.size() > 0) {
						adapter1 = new PreordainServiceAdapter(
								PreordainPayActivity.this, list_service);
						list_2.setAdapter(adapter1);
					} else {
						Toast.makeText(PreordainPayActivity.this, "无服务",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(PreordainPayActivity.this, "无服务",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(PreordainPayActivity.this, "无服务",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getstafflist(String staffId2) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstafflistbyid" };
		String[] name1 = new String[] { "staff_id", staffId2 };
		String[] storeid = new String[] { "storeid", mOrderFormObject.store_id };

		params2.add(funParam2);
		params2.add(name1);
		params2.add(storeid);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener3);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list_staff = mStaffListObject.response;
				if (mStaffListObject.meta.getMsg().equals("OK")) {
					if (list_staff.size() > 0) {
						adapter2 = new PreordainStaffAdapter(
								PreordainPayActivity.this, list_staff);
						list_1.setAdapter(adapter2);
					} else {
						Toast.makeText(PreordainPayActivity.this, "无员工",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(PreordainPayActivity.this, "无员工",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(PreordainPayActivity.this, "无员工",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	

	private void Deldialog() {
		Dialog alertDialog = new AlertDialog.Builder(this).setTitle("确认现金支付吗？")
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						setOrderStatus("20", "");
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
				}).create();
		alertDialog.show();
	}

	private void Yue() {
		Dialog alertDialog = new AlertDialog.Builder(this).setTitle("确认余额支付吗？")
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						UpClientAmount();
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
				}).create();
		alertDialog.show();
	}

	private void delDialog() {
		AlertDialog.Builder builder = new Builder(PreordainPayActivity.this);
		builder.setMessage("确认取消订单吗？");
		builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				orderform_ok();
				arg0.dismiss();
			}
		});
		builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				arg0.dismiss();
			}
		});
		builder.create().show();
	}

	//
	// close the progress bar
	// 关闭进度框
	void closeProgress() {
		try {
			if (mProgress != null) {
				mProgress.dismiss();
				mProgress = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void orderform_ok() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "orderform_ok" };
		String[] name1 = new String[] { "order_id", orderId };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils
				.getCurrentNetWork(PreordainPayActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener4);
		} else {
			Toast.makeText(PreordainPayActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	};

	private CallbackListener type_callbackListener4 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				setResult(156);
				Intent mIntent = new Intent();
				setResult(resultCode, mIntent);
				mIntent.putExtra("issure", "1");
				finish();
			}
		}
	};

	// 添加消费记录
	private void addconsumption() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "addclientconsumption" };
		String[] name1 = new String[] { "services", serviceId };
		String[] name2 = new String[] { "number",
				mOrderFormObject.staff_id.split(",").length + "" };
		String[] name3 = new String[] { "client_id", mOrderFormObject.client_id };
		String[] name4 = new String[] { "money", mOrderFormObject.pay_manoy };
		String[] name5 = new String[] { "orderform_id", orderId };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		params2.add(name5);
		Constant.NET_STATUS = Utils
				.getCurrentNetWork(PreordainPayActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener5);
		} else {
			Toast.makeText(PreordainPayActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener5 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Intent intent = new Intent();
				intent.setClass(PreordainPayActivity.this,
						OrderFormDetailClientActivity.class);
				intent.putExtra("order_id", orderId);
				startActivity(intent);
				finish();
			}
		}

	};

	private void setRoomStatus(String status) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "uproomstatus" };
		String[] name1 = new String[] { "status", status }; // 0 空闲 1 忙碌 2 预订 3
															// 维修
		String[] name2 = new String[] { "order_id", orderId };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					uproomstatus_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener uproomstatus_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				// Toast.makeText(getApplicationContext(), "房间状态更新成功",
				// Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(getApplicationContext(), "房间状态更新失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	/**
	 * 订单收藏
	 */
	private void setCollection() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setCollect" };
		String[] name2 = new String[] { "store_id", storeId };
		String[] name3 = new String[] { "staff_id", staffId };
		String[] name4 = new String[] { "client_id",
				MyApplication.Clientuser.id };
		params2.add(funParam2);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		Constant.NET_STATUS = Utils
				.getCurrentNetWork(PreordainPayActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					collect_callbackListener);
		} else {
			Toast.makeText(PreordainPayActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	};

	private CallbackListener collect_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Toast.makeText(getApplicationContext(), "收藏成功",
						Toast.LENGTH_SHORT).show();
				setResult(156);
				finish();
			} else {
				Toast.makeText(getApplicationContext(), "收藏失败",
						Toast.LENGTH_SHORT).show();
			}
		}

	};

	private void setOrderStatus(String states, String Paytype) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setOrderStatus" };
		String[] nameParam1 = new String[] { "order_id", orderId };
		String[] nameParam2 = new String[] { "orderstatus", states }; // 订单付款完成
		String[] nameParam3 = new String[] { "paytype", Paytype }; // 订单付款完成
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getOrder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getOrder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);

			if (!v.equals("fail")) {
				if (status.equals("10")) {
					Toast.makeText(PreordainPayActivity.this, "确认收到现金",
							Toast.LENGTH_LONG).show();
					Intent mIntent = new Intent();
					setResult(resultCode, mIntent);
					mIntent.putExtra("issure", "1");
					finish();
				} else {
					Toast.makeText(PreordainPayActivity.this, "订单已完成",
							Toast.LENGTH_LONG).show();
					Intent mIntent = new Intent();
					setResult(resultCode, mIntent);
					mIntent.putExtra("issure", "2");
					finish();
				}

			}
		}
	};

	private void UpClientAmount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "UpClientAmount" };
		String[] name1 = new String[] { "client_id",
				list_order.get(0).client_id };
		String[] name2 = new String[] { "store_id", list_order.get(0).store_id };
		String[] name3 = new String[] { "balance", list_order.get(0).pay_manoy };

		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					UpClientAmount_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener UpClientAmount_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				setOrderStatus("20", "余额支付");

			}
		}
	};

	// 判断是否评价过
	private void getcollectNum() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getcollectNum" };
		String[] name1 = new String[] { "store_id", mOrderFormObject.store_id };
		String[] name2 = new String[] { "staff_id", mOrderFormObject.staff_id };
		String[] name3 = new String[] { "client_id", mOrderFormObject.client_id };
		String[] orderid = new String[] { "order_id", mOrderFormObject.order_id };
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		params1.add(name3);
		params1.add(orderid);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					getcollectNum_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getcollectNum_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list_order.size() > 0) {
						String num = list_order.get(0).nums;
						if (Integer.parseInt(num) > 0) {
							btn_evaluation.setVisibility(View.GONE);
						}
					}
				}
			}
			// 判断收藏
			getshoucangNum();
		}
	};

	// 判断是否已收藏
	private void getshoucangNum() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getshoucangNum" };
		String[] name2 = new String[] { "staff_id", mOrderFormObject.staff_id };
		String[] name3 = new String[] { "client_id", mOrderFormObject.client_id };
		String[] name1 = new String[] { "store_id", mOrderFormObject.store_id };
		params1.add(funParam1);
		params1.add(name2);
		params1.add(name3);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					getshoucangNum_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getshoucangNum_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			
//			showbtn();
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list_order = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list_order.size() > 0) {
						String num = list_order.get(0).nums;
						if (Integer.parseInt(num) > 0) {
							btn_collection.setVisibility(View.GONE);
						}
					}
				}
			}
		}
	};

	// 获取用户在该商户下的余额
	private void getClientAmount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getClientAmount" };
		String[] name1 = new String[] { "client_id",
				MyApplication.Clientuser.id };
		String[] name2 = new String[] { "store_id", mOrderFormObject.store_id };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getClientAmount_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getClientAmount_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffListObject = JSON.parseObject(v, StaffListObject.class);
				list_staff1 = mStaffListObject.response;
				if (list_staff1.size() > 0) {
					if (list_staff1.get(0).balance.equals("0")) {
						yue = "0";
					} else {
						yue = list_staff1.get(0).balance;
					}
				} else {
					yue = "0";
				}
			}
			// 判断评价
			getcollectNum();

		}
	};
	
	// 回调方法，从第二个页面回来的时候会执行这个方法  
    @Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
        // 根据上面发送过去的请求吗来区别  
        switch (requestCode) {  
        case 1: 
        	if(EvaluationActivity.eva){
        		btn_evaluation.setVisibility(View.GONE);
        	}
            break;  
        default:  
            break;  
        }  
    }  
}
