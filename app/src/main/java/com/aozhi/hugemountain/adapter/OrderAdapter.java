package com.aozhi.hugemountain.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.utils.Constant;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class OrderAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
    private TextView day, month;

    public OrderAdapter(Context ctx, ArrayList<OrderFormObject> li) {
        mContext = ctx;
        list = li;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder mHolder;
        if (convertView == null) {
            mHolder = new Holder();
            convertView = inflater.inflate(R.layout.item_order, null);
            mHolder.img = (ImageView) convertView.findViewById(R.id.img);
            mHolder.ordername = (TextView) convertView
                    .findViewById(R.id.ordername);
            mHolder.remark = (TextView) convertView.findViewById(R.id.remark);
            mHolder.create_time = (TextView) convertView
                    .findViewById(R.id.create_time);
            mHolder.pay_manoy = (TextView) convertView
                    .findViewById(R.id.pay_manoy);

            mHolder.img_serice = (ImageView) convertView
                    .findViewById(R.id.img_serice);
            mHolder.tv_sname = (TextView) convertView
                    .findViewById(R.id.tv_sname);
            mHolder.tv_states = (TextView) convertView
                    .findViewById(R.id.tv_states);
            mHolder.tv_serice = (TextView) convertView
                    .findViewById(R.id.tv_serice);
            mHolder.tv_smoney = (TextView) convertView
                    .findViewById(R.id.tv_smoney);
            mHolder.tv_time = (TextView) convertView.findViewById(R.id.tv_time);
            mHolder.tv_money = (TextView) convertView
                    .findViewById(R.id.tv_money);
            mHolder.tv_fu = (TextView) convertView.findViewById(R.id.tv_fu);
            mHolder.text = (TextView) convertView.findViewById(R.id.text);
            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        mHolder.ordername.setText("订单号：\n" + list.get(position).order_id);
        mHolder.remark.setText(list.get(position).reamrk);
        mHolder.create_time.setText(list.get(position).create_time);

        MyApplication.client_storeName = list.get(position).storename;
        mHolder.tv_fu.setText("附加费:￥"
                + Double.parseDouble(list.get(position).surchange));

        mHolder.tv_sname.setText("店铺名称：" + list.get(position).storename);

        mHolder.tv_serice.setText(list.get(position).sname);
        // mHolder.tv_smoney.setText("价格:￥"+list.get(position).pay_manoy);
        // mHolder.tv_time.setText("预约时间:"+list.get(position).service_time);
        mHolder.tv_time.setText(list.get(position).begin_time + " "
                + list.get(position).service_time);
        mHolder.tv_money.setText("实付金额:￥"
                + Double.parseDouble(list.get(position).pay_manoy));
        double s = Double.parseDouble(list.get(position).pay_manoy) - Double
                .parseDouble(list.get(position).surchange);
        BigDecimal bigDecimal = new BigDecimal(s);
        DecimalFormat df = new DecimalFormat("#.##");
        mHolder.tv_smoney.setText(df.format(bigDecimal));
        // if(list.get(position).orderstatus.equals("10")){
        // mHolder.tv_states.setText("");
        // mHolder.text.setVisibility(View.VISIBLE);
        // mHolder.text.setText("待支付");
        // }
        if (list.get(position).orderstatus.equals("10")) {
            if (list.get(position).orderstatus1.equals("40")) {
                mHolder.text.setText("待确认收现");
                mHolder.text.setVisibility(View.VISIBLE);
            } else {
//				mHolder.tv_states.setText("");
                mHolder.text.setVisibility(View.VISIBLE);
                mHolder.text.setText("待支付");
            }
        }

        Log.i("downloadImage", list.get(position).photo);

        if (list.get(position).photo.equals("") || list.get(position).photo == null) {

        } else {
            MyApplication.downloadImage.addTask(list.get(position).photo, mHolder.img_serice, new DownloadImage.ImageCallback() {

                        @Override
                        public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                            if (imageBitmap != null && mHolder.img_serice != null) {
                                mHolder.img_serice.setImageBitmap(imageBitmap);
                            }
                        }
                        @Override
                        public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                            // TODO 自动生成的方法存根
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                mHolder.img_serice
                                        .setBackgroundDrawable(drawable);
                            }
                        }

                    });
        }
        MyApplication.downloadImage.doTask();
        return convertView;
    }

    class Holder {
        ImageView img, img_serice;
        TextView ordername, remark, create_time, pay_manoy, tv_sname,
                tv_states, tv_serice, tv_smoney, tv_time, tv_money;
        TextView tv_miaoshu, tv_fu, text;
    }
}
