package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ShopCenterAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StaffOrdersActivity extends Activity {
	Button btn_back;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	ConsumptionObject mConsumptionObject;
	ConsumptionListObject mConsumptionListObject;
	private ShopCenterAdapter adapter;
	private ListView list_shopcenter;
	private TextView tv_show,tv_title;
	private String title="",date="",staff_id="",status="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stafforders);
		initView();
		myclick();
		getstoreorderlist();
	}
	
	private void initView() {
		staff_id= getIntent().getStringExtra("staff_id");
		date= getIntent().getStringExtra("date");
		title = getIntent().getStringExtra("title");
		status = getIntent().getStringExtra("status");
		tv_title=(TextView) findViewById(R.id.tv_title);
		tv_show=(TextView) findViewById(R.id.tv_show);
		list_shopcenter = (ListView) findViewById(R.id.list_shopcenter);
		adapter = new ShopCenterAdapter(this, list);
		list_shopcenter.setAdapter(adapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_title.setText(title);
	}
	
	private void myclick() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		list_shopcenter.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Intent mIntent=new Intent(StaffOrdersActivity.this,OrderFormDetailClientActivity.class);
				mIntent.putExtra("order_id", list.get(arg2).order_id);
				startActivity(mIntent);
				}
		});
	}

	//listview
	public void getstoreorderlist() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstoreorderlist" };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.id };
		String[] staffParam2 = new String[] { "staff_id", staff_id };
		String[] dates = new String[] { "date", date };
		String[] statusp = new String[] { "status", status };
		params2.add(funParam2);
		params2.add(statusp);
		params2.add(dates);
		params2.add(name1);
		params2.add(staffParam2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,getstoreorderlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getstoreorderlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					tv_show.setVisibility(View.GONE);
					list_shopcenter.setVisibility(View.VISIBLE);
					adapter = new ShopCenterAdapter(StaffOrdersActivity.this, list);
					list_shopcenter.setAdapter(adapter);
				} else {
					list.clear();
					tv_show.setVisibility(View.VISIBLE);
					list_shopcenter.setVisibility(View.GONE);
				} 
			}else{
				Toast.makeText(StaffOrdersActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
}
