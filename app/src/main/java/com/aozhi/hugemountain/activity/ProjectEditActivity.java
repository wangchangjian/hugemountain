package com.aozhi.hugemountain.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ImageAddAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;

public class ProjectEditActivity extends Activity implements OnClickListener {
	
	
	private Bitmap bitmap;
	private TextView tv_camera, tv_photo;
	private LinearLayout img_liner;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// 拍照
	public static final int PHOTOZOOM = 2; // 缩放
	public static final int PHOTORESOULT = 3;// 结果
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String serverFile = "";
	private TextView gone,btn_back;
	private TextView projectname;
	private EditText projectprice,servicetime,servicecontent;
	private ImageAddAdapter adapter;
	//added by fgrass
	private ArrayList<String> pt = new ArrayList<String>();//照片路径
	private List<Bitmap> listPhotos = new ArrayList<Bitmap>();//照片图片
	//add end
	private GridView add_photo;
	private ImageView img_avatar;
	private ProjectObject mProjectObject;
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_project_edit);
		initView();
		initListener();
		setView();
	}
	private void setView() {
		mProjectObject=(ProjectObject) getIntent().getSerializableExtra("list");
		projectname.setText(mProjectObject.name);
		servicecontent.setText(mProjectObject.remark);
		projectprice.setText(mProjectObject.money);
		servicetime.setText(mProjectObject.service_time);

		if (mProjectObject.avatar.equals("") || mProjectObject.avatar == null) {
			img_avatar.setBackgroundResource(R.drawable.img_add);
		} else {
			loadPhotos();
		}
		/*
			MyApplication.downloadImage.addTasks(mProjectObject.avatar,
				img_avatar, new DownloadImage.ImageCallback() {

					@Override
					public void imageLoaded(Bitmap imageBitmap,
							String imageUrl) {
						if (imageBitmap != null) {
							Drawable drawable = new BitmapDrawable(
									imageBitmap);
							img_avatar.setBackgroundDrawable(drawable);
						}
					}

					@Override
					public void imageLoaded(Bitmap imageBitmap,
							DownloadImageMode callBackTag) {
						// TODO 自动生成的方法存根
						if (imageBitmap != null) {
							Drawable drawable = new BitmapDrawable(
									imageBitmap);
							img_avatar.setBackgroundDrawable(drawable);
						}
					}
				});
		}
		MyApplication.downloadImage.doTask();
		*/
	}
	
	
	private void initListener() {
		btn_back.setOnClickListener(this);
		gone.setOnClickListener(this);
		img_avatar.setOnClickListener(this);
	}
	private void initView() {
		add_photo=(GridView) findViewById(R.id.add_photo);
		projectname=(TextView) findViewById(R.id.projectname);
		projectprice=(EditText) findViewById(R.id.projectprice);
		servicetime=(EditText) findViewById(R.id.servicetime);
		servicecontent=(EditText) findViewById(R.id.servicecontent);
		gone=(TextView) findViewById(R.id.gone);
		img_avatar=(ImageView)findViewById(R.id.img_avatar);
		btn_back=(TextView) findViewById(R.id.btn_back);
		
		//added by fgrass
		Bitmap bmp = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_addpic_focused);
		listPhotos.add(bmp);
		adapter = new ImageAddAdapter(this, listPhotos);
		add_photo.setAdapter(adapter);
		add_photo.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showAtaver();
			}
		});
		
		
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.gone:
//			if(projectname.getText().equals(mProjectObject.name)||projectprice.getText().equals(mProjectObject.money)||servicetime.getText().equals(mProjectObject.service_time)||servicecontent.getText().equals(mProjectObject.remark)){
//				Toast.makeText(this, "你没有更改任何内容，更新服务不可用", 1).show();
//			}else{
			upProjectInfo();
//			}
			break;
		case R.id.btn_back:
			finish();
			break;
		case R.id.img_avatar:{
			showAtaver();
		}break;
		default:
			break;
		}

	}
	
	private void loadPhotos() {
		String[]photos = mProjectObject.avatar.split(",");
		if (photos.length > 0) {
			for (int n = 0; n < photos.length; n++) {
				final ImageView imgView = new ImageView(
						ProjectEditActivity.this);
				imgView.setClickable(true);
				imgView.setScaleType(ScaleType.FIT_XY);
				MyApplication.downloadImage.addTask(photos[n], imgView,
						new DownloadImage.ImageCallback() {
							public void imageLoaded(Bitmap imageBitmap,
									String imageUrl) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									listPhotos.add(imageBitmap);
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											adapter.notifyDataSetChanged();
										}
									});
								}
							}
							public void imageLoaded(Bitmap imageBitmap,
									DownloadImageMode callBackTag) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									
									listPhotos.add(imageBitmap);
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											adapter.notifyDataSetChanged();
										}
									});
								}
							}
						});
				MyApplication.downloadImage.doTask();
				final String url = photos[n];
				pt.add(url);
			}
		}
	}
	private void upProjectInfo() {
		String img="";
		/*
		if(serverFile.equals("")){
			img=mProjectObject.avatar;
		}else{
			img=serverFile;
		}*/
		//此处应该将多个图片的路径上传到服务器，使用逗号分隔图片
		String photos = "";
		for (String phot : pt) {
			if(photos != ""){
				photos += ",";
			}
			photos += phot;
		}
		img = photos;
		
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upProjectInfo" };
		String[] idParam2 = new String[] { "service_id",mProjectObject.id};
		String[] nameParam1=new String[]{"store_id",MyApplication.Storeuser.id};
		String[] nameParam2=new String[]{"remark",servicecontent.getText().toString()};
		String[] nameParam3=new String[]{"money",projectprice.getText().toString()};
		String[] nameParam4=new String[]{"service_time",servicetime.getText().toString()};
		String[] nameParam5=new String[]{"avatar",img};
		
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				list = mProjectListObject.response;
				if (mProjectListObject.meta.getMsg().equals("OK")) {
					Intent mIntent=new Intent(ProjectEditActivity.this,ProjectActivity.class);
					setResult(336, mIntent);
					finish();
					
				}
			} else {
				Toast.makeText(ProjectEditActivity.this, "更新失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};
	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.renwufabu_shooting);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, 11);
				dlg.cancel();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(new File(Environment
								.getExternalStorageDirectory(), "juyue.jpg")));
				startActivityForResult(intent, 12);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode != RESULT_OK) {
			return;
		}
		if (data != null) {
			switch (requestCode) {
			case 11:
				startPhotoZoom(data.getData());
				break;
			case 12:
				Log.i("OnActivityResult", "**********************case camera!");
				File temp = new File(Environment.getExternalStorageDirectory()
						+ "/juyue.jpg");
				startPhotoZoom(Uri.fromFile(temp));
				break;

			case 13: {
				setPicToView(data);
			}
				break;

			default:
				break;
			}

		} else if (Build.MODEL.equals("MI 2C")) {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			startPhotoZoom(Uri.fromFile(temp));
		} else {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			if (temp.exists()) {
				startPhotoZoom(Uri.fromFile(temp));
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			pt.add(serverFile);
			return serverFile;
		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 13);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	@SuppressWarnings("deprecation")
	@SuppressLint({ "NewApi", "SdCardPath" })
	private void setPicToView(Intent picdata) {
		Bundle extras = picdata.getExtras();
		if (extras != null) {
			bitmap = extras.getParcelable("data");
			String sdStatus = Environment.getExternalStorageState();
			if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
				// Log.i("TestFile"
				// "SD card is not avaiable/writeable right now.");
				return;
			}
			@SuppressWarnings("static-access")
			String fileName = new DateFormat().format("yyyyMMdd_hhmmss",
					Calendar.getInstance(Locale.CHINA)) + ".jpg";
			File dir = new File("/sdcard/juyue/cache/");
			dir.mkdirs();// 创建文件夹

			fileName = "/sdcard/juyue/cache/" + fileName;
			String result = Utils.saveBitmapToFile(bitmap, fileName);
			if (result == null || result.trim().equals("")) {
				Utils.DisplayToast(this, "存储空间不足");
			}
			// @SuppressWarnings("unused")
			File file = new File(fileName);
			//img_avatar.setImageBitmap(bitmap);
			new UploadAsyncTask().execute(file);
			if (listPhotos.size() < 9) {
				listPhotos.add(bitmap);
				adapter.notifyDataSetChanged();
			}
		}
	}

}
