package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class ADListObject {

	public Meta meta;
	public ArrayList<AdObject> response;

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public ArrayList<AdObject> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<AdObject> response) {
		this.response = response;
	}
}
