package com.aozhi.hugemountain.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PreordainPayActivity;
import com.aozhi.hugemountain.adapter.OrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.OkHttpClientManager;
import com.aozhi.hugemountain.utils.Utils;
import com.squareup.okhttp.Request;

import java.util.ArrayList;

/**
 * Created by ${wangchangjian} on 2016/4/25.
 */
public class OrderView extends FrameLayout {
    private final TextView nodata;
    protected ArrayList<OrderFormObject> list;
    protected ListView list_order;
    public static  JumpProgressDialog jumpProgressDialog ;

    public OrderView(Context context) {
        this(context, null);
    }

    public OrderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        View view = LayoutInflater.from(this.getContext()).inflate(R.layout.order_view, this);
        list_order = (ListView) view.findViewById(R.id.list_order);
        nodata = (TextView) findViewById(R.id.nodata);
        if (jumpProgressDialog==null){
            jumpProgressDialog=new JumpProgressDialog(this.getContext());
        }
    }

    public void setAdapter(BaseAdapter adapter) {
        list_order.setAdapter(adapter);
        list_order.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mIntent = new Intent(OrderView.this.getContext(), PreordainPayActivity.class);
                mIntent.putExtra("order_id", list.get(position).order_id);
                mIntent.putExtra("status", list.get(position).orderstatus);
                mIntent.putExtra("orderstatus1", list.get(position).orderstatus1);
                mIntent.putExtra("type", "");
                ((Activity) OrderView.this.getContext()).startActivityForResult(mIntent, 156);
            }
        });
        list_order.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long ids) {
                final String id = list.get(position).id;
                final ConfirmDialog confirmDialog = new ConfirmDialog(OrderView.this.getContext(), "确定删除该订单吗？\n一旦删除系统不可恢复");
                confirmDialog.showConfirmAndCancel();
                confirmDialog.confirmListener(new ConfirmDialog.ConfirmListener() {
                    @Override
                    public void confirm() {
                        DelOrder(id);
                        confirmDialog.dismiss();
                    }
                });
                return true;
            }
        });
    }

    public void DelOrder(String id) {
        ArrayList<String[]> params1 = new ArrayList<String[]>();
        String[] funParam1 = new String[]{"fun", "delorder"};
        String[] name1 = new String[]{"id", id};
        params1.add(funParam1);
        params1.add(name1);
        Constant.NET_STATUS = Utils.getCurrentNetWork(OrderView.this.getContext());
        if (Constant.NET_STATUS) {
//            new HttpConnection().get(Constant.URL, params1, typeday_callbackListener);
        } else {
            Toast.makeText(OrderView.this.getContext(), "请检查网络连接",
                    Toast.LENGTH_LONG).show();
        }
    }

    public void getData(String status) {
        Constant.NET_STATUS = Utils.getCurrentNetWork(this.getContext());
        if (!Constant.NET_STATUS) {
            Toast.makeText(OrderView.this.getContext(), "没有网络", Toast.LENGTH_LONG).show();
            return;
        }
        String url = Constant.URL + "?fun=getorderformbyclient&client_id=" + MyApplication.Clientuser.id + "&status=" + status;
        OkHttpClientManager.getAsyn(url, new OkHttpClientManager.ResultCallback<OrderListObject>() {
            @Override
            public void onError(Request request, Exception e) {
                jumpProgressDialog.dimissDialog();
                e.printStackTrace();
                Toast.makeText(OrderView.this.getContext(), "请检查网络连接", Toast.LENGTH_LONG).show();
                Log.i("ProcessingOrderView", e + "     " + request.urlString());
            }

            @Override
            public void onResponse(OrderListObject orderListObject) {
                jumpProgressDialog.dimissDialog();
                list = orderListObject.response;
                if (orderListObject.meta.getMsg().equals("OK")) {
                    if (list.size() > 0) {
                        list_order.setVisibility(View.VISIBLE);
                        nodata.setVisibility(GONE);
                        setAdapter(new OrderAdapter(OrderView.this.getContext(), list));
                    } else {
                        list_order.setVisibility(View.GONE);
                        nodata.setVisibility(VISIBLE);
                    }
                } else {
                    nodata.setVisibility(VISIBLE);
                    Toast.makeText(OrderView.this.getContext(), "无数据", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
