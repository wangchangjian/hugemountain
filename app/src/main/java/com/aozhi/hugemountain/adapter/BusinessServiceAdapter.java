package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ServiceObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BusinessServiceAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<ServiceObject> list = new ArrayList<ServiceObject>();

    public BusinessServiceAdapter(Context ctx, ArrayList<ServiceObject> li) {
        mContext = ctx;
        list = li;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder mHolder;
//		if (convertView == null) {
        mHolder = new Holder();
        convertView = inflater.inflate(R.layout.item_project2, null);
        mHolder.name = (TextView) convertView.findViewById(R.id.tv_name1);
        mHolder.remark = (TextView) convertView.findViewById(R.id.tv_count);
        mHolder.manoy = (TextView) convertView.findViewById(R.id.tv_money1);
        mHolder.iv_img1 = (ImageView) convertView.findViewById(R.id.iv_img1);
        mHolder.origin_money1 = (TextView) convertView.findViewById(R.id.origin_money1);
        convertView.setTag(mHolder);
//		} else {
//			mHolder = (Holder) convertView.getTag();
//		}
        mHolder.name.setText(list.get(position).name);
        mHolder.remark.setText("已售" + list.get(position).click_count + "次");
        mHolder.manoy.setText("￥" + list.get(position).money);

        // TODO: 2016/4/24 假的数据
        try {
            mHolder.origin_money1.setText("￥" + Integer.parseInt(list.get(position).money)*2);
        }catch (Exception e){
            e.printStackTrace();
        }
        if (list.get(position).photo.equals("") || list.get(position).photo == null) {
        } else {
            String[] s = list.get(position).photo.split(",");
            MyApplication.downloadImage.addTasks(s[0], mHolder.iv_img1,
                    new DownloadImage.ImageCallback() {
                        @Override
                        public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                            if (imageBitmap != null) {
                                Drawable drawable = new BitmapDrawable(imageBitmap);
                                mHolder.iv_img1.setBackgroundDrawable(drawable);
                            }
                        }
                        @Override
                        public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                            if (imageBitmap != null) {
                                Drawable drawable = new BitmapDrawable(imageBitmap);
                                mHolder.iv_img1.setBackgroundDrawable(drawable);
                            }
                        }
                    });
        }
        MyApplication.downloadImage.doTask();
        return convertView;
    }

    class Holder {
        TextView name, remark, manoy;
        ImageView iv_img1;
        TextView origin_money1;
    }
}
