package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ShopActivity.AddStaffActivity;
import com.aozhi.hugemountain.adapter.ManageStaffAdapter;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StaffsListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ManagementStaffActivity extends Activity implements OnClickListener{
	private Button btn_back;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();
	private StaffListObject mLoginListObject;
	private ManageStaffAdapter adapter;
	private ListView list_manage;
	private GridView grid_manage;
	private TextView tv_add,nodata;
	final int RESULT_CODE = 101;
	private ProgressDialog progressDialog = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_managementstaff);
		initView();
        initControl();
	}

	private void initView() {
		btn_back = (Button)findViewById(R.id.btn_back);
		tv_add=(TextView)findViewById(R.id.tv_add);
		list_manage=(ListView)findViewById(R.id.list_manage);
		grid_manage=(GridView)findViewById(R.id.list_manage1);
		nodata=(TextView) findViewById(R.id.nodata);
		adapter=new ManageStaffAdapter(this, list);
		grid_manage.setAdapter(adapter);
		getStaffList();
		list_manage.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent mIntent=new Intent(ManagementStaffActivity.this,UpdateStaffActivity.class);
				mIntent.putExtra("id", list.get(arg2).getId());
				startActivityForResult(mIntent,25);
			}
		});
		grid_manage.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long ids) {
				Intent mIntent=new Intent(ManagementStaffActivity.this,UpdateStaffActivity.class);
				mIntent.putExtra("id", list.get(position).id);
				mIntent.putExtra("img_path",list.get(position).avatar);
				startActivityForResult(mIntent, 25);
			}
		});
		grid_manage.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long ids) {
				Dialog(position);
				return true;
			}
		});
	}

	private void Dialog(final int position){
		// 创建对话框
		AlertDialog.Builder builder = new Builder(ManagementStaffActivity.this);
		builder.setMessage("确认删除吗？");
		builder.setTitle("删除提示");
		builder.setPositiveButton("确认", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				DelStaffInfo(list.get(position).id);
				dialog.dismiss();
			}
		});
		builder.setNegativeButton("取消", new android.content.DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}
	private void initControl() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		tv_add.setOnClickListener(this);
	}

	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;

		case R.id.tv_add:{
			Intent mIntent=new Intent(ManagementStaffActivity.this,AddStaffActivity.class);
			startActivityForResult(mIntent, 30);
		}break;
		default:
			break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("", "onActivityResult" + "requestCode" + requestCode
				+ "\n resultCode=" + resultCode);
		switch (resultCode) {
		case 25:
			getStaffList();
			break;
		case 30:
			getStaffList();
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffList" };
		String[] idParam2 = new String[] { "id", MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, StaffListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					nodata.setVisibility(View.GONE);
					grid_manage.setVisibility(View.VISIBLE);
					adapter=new ManageStaffAdapter(ManagementStaffActivity.this, list);
					grid_manage.setAdapter(adapter);
				}else{
					nodata.setVisibility(View.VISIBLE);
					grid_manage.setVisibility(View.GONE);
				}

			} else {
				Toast.makeText(ManagementStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	//删除用户
		private void DelStaffInfo(String id) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			// {name}','{pwd}','{types}
			String[] funParam2 = new String[] { "fun", "deluserstaff" };
			String[] nameParam2 = new String[] { "id",id};
			String[] nameParam1=new String[]{"store_id",MyApplication.Storeuser.id};
			params2.add(funParam2);
			params2.add(nameParam1);
			params2.add(nameParam2);
			progressDialog = ProgressDialog.show(this,
					getString(R.string.app_name),
					getString(R.string.tv_dialog_context), false);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						DelStaffInfo_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener DelStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!v.equals("fail")) {
					StaffsListObject mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
					if (mStaffsListObject.getMeta().msg.equals("OK")) {
						getStaffList();
						Toast.makeText(ManagementStaffActivity.this, "删除成功",
								Toast.LENGTH_LONG).show();
						
					} else {
						Toast.makeText(ManagementStaffActivity.this, "删除失败",
								Toast.LENGTH_LONG).show();
					}

				} else {
					Toast.makeText(ManagementStaffActivity.this, "添加失败", Toast.LENGTH_LONG)
							.show();
				}

			}
		};

}
