package com.aozhi.hugemountain.http;

import com.aozhi.hugemountain.http.DownloadImage.ImageCallback;
import android.graphics.Point;


public class DownloadImageMode {
	private String imageUrl;
	private ImageCallback callback;

	private Point mPoint ;
	public boolean isPhoto ;
	public int imageType = NATIVE_PICTURE;
	public final static int NATIVE_PICTURE 	= 0 ;
	public final static int NET_PICTURE 	= 1 ;
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ImageCallback getCallback() {
		return callback;
	}

	public void setCallback(ImageCallback callback) {
		this.callback = callback;
	}
	
	public Point getPoint() {
		return mPoint;
	}

	public void setPoint(Point point) {
		this.mPoint = point;
	}

}
