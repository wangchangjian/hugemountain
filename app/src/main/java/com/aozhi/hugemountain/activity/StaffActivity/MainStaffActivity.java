package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cn.jpush.android.api.JPushInterface;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.TimeSettingActivity;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.TabActivity;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.Toast;

public class MainStaffActivity extends TabActivity {
	TabHost tabhost;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_mainstaff);
		initView();
		if(MyApplication.msgreg_id.equals("")&&MyApplication.Status.equals("staff")){
			setMsgRegId();
		}
	}
	
	private void initView() {
		// TODO Auto-generated method stub
		tabhost = this.getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		intent = new Intent().setClass(this, OrderStaffActivity.class);//PKHubActivity
		spec = tabhost.newTabSpec("home").setIndicator("home")
				.setContent(intent);
		tabhost.addTab(spec);
//		TimeSettingActivity
		intent = new Intent().setClass(this, StaffStoreListActivity.class);//OrderStaffActivity
		spec = tabhost.newTabSpec("order").setIndicator("order")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, UserSettingActivity.class);   //
		spec = tabhost.newTabSpec("account").setIndicator("account")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, TimeSettingActivity.class);   //SettingActivity  UserSettingActivity
		spec = tabhost.newTabSpec("setting").setIndicator("setting")
				.setContent(intent);
		tabhost.addTab(spec);
		tabhost.setCurrentTab(0);

		RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.main_tab_group);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						// TODO Auto-generated method stub
						switch (checkedId) {
						case R.id.staff_order:
							tabhost.setCurrentTabByTag("home");
							break;
						case R.id.staff_store:
							tabhost.setCurrentTabByTag("order");
							break;
						case R.id.staff_account:
							tabhost.setCurrentTabByTag("account");
							break;
						case R.id.staff_state:
							tabhost.setCurrentTabByTag("setting");
							break;
						default:
							;
							break;
						}
					}
				});
	}
	private void setMsgRegId() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setStaffMsgRegId" };
		String[] name1 = new String[] { "id",MyApplication.Staffuser.id};
		String[] name2=new String[]{"reg_id",JPushInterface.getRegistrationID(getApplicationContext())};
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(MainStaffActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					typeday_callbackListener);
		} else {
			Toast.makeText(MainStaffActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				
			} else {
				
			}
		}
	};
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                System.exit(0);
	            }
	        }
	        return false;
	}
}
