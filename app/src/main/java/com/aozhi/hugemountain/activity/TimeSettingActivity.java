package com.aozhi.hugemountain.activity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.ApplicationTopView;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.aozhi.hugemountain.view.OnTopClickListener;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

public class TimeSettingActivity extends Activity implements OnClickListener {

    private Spinner begin, end;
    private Button set_ok;

    private ApplicationTopView at_top_view;
    private JumpProgressDialog progressDialog;
    private ArrayAdapter<String> sp_adapter;
    private StaffListObject mStaffListObject;
    private ArrayList<StaffObject> list = new ArrayList<StaffObject>();
    private EditText surchange;
    Calendar calendar;
    SimpleDateFormat formatter;
    private String tag = "";
    JSONObject item;
    private String tag1 = "";
    GridView gridView;
    BaseAdapter adapter;
    JSONArray datas = new JSONArray();
    JSONArray headDatas;
    LinearLayout container;
    String times = "";
    String datass = "";
    ArrayList<String> arr_time = new ArrayList<String>();
    String tags = "";
    private RadioButton startwork, endwork, rest;
    private ArrayList<StoreObject> slist = new ArrayList<StoreObject>();
    private StoreListObject mStoreListObject;
    ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timesetting);
        initView();
        initListener();
        //
    }

    private void initView() {
        progressDialog = new JumpProgressDialog(this);
        at_top_view = (ApplicationTopView) findViewById(R.id.at_top_view);


        begin = (Spinner) findViewById(R.id.starttime);
        end = (Spinner) findViewById(R.id.endtime);
        set_ok = (Button) findViewById(R.id.set_ok);
        surchange = (EditText) findViewById(R.id.surchange);
        container = (LinearLayout) findViewById(R.id.week_days);
        gridView = (GridView) findViewById(R.id.gridView);
        surchange.setText(MyApplication.Staffuser.surchange);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        startwork = (RadioButton) findViewById(R.id.startwork);
        endwork = (RadioButton) findViewById(R.id.endwork);
        rest = (RadioButton) findViewById(R.id.rest);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");// 设置日期格式
        getStaffSWorkTime(df.format(new Date()));
        datass = df.format(new Date());
        getorder();
    }

    private void initListener() {
        set_ok.setOnClickListener(this);
        at_top_view.setOnTopClickListener(new OnTopClickListener() {
            @Override
            public void onLeftbuttonClick() {
            }

            @Override
            public void onRightbuttonClick() {
                if (!surchange.getText().toString().equals("")) {
                    for (int s = 0; s < arr_time.size(); s++) {
                        if (s > 0) {
                            tags = tags + "," + arr_time.get(s);
                        } else {
                            tags = arr_time.get(s);
                        }
                    }
                    System.out.println(tags);
                    setStaffSWorkTime(tags);
                } else {
                    Toast.makeText(TimeSettingActivity.this, "请输入附加金额", Toast.LENGTH_SHORT).show();
                }
            }
        });
        startwork.setOnClickListener(this);
        endwork.setOnClickListener(this);
        rest.setOnClickListener(this);
    }

    public void Spinner(Spinner sp) {
        ArrayList<String> sp_list = new ArrayList<String>();
        for (int i = 0; i < 24; i++) {
            if (i < 10) {
                sp_list.add("0" + i + ":00:00");
                // sp_list.add("0"+i+":30");
            } else {
                sp_list.add(i + ":00:00");
                // sp_list.add(i+":30");
            }
            sp_adapter = new ArrayAdapter<String>(TimeSettingActivity.this,
                    R.layout.item_time, sp_list);
            sp.setAdapter(sp_adapter);
        }
    }

    public void getorder() {
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params2 = new ArrayList<String[]>();
            String[] funParam2 = new String[]{"fun", "getstaffoffice"};
            String[] name1 = new String[]{"staff_id", MyApplication.Staffuser.id};
            params2.add(funParam2);
            params2.add(name1);

            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params2,
                        type1_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        }
    }

    private CallbackListener type1_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);

            if (!v.equals("fail")) {
                mStoreListObject = JSON.parseObject(v, StoreListObject.class);
                slist = mStoreListObject.response;
                if (mStoreListObject.meta.getMsg().equals("OK")) {
                    if (slist.size() > 0) {
                        if (slist.get(0).workstatus.equals("0")) {
                            startwork.setTextColor(Color.parseColor("#000000"));
                            endwork.setTextColor(Color.parseColor("#000000"));
                            rest.setTextColor(Color.parseColor("#000000"));
                        } else if (slist.get(0).workstatus.equals("1")) {
                            startwork.setTextColor(Color.parseColor("#ffffff"));
                            endwork.setTextColor(Color.parseColor("#000000"));
                            rest.setTextColor(Color.parseColor("#000000"));
                        } else if (slist.get(0).workstatus.equals("2")) {
                            startwork.setTextColor(Color.parseColor("#000000"));
                            endwork.setTextColor(Color.parseColor("#ffffff"));
                            rest.setTextColor(Color.parseColor("#000000"));
                        } else if (slist.get(0).workstatus.equals("3")) {
                            startwork.setTextColor(Color.parseColor("#000000"));
                            startwork.setBackgroundColor(Color.parseColor("#ffffff"));
                            endwork.setTextColor(Color.parseColor("#000000"));
                            endwork.setBackgroundColor(Color.parseColor("#ffffff"));
                            rest.setTextColor(Color.parseColor("#ffffff"));
                            rest.setBackgroundColor(Color.parseColor("#FF9201"));
                        }
                    }

                } else {
                    Toast.makeText(TimeSettingActivity.this, "无数据",
                            Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(TimeSettingActivity.this, "无数据", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    private void setStaffSWorkTime(String tags) {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "SetStaffTimes"};
        String[] name1 = new String[]{"staff_id", MyApplication.Staffuser.id};
        String[] name2 = new String[]{"datas", datass};
        String[] name3 = new String[]{"times", tags};
        // datas
        String[] name4 = new String[]{"money", surchange.getText().toString()};
        params2.add(funParam2);
        params2.add(name1);
        params2.add(name2);
        params2.add(name3);
        params2.add(name4);
        progressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    type_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {

        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (!v.equals("fail")) {
//				arr_time.clear();
//				tags="";
                Toast.makeText(TimeSettingActivity.this, "设置成功",
                        Toast.LENGTH_SHORT).show();
                mStaffListObject = JSON.parseObject(v, StaffListObject.class);
                list = mStaffListObject.response;
                surchange.setText(list.get(0).surchange);
            } else {
                Toast.makeText(TimeSettingActivity.this, "设置失败",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

    private void getStaffSWorkTime(String datas) {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "GetStaffTimes"};
        String[] name1 = new String[]{"staff_id", MyApplication.Staffuser.id};
        String[] name2 = new String[]{"datas", datas};
        params2.add(funParam2);
        params2.add(name1);
        params2.add(name2);
        progressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    type_callbackListener11);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener type_callbackListener11 = new HttpConnection.CallbackListener() {

        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (!v.equals("fail")) {
                mStaffListObject = JSON.parseObject(v, StaffListObject.class);
                list = mStaffListObject.response;
                if (mStaffListObject.meta.getMsg().equals("OK")) {
                    if (list.size() > 0) {
                        times = list.get(0).times;
                        arr_time.clear();
                        String str = times;
                        String[] strarray = str.split(",");
                        for (int j = 0; j < strarray.length; j++) {
                            arr_time.add(strarray[j]);
                        }
                        Settime();
                    } else {
                        times = "";
                        Settime();
                    }
                }
            } else {
                Toast.makeText(TimeSettingActivity.this, "获取失败",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onClick(View arg0) {
        switch (arg0.getId()) {
            case R.id.startwork:
                startwork.setChecked(true);
                endwork.setChecked(false);
                rest.setChecked(false);
                endwork.setTextColor(Color.parseColor("#000000"));
                rest.setTextColor(Color.parseColor("#000000"));
                setWorkStatus("1");
                break;
            case R.id.endwork:
                startwork.setChecked(false);
                endwork.setChecked(true);
                rest.setChecked(false);
                startwork.setTextColor(Color.parseColor("#000000"));
                endwork.setTextColor(Color.parseColor("#ffffff"));
                rest.setTextColor(Color.parseColor("#000000"));
                setWorkStatus("2");
                break;
            case R.id.rest:
                startwork.setChecked(false);
                endwork.setChecked(false);
                rest.setChecked(true);
                startwork.setTextColor(Color.parseColor("#000000"));
                endwork.setTextColor(Color.parseColor("#000000"));
                rest.setTextColor(Color.parseColor("#ffffff"));
                setWorkStatus("3");
                break;
            default:
                break;
        }

    }

    public void setWorkStatus(String status) {

        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params2 = new ArrayList<String[]>();
            String[] funParam2 = new String[]{"fun", "setWorkstatus"};
            String[] name1 = new String[]{"staff_id", MyApplication.Staffuser.id};
            String[] name2 = new String[]{"status", status};
            params2.add(funParam2);
            params2.add(name1);
            params2.add(name2);
            progressDialog.showDialog();
            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params2,
                        setwork_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        }
    }

    private CallbackListener setwork_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
            if (v.equals("fail")) {
                Toast.makeText(TimeSettingActivity.this, "状态更新失败", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };
    OnClickListener clickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            for (int i = 0; i < 7; i++) {
                View view = container.getChildAt(i);
                view.setBackgroundColor(getResources().getColor(R.color.white));

            }
            View view = container.getChildAt(v.getId());
            view.setBackgroundColor(getResources().getColor(R.color.red));
            v.setTag(view);
            TextView txtv = (TextView) v;
            String timmeString = txtv.getText().toString();
            list.clear();
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            int mScreenWidth = dm.widthPixels;

            for (int i = 0; i < headDatas.length(); i++) {
                JSONObject item = headDatas.optJSONObject(i);
                try {
                    Calendar ca = (Calendar) item.get("time");
                    TextView text = new TextView(TimeSettingActivity.this);
                    text.setClickable(true);
                    text.setId(i);
                    text.setOnClickListener(clickListener);

                    text.setGravity(Gravity.CENTER);
                    text.setBackgroundColor(getResources().getColor(R.color.white));
                    text.setLayoutParams(new LayoutParams(mScreenWidth / 7,
                            LayoutParams.MATCH_PARENT));
                    calendar = (Calendar) item.get("time");
                    formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String ttString = formatter.format(ca
                            .getTime());


                    String weekStr = getWeek(ca);

                    if (weekStr.equals(timmeString)) {
                        datass = ttString;
                        getStaffSWorkTime(ttString);
                        break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//		
        }
    };

    class ViewHolder {
        public TextView timeTextView;

    }

    // 头部构造31天时间
    public void getHeadDates() throws JSONException {
        headDatas = new JSONArray();
        for (int i = 0; i < 7; i++) {
            JSONObject item = new JSONObject();
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, i);
            item.put("state", 0);
            item.put("time", calendar);
            headDatas.put(item);
        }
    }

    // 构造时间段
    public void getOtherDay() throws JSONException {
        int hour = 8;

        datas = new JSONArray();
        for (int i = 0; i < 30; i++) {
            JSONObject item = new JSONObject();
            Calendar c = Calendar.getInstance();
            if (i % 2 == 0) {
                hour++;
                c.set(Calendar.MINUTE, 00);
            } else {
                c.set(Calendar.MINUTE, 30);
            }
            c.set(Calendar.HOUR_OF_DAY, hour);
            item.putOpt("time", c);
            // if (i<40&&i>14) {
            // item.put("state", 0);
            // } else {
            // item.put("state", 1);// 0 normal 1 choose 2 disable
            // }

            calendar = (Calendar) item.get("time");
            formatter = new SimpleDateFormat("HH:mm");

            if (!times.equals("")) {
                String str = times;
                String[] strarray = str.split(",");
                for (int j = 0; j < strarray.length; j++) {
                    if (formatter.format(calendar.getTime())
                            .equals(strarray[j])) {
                        item.put("state", 1);
                    }
                }
            } else {
                item.put("state", 0);
            }
            datas.put(item);
        }
    }

    // 根据claendat获取星期几
    private String getWeek(Calendar c) {
        String Week = "周";
        if (c.get(Calendar.DAY_OF_WEEK) == 1) {
            Week += "天";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 2) {
            Week += "一";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 3) {
            Week += "二";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 4) {
            Week += "三";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 5) {
            Week += "四";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 6) {
            Week += "五";
        }
        if (c.get(Calendar.DAY_OF_WEEK) == 7) {
            Week += "六";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
        String result = Week + "\n" + formatter.format(c.getTime());
        return result;
    }

    private static Boolean isQuit = false;
    Timer timer = new Timer();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isQuit == false) {
                isQuit = true;
                Toast.makeText(getBaseContext(), "再按一次返回键退出程序",
                        Toast.LENGTH_SHORT).show();
                TimerTask task = null;
                task = new TimerTask() {
                    @Override
                    public void run() {
                        isQuit = false;
                    }
                };
                timer.schedule(task, 2000);
            } else {
                finish();
                System.exit(0);
            }
        }
        return false;
    }

    private void Settime() {
        try {
            getOtherDay();
            getHeadDates();
        } catch (JSONException e1) {
            e1.printStackTrace();
        }
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int mScreenWidth = dm.widthPixels;
        for (int i = 0; i < headDatas.length(); i++) {
            JSONObject item = headDatas.optJSONObject(i);
            try {
                Calendar ca = (Calendar) item.get("time");
                TextView text = new TextView(this);
                text.setClickable(true);
                text.setId(i);
                text.setOnClickListener(clickListener);

                text.setGravity(Gravity.CENTER);
//				text.setBackgroundColor(getResources().getColor(R.color.white));
                text.setBackgroundColor(Color.parseColor("#FFFFCC"));
                text.setLayoutParams(new LayoutParams(mScreenWidth / 7,
                        LayoutParams.MATCH_PARENT));
                calendar = (Calendar) item.get("time");
                formatter = new SimpleDateFormat("yyyy-MM-dd");
                String ttString = formatter.format(ca.getTime());
                text.setTag(i);
                container.addView(text);
                text.setText(getWeek(ca));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        adapter = new BaseAdapter() {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                ViewHolder holder = null;
                item = datas.optJSONObject(position);
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = LayoutInflater.from(TimeSettingActivity.this)
                            .inflate(R.layout.cell_time, null);
                    holder.timeTextView = (TextView) convertView
                            .findViewById(R.id.timeTextView);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();

                }
                try {
                    calendar = (Calendar) item.get("time");
                    formatter = new SimpleDateFormat("HH:mm");
                    holder.timeTextView.setText(formatter.format(calendar
                            .getTime()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                int state = item.optInt("state");
                switch (state) {
                    case 0:
                        holder.timeTextView.setBackgroundColor(getResources()
                                .getColor(R.color.white));
                        break;
                    case 1:
                        holder.timeTextView.setBackgroundColor(getResources()
                                .getColor(R.color.red));
                        break;
                    case 2:
                        holder.timeTextView.setBackgroundColor(getResources()
                                .getColor(R.color.gray));
                        break;

                    default:
                        break;
                }
                return convertView;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public int getCount() {
                return datas.length();
            }
        };
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                JSONObject item = datas.optJSONObject(position);
                if (item.optInt("state") == 1) {
                    try {
                        item.put("state", 0);

                        calendar = (Calendar) item.get("time");
                        formatter = new SimpleDateFormat("HH:mm");

                        tag = formatter.format(calendar.getTime());

                        for (int i = 0; i < arr_time.size(); i++) {

                            if (arr_time.get(i).equals(tag)) {
                                arr_time.remove(i);
                                break;
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        item.put("state", 1);
                        calendar = (Calendar) item.get("time");
                        formatter = new SimpleDateFormat("HH:mm");
                        tag = formatter.format(calendar.getTime());
                        arr_time.add(tag);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }
}
