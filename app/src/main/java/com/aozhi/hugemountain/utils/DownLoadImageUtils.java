package com.aozhi.hugemountain.utils;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.aozhi.hugemountain.MyApplication;

/**
 * Created by ${wangchangjian} on 2016/4/27.
 */
public class DownLoadImageUtils {
    private static RequestQueue mQueue;
    private static ImageLoader imageLoader;

    public static void addRequest(ImageRequest imageRequest) {
        if (null == mQueue) {
            mQueue = Volley.newRequestQueue(MyApplication.getInstance());
        }
        mQueue.add(imageRequest);
    }
}
