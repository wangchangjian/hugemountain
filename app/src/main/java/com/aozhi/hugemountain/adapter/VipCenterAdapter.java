package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.VipCenterListObject;
import com.aozhi.hugemountain.model.VipCenterObject;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class VipCenterAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<VipCenterObject> list = new ArrayList<VipCenterObject>();

	public VipCenterAdapter(Context ctx, ArrayList<VipCenterObject> list2) {
		mContext = ctx;
		list = list2;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_managestaff2, null);
			mHolder.user_id = (TextView) convertView
					.findViewById(R.id.user_id);
			mHolder.regtime = (TextView) convertView.findViewById(R.id.regtime);
			mHolder.balance = (TextView) convertView.findViewById(R.id.balance);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.user_id.setText(list.get(position).clientphoto);
		mHolder.regtime.setText(list.get(position).create_time);
		mHolder.balance.setText("￥"+list.get(position).balance);
		return convertView;
	}

	class Holder {
		TextView user_id, regtime, balance;
	}
}
