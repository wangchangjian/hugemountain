package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.activity.ConsumerActivity.BusinessActivity;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.*;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class InviteListAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ClientObject> invite_list = new ArrayList<ClientObject>();
	
	public InviteListAdapter(Context ctx, ArrayList<ClientObject> li) {
		mContext = ctx;
		invite_list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return invite_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return invite_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_invite, null);
			mHolder.phone= (ImageView) convertView.findViewById(R.id.photo);
			mHolder.name=(TextView) convertView.findViewById(R.id.name);
			mHolder.remark=(TextView) convertView.findViewById(R.id.remark);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(invite_list.get(position).name);
		mHolder.remark.setText(invite_list.get(position).remark);
		
		
		
		
		
		if (invite_list.get(position).phone.equals("")|| invite_list.get(position).phone==null ) {
			
		} else {
			MyApplication.downloadImage.addTask(
					invite_list.get(position).phone, mHolder.phone,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(mContext,BusinessActivity.class);
				mIntent.putExtra("store_id", invite_list.get(position).id);
				mContext.startActivity(mIntent);
			}
		});
		return convertView;
	}
	
	class Holder {
		ImageView phone;
		TextView name,remark;
	}
	
}
