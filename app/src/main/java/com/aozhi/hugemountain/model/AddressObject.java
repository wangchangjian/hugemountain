package com.aozhi.hugemountain.model;

/**
 * Created by Administrator on 2015/5/27.
 */
public class AddressObject {
	public String id;
    public String member_name;
    public String member_id;
    public String cell_no;
    public String address;
    public String deliver_time;
//  备注
    public String note;
    public String create_time;
    public String modify_time;
    public String del_flag;
	public String name;
	
	public void setName(String name) {
		this.name = name;
	}
	public String order_id;
	public String content;
	public String is_step;
	
	
	public String lat;
	public String lng;
    public String areaid;
    public String area;
    public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String cityid;
   
}
