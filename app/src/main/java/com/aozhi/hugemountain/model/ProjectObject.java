package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class ProjectObject implements Serializable{

	 public String id;
     public String name;
     public String remark;
     public String money;
     public String store_id;
     public String staff_id;
     public String create_time;
     public String update_time;
     public String mark;
     public String del_flag;
     public String photo;
     public String click_count;
     public String daymoney;
     public String monthmoney;
     public String store_img;
     public String service_time;
     public String store_service_id;
     public String avatar;
     
     public String remarks;
}
