package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class VipCenterObject implements Serializable{

	public String id;
	public String clientphoto;
	public String types;
	public String level;
	public String clientname;
	public String create_time;
	public String balance;
	public String percent;
	public String name;
	public String namelevel;
	public String sex;
	public String age;
	public String remark;
	public String mark;
	public String phone;
	public String staff_id;
	public String deleteStatus;
	public String money;
	public String pay_status;
	public String store_id;
	public String client_id;
	public String level_id;
	public String recharge_money;
	public String update_time;
}
