package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ConsumerActivity.ChooseDatetimeActivity;
import com.aozhi.hugemountain.adapter.RoomAdapter;
import com.aozhi.hugemountain.model.RoomListObject;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class RoomActivity extends Activity implements OnClickListener{
	private Button btn_back,ok;
	private ArrayList<RoomObject> list = new ArrayList<RoomObject>();
	private RoomListObject mRoomListObject;
	private RoomAdapter mRoomAdapter;
	private GridView grid_view;
	private ProgressDialog progressDialog = null;
	private TextView roominfo;
	private String id="";
	private String store_id="",roomname="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_room);
		store_id=getIntent().getStringExtra("store_id");
		btn_back = (Button)findViewById(R.id.btn_back);
		ok=(Button) findViewById(R.id.ok);
		ok.setOnClickListener(this);
		roominfo=(TextView)findViewById(R.id.roominfo);
		btn_back.setOnClickListener(this);
		grid_view=(GridView)findViewById(R.id.grid_view);
		mRoomAdapter=new RoomAdapter(this, list);
		grid_view.setAdapter(mRoomAdapter);
		getStaffList();
		grid_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
//				Intent mintent=new Intent(RoomActivity.this,RoomDetails.class);
//				mintent.putExtra("id", list.get(arg2).id);
//				startActivity(mintent);
				roominfo.setText("名称："+list.get(arg2).name+">>编号："+list.get(arg2).code);
				roomname=list.get(arg2).name;
				id=list.get(arg2).id;
			}
		});
		
	}
	
	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getFreeRoom" };
		String[] idParam2 = new String[] { "store_id", store_id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mRoomListObject = JSON.parseObject(v, RoomListObject.class);
				list = mRoomListObject.response;
				if (list.size() > 0) {
					mRoomAdapter=new RoomAdapter(RoomActivity.this, list);
					grid_view.setAdapter(mRoomAdapter);
				}

			} else {
				Toast.makeText(RoomActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			setResult(129);
			finish();
			break;
		case R.id.ok:
			Intent intent=new Intent(getApplicationContext(),ChooseDatetimeActivity.class);
			intent.putExtra("name", roomname);
			intent.putExtra("room_id", id);
			setResult(126, intent);
			finish();
			break;
		default:
			break;
		}
	}

}
