package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.activity.ConsumerActivity.BusinessActivity;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.*;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CollectStoreListAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StoreObject> store_list = new ArrayList<StoreObject>();

	public CollectStoreListAdapter(Context ctx, ArrayList<StoreObject> li) {
		mContext = ctx;
		store_list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return store_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return store_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_collect_store, null);
			mHolder.phone= (ImageView) convertView.findViewById(R.id.phone);
			mHolder.name=(TextView) convertView.findViewById(R.id.name);
			mHolder.remark=(TextView) convertView.findViewById(R.id.remark);
			mHolder.star=(TextView) convertView.findViewById(R.id.star);

			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(store_list.get(position).name);
		mHolder.remark.setText(store_list.get(position).remark);

		if(store_list.get(position).star.equals("0")){
			mHolder.star.setText("☆☆☆☆☆");
		}else if(store_list.get(position).star.equals("1")){
			mHolder.star.setText("★☆☆☆☆");
		}else if(store_list.get(position).star.equals("2")){
			mHolder.star.setText("★★☆☆☆");
		}else if(store_list.get(position).star.equals("3")){
			mHolder.star.setText("★★★☆☆");
		}else if(store_list.get(position).star.equals("4")){
			mHolder.star.setText("★★★★☆");
		}else if(store_list.get(position).star.equals("5")){
			mHolder.star.setText("★★★★★");
		}else{
			mHolder.star.setText("☆☆☆☆☆☆");
		}



		if (store_list.get(position).phone.equals("")|| store_list.get(position).phone==null ) {

		} else {
			MyApplication.downloadImage.addTask(
					store_list.get(position).phone, mHolder.phone,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(mContext,BusinessActivity.class);
				mIntent.putExtra("store_id", store_list.get(position).id);
				mContext.startActivity(mIntent);
			}
		});
		return convertView;
	}

	class Holder {
		ImageView phone;
		TextView name,remark,star;
		TextView tv_miaoshu;
	}

}
