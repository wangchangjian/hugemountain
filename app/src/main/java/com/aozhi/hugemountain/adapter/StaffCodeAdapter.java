package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ManageStaffObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StaffCodeAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ManageStaffObject> list = new ArrayList<ManageStaffObject>();

	public StaffCodeAdapter(Context ctx, ArrayList<ManageStaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater
					.inflate(R.layout.item_code, null);
			mHolder.tv_code = (TextView) convertView.findViewById(R.id.tv_code);
			mHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);		
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_code.setText(list.get(position).code_id);
		mHolder.tv_name.setText(list.get(position).name);
		return convertView;
	}

	class Holder {
		TextView tv_name,tv_code;
		ImageView iv_img1;
	}
}
