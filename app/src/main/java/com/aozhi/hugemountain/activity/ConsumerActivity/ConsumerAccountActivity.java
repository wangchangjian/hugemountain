package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.AboutActivity;
import com.aozhi.hugemountain.activity.PublicActivity.FeedbackActivity;
import com.aozhi.hugemountain.activity.PublicActivity.LoginsActivity;
import com.aozhi.hugemountain.activity.MyCollectActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.view.ConfirmDialog;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ConsumerAccountActivity extends Activity {

    private TextView user_id, balance, tv_exit;
    private RelativeLayout rl_userinfo;
    private LinearLayout layout_consumerbill, layout_shoucang, layout_pingjia,
            layout_feedback, layout_about;
    private ImageView img_avatar;
    private LinearLayout layout_balance;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_account);
        layout_consumerbill = (LinearLayout) findViewById(R.id.layout_consumerbill);
        layout_shoucang = (LinearLayout) findViewById(R.id.layout_shoucang);
        layout_pingjia = (LinearLayout) findViewById(R.id.layout_pingjia);
        layout_feedback = (LinearLayout) findViewById(R.id.layout_feedback);
        layout_about = (LinearLayout) findViewById(R.id.layout_about);
        layout_balance = (LinearLayout) findViewById(R.id.layout_balance);
        layout_balance.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConsumerAccountActivity.this,
                        ConsumerBalanceActivity.class);
                startActivity(intent);
            }
        });

        layout_consumerbill.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ConsumerAccountActivity.this,
                        ConsumerBillActivity.class);
                startActivity(intent);
            }
        });
        layout_shoucang.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ConsumerAccountActivity.this,
                        MyCollectActivity.class);
                intent.putExtra("stats", "我的收藏");
                startActivity(intent);
            }
        });
        layout_feedback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ConsumerAccountActivity.this,
                        FeedbackActivity.class);
                startActivity(intent);
            }
        });


//        layout_pingjia.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//                Intent intent = new Intent(ConsumerAccountActivity.this,
//                        MyCollectActivity.class);
//                intent.putExtra("stats", "client");
//                startActivity(intent);
//            }
//        });
        user_id = (TextView) findViewById(R.id.user_id);
        user_id.setText(MyApplication.Clientuser.clientphoto);
        balance = (TextView) findViewById(R.id.balance);
        balance.setText(MyApplication.Clientuser.balance);
        tv_exit = (TextView) findViewById(R.id.tv_exit);
        img_avatar = (ImageView) findViewById(R.id.img_avatar);
        tv_exit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                quitDialog();
            }
        });
        layout_about.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(ConsumerAccountActivity.this, AboutActivity.class);
                startActivity(intent);
            }
        });
        SetAvatar();
        rl_userinfo = (RelativeLayout) findViewById(R.id.rl_userinfo);
        rl_userinfo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void SetAvatar() {
        if (MyApplication.Clientuser.phone==null||MyApplication.Clientuser.phone.equals("")) {
        } else {
            MyApplication.downloadImage.addTasks(
                    MyApplication.Clientuser.phone, img_avatar,
                    new DownloadImage.ImageCallback() {
                        @Override
                        public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                            if (imageBitmap != null) {
                                img_avatar.setImageBitmap(imageBitmap);
                            }
                        }

                        @Override
                        public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                            // TODO 自动生成的方法存根
                            if (imageBitmap != null) {
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                img_avatar.setBackgroundDrawable(drawable);
                            }
                        }

                    });
        }
        MyApplication.downloadImage.doTask();
    }

    protected void quitDialog() { // 确认退出系统
        final ConfirmDialog confirmDialog = new ConfirmDialog(this, getString(R.string.dialog_quit_prompt));
        confirmDialog.showConfirmAndCancel();
        confirmDialog.confirmListener(new ConfirmDialog.ConfirmListener() {
            @Override
            public void confirm() {
                confirmDialog.dismiss();
                SharedPreferences mySharedPreferences = getSharedPreferences(
                        "client_user", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = mySharedPreferences
                        .edit();
                editor.putString("name", "");
                editor.putString("pwd", "");
                editor.putBoolean("ok", false);
                editor.commit();
                JPushInterface.setAlias(getApplicationContext(),
                        "", mTagsCallback);
//						JPushInterface.stopPush(getApplicationContext());
                Intent mIntent = new Intent(ConsumerAccountActivity.this,
                        LoginsActivity.class);
                startActivity(mIntent);
                finish();
            }
        });
    }

    private final TagAliasCallback mTagsCallback = new TagAliasCallback() {

        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
                    // Log.i(TAG, logs);
                    break;

                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    // Log.i(TAG, logs);
                    // if (ExampleUtil.isConnected(getApplicationContext())) {
                    // mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS,
                    // tags), 1000 * 60);
                    // } else {
                    // // Log.i(TAG, "No network");
                    // }
                    break;

                default:
                    logs = "Failed with errorCode = " + code;
                    // Log.e(TAG, logs);
            }

            // ExampleUtil.showToast(logs, getApplicationContext());
        }

    };

    private static Boolean isQuit = false;
    Timer timer = new Timer();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isQuit == false) {
                isQuit = true;
                Toast.makeText(getBaseContext(), "再按一次返回键退出程序",
                        Toast.LENGTH_SHORT).show();
                TimerTask task = null;
                task = new TimerTask() {
                    @Override
                    public void run() {
                        isQuit = false;
                    }
                };
                timer.schedule(task, 2000);
            } else {
                finish();
                MyApplication.IS_LOGINS = false;
                System.exit(0);
            }
        }
        return false;
    }
}
