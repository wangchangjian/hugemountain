package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class ConsumptionListObject {
	public Meta meta;
	public ArrayList<ConsumptionObject> response;
	
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ConsumptionObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ConsumptionObject> response) {
		this.response = response;
	}
	
}
