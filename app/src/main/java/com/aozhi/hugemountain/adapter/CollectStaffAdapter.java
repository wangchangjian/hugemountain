package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.StaffDataActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.StaffObject;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CollectStaffAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();
	
	
	public CollectStaffAdapter(Context ctx, ArrayList<StaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_collect_staff, null);
			mHolder.img_avatar= (ImageView) convertView.findViewById(R.id.img_avatar);
			mHolder.name=(TextView) convertView.findViewById(R.id.name);
			mHolder.service=(TextView) convertView.findViewById(R.id.service);
			mHolder.age=(TextView) convertView.findViewById(R.id.age);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		
		mHolder.age.setText("年龄："+list.get(position).age);
		if(list.get(position).code_id==null){
			mHolder.service.setVisibility(View.GONE);
			mHolder.img_avatar.setVisibility(View.GONE);
			mHolder.name.setText("客户："+list.get(position).clientphoto);
			mHolder.age.setVisibility(View.GONE);
		}else{
			mHolder.name.setText(list.get(position).name);
			mHolder.service.setText("员工编号："+list.get(position).code_id);
		}
		//员工端
		if (list.get(position).phone!=null &&!list.get(position).phone.equals("")) {
			MyApplication.downloadImage.addTask(
					list.get(position).phone, mHolder.img_avatar,
					new DownloadImage.ImageCallback() {
						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.img_avatar.setBackgroundDrawable(drawable);
							}
						}
						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_avatar.setBackgroundDrawable(drawable);
							}
						}
					});
		} else {
			//客户端
//			if(list.get(position).avatar.equals("")){}else{
			MyApplication.downloadImage.addTask(
					list.get(position).avatar, mHolder.img_avatar,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.img_avatar.setBackgroundDrawable(drawable);
							}
						}
						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_avatar.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		if(list.get(position).code_id!=null){
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent = new Intent(mContext,StaffDataActivity.class);
				mIntent.putExtra("staff_info", list.get(position));
				mContext.startActivity(mIntent);
			}
		});}
		return convertView;
	}
	
	class Holder {
		ImageView img_avatar;
		TextView name,age,service;
	}
}
