package com.aozhi.hugemountain.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PreordainPayActivity;
import com.aozhi.hugemountain.adapter.OrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.OkHttpClientManager;
import com.aozhi.hugemountain.utils.Utils;
import com.squareup.okhttp.Request;

import java.util.ArrayList;

/**
 * Created by ${wangchangjian} on 2016/4/25.
 */
public class AlreadyPaidOrderView extends OrderView {
    public AlreadyPaidOrderView(Context context) {
        this(context, null);
    }

    public AlreadyPaidOrderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
