package com.aozhi.hugemountain.model;

import java.util.ArrayList;


public class ImageListObject {
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<ImageObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<ImageObject> response) {
		this.response = response;
	}
	public Meta meta;
	public ArrayList<ImageObject> response;
	
}
