package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.ArrayList;


import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.view.IndexDotView;

import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

@Deprecated
public class ServiceDetaileActivity extends Activity {
	Button btn_back;
	private ServiceObject mProjectObject;
	private TextView tv_name, tv_content, tv_money, count;
	private Button clearcount;
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private ViewPager vp_mainadv;
	private int currentItem = 0;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
	private TextView tv_time, btn_xc,btn_yy;
	private IndexDotView dot_indicator;
	private ImageView img_project;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_servicedetaile);
		tv_name = (TextView) findViewById(R.id.tv_name);
		tv_content = (TextView) findViewById(R.id.tv_content);
		tv_money = (TextView) findViewById(R.id.tv_money);
		btn_back = (Button) findViewById(R.id.btn_back);
		count = (TextView) findViewById(R.id.count);
		tv_time = (TextView) findViewById(R.id.tv_time);
		btn_xc = (TextView) findViewById(R.id.btn_xc);
		dot_indicator= (IndexDotView) findViewById(R.id.dot_indicator);

		btn_xc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(), BusinessActivity.class);
				intent.putExtra("service", mProjectObject);
				intent.putExtra("type", "预订");
				setResult(503, intent);
				finish();
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						BusinessActivity.class);
				intent.putExtra("type", "");
				intent.putExtra("service", mProjectObject);
				setResult(505, intent);
				finish();
			}
		});
		btn_yy=(TextView) findViewById(R.id.btn_yy);
		btn_yy.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),
						BusinessActivity.class);
				intent.putExtra("service", mProjectObject);
				intent.putExtra("type", "现场");
				setResult(503, intent);
				finish();
				
			}
		});
		mProjectObject = (ServiceObject) getIntent().getSerializableExtra(
				"mProjectObject");
		tv_name.setText(mProjectObject.name);
		tv_content.setText(mProjectObject.remark);
		tv_money.setText("￥" + mProjectObject.money);
		tv_time.setText(mProjectObject.service_time + "分钟");
		count.setText(mProjectObject.remarks);
		String imagurl="";
		String [] s=mProjectObject.photo.split(",");
		if(s.length>0){
			imagurl=s[0];
		}
		
//		downloadImage.addTasks(imagurl,
//				img_project, new DownloadImage.ImageCallback() {
//					public void imageLoaded(Bitmap imageBitmap,
//							String imageUrl) {
//						// TODO Auto-generated method stub
//						if (imageBitmap != null) {
//							Drawable drawable = new BitmapDrawable(
//									imageBitmap);
//							img_project.setBackgroundDrawable(drawable);
//						}
//					}
//
//					public void imageLoaded(Bitmap imageBitmap,
//							DownloadImageMode callBackTag) {
//						// TODO Auto-generated method stub
//						if (imageBitmap != null) {
//							Drawable drawable = new BitmapDrawable(
//									imageBitmap);
//							img_project.setBackgroundDrawable(drawable);
//						}
//					}
//				});
//		downloadImage.doTask();
		getPhoto();
	}

	// 图片轮播
	String serversImageString = "";
	private void getPhoto() {
		String[]photos = mProjectObject.photo.split(",");
		if (photos.length > 0) {
			int n = 0;
			for (n = 0; n < photos.length; n++) {
				final ImageView imgView = new ImageView(
						ServiceDetaileActivity.this);
				imgView.setClickable(true);
				imgView.setScaleType(ScaleType.FIT_XY);
				downloadImage.addTasks(photos[n], imgView,
						new DownloadImage.ImageCallback() {
							public void imageLoaded(Bitmap imageBitmap,
									String imageUrl) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									imgView.setImageBitmap(imageBitmap);
									imgView.setTag(imageUrl);
								}
							}

							public void imageLoaded(Bitmap imageBitmap,
									DownloadImageMode callBackTag) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									imgView.setImageBitmap(imageBitmap);
									imgView.setTag("");
								}
							}
						});
				downloadImage.doTask();
				final String url = photos[n];
				final String title = photos[n];

				if (serversImageString.equals("")) {
					serversImageString += url;
				} else {
					serversImageString += "," + url;
				}

				imgView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent = new Intent(
								ServiceDetaileActivity.this,
								ImagePagerActivity.class);
						mIntent.putExtra("images", serversImageString);
						startActivity(mIntent);
					}
				});
				imgViews.add(imgView);
			}
			vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
			vp_mainadv.setAdapter(new MyAdapter());
			dot_indicator.setNums(vp_mainadv.getAdapter().getCount());
			Log.i("getChildCount", vp_mainadv.getAdapter().getCount() + "");
			// 设置监听，当ViewPager中的页面改变时调用
			dot_indicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					vp_mainadv.setOnPageChangeListener(new OnPageChangeListener() {
						@Override
						public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
							dot_indicator.setMoveDotPercent((positionOffset + position) / vp_mainadv.getAdapter().getCount());

						}

						@Override
						public void onPageSelected(int position) {

						}

						@Override
						public void onPageScrollStateChanged(int state) {

						}
					});
					dot_indicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
				}
			});


		}
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
		
		/*Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getProjectAD" };
			String[] paramName=new String[] {"service_id",mProjectObject.id};
			String[] paramName1=new String[]{"store_id",MyApplication.Storeuser.id};
			params.add(methodParam);
			params.add(paramName);
			params.add(paramName1);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
		*/
	}

 
	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			String []strings=serversImageString.split(",");
			int count=strings.length;
			return count;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 336:

			break;

		default:
			break;
		}

	}
	
	 @Override  
	    public boolean onKeyDown(int keyCode, KeyEvent event)  
	    {  
	        if (keyCode == KeyEvent.KEYCODE_BACK )  
	        {  
	            // 创建退出对话框  
	        	Intent intent = new Intent(getApplicationContext(),
						BusinessActivity.class);
				intent.putExtra("type", "");
				setResult(505, intent);
				finish();
	  
	        }  
	          
	        return false;  
	          
	    }
}
