package com.aozhi.hugemountain;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.aozhi.hugemountain.activity.StaffServiceListActivity;

public class PersonalStaffActivity extends Activity {
	Button btn_back;
	RelativeLayout rl_month,rl_all,rl_service;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_personalstaff);
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		rl_month=(RelativeLayout)findViewById(R.id.rl_month);
		rl_month.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				MyApplication.StaffConsumer="month";
				startActivity(new Intent(PersonalStaffActivity.this, StaffConsumerBillActivity.class));
			}
		});
		rl_all=(RelativeLayout)findViewById(R.id.rl_all);
		rl_all.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				MyApplication.StaffConsumer="all";
				startActivity(new Intent(PersonalStaffActivity.this, StaffConsumerBillActivity.class));
			}
		});
		rl_service=(RelativeLayout)findViewById(R.id.rl_service);
		rl_service.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(PersonalStaffActivity.this, StaffServiceListActivity.class));
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.account_staff, menu);
		return true;
	}

}
