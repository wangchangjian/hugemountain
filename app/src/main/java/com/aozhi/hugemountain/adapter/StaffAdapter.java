package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.StaffObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StaffAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();
	
	
	public StaffAdapter(Context ctx, ArrayList<StaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_money, null);
			mHolder.tv_time=(TextView) convertView.findViewById(R.id.tv_time);
			mHolder.tv_money=(TextView) convertView.findViewById(R.id.tv_money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.tv_time.setText(list.get(position).create_time+"收入");
		mHolder.tv_money.setText(" ￥"+list.get(position).money);		
		
		return convertView;
	}
	
	class Holder {
		ImageView phone;
		TextView name,age,tv_time,tv_money;
	}
}
