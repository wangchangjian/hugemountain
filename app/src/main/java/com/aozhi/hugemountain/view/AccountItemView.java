package com.aozhi.hugemountain.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aozhi.hugemountain.R;

/**
 * Created by ${wangchangjian} on 2016/4/24.
 */
public class AccountItemView extends LinearLayout {
    private TextView img;
    private TextView text;

    public AccountItemView(Context context) {
        this(context, null);
    }

    public AccountItemView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AccountItemView);
        String s = a.getString(R.styleable.AccountItemView_item_text);
        int left_bg = a.getResourceId(R.styleable.AccountItemView_left_bg, 0);
        a.recycle();
        View view = LayoutInflater.from(this.getContext()).inflate(R.layout.activity_account_item, this);
        img = (TextView) view.findViewById(R.id.img);
        text = (TextView) view.findViewById(R.id.text);
        img.setBackgroundResource(left_bg);
        text.setText(s);}
}
