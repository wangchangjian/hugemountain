package com.aozhi.hugemountain.model;

 public class StoreObject {

	 public String store_id;
	 public String id;
     public String seller_id;
     public String storephoto;
     public String storepwd;
     public String name;
     public String phone;
     public String address;
     public String business_hours;
     public String remark;
     public String create_time;
     public String update_time;
     public String mark;
     public String star;
     public String del_flag;
     public String service;
     public String longitude;
     public String latitude;
     public String user_id;
     public String type_id;
     public String workstatus;
     public String startwork;
     public String endwork;
     public String begin_time;
     public String end_time;
     public String location;
     public String mobile;
     public String remarks;
     
	@Override
	public String toString() {
		return "StoreObject [id=" + id + ", seller_id=" + seller_id
				+ ", storephoto=" + storephoto + ", storepwd=" + storepwd
				+ ", name=" + name + ", phone=" + phone + ", address="
				+ address + ", business_hours=" + business_hours + ", remark="
				+ remark + ", create_time=" + create_time + ", update_time="
				+ update_time + ", mark=" + mark + ", del_flag=" + del_flag
				+ ", service=" + service + "]";
	}
     
}
