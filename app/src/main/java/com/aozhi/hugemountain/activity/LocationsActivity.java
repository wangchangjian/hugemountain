package com.aozhi.hugemountain.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import com.baidu.navisdk.adapter.BNOuterTTSPlayerCallback;
import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BNRoutePlanNode.CoordinateType;
import com.baidu.navisdk.adapter.BaiduNaviManager;
import com.baidu.navisdk.adapter.BaiduNaviManager.NaviInitListener;
import com.baidu.navisdk.adapter.BaiduNaviManager.RoutePlanListener;

public class LocationsActivity extends Activity implements OnClickListener {

	private Button btn_back, btn_screen;
	private MapView mMapView;
	private BaiduMap mBaiduMap;
	private String lat1 = "";
	private String lng1 = "";
	private String address = "";
	String[] sourceStrArray;

	public static List<Activity> activityList = new LinkedList<Activity>();

	private static final String APP_FOLDER_NAME = "LocationsActivity";

//	private Button mWgsNaviBtn = null;
//	private Button mGcjNaviBtn = null;
//	private Button mBdmcNaviBtn = null;
//	private Button mDb06ll = null;
	private String mSDCardPath = null;

	public static final String ROUTE_PLAN_NODE = "routePlanNode";
	public static final String SHOW_CUSTOM_ITEM = "showCustomItem";
	public static final String RESET_END_NODE = "resetEndNode";
	public static final String VOID_MODE = "voidMode";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_locations);
		initView();
		initListnner();

		if (initDirs()) {
			initNavi();
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		lat1 = getIntent().getStringExtra("lat");
		lng1 = getIntent().getStringExtra("lng");
		address = getIntent().getStringExtra("address");
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_screen = (Button) findViewById(R.id.btn_screen);
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();

		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		// mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		// MapStatusUpdate lat = MapStatusUpdateFactory.zoomTo(18.0f);//
		// 设置地图的缩放比例
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(18.0f);
		mBaiduMap.setMapStatus(msu);// 将前面的参数交给BaiduMap类
		mMapView.showZoomControls(false);
		initOverLay(Double.valueOf(lat1), Double.valueOf(lng1));
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		btn_screen.setOnClickListener(this);

//		if (mWgsNaviBtn != null) {
//			mWgsNaviBtn.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					if (BaiduNaviManager.isNaviInited()) {
//						routeplanToNavi(CoordinateType.WGS84);
//					}
//				}
//
//			});
//		}
//		if (mGcjNaviBtn != null) {
//			mGcjNaviBtn.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//					if (BaiduNaviManager.isNaviInited()) {
//						routeplanToNavi(CoordinateType.GCJ02);
//					}
//				}
//
//			});
//		}
//		if (mBdmcNaviBtn != null) {
//			mBdmcNaviBtn.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View arg0) {
//
//					if (BaiduNaviManager.isNaviInited()) {
//						routeplanToNavi(CoordinateType.BD09_MC);
//					}
//				}
//			});
//		}
//
//		if (mDb06ll != null) {
//			mDb06ll.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View arg0) {
//					if (BaiduNaviManager.isNaviInited()) {
//						routeplanToNavi(CoordinateType.BD09LL);
//					}
//				}
//			});
//		}

	}

	public void startNavi() {
		if (BaiduNaviManager.isNaviInited()) {
			routeplanToNavi(CoordinateType.BD09LL);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
			finish();
		}
			break;
		case R.id.btn_screen: {
			startNavi();
		}
			break;
		default:
			break;
		}
	}

	public void initOverLay(double latitude, double longitude) {
		// 定义Maker坐标点
		LatLng ll = new LatLng(latitude, longitude);
		// 构建Marker图标
		BitmapDescriptor bd1 = BitmapDescriptorFactory
				.fromResource(R.drawable.icon_gcoding);
		// 构建MarkerOption，用于在地图上添加Marker
		OverlayOptions option = new MarkerOptions().position(ll).icon(bd1);
		// 在地图上添加Marker，并显示
		mBaiduMap.addOverlay(option);// 添加当前分店信息图层
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
		mBaiduMap.setMapStatus(u);
	}

	/**
	 * 清除所有Overlay
	 * 
	 * @param view
	 */
	public void clearOverlay(View view) {
		mBaiduMap.clear();
	}

	/**
	 * 重新添加Overlay
	 * 
	 * @param view
	 */
	public void resetOverlay(View view) {
		clearOverlay(null);
		// initOverlay();
	}

	@Override
	protected void onPause() {
		// MapView的生命周期与Activity同步，当activity挂起时需调用MapView.onPause()
		mMapView.onPause();
		super.onPause();
	}

	@Override
	protected void onResume() {
		// MapView的生命周期与Activity同步，当activity恢复时需调用MapView.onResume()
		mMapView.onResume();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		// MapView的生命周期与Activity同步，当activity销毁时需调用MapView.destroy()
		mMapView.onDestroy();
		super.onDestroy();
		// 回收 bitmap 资源
	}

	private boolean initDirs() {
		mSDCardPath = getSdcardDir();
		if (mSDCardPath == null) {
			return false;
		}
		File f = new File(mSDCardPath, APP_FOLDER_NAME);
		if (!f.exists()) {
			try {
				f.mkdir();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	String authinfo = null;

	private void initNavi() {
		// BaiduNaviManager.getInstance().setNativeLibraryPath(mSDCardPath +
		// "/BaiduNaviSDK_SO");

		BNOuterTTSPlayerCallback ttsCallback = null;

		BaiduNaviManager.getInstance().init(this, mSDCardPath, APP_FOLDER_NAME,
				new NaviInitListener() {
					@Override
					public void onAuthResult(int status, String msg) {
						if (0 == status) {
							authinfo = "key校验成功!";
						} else {
							authinfo = "key校验失败, " + msg;
						}
						LocationsActivity.this.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(LocationsActivity.this,
										authinfo, Toast.LENGTH_LONG).show();
							}
						});
					}

					public void initSuccess() {
						Toast.makeText(LocationsActivity.this, "百度导航引擎初始化成功",
								Toast.LENGTH_SHORT).show();
					}

					public void initStart() {
						Toast.makeText(LocationsActivity.this, "百度导航引擎初始化开始",
								Toast.LENGTH_SHORT).show();
					}

					public void initFailed() {
						Toast.makeText(LocationsActivity.this, "百度导航引擎初始化失败",
								Toast.LENGTH_SHORT).show();
					}

				}, null/* null mTTSCallback */);
	}

	private String getSdcardDir() {
		if (Environment.getExternalStorageState().equalsIgnoreCase(
				Environment.MEDIA_MOUNTED)) {
			return Environment.getExternalStorageDirectory().toString();
		}
		return null;
	}

	private void routeplanToNavi(CoordinateType coType) {
		BNRoutePlanNode sNode = null;
		BNRoutePlanNode eNode = null;
		switch (coType) {
		case GCJ02: {
			sNode = new BNRoutePlanNode(MyApplication.app_longitude,
					MyApplication.app_latitude, MyApplication.address, null,
					coType);
			eNode = new BNRoutePlanNode(Double.valueOf(lng1),
					Double.valueOf(lat1), address, null, coType);
			break;
		}
		case WGS84: {
			sNode = new BNRoutePlanNode(116.300821, 40.050969, "百度大厦", null,
					coType);
			eNode = new BNRoutePlanNode(116.397491, 39.908749, "北京天安门", null,
					coType);
			break;
		}
		case BD09_MC: {
			sNode = new BNRoutePlanNode(12947471, 4846474, "百度大厦", null, coType);
			eNode = new BNRoutePlanNode(12958160, 4825947, "北京天安门", null,
					coType);
			break;
		}
		case BD09LL: {
			sNode = new BNRoutePlanNode(MyApplication.app_longitude,
					MyApplication.app_latitude, MyApplication.address, null,
					coType);
			eNode = new BNRoutePlanNode(Double.valueOf(lng1),
					Double.valueOf(lat1), address, null, coType);
			break;
		}
		default:
			;
		}
		if (sNode != null && eNode != null) {
			List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
			list.add(sNode);
			list.add(eNode);
			BaiduNaviManager.getInstance().launchNavigator(this, list, 1, true,
					new DemoRoutePlanListener(sNode));
		}
	}

	public class DemoRoutePlanListener implements RoutePlanListener {

		private BNRoutePlanNode mBNRoutePlanNode = null;

		public DemoRoutePlanListener(BNRoutePlanNode node) {
			mBNRoutePlanNode = node;
		}

		@Override
		public void onJumpToNavigator() {
			/*
			 * 设置途径点以及resetEndNode会回调该接口
			 */

			for (Activity ac : activityList) {

				if (ac.getClass().getName().endsWith("BNDemoGuideActivity")) {

					return;
				}
			}
			Intent intent = new Intent(LocationsActivity.this,
					BNDemoGuideActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable(ROUTE_PLAN_NODE,
					(BNRoutePlanNode) mBNRoutePlanNode);
			intent.putExtras(bundle);
			startActivity(intent);

		}

		@Override
		public void onRoutePlanFailed() {
			// TODO Auto-generated method stub
			Toast.makeText(LocationsActivity.this, "算路失败", Toast.LENGTH_SHORT)
					.show();
		}
	}

	private BNOuterTTSPlayerCallback mTTSCallback = new BNOuterTTSPlayerCallback() {

		@Override
		public void stopTTS() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "stopTTS");
		}

		@Override
		public void resumeTTS() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "resumeTTS");
		}

		@Override
		public void releaseTTSPlayer() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "releaseTTSPlayer");
		}

		@Override
		public int playTTSText(String speech, int bPreempt) {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "playTTSText" + "_" + speech + "_" + bPreempt);

			return 1;
		}

		@Override
		public void phoneHangUp() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "phoneHangUp");
		}

		@Override
		public void phoneCalling() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "phoneCalling");
		}

		@Override
		public void pauseTTS() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "pauseTTS");
		}

		@Override
		public void initTTSPlayer() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "initTTSPlayer");
		}

		@Override
		public int getTTSState() {
			// TODO Auto-generated method stub
			Log.e("test_TTS", "getTTSState");
			return 1;
		}
	};

}
