package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.EvaListActivity;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
@Deprecated
public class StaffDetailActivity extends Activity implements OnClickListener {

	private ViewPager vp_mainadv;
	private StaffObject staffinfo;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
	private TextView jd_count1, sc_count1, pj_count1, marks, tv_ok;
	private LinearLayout collect;
	private ProgressDialog progressDialog = null;
	private Button btn_back;
	private TextView tv_number, tv_tel, tv_remark, tv_serice, jd_count,
			sc_count, sc_counts, tv_collect, tv_collect_name;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();

	private ArrayList<LoginBean> mlist = new ArrayList<LoginBean>();
	private LoginListObject mLoginListObject;

	private boolean flag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staffcolloct);
		initView();
		initListener();
		getPhoto();
		getStaffCollect();
	}

	private void initListener() {
		btn_back.setOnClickListener(this);
		tv_ok.setOnClickListener(this);
		collect.setOnClickListener(this);
		tv_collect.setOnClickListener(this);
		tv_collect_name.setOnClickListener(this);
	}

	private void initView() {
		staffinfo = (StaffObject) getIntent().getSerializableExtra("staff_info");
		jd_count = (TextView) findViewById(R.id.jd_count);
		sc_count = (TextView) findViewById(R.id.sc_count);
		sc_counts = (TextView) findViewById(R.id.sc_counts);
		tv_serice = (TextView) findViewById(R.id.tv_serice);
		collect = (LinearLayout) findViewById(R.id.collect);
		tv_ok = (TextView) findViewById(R.id.tv_ok);
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_number = (TextView) findViewById(R.id.tv_number);
		tv_tel = (TextView) findViewById(R.id.tv_tel);
		tv_remark = (TextView) findViewById(R.id.tv_remark);
		tv_collect = (TextView) findViewById(R.id.tv_collect);
		tv_collect_name = (TextView) findViewById(R.id.tv_collect_name);
		tv_number.setText(staffinfo.code_id);
		// tv_tel.setText("联系电话："+staffinfo.staffphoto);
		tv_remark.setText(staffinfo.remarks);
		tv_serice.setText(staffinfo.service_name);
		getStaffList();
	}
	// 图片轮播
	private void getPhoto() {
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getUserImg" };
			String[] paramName = new String[] { "user_id", staffinfo.id };
			params.add(methodParam);
			params.add(paramName);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}
	private CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mImageListObject = JSON.parseObject(v, ImageListObject.class);
				imglist = mImageListObject.getResponse();
				int n = 0;
				if (imglist.size() > 0) {
					for (n = 0; n < imglist.size(); n++) {
						final ImageView imgView = new ImageView(
								getApplicationContext());
						imgView.setClickable(true);
						imgView.setScaleType(ScaleType.FIT_XY);
						downloadImage.addTasks(imglist.get(n).img, imgView,
								new DownloadImage.ImageCallback() {
									public void imageLoaded(Bitmap imageBitmap,
											String imageUrl) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag(imageUrl);
										}
									}

									public void imageLoaded(Bitmap imageBitmap,
											DownloadImageMode callBackTag) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag("");
										}
									}
								});
						downloadImage.doTask();
						final String url = imglist.get(n).url;
						final String title = imglist.get(n).name;

						if (images.equals("")) {
							images = imglist.get(n).img;
						} else {
							images = images + "," + imglist.get(n).img;
						}

						imgView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent mIntent = new Intent(
										getApplicationContext(),
										ImagePagerActivity.class);
								mIntent.putExtra("images", images);
								startActivity(mIntent);
							}
						});
						imgViews.add(imgView);

					}
					vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
					vp_mainadv.setAdapter(new MyAdapter());
					// 设置监听，当ViewPager中的页面改变时调用
					vp_mainadv
							.setOnPageChangeListener(new MyPageChangeListener());
				}
			}
		}

	};

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return imglist.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onPageSelected(int arg0) {
			// for(int i=0;i<imglist.size();i++){
			// if(i!=arg0){
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_normal);
			// }else{
			// imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_focused);
			// }
			// }
		}
	}

	// 获取评价数和订单数
	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getJiedanshu" };
		String[] idParam2 = new String[] { "id", staffinfo.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					jd_count.setText(list.get(0).num1);
					sc_count.setText(list.get(0).num2);
					sc_counts.setText(list.get(0).num3);
				}
			} else {
				Toast.makeText(StaffDetailActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 获取该用户是否收藏
	private void getStaffCollect() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffCollect" };
		String[] idParam2 = new String[] { "staff_id", staffinfo.id };
		String[] clientParam = new String[] { "client_id",
				MyApplication.Clientuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(clientParam);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffCollect_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffCollect_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);

			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				mlist = mLoginListObject.getResponse();
				if (mlist.size() > 0) {
					if (Integer.valueOf(mlist.get(0).num2) > 0) {
						flag = true;
						tv_collect_name.setText("已收藏(");
						tv_collect.setBackgroundResource(R.drawable.yishoucang);
					} else {
						flag = false;
						tv_collect.setBackgroundResource(R.drawable.staff_11);
					}
				}

			} else {
				Toast.makeText(StaffDetailActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 收藏
	private void setCollctions() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "setStaffCollect" };
			String[] telParam = new String[] { "staff_id", staffinfo.id };
			String[] noteParam = new String[] { "client_id",
					MyApplication.Clientuser.id };
			params.add(methodParam);
			params.add(telParam);
			params.add(noteParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					setGoodsDianDan_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener setGoodsDianDan_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mLoginListObject = JSON.parseObject(result,
						LoginListObject.class);
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(StaffDetailActivity.this, "收藏成功",
							Toast.LENGTH_LONG).show();
					flag = true;
					String s = sc_count.getText().toString();
					int n = Integer.valueOf(s) + 1;
					sc_count.setText(String.valueOf(n));
					tv_collect_name.setText("已收藏(");
					tv_collect.setBackgroundResource(R.drawable.yishoucang);
				} else {
					Toast.makeText(StaffDetailActivity.this, "收藏失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(StaffDetailActivity.this, "收藏失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	// 取消收藏
	private void upCollctions() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upCollctions" };
			String[] telParam = new String[] { "staff_id", staffinfo.id };
			String[] noteParam = new String[] { "client_id",
					MyApplication.Clientuser.id };
			params.add(methodParam);
			params.add(telParam);
			params.add(noteParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					upCollctions_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener upCollctions_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mLoginListObject = JSON.parseObject(result,
						LoginListObject.class);
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(StaffDetailActivity.this, "取消收藏成功",
							Toast.LENGTH_LONG).show();
					flag = false;
					String s = sc_count.getText().toString();
					int n = Integer.valueOf(s) - 1;
					sc_count.setText(String.valueOf(n));
					tv_collect_name.setText("收藏TA(");
					tv_collect.setBackgroundResource(R.drawable.staff_11);
				} else {
					Toast.makeText(StaffDetailActivity.this, "取消收藏失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(StaffDetailActivity.this, "取消收藏失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.collect:
			Intent mintent = new Intent(StaffDetailActivity.this,
					EvaListActivity.class);
			mintent.putExtra("staff_id", staffinfo.id);
			startActivity(mintent);
			break;
		case R.id.tv_ok: {
			Intent intent = new Intent(getApplicationContext(), BusinessActivity.class);
			intent.putExtra("staff", staffinfo);
			setResult(504, intent);
			finish();
		}
			break;
		case R.id.btn_back: {
			Intent intent = new Intent(getApplicationContext(),
					BusinessActivity.class);
			intent.putExtra("staff", staffinfo);
			setResult(504, intent);
			finish();
		}
			break;
		case R.id.tv_collect: {
			// 取消收藏
			if (flag) {
				upCollctions();
			}
			// 收藏
			if (!flag) {
				setCollctions();
			}
		}
			break;
		default:
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Intent intent = new Intent(getApplicationContext(),
					BusinessActivity.class);
			intent.putExtra("staff", staffinfo);
			setResult(504, intent);
			finish();

		}

		return false;

	}
}
