package com.aozhi.hugemountain.activity.PublicActivity;

import java.util.ArrayList;

import com.aozhi.hugemountain.view.HackyViewPager;
import com.aozhi.hugemountain.ImageDetailFragment;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.SplitString;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.TextView;

public class ImagePagerActivity extends FragmentActivity {
	private static final String STATE_POSITION = "STATE_POSITION";
	public static final String EXTRA_IMAGE_INDEX = "image_index";
	public static final String EXTRA_IMAGE_URLS = "image_urls";

	private HackyViewPager mPager;
	private int pagerPosition;
	private TextView indicator;
	private String[] imgs;
	private String arg2="1";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_detail_pager);
		ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(ImagePagerActivity.this));
		String images = getIntent().getStringExtra("images");
		String imageslist=getIntent().getStringExtra("imageslist");
		arg2 =getIntent().getStringExtra("arg2");
//		int pagerPosition = getIntent().getIntExtra("page", 0);
		boolean local = getIntent().getBooleanExtra("local", false);
		urls = new ArrayList<String>();
		if (images!=null&&!images.equals("")) {
			imgs = SplitString.split(images, ",");
			for (int i = 0; i < imgs.length; i++) {
				if (imgs[i]!=null&&!imgs[i].equals("")) {
						urls.add(Constant.DOWNLOAD+imgs[i]);
				}
			}
		}
		
		if (imageslist!=null&&!imageslist.equals("")) {
			imgs = SplitString.split(imageslist, ",");
			for (int i = 0; i < imgs.length; i++) {
				if (imgs[i]!=null&&!imgs[i].equals("")) {
						urls.add(Constant.DOWNLOAD+imgs[i]);
				}
			}
		}
		
		mPager = (HackyViewPager) findViewById(R.id.pager);
		ImagePagerAdapter mAdapter = new ImagePagerAdapter(
				getSupportFragmentManager(), urls);
		mPager.setAdapter(mAdapter);
		indicator = (TextView) findViewById(R.id.indicator);

		
		if(imageslist!=null&&!imageslist.equals("")){
		CharSequence text = getString(R.string.viewpager_indicator, Integer.parseInt(arg2)+1, mPager.getAdapter().getCount());
		indicator.setText(text);
		}
		
		if (images!=null&&!images.equals("")) {
			CharSequence text = getString(R.string.viewpager_indicator,1, mPager
					.getAdapter().getCount());
			indicator.setText(text);
		}
		// 更新下标
		mPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageSelected(int arg0) {
				CharSequence text = getString(R.string.viewpager_indicator,
						arg0 + 1, mPager.getAdapter().getCount());
				indicator.setText(text);
			}

		});

		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
		}

		mPager.setCurrentItem(pagerPosition);
	}
	private ArrayList<String> urls;
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, mPager.getCurrentItem());
	}

	private class ImagePagerAdapter extends FragmentStatePagerAdapter {

		public ArrayList<String> fileList;

		public ImagePagerAdapter(FragmentManager fm, ArrayList<String> fileList) {
			super(fm);
			this.fileList = fileList;
		}

		@Override
		public int getCount() {
			return fileList == null ? 0 : fileList.size();
		}

		@Override
		public Fragment getItem(int position) {
			String url = fileList.get(position);
			return ImageDetailFragment.newInstance(position, url, handler);
		}
	}
	private Handler handler = new Handler(){
		public void handleMessage(Message msg) {
			int i = msg.what;
			if (i>=0&&i<imgs.length&&imgs[i]!=null) {
//				String img = imgs[i];
//				Intent intent = new Intent(ImagePagerActivity.this, ShareActivity.class);
//				intent.putExtra("url", img);
//				startActivity(intent);
			}
			
		};
	};
	
}