package com.aozhi.hugemountain.adapter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Timer;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.*;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class StaffStoreListAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater inflater;
    private int selectedPosition = -1;// 选中的位置
    private ArrayList<StoreObject> store_list = new ArrayList<StoreObject>();
    private ProgressDialog progressDialog = null;
    private StoreListObject mStoreListObject;
    private ArrayList<StoreObject> list = new ArrayList<StoreObject>();
    private int num = -1;
    private View stet;
    private Timer timer;

    public StaffStoreListAdapter(Context ctx, ArrayList<StoreObject> li) {
        mContext = ctx;
        store_list = li;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return store_list.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return store_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public void setSelectedPosition(int position) {
        selectedPosition = position;
    }

    public static String getDistanceFromXtoY(double lat_a, double lng_a, double lat_b, double lng_b) {
        LatLng start = new LatLng(lat_a, lng_a);
        LatLng end = new LatLng(lat_b, lng_b);
        DistanceUtil.getDistance(start, end);
        double f = DistanceUtil.getDistance(start, end) / 1000;
        BigDecimal b = new BigDecimal(f);
        double f1 = b.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
        return String.valueOf(f1);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final Holder mHolder;
        if (convertView == null) {
            mHolder = new Holder();
            convertView = inflater.inflate(R.layout.staff_item_storelist, null);
            mHolder.photo = (ImageView) convertView.findViewById(R.id.photo);
            mHolder.juli = (TextView) convertView.findViewById(R.id.juli);
            mHolder.name = (TextView) convertView.findViewById(R.id.name);
            mHolder.phone = (TextView) convertView.findViewById(R.id.phone);
            mHolder.address = (TextView) convertView.findViewById(R.id.address_name);
            mHolder.business_hours = (TextView) convertView
                    .findViewById(R.id.business_hours);
            mHolder.service = (TextView) convertView.findViewById(R.id.service);
            convertView.setTag(mHolder);
        } else {
            mHolder = (Holder) convertView.getTag();
        }
        mHolder.name.setText(store_list.get(position).name);
        mHolder.phone.setText(store_list.get(position).storephoto);
        mHolder.address.setText(store_list.get(position).address);
        mHolder.business_hours.setText(store_list.get(position).business_hours);
        String jili = getDistanceFromXtoY(MyApplication.app_latitude, MyApplication.app_longitude,
                Double.parseDouble(store_list.get(position).latitude),
                Double.parseDouble(store_list.get(position).longitude));
        mHolder.juli.setText(jili + "km");
        mHolder.service.setText(store_list.get(position).service);
        if (store_list.get(position).phone.equals("") || store_list.get(position).phone == null) {

        } else {
            MyApplication.downloadImage.addTask(store_list.get(position).phone, mHolder.photo, new DownloadImage.ImageCallback() {

                        @Override
                        public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(imageBitmap);
                                mHolder.photo.setBackgroundDrawable(drawable);
                            }
                        }
                        @Override
                        public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(imageBitmap);
                                mHolder.photo.setBackgroundDrawable(drawable);
                            }
                        }
                    });
        }
        MyApplication.downloadImage.doTask();
        return convertView;
    }

    public class Holder {
        TextView phone;
        TextView name;
        ImageView photo;
        TextView address;
        TextView business_hours;
        TextView service;
        TextView tv_miaoshu, juli;
    }

    private void upStaffStatus(String status, int post) {
        num = post;
        Constant.NET_STATUS = Utils.getCurrentNetWork(mContext);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params2 = new ArrayList<String[]>();
            String[] funParam2 = new String[]{"fun", "upWorkStatus"};
            String[] name1 = new String[]{"user_id",
                    MyApplication.Staffuser.id};
            String[] name2 = new String[]{"store_id", store_list.get(post).id};
            String[] name3 = new String[]{"status", status};
            params2.add(funParam2);
            params2.add(name1);
            params2.add(name2);
            params2.add(name3);
            progressDialog = ProgressDialog.show(mContext, null, "正在加载", false);
            progressDialog.setCancelable(true);
            Constant.NET_STATUS = Utils.getCurrentNetWork(mContext);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params2,
                        type_callbackListener);
            } else {
                Toast.makeText(mContext, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        }
    }

    private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
        final Holder mHolder = new Holder();

        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
            if (!v.equals("fail")) {
                mStoreListObject = JSON.parseObject(v, StoreListObject.class);
                list = mStoreListObject.response;
                if (mStoreListObject.meta.getMsg().equals("OK")) {
                    if (list.size() > 0) {
                        if (stet != null) {
                            Toast.makeText(mContext, "签到成功", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(mContext, "签到失败", Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(mContext, "签到失败", Toast.LENGTH_LONG).show();
            }
        }
    };

}
