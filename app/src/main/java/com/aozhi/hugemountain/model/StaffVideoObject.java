package com.aozhi.hugemountain.model;

import java.io.Serializable;

public class StaffVideoObject implements Serializable{
	  public String id;
      public String name;
      public String src;
      public String staff_id;
      public String create_time;
      public String update_time;
      public String mark;
      public String del_flag;
      public String link;
}
