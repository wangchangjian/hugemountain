package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.CollectClientAdapter;
import com.aozhi.hugemountain.model.CollectListObject;
import com.aozhi.hugemountain.model.CollectObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EvaListActivity extends Activity {

	private TextView eva_title,nodata;
	private ListView list_eva;
	private ProgressDialog progressDialog=null;
	private String user_id;
	private CollectListObject mCollectListObject;
	private ArrayList<CollectObject> list=new ArrayList<CollectObject>();
	private CollectClientAdapter adapter;
	private ImageView img1;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_evalutelist);
		initView();
		initListener();
		getStaffCollect();
	}

	private void initListener() {
		img1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();				
			}
		});
		adapter=new CollectClientAdapter(EvaListActivity.this, list);
		list_eva.setAdapter(adapter);
	}

	private void initView() {
		user_id=getIntent().getStringExtra("staff_id");
		eva_title=(TextView) findViewById(R.id.eva_title);
		list_eva=(ListView) findViewById(R.id.list_eva);
		nodata=(TextView) findViewById(R.id.nodata);
		img1=(ImageView) findViewById(R.id.img1);
	}
	
		private void getStaffCollect() {
			// fun=getCollect&staff_id=20151106093853
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getCollect" };
			String[] pageParam=new String[] {"staff_id",user_id};
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载中。。。", false);
			progressDialog.setCancelable(true);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params,
						getGoodsList_callbackListener);
			} else
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

		private CallbackListener getGoodsList_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String result) {
				Log.d("返回数据", result);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!result.equals("fail")) {
					mCollectListObject = JSON.parseObject(result,CollectListObject.class);
					list = mCollectListObject.response;
					if (mCollectListObject.meta.getMsg().equals("OK")) {
						if (list.size() > 0) {
							nodata.setVisibility(View.GONE);
							list_eva.setVisibility(View.VISIBLE);
							adapter=new CollectClientAdapter(EvaListActivity.this, list);
							list_eva.setAdapter(adapter);
						}else{
							nodata.setVisibility(View.VISIBLE);
							list_eva.setVisibility(View.GONE);
							Toast.makeText(EvaListActivity.this, "无数据", Toast.LENGTH_SHORT).show();
						}
					}
				}else{
					nodata.setVisibility(View.VISIBLE);
					list_eva.setVisibility(View.GONE);
					Toast.makeText(EvaListActivity.this, "无数据", Toast.LENGTH_SHORT).show();
				}
			}
		};
	
}
