package com.aozhi.hugemountain.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ConsumerActivity.BusinessActivity;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.model.ServiceObject;

import java.util.ArrayList;

/**
 * Created by ${wangchangjian} on 2016/4/28.
 */
public class ServiceDetaileView extends LinearLayout {

    private final TextView btn_back;
    private ServiceObject mProjectObject;
    private TextView tv_name;
    private ClickExtensionTextLayout tv_content;
    private TextView tv_money;
    private TextView count;
    private ViewPager vp_mainadv;
    private DownloadImage downloadImage = new DownloadImage();
    private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
    private TextView tv_time, btn_xc, btn_yy;
    private IndexDotView dot_indicator;
    private static ViewGroup v;
    private Dialog dialog;

    public ServiceDetaileView(Context context) {
        super(context);
        v = (ViewGroup) LayoutInflater.from(MyApplication.getInstance()).inflate(R.layout.service_detail_view, null);// 得到加载view
        tv_name = (TextView) v.findViewById(R.id.tv_name);
        tv_content = (ClickExtensionTextLayout) v.findViewById(R.id.tv_content);
        tv_money = (TextView) v.findViewById(R.id.tv_money);
        count = (TextView) v.findViewById(R.id.count);
        tv_time = (TextView) v.findViewById(R.id.tv_time);
        btn_xc = (TextView) v.findViewById(R.id.btn_xc);
        dot_indicator = (IndexDotView) v.findViewById(R.id.dot_indicator);
        btn_yy = (TextView) v.findViewById(R.id.btn_yy);
        btn_back = (TextView) v.findViewById(R.id.btn_back);
        vp_mainadv = (ViewPager) v.findViewById(R.id.viewPager);
        dialog = new Dialog(this.getContext(), R.style.loading_dialog);
        dialog.setCancelable(true);// 不可以用“返回键”取消
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(v, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));// 设置布局

    }

    public void setData(final ServiceObject mProjectObject) {
        this.mProjectObject = mProjectObject;
        imgViews.clear();
        btn_xc.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                onSelectChangedListener.onBookPressed();
//                Intent intent = new Intent(ServiceDetaileView.this.getContext(), BusinessActivity.class);
//                intent.putExtra("service", mProjectObject);
//                intent.putExtra("type", "预订");
//                ((Activity) ServiceDetaileView.this.getContext()).setResult(503, intent);
                ServiceDetaileView.this.dialog.dismiss();
            }
        });
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                ServiceDetaileView.this.dialog.dismiss();
            }
        });
        btn_yy.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onSelectChangedListener.onRightNowPressed();
//                Intent intent = new Intent(ServiceDetaileView.this.getContext(),
//                        BusinessActivity.class);
//                intent.putExtra("service", mProjectObject);
//                intent.putExtra("type", "现场");
//                ((Activity) ServiceDetaileView.this.getContext()).setResult(503, intent);
                ServiceDetaileView.this.dialog.dismiss();

            }
        });
        tv_name.setText(mProjectObject.name);
        tv_content.setText(null,mProjectObject.remark);
        tv_money.setText("￥" + mProjectObject.money);
        tv_time.setText(mProjectObject.service_time + "分钟");
        count.setText(mProjectObject.remarks);
        String imagurl = "";
        String[] s = mProjectObject.photo.split(",");
        if (s.length > 0) {
            imagurl = s[0];
        }
        dialog.show();
        getPhoto();
    }

    // 图片轮播
    private void getPhoto() {
        String[] photos = mProjectObject.photo.split(",");

        if (photos.length > 0) {
            int n = 0;
            for (n = 0; n < photos.length; n++) {
                final ImageView imgView = new ImageView(v.getContext());
                imgView.setClickable(true);
                imgView.setScaleType(ImageView.ScaleType.FIT_XY);

                downloadImage.addTasks(photos[n], imgView, new DownloadImage.ImageCallback() {
                    public void imageLoaded(Bitmap imageBitmap, String imageUrl) {
                        if (imageBitmap != null) {
                            imgView.setImageBitmap(imageBitmap);
                            imgView.setTag(imageUrl);
                        }
                    }

                    public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                        if (imageBitmap != null) {
                            imgView.setImageBitmap(imageBitmap);
                            imgView.setTag("");
                        }
                    }
                });
                downloadImage.doTask();
                final String url = photos[n];
                final String title = photos[n];

                imgView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mIntent = new Intent(ServiceDetaileView.this.getContext(), ImagePagerActivity.class);
                        mIntent.putExtra("images", mProjectObject.photo);
                        ServiceDetaileView.this.getContext().startActivity(mIntent);
                    }
                });
                imgViews.add(imgView);
            }
            vp_mainadv.setAdapter(new MyAdapter());
            Log.i("getChildCount", photos.length + "");
            dot_indicator.setNums(photos.length);

            // 设置监听，当ViewPager中的页面改变时调用
            dot_indicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    vp_mainadv.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                            dot_indicator.setMoveDotPercent((positionOffset + position) / vp_mainadv.getAdapter().getCount());

                        }

                        @Override
                        public void onPageSelected(int position) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                    dot_indicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    private OrderSelectedListener onSelectChangedListener;

    public void setOnSelectedListener(OrderSelectedListener onSelectChangedListener) {
        this.onSelectChangedListener = onSelectChangedListener;
    }

    public interface OnSelectChangedListener {
        void onBookPressed();

        void onRightNowPressed();
    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return imgViews.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(View arg0, int arg1) {
            ((ViewPager) arg0).addView(imgViews.get(arg1));
            return imgViews.get(arg1);
        }

        @Override
        public void destroyItem(View arg0, int arg1, Object arg2) {
            ((ViewPager) arg0).removeView((View) arg2);
        }

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1) {

        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View arg0) {

        }

        @Override
        public void finishUpdate(View arg0) {

        }
    }


}
