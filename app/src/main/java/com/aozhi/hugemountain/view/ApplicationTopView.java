package com.aozhi.hugemountain.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aozhi.hugemountain.R;

/**
 * Created by ${wangchangjian} on 2016/4/24.
 */
public class ApplicationTopView extends LinearLayout {
    private Button btn_left;
    private TextView tv_title;
    private Button btn_right;

    public void setOnTopClickListener(OnTopClickListener onTopClickListener) {
        this.onTopClickListener = onTopClickListener;
    }

    private OnTopClickListener onTopClickListener;


    public ApplicationTopView(Context context) {
        this(context, null);
    }

    public ApplicationTopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ApplicationTopView);
        String s = a.getString(R.styleable.ApplicationTopView_top_text);
        int left_bg = a.getResourceId(R.styleable.ApplicationTopView_top_left_bg, 0);
        int right_bg = a.getResourceId(R.styleable.ApplicationTopView_top_right_bg, 0);
        boolean l_visible = a.getBoolean(R.styleable.ApplicationTopView_top_left_visible, true);
        boolean r_visible = a.getBoolean(R.styleable.ApplicationTopView_top_right_visible, true);
        a.recycle();

        View view = LayoutInflater.from(this.getContext()).inflate(R.layout.app_top_layout, this);
        btn_left = (Button) view.findViewById(R.id.btn_left);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        btn_right = (Button) view.findViewById(R.id.btn_right);
        btn_left.setBackgroundResource(left_bg);
        btn_right.setBackgroundResource(right_bg);
        if (!l_visible) btn_left.setVisibility(INVISIBLE);
        if (!r_visible) btn_right.setVisibility(INVISIBLE);

        tv_title.setText(s);

        btn_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onTopClickListener != null) {
                    onTopClickListener.onLeftbuttonClick();
                }
            }
        });
        btn_right.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onTopClickListener != null) {
                    onTopClickListener.onRightbuttonClick();
                }
            }
        });


    }
}
