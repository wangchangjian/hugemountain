package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.activity.StaffActivity.StaffAccountActivity;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class Addcard extends BaseActivity implements OnClickListener {

	private Button img1, btn_card;
	private EditText card_number, card_password, card_addr;
	private Spinner sp_card;
	private ArrayList<String> sp_list = new ArrayList<String>();
	private ProgressDialog progressDialog;
	private String stats;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cardinfo);
		initView();
		initListener();
	}

	@Override
	protected void initData() {

	}

	private void initListener() {
		img1.setOnClickListener(this);
		btn_card.setOnClickListener(this);
		sp_list.add("中国银行");
		sp_list.add("建设银行");
		sp_list.add("交通银行");
		sp_list.add("工商银行");
		sp_list.add("中信银行");
		sp_list.add("浦发银行");
		sp_list.add("农业银行");
		ArrayAdapter<String> sp_adapter = new ArrayAdapter<String>(
				Addcard.this, R.layout.item_type, sp_list);
		sp_card.setAdapter(sp_adapter);
	}

	public void initView() {
		stats = getIntent().getStringExtra("stats");
		img1 = (Button) findViewById(R.id.img1);
		card_number = (EditText) findViewById(R.id.card_number);
		btn_card = (Button) findViewById(R.id.btn_card);
		sp_card = (Spinner) findViewById(R.id.sp_card);
		card_addr = (EditText) findViewById(R.id.card_addr);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			setResult(386);
			finish();
			break;
		case R.id.btn_card:
			if (card_number.equals("") && card_addr.equals("")) {
				Toast.makeText(getApplicationContext(), "你输入的信息有误，请重新输入",
						Toast.LENGTH_SHORT).show();
			} else {
				if (stats.equals("staff")) {
					setStaffAccountCard();
				} else {
					setAccountCard();
				}
			}
			break;
		default:
			break;
		}

	}

	private void setAccountCard() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setAccount" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "staff_id", "" };
		String[] nameParam3 = new String[] { "card_number",
				card_number.getText().toString() };
		String[] nameParam4 = new String[] { "card_type",
				sp_card.getSelectedItem().toString() };
		String[] nameParam5 = new String[] { "card_address",
				card_addr.getText().toString() };
		String[] nameParam6 = new String[] { "zfb_name", "" };
		String[] nameParam7 = new String[] { "remarks", "" };
		String[] nameParam8 = new String[] { "del_flag", "5" }; // 0为删除，5为银行卡，6为支付宝
		String[] nameParam9 =new String[]{"zfb_username",""};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		params2.add(nameParam8);
		params2.add(nameParam9);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				Toast.makeText(Addcard.this, "添加成功", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(Addcard.this,
						StaffAccountActivity.class);
				setResult(386, intent);
				finish();
			} else {
				Toast.makeText(Addcard.this, "添加失败", Toast.LENGTH_LONG).show();
			}

		}
	};

	// 上传设置的员工卡号
	private void setStaffAccountCard() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setAccount" };
		String[] nameParam1 = new String[] { "store_id", "" };
		String[] nameParam2 = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		String[] nameParam3 = new String[] { "card_number",
				card_number.getText().toString() };
		String[] nameParam4 = new String[] { "card_type",
				sp_card.getSelectedItem().toString() };
		String[] nameParam5 = new String[] { "card_address",
				card_addr.getText().toString() };
		String[] nameParam6 = new String[] { "zfb_name", "" };
		String[] nameParam7 = new String[] { "remarks", "" };
		String[] nameParam8 = new String[] { "del_flag", "5" }; // 0为删除，5为银行卡，6为支付宝
		String[] nameParam9 =new String[]{"zfb_username",""};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		params2.add(nameParam8);
		params2.add(nameParam9);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaff_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaff_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				Toast.makeText(Addcard.this, "添加成功", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(Addcard.this,
						StaffAccountActivity.class);
				setResult(386, intent);
				finish();
			} else {
				Toast.makeText(Addcard.this, "添加失败", Toast.LENGTH_LONG).show();
			}

		}
	};
}
