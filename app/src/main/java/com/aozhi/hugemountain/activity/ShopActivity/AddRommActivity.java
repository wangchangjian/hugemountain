package com.aozhi.hugemountain.activity.ShopActivity;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddRommActivity extends BaseActivity implements OnClickListener {

	public static final int RESULT_CODE = 101;
	private EditText et_number, et_name;
	private TextView tv_sure;
	private Button btn_back;
	private ProgressDialog progressDialog = null;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_addroom);
		initView();
		initListnner();
	}

	@Override
	protected void initData() {

	}

	public void initView() {
		// TODO Auto-generated method stub
		et_number = (EditText) findViewById(R.id.et_number);
		et_name = (EditText) findViewById(R.id.et_name);
		tv_sure = (TextView) findViewById(R.id.tv_sure);
		btn_back = (Button) findViewById(R.id.btn_back);
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		tv_sure.setOnClickListener(this);
		btn_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back:
			setResult(RESULT_CODE);
			finish();
			break;
		case R.id.tv_sure: {
			if (et_number.getText().toString().equals("")) {
				Toast.makeText(AddRommActivity.this, "请输入房间号",
						Toast.LENGTH_LONG).show();
			} else if (et_name.getText().toString().equals("")) {
				Toast.makeText(AddRommActivity.this, "请输入房间名称",
						Toast.LENGTH_LONG).show();
			}else {
				AddProject();
			}
		}
			break;
		default:
			break;
		}
	}
	
	private void AddProject() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "addRoom" };
		String[] nameParam2 = new String[] { "name",
				et_name.getText().toString() };
		String[] pwdParam2 = new String[] { "codo",
				et_number.getText().toString() };
		String[] store_idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(pwdParam2);
		params2.add(store_idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddStaffInfo_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {
					Toast.makeText(AddRommActivity.this, "房间添加成功",
							Toast.LENGTH_LONG).show();
					Intent intent = new Intent();
					setResult(RESULT_CODE, intent);
					finish();
				} else {
					Toast.makeText(AddRommActivity.this, "房间添加失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(AddRommActivity.this, "房间添加失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

}
