package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.activity.StaffActivity.StaffAccountActivity;
import com.aozhi.hugemountain.model.AccountListObject;
import com.aozhi.hugemountain.model.AccountObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddNet extends BaseActivity {

	private EditText net_code,net_name;
	private Button btn_net,img1;
	private ProgressDialog progressDialog;
	private AccountListObject mAccountListObject;
	private ArrayList<AccountObject> list = new ArrayList<AccountObject>();
	private String stats;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_netinfo);
		stats=getIntent().getStringExtra("stats");
		net_code = (EditText) findViewById(R.id.net_code);
		net_name=(EditText)findViewById(R.id.net_name);
		btn_net = (Button) findViewById(R.id.btn_net);
		btn_net.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if(!net_code.getText().toString().equals("")&&!net_name.getText().toString().equals("")){
					if(stats.equals("staff")){
						setStaffAccountCard();
					}else{
						setAccountCard();
					}
				}
				
			}
		});
		img1=(Button) findViewById(R.id.img1);
		img1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				setResult(387);
				finish();
			}
		});
	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {

	}

	private void setAccountCard() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setAccount" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "staff_id", "" };
		String[] nameParam3 = new String[] { "card_number", "" };
		String[] nameParam4 = new String[] { "card_type", "" };
		String[] nameParam5 = new String[] { "card_address", "" };
		String[] nameParam6 = new String[] { "zfb_name",
				net_code.getText().toString().trim() };
		String[] nameParam7 = new String[] { "remarks", "" };
		String[] nameParam8=new String[]{"del_flag","6"};  //0为删除，5为银行卡，6为支付宝
		String[] nameParam9=new String[]{"zfb_username",net_name.getText().toString()};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		params2.add(nameParam8);
		params2.add(nameParam9);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(AddNet.this, "添加成功", Toast.LENGTH_LONG)
							.show();
					Intent mIntent = new Intent(getApplicationContext(),
							StaffAccountActivity.class);
					setResult(387, mIntent);
					finish();
				}
			} else {
				Toast.makeText(AddNet.this, "添加失败", Toast.LENGTH_LONG).show();
			}

		}
	};
	
	//上传员工支付宝账号
	private void setStaffAccountCard() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setAccount" };
		String[] nameParam1 = new String[] { "store_id","" };
		String[] nameParam2 = new String[] { "staff_id", MyApplication.Staffuser.id };
		String[] nameParam3 = new String[] { "card_number", "" };
		String[] nameParam4 = new String[] { "card_type", "" };
		String[] nameParam5 = new String[] { "card_address", "" };
		String[] nameParam6 = new String[] { "zfb_name",
				net_code.getText().toString().trim() };
		String[] nameParam7 = new String[] { "remarks", "" };
		String[] nameParam8=new String[]{"del_flag","6"};  //0为删除，5为银行卡，6为支付宝
		String[] nameParam9=new String[]{"zfb_username",net_name.getText().toString()};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		params2.add(nameParam8);
		params2.add(nameParam9);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaff_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaff_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(AddNet.this, "添加成功", Toast.LENGTH_LONG)
							.show();
					Intent mIntent = new Intent(getApplicationContext(),
							StaffAccountActivity.class);
					setResult(387, mIntent);
					finish();
				}
			} else {
				Toast.makeText(AddNet.this, "添加失败", Toast.LENGTH_LONG).show();
			}

		}
	};
}
