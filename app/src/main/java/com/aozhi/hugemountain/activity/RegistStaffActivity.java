package com.aozhi.hugemountain.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.LoginActivity;
import com.aozhi.hugemountain.model.*;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.alibaba.fastjson.JSON;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class RegistStaffActivity extends Activity {
	Button btn_ok, btn_yanzhengma, btn_back;
	TextView text_xieyi;

	private String num, str;
	private TimeCount time;
	private ProgressDialog progressDialog = null;
	private EditText et_name, et_pwd, et_surepwd, et_staffname, et_staffage;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private CheckBox cb;
	private TextView et_photo;
	private RadioGroup sex;
	private RadioButton sex1, sex2;
	private String number = "";
	private String xingb ="女";
	// 填写从短信SDK应用后台注册得到的APPKEY
	private static String APPKEY = "a60f6c4438e8";
	// 填写从短信SDK应用后台注册得到的APPSECRET
	private static String APPSECRET = "35b4d102dd365758244b77426d483d70";
	// 短信注册，随机产生头像
	private static final String[] AVATARS = {
			"http://tupian.qqjay.com/u/2011/0729/e755c434c91fed9f6f73152731788cb3.jpg",
			"http://99touxiang.com/public/upload/nvsheng/125/27-011820_433.jpg",
			"http://img1.touxiang.cn/uploads/allimg/111029/2330264224-36.png",
			"http://img1.2345.com/duoteimg/qqTxImg/2012/04/09/13339485237265.jpg",
			"http://diy.qqjay.com/u/files/2012/0523/f466c38e1c6c99ee2d6cd7746207a97a.jpg",
			"http://img1.touxiang.cn/uploads/20121224/24-054837_708.jpg",
			"http://img1.touxiang.cn/uploads/20121212/12-060125_658.jpg",
			"http://img1.touxiang.cn/uploads/20130608/08-054059_703.jpg",
			"http://diy.qqjay.com/u2/2013/0422/fadc08459b1ef5fc1ea6b5b8d22e44b4.jpg",
			"http://img1.2345.com/duoteimg/qqTxImg/2012/04/09/13339510584349.jpg",
			"http://img1.touxiang.cn/uploads/20130515/15-080722_514.jpg",
			"http://diy.qqjay.com/u2/2013/0401/4355c29b30d295b26da6f242a65bcaad.jpg" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_registstaff);
		initView();
		initListener();
	}

	private void initView() {
		number = getIntent().getStringExtra("staff_phonenumber");
		btn_ok = (Button) findViewById(R.id.btn_ok);
		btn_back = (Button) findViewById(R.id.btn_back);
		et_photo = (TextView) findViewById(R.id.et_photo);
		et_pwd = (EditText) findViewById(R.id.et_pwd);
		et_surepwd = (EditText) findViewById(R.id.et_surepwd);
		et_name = (EditText) findViewById(R.id.et_name);
		text_xieyi = (TextView) findViewById(R.id.text_xieyi);
		btn_yanzhengma = (Button) findViewById(R.id.btn_yanzhengma);
		et_staffname = (EditText) findViewById(R.id.et_staffname);
		et_staffage = (EditText) findViewById(R.id.et_staffage);
		cb = (CheckBox) findViewById(R.id.cb);
		sex = (RadioGroup) findViewById(R.id.sex);
		sex1 = (RadioButton) findViewById(R.id.sex1);
		sex2 = (RadioButton) findViewById(R.id.sex2);
	}

	private String Split(String String) {
		String[] name = String.split("[,]");
		String result1 = name[0].substring(7);
		String result2 = name[1].substring(8, 10);
		return result1;
	}

	private void initListener() {
		et_photo.setText(Split(number));
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(getApplicationContext(),
						LoginActivity.class);
				startActivity(intent);
				finish();
			}
		});

		text_xieyi.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// Intent mIntent = new Intent(this,XieYiActivity.class);
				// startActivity(mIntent);
			}
		});

		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				String regEx = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]";
//				Pattern p = Pattern.compile(regEx);
				if (et_pwd.getText().toString().equals("")) {
					Toast.makeText(RegistStaffActivity.this, "请输入密码！",
							Toast.LENGTH_LONG).show();
				} else if (et_pwd.getText().toString().length() < 6) {
					Toast.makeText(RegistStaffActivity.this, "密码不得少于6位！",
							Toast.LENGTH_LONG).show();
				} else if (!et_pwd.getText().toString()
						.equals(et_surepwd.getText().toString())) {
					Toast.makeText(RegistStaffActivity.this, "密码与确认密码不一致！",
							Toast.LENGTH_LONG).show();
				} else if (et_staffname.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "请输入你的姓名",
							Toast.LENGTH_SHORT).show();
				} else if (et_staffage.getText().toString().equals("")) {
					Toast.makeText(getApplicationContext(), "请输入你的年龄",
							Toast.LENGTH_SHORT).show();
				} else if (!cb.isChecked()) {
					Toast.makeText(RegistStaffActivity.this, "请同意巨岳注册协议！",
							Toast.LENGTH_LONG).show();
				} else {
					getuser();
				}
			}
		});
		sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1) {
				switch (arg1) {
				case R.id.sex1:
					xingb = "男";
					break;
				case R.id.sex2:
					xingb = "女";
					break;
				default:
					break;
				}
			}
		});
	}

	private void getuser() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "selectstaffbyphoto" };
		String[] funParam3 = new String[] { "photo",
				et_photo.getText().toString() };
		params1.add(funParam1);
		params1.add(funParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(RegistStaffActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					user_callbackListener);
		} else {
			Toast.makeText(RegistStaffActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
		SharedPreferences mySharedPreferences = getSharedPreferences(
				"client_user", Activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("name", et_photo.getText().toString());
		editor.putString("pwd", et_pwd.getText().toString());
		editor.putBoolean("ok", true);
		editor.commit();
	}

	private CallbackListener user_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					Toast.makeText(RegistStaffActivity.this, "登录账号已存在，请重新输入",
							Toast.LENGTH_LONG).show();
				} else {
					getregister();
				}

			}
		}

	};

	private void getregister() {

		ArrayList<String[]> params = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "registstaff" };
		String[] funParam2 = new String[] { "photo",
				et_photo.getText().toString() };
		String[] funParam3 = new String[] { "img", "/picture/u11.png" };
		String[] funParam4 = new String[] { "pwd",
				encryption(et_pwd.getText().toString()) };
		String[] funParam5 = new String[] { "name",
				et_staffname.getText().toString().trim() };
		String[] funParam6 = new String[] { "sex", xingb };
		String[] funParam7 = new String[] { "age",
				et_staffage.getText().toString().trim() };
		params.add(funParam1);
		params.add(funParam2);
		params.add(funParam3);
		params.add(funParam4);
		params.add(funParam5);
		params.add(funParam6);
		params.add(funParam7);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list=mLoginListObject.response;
				if (mLoginListObject.getMeta().msg.equals("OK")) {
					setStaffSWorkTime(list.get(0).id);
					Toast.makeText(RegistStaffActivity.this, "注册成功",
							Toast.LENGTH_LONG).show();
					Intent mintent = new Intent(RegistStaffActivity.this,
							LoginStaffActivity.class);
					startActivity(mintent);
					finish();
				} else {
					Toast.makeText(RegistStaffActivity.this, "注册失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RegistStaffActivity.this, "注册失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}

	/* 定义一个倒计时的内部类 */
	class TimeCount extends CountDownTimer {
		public TimeCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
		}

		public void onFinish() {// 计时完毕时触发
			btn_yanzhengma.setText("重新验证");
			btn_yanzhengma.setBackgroundColor(getResources().getColor(
					R.color.btn_yanzhengma));
			btn_yanzhengma.setClickable(true);
		}

		public void onTick(long millisUntilFinished) {// 计时过程显示
			btn_yanzhengma.setClickable(false);
			btn_yanzhengma.setText(millisUntilFinished / 1000 + "秒");
		}
	}
	
	private void setStaffSWorkTime(String id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setDefaultStaffTime" };
		String[] name1 = new String[] { "staff_id",id};
		String[] name2 = new String[] { "begin","08:00:00"};
		String[] name3 = new String[] { "end", "18:00:00" };
		String[] name4 = new String[]{"remarks",""};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		progressDialog = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {

	@Override
	public void callBack(String v) {
		Log.d("返回数据", v);
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
		if (v.equals("fail")) {
			Toast.makeText(RegistStaffActivity.this, "设置失败", Toast.LENGTH_LONG).show();
		}
	}
};
}
