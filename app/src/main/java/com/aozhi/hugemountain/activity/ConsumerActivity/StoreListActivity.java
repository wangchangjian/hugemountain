package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.CityActivity;
import com.aozhi.hugemountain.activity.MapActivity;
import com.aozhi.hugemountain.adapter.MainAdapter;
import com.aozhi.hugemountain.adapter.StoreListAdapter;
import com.aozhi.hugemountain.list.XListView;
import com.aozhi.hugemountain.list.XListView.IXListViewListener;
import com.aozhi.hugemountain.model.MainListObject;
import com.aozhi.hugemountain.model.MainObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class StoreListActivity extends Activity implements OnClickListener,
		IXListViewListener {
	private Button btn_back, btn_map;
	private XListView list_store;
	private ArrayList<StoreObject> list = new ArrayList<StoreObject>();
	private ArrayList<StoreObject> listafter = new ArrayList<StoreObject>();
	private static final int RESULT_CODE1 = 102;
	private ArrayList<StoreObject> list1 = new ArrayList<StoreObject>();
	private StoreListAdapter mStoreListAdapter;
	private StoreListObject mStoreListObject;
	private String seller_id = "", search, orderid;
	private LocationMode tempMode = LocationMode.Hight_Accuracy;
	private String tempcoor = "gcj02";
	private LocationClient mLocationClient;
	private TextView tv_title, tv_address, tv_acquiesce, tv_area, tv_sort,
			tv_city, tv_news;
	private MainAdapter mMainAdapter;
	private String class_id = "";
	private String start = "";
	private boolean isRead = false;
	private int liststate = 0; // 0表示第一次进入后加载数据，1表示刷新，2表示加载更多
	private int page = 0;
	private boolean isReadclass = false;
	private int liststates = 0; // 0表示第一次进入后加载数据，1表示刷新，2表示加载更多
	private int pages = 0;
	private ArrayList<MainObject> mlist = new ArrayList<MainObject>();
	private MainListObject mMainListObject;

	private LinearLayout tv_anmo, tv_meifa, tv_yuchang, tv_ktv;
	private JumpProgressDialog jumpProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_storelist);
		// initLoc();
		initView();
		initListnner();
		tv_city.setText(MyApplication.app_current_city);
		GetMainList();
		getorderclass(tv_city.getText().toString(), 0);
	}

	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		int span = 1000;
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	private void initLoc() {
		mLocationClient = ((MyApplication) getApplication()).mLocationClient;
		((MyApplication) getApplication()).mLocationResult = tv_city;
		InitLocation();
		mLocationClient.start();
	}

	private void initView() {
		jumpProgressDialog=new JumpProgressDialog(this);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_map = (Button) findViewById(R.id.btn_map);
		list_store = (XListView) findViewById(R.id.list_store);
		list_store.setXListViewListener(this);
		list_store.setPullLoadEnable(true);
		list_store.setPullRefreshEnable(true);
		tv_title = (TextView) findViewById(R.id.tv_title);
		tv_address = (TextView) findViewById(R.id.tv_address);
		tv_anmo = (LinearLayout) findViewById(R.id.tv_anmo);
		tv_meifa = (LinearLayout) findViewById(R.id.tv_meifa);
		tv_yuchang = (LinearLayout) findViewById(R.id.tv_yuchang);
		tv_ktv = (LinearLayout) findViewById(R.id.tv_ktv);
		mMainAdapter = new MainAdapter(this, mlist);
		tv_acquiesce = (TextView) findViewById(R.id.tv_acquiesce);
		tv_area = (TextView) findViewById(R.id.tv_area);
		tv_sort = (TextView) findViewById(R.id.tv_sort);
		tv_city = (TextView) findViewById(R.id.tv_city);
		tv_news = (TextView) findViewById(R.id.tv_news);
		mStoreListAdapter = new StoreListAdapter(StoreListActivity.this, list);
		list_store.setAdapter(mStoreListAdapter);
		try {
			String ad = MyApplication.address;;
			ad = ad.replaceAll(MyApplication.app_current_city, "");
			ad = ad.substring(3);
			tv_title.setText(ad);
		}catch (Exception e){

		}
		// tv_title.setText(MyApplication.address);
	}

	private void GetMainList() {
		// TODO Auto-generated method stub
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "gethome" };
			params.add(methodParam);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			if (!result.equals("fail")) {
				mMainListObject = JSON
						.parseObject(result, MainListObject.class);
				mlist = mMainListObject.response;
				if (mMainListObject.meta.getMsg().equals("OK")) {
					if (mlist.size() > 0) {
						mMainAdapter = new MainAdapter(StoreListActivity.this,
								mlist);
					}
				}
			}
		}
	};

	private void initListnner() {
		btn_back.setOnClickListener(this);
		btn_map.setOnClickListener(this);
		tv_city.setOnClickListener(this);
		tv_anmo.setOnClickListener(this);
		tv_meifa.setOnClickListener(this);
		tv_yuchang.setOnClickListener(this);
		tv_ktv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_back:
				finish();
				break;
			case R.id.btn_map: {
				if (list.size() > 0) {
					Intent mIntent = new Intent(this, MapActivity.class);
					mIntent.putExtra("seller_id", list.get(0).id);
					mIntent.putExtra("storename", "店铺位置");
					mIntent.putExtra("latitude",
							String.valueOf(MyApplication.app_latitude));
					mIntent.putExtra("longitude",
							String.valueOf(MyApplication.app_longitude));
					mIntent.putExtra("city", tv_city.getText().toString());
					startActivity(mIntent);
				} else {
					Toast.makeText(this, "该城市下无商铺,请选择其他城市,重新导航", Toast.LENGTH_LONG)
							.show();
				}
			}
			break;
			case R.id.tv_city: {
				Intent intent = new Intent(StoreListActivity.this,
						CityActivity.class);
				startActivityForResult(intent, RESULT_CODE1);
			}
			break;
			case R.id.tv_anmo: {
				class_id = "1";
				pages = 0;
				isReadclass = false;
				getorder(tv_city.getText().toString(), 0);
			}
			break;
			case R.id.tv_meifa: {
				class_id = "4";
				pages = 0;
				isReadclass = false;
				getorder(tv_city.getText().toString(), 0);
			}
			break;
			case R.id.tv_yuchang: {
				class_id = "5";
				pages = 0;
				isReadclass = false;
				getorder(tv_city.getText().toString(), 0);
			}
			break;
			case R.id.tv_ktv: {
				class_id = "48";
				pages = 0;
				isReadclass = false;
				getorder(tv_city.getText().toString(), 0);
			}
			break;
			default:
				break;
		}
	}

	private void getorder(String city, int state) {
		if (isReadclass) {
			Toast.makeText(this,
					getResources().getString(R.string.tv_dialog_context),
					Toast.LENGTH_SHORT).show();
			return;
		}
		isReadclass = true;
		if (state == 0 || state == 1) {
			pages = 0;
			liststates = 1;
		} else {
			pages++;
			liststates = 2;
		}
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			if (MyApplication.app_current_city.equals(tv_city.getText()
					.toString())) {
				String[] methodParam = new String[] { "ak",
						"GM3NqGyZDhXqUExqGNeU8lMc" };
				String[] geotable_idParam = new String[] { "geotable_id",
						"134513" };
				String[] geo_position1 = new String[] { "region", city };
				params.add(methodParam);
				params.add(geotable_idParam);
				params.add(geo_position1);
				String[] locationParam = new String[] {
						"location",
						MyApplication.app_longitude + ","
								+ MyApplication.app_latitude };
				params.add(locationParam);
				String[] radiusss = new String[] { "radius", "1000000" };
				params.add(radiusss);
				if (start.equals("")) {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1" };
					params.add(sortbyParam);
				} else {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1|star:1" };
					params.add(sortbyParam);
				}
				if (!class_id.equals("")) {
					String[] comments = new String[] { "filter",
							"classid:" + class_id };
					params.add(comments);
				}
				String[] page_indexParam = new String[] { "page_index",
						String.valueOf(pages) };
				String[] comments1 = new String[] { "page_size", "10" };
				params.add(page_indexParam);
				params.add(comments1);
				jumpProgressDialog.showDialog();
				new HttpConnection().get(Constant.BAIDUURL, params,
						type_callbackListener);
			} else {
				String[] methodParam = new String[] { "ak",
						"GM3NqGyZDhXqUExqGNeU8lMc" };
				String[] geotable_idParam = new String[] { "geotable_id",
						"134513" };
				String[] geo_position1 = new String[] { "region", city };
				params.add(methodParam);
				params.add(geotable_idParam);
				params.add(geo_position1);
				if (start.equals("")) {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1" };
					params.add(sortbyParam);
				} else {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1|star:1" };
					params.add(sortbyParam);
				}
				if (!class_id.equals("")) {
					String[] comments = new String[] { "filter",
							"classid:" + class_id };
					params.add(comments);
				}
				String[] page_indexParam = new String[] { "page_index",
						String.valueOf(pages) };
				String[] comments1 = new String[] { "page_size", "10" };
				params.add(page_indexParam);
				params.add(comments1);
				jumpProgressDialog.showDialog();
				new HttpConnection().get(Constant.BAIDUURL1, params,
						type_callbackListener);
			}
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			jumpProgressDialog.dimissDialog();
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list.clear();
				list = mStoreListObject.contents;
				isReadclass = false;
				onLoad();
				if (list != null) {
					if (list.size() > 0) {
						if (pages == 0) {
							listafter.clear();
							listafter.addAll(list);
						}else{
							listafter.addAll(list);
						}
						tv_news.setVisibility(View.GONE);
						list_store.setVisibility(View.VISIBLE);
					} else {
						if(pages==0){
							tv_news.setVisibility(View.VISIBLE);
							list_store.setVisibility(View.GONE);}
					}
				} else {
					tv_news.setVisibility(View.VISIBLE);
					list_store.setVisibility(View.GONE);
				}
				mStoreListAdapter = new StoreListAdapter(
						StoreListActivity.this, listafter);
				list_store.setAdapter(mStoreListAdapter);
				if(list!=null){
					if(list.size()>0){
						list_store.setSelection(mStoreListAdapter.getCount()-list.size());
					}
				}
			}
		}
	};

	//	按城市区
	private void getorderclass(String city, int state) {
		if (isRead) {
			Toast.makeText(this,
					getResources().getString(R.string.tv_dialog_context),
					Toast.LENGTH_SHORT).show();
			return;
		}
		isRead = true;
		if (state == 0 || state == 1) {
			page = 0;
			liststate = 1;
		} else {
			page++;
			liststate = 2;
		}
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			// 城市相等用 nearyby
			if (MyApplication.app_current_city.equals(tv_city.getText()
					.toString())) {
				String[] methodParam = new String[] { "ak",
						"GM3NqGyZDhXqUExqGNeU8lMc" };
				String[] geotable_idParam = new String[] { "geotable_id",
						"134516" };
				String[] geo_position1 = new String[] { "region", city };
				params.add(methodParam);
				params.add(geotable_idParam);
				params.add(geo_position1);
				String[] locationParam = new String[] {
						"location",
						MyApplication.app_longitude + ","
								+ MyApplication.app_latitude };
				params.add(locationParam);
				String[] radiusss = new String[] { "radius", "1000000" };
				params.add(radiusss);
				if (start.equals("")) {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1" };
					params.add(sortbyParam);
				} else {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1|star:1" };
					params.add(sortbyParam);
				}
				String[] page_indexParam = new String[] { "page_index",
						String.valueOf(page) };
				String[] comments1 = new String[] { "page_size", "10" };
				params.add(page_indexParam);
				params.add(comments1);
				jumpProgressDialog.showDialog();
				new HttpConnection().get(Constant.BAIDUURL, params,
						getorderclass_callbackListener);
			} else {
				// 城市不相等用 local
				String[] methodParam = new String[] { "ak",
						"GM3NqGyZDhXqUExqGNeU8lMc" };
				String[] geotable_idParam = new String[] { "geotable_id",
						"134516" };
				String[] geo_position1 = new String[] { "region", city };
				params.add(methodParam);
				params.add(geotable_idParam);
				params.add(geo_position1);
				if (start.equals("")) {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1" };
					params.add(sortbyParam);
				} else {
					String[] sortbyParam = new String[] { "sortby",
							"distance:1|star:1" };
					params.add(sortbyParam);
				}
				String[] page_indexParam = new String[] { "page_index",
						String.valueOf(page) };
				String[] comments1 = new String[] { "page_size", "10" };
				params.add(page_indexParam);
				params.add(comments1);
				jumpProgressDialog.showDialog();
				new HttpConnection().get(Constant.BAIDUURL1, params,
						getorderclass_callbackListener);
			}
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getorderclass_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			jumpProgressDialog.dimissDialog();
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v, StoreListObject.class);
				list.clear();
				list = mStoreListObject.contents;
				isRead = false;
				onLoad();
				if (list != null) {
					if (list.size() > 0) {
						if (page == 0) {
							listafter.clear();
							listafter.addAll(list);
						}else{
							listafter.addAll(list);
						}
						tv_news.setVisibility(View.GONE);
						list_store.setVisibility(View.VISIBLE);
					} else {
						if(page==0){
							tv_news.setVisibility(View.VISIBLE);
							list_store.setVisibility(View.GONE);
						}
					}
				} else {
					tv_news.setVisibility(View.VISIBLE);
					list_store.setVisibility(View.GONE);
				}
				mStoreListAdapter = new StoreListAdapter(
						StoreListActivity.this, listafter);
				list_store.setAdapter(mStoreListAdapter);
				if(list!=null){
					if(list.size()>0){
						list_store.setSelection(mStoreListAdapter.getCount()-list.size());
					}
				}


			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) { // resultCode为回传的标记，我在B中回传的是RESULT_OK
			case RESULT_CODE1:
				Bundle b = data.getExtras(); // data为B中回传的Intent
				String cityname = b.getString("cityname");// str即为回传的值
				tv_city.setText(cityname);
				page = 0;
				isRead = false;
				getorderclass(cityname, 0);
				break;
			default:
				break;

		}
	}

	private static Boolean isQuit = false;
	Timer timer = new Timer();

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (isQuit == false) {
				isQuit = true;
				Toast.makeText(getBaseContext(), "再按一次返回键退出程序",
						Toast.LENGTH_SHORT).show();
				TimerTask task = null;
				task = new TimerTask() {
					@Override
					public void run() {
						isQuit = false;
					}
				};
				timer.schedule(task, 2000);
			} else {
				finish();
				MyApplication.IS_LOGINS = false;
				System.exit(0);
			}
		}
		return false;
	}


	public void onRefresh() {
		// TODO Auto-generated method stub
		if (!class_id.equals("")) {
			getorder(tv_city.getText().toString(), 1);
		} else {
			getorderclass(tv_city.getText().toString(), 1);
		}
	}

	public void onLoadMore() {
		// TODO Auto-generated method stub
		if (!class_id.equals("")) {
			getorder(tv_city.getText().toString(), 2);

		} else {
			getorderclass(tv_city.getText().toString(), 2);
		}
	}

	private void onLoad() {
		list_store.stopRefresh();
		list_store.stopLoadMore();
	}

	public void onDestory() {
		list = null;
		System.gc();
		super.onDestroy();
	}

}
