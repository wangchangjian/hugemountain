package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class StaffUserInfoActivity extends Activity implements OnClickListener {

	private Button btn_back;
	private ImageView user_img;
	private TextView user_code, user_login, user_name, user_sex, user_age,
			user_storename, user_service, user_type, user_comm, user_phone;
	private TextView user_edit;
	private ProgressDialog progressDialog = null;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LinearLayout collect;
	private TextView jd_count, sc_count, pj_count,tv_remark,sc_counts;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_useraccount);
		initView();
		initListener();
		getStaffUser();
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		user_edit = (TextView) findViewById(R.id.user_edit);
		user_img = (ImageView) findViewById(R.id.user_img);
		user_code = (TextView) findViewById(R.id.user_code);
		user_login = (TextView) findViewById(R.id.user_login);
		user_name = (TextView) findViewById(R.id.user_name);
		user_sex = (TextView) findViewById(R.id.user_sex);
		user_age = (TextView) findViewById(R.id.user_age);
		user_phone = (TextView) findViewById(R.id.user_phone);
		collect = (LinearLayout) findViewById(R.id.collect);
		jd_count = (TextView) findViewById(R.id.jd_count);
		sc_count = (TextView) findViewById(R.id.sc_count);
		sc_counts = (TextView) findViewById(R.id.sc_counts);
		pj_count = (TextView) findViewById(R.id.pj_count);
		tv_remark=(TextView)findViewById(R.id.tv_remark);
	}

	private void initListener() {
		btn_back.setOnClickListener(this);
		user_edit.setOnClickListener(this);
		collect.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.user_edit:
			Intent mIntent = new Intent(StaffUserInfoActivity.this,
					StaffUserEditActivity.class);
			mIntent.putExtra("userlist", list);
			startActivityForResult(mIntent, 300);
			break;
		case R.id.collect:
			Intent mintent = new Intent(StaffUserInfoActivity.this,
					EvaListActivity.class);
			mintent.putExtra("staff_id", MyApplication.Staffuser.id);
			startActivity(mintent);
			break;
		default:
			break;
		}

	}

	// 获取用户信息并设置显示
	private void getStaffUser() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			// fun=getAddressList&userid=32772
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getStaffUserInfo" };
			String[] pageParam = new String[] { "login_id",
					MyApplication.Staffuser.getStaffphoto() };
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					getGoodsList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接!", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getGoodsList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mLoginListObject = JSON.parseObject(result,
						LoginListObject.class);
				list = mLoginListObject.response;
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						setView();
					} else {
						Toast.makeText(StaffUserInfoActivity.this, "获取用户信息失败",
								Toast.LENGTH_SHORT).show();
						finish();
					}
				}
			} else {
				Toast.makeText(StaffUserInfoActivity.this, "获取用户信息失败",
						Toast.LENGTH_SHORT).show();
				finish();
			}
		}
	};

	private void setView() {
		user_code.setText("编号" + list.get(0).code_id);
		user_login.setText(list.get(0).staffphoto);
		user_name.setText(list.get(0).name);
		user_sex.setText(list.get(0).sex);
		user_age.setText(list.get(0).age);
		user_phone.setText(list.get(0).phone);
		jd_count.setText(list.get(0).num1);
		tv_remark.setText(list.get(0).remarks);
		sc_count.setText(list.get(0).num2);
		sc_counts.setText(list.get(0).num3);
		if (list.get(0).avatar.equals("") || list.get(0).avatar == null) {

		} else {
			MyApplication.downloadImage.addTasks(list.get(0).avatar, user_img,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								user_img.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 300:
			getStaffUser();
			break;
		default:
			break;
		}
	}
}
