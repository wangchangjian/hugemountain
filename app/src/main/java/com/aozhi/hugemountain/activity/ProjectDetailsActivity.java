package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class ProjectDetailsActivity extends Activity {
	Button btn_back;
	private ProjectObject mProjectObject;
	private TextView tv_name, tv_content, tv_money,count;
	private Button clearcount;
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private ViewPager vp_mainadv;
	private int currentItem = 0;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> imglist = new ArrayList<ImageObject>();
	private DownloadImage downloadImage = new DownloadImage();
	private String images = "";
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
	private TextView project_edit,tv_time;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_projectdetails);
		tv_name = (TextView) findViewById(R.id.tv_name);
		tv_content = (TextView) findViewById(R.id.tv_content);
		tv_money = (TextView) findViewById(R.id.tv_money);
		btn_back = (Button) findViewById(R.id.btn_back);
		count=(TextView) findViewById(R.id.count);
		clearcount = (Button) findViewById(R.id.clearcount);
		tv_time=(TextView) findViewById(R.id.tv_time);
		project_edit=(TextView) findViewById(R.id.project_edit);
		project_edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent=new Intent(ProjectDetailsActivity.this,ProjectEditActivity.class);
				mIntent.putExtra("list", mProjectObject);
				startActivityForResult(mIntent, 336);
			}
		});
		clearcount.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ClearCount();
				
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		mProjectObject = (ProjectObject) getIntent().getSerializableExtra(
				"mProjectObject");
		tv_name.setText(mProjectObject.name);
		tv_content.setText(mProjectObject.remark);
		tv_money.setText("￥" + mProjectObject.money);
		tv_time.setText(mProjectObject.service_time+"分钟");
		count.setText(mProjectObject.click_count+"次");
//		if (mProjectObject.photo.equals("") || mProjectObject.photo == null) {
//			img_logo.setBackgroundResource(R.drawable.img_add);
//		} else {
//			MyApplication.downloadImage.addTasks(mProjectObject.photo,
//					img_logo, new DownloadImage.ImageCallback() {
//
//						@Override
//						public void imageLoaded(Bitmap imageBitmap,
//								String imageUrl) {
//							if (imageBitmap != null) {
//								Drawable drawable = new BitmapDrawable(
//										imageBitmap);
//								img_logo.setBackgroundDrawable(drawable);
//							}
//						}
//
//						@Override
//						public void imageLoaded(Bitmap imageBitmap,
//								DownloadImageMode callBackTag) {
//							// TODO 自动生成的方法存根
//							if (imageBitmap != null) {
//								Drawable drawable = new BitmapDrawable(
//										imageBitmap);
//								img_logo.setBackgroundDrawable(drawable);
//							}
//						}
//					});
//		}
//		MyApplication.downloadImage.doTask();
		getPhoto();
	}

	private void ClearCount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upservicecount" };
		String[] idParam2 = new String[] { "ser_id",mProjectObject.store_service_id};
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				list = mProjectListObject.response;
				if (mProjectListObject.meta.getMsg().equals("OK")) {
					Intent mIntent=new Intent(ProjectDetailsActivity.this,ProjectActivity.class);
					setResult(152, mIntent);
					finish();
					Toast.makeText(ProjectDetailsActivity.this, "清零成功",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(ProjectDetailsActivity.this, "清零失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};
	
	
	//服务图片轮播
	String serversImageString = "";
	private void getPhoto() {
		String[]photos = mProjectObject.avatar.split(",");
		if (photos.length > 0) {
			int n = 0;
			for (n = 0; n < photos.length; n++) {
				final ImageView imgView = new ImageView(
						ProjectDetailsActivity.this);
				imgView.setClickable(true);
				imgView.setScaleType(ScaleType.FIT_XY);
				downloadImage.addTasks(photos[n], imgView,
						new DownloadImage.ImageCallback() {
							public void imageLoaded(Bitmap imageBitmap,
									String imageUrl) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									imgView.setImageBitmap(imageBitmap);
									imgView.setTag(imageUrl);
								}
							}

							public void imageLoaded(Bitmap imageBitmap,
									DownloadImageMode callBackTag) {
								// TODO Auto-generated method stub
								if (imageBitmap != null) {
									imgView.setImageBitmap(imageBitmap);
									imgView.setTag("");
								}
							}
						});
				downloadImage.doTask();
				final String url = photos[n];
				final String title = photos[n];

				if (serversImageString.equals("")) {
					serversImageString += url;
				} else {
					serversImageString += "," + url;
				}

				imgView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent mIntent = new Intent(
								ProjectDetailsActivity.this,
								ImagePagerActivity.class);
						mIntent.putExtra("images", serversImageString);
						startActivity(mIntent);
					}
				});
				imgViews.add(imgView);

			}
			vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
			vp_mainadv.setAdapter(new MyAdapter());
			// 设置监听，当ViewPager中的页面改变时调用
			vp_mainadv
					.setOnPageChangeListener(new MyPageChangeListener());
		}
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面，16项目详情
		
		/*Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getProjectAD" };
			String[] paramName=new String[] {"service_id",mProjectObject.id};
			String[] paramName1=new String[]{"store_id",MyApplication.Storeuser.id};
			params.add(methodParam);
			params.add(paramName);
			params.add(paramName1);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
		*/
	}

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			String []strings=serversImageString.split(",");
			int count=strings.length;
			return count;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}
	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onPageSelected(int arg0) {
			//Animation animation = null;
			//animation = new TranslateAnimation(context, attrs)
//			 for(int i=0;i<imglist.size();i++){
//			 if(i!=arg0){
//				 imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_normal);
//			 }else{
//				 imglist.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_focused);
//			 }
//			 }
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 336:
			Intent mIntent=new Intent(this,ProjectActivity.class);
			setResult(152, mIntent);
			finish();
			break;

		default:
			break;
		}
		
	}
}
