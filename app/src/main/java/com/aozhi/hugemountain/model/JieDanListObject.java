package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class JieDanListObject {
	public Meta meta;
	public ArrayList<JiaDanBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<JiaDanBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<JiaDanBean> response) {
		this.response = response;
	}
}
