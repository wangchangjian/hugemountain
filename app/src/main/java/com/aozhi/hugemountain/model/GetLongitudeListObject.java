package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class GetLongitudeListObject {

	public Meta meta;
	public ArrayList<GetLongitudeObject> response;

	public Meta getMeta() {
		return meta;
	}

	public void setMeta(Meta meta) {
		this.meta = meta;
	}

	public ArrayList<GetLongitudeObject> getResponse() {
		return response;
	}

	public void setResponse(ArrayList<GetLongitudeObject> response) {
		this.response = response;
	}
}
