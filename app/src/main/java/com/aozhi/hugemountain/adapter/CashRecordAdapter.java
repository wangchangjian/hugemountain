package com.aozhi.hugemountain.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;



import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.CashRecordObject;

public class CashRecordAdapter extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<CashRecordObject> list = new ArrayList<CashRecordObject>();

	public CashRecordAdapter(Context ctx, ArrayList<CashRecordObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_tx_record, null);
			mHolder.time=(TextView)convertView.findViewById(R.id.time);
			mHolder.money=(TextView)convertView.findViewById(R.id.money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.time.setText(list.get(position).create_time);
		mHolder.money.setText(list.get(position).money);
//		if (list.get(position).avatar.equals("")
//				|| list.get(position).avatar==null ) {
//			
//		} else {
//			MyApplication.downloadImage.addTask(
//					list.get(position).avatar, mHolder.img,
//					new DownloadImage.ImageCallback() {
//
//						@Override
//						public void imageLoaded(Bitmap imageBitmap,
//								String imageUrl) {
//							if (imageBitmap != null) {
//								// xHolder.tx_image.setImageBitmap(imageBitmap);
//								// xHolder.tx_image.setTag("");
//								Drawable drawable = new BitmapDrawable(
//										imageBitmap);
//								mHolder.img.setBackgroundDrawable(drawable);
//							}
//						}
//
//						@Override
//						public void imageLoaded(Bitmap imageBitmap,
//								DownloadImageMode callBackTag) {
//							// TODO 自动生成的方法存根
//							if (imageBitmap != null) {
//								// xHolder.tx_image.setImageBitmap(imageBitmap);
//								// xHolder.tx_image.setTag("");
//								Drawable drawable = new BitmapDrawable(
//										imageBitmap);
//								mHolder.img.setBackgroundDrawable(drawable);
//							}
//						}
//
//					});
//		}
//		MyApplication.downloadImage.doTask();
		return convertView;
	}
	
	
	
	class Holder {
		TextView time,money;
	}
}
