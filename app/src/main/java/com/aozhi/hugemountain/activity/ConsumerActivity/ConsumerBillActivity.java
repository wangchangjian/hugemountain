package com.aozhi.hugemountain.activity.ConsumerActivity;


import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ConsumerbillList2Adapter;
import com.aozhi.hugemountain.adapter.ConsumerbillListAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ConsumerBillActivity extends Activity {
	Button btn_back;

	TextView balance;
	LinearLayout item1;
	private ProgressDialog progressDialog1 = null;
	ArrayList<ConsumptionObject> list_consumption=new ArrayList<ConsumptionObject>();
	ConsumptionListObject mConsumerListObject;
	ConsumptionObject mConsumptionObject;
	ConsumerbillListAdapter adapter1;
	ConsumerbillList2Adapter adapter2;
	private String client_id="";
	private ListView list_1;
//	private ListView list_2;
	String Status= MyApplication.Status;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_consumerbill);
		initView();
//		getconsumer("getUserBalanceList");
		getconsumer("getUserConsumptionList");
		myonclick();
//		getUserBalanceList
//		getUserConsumptionList
		//getrecharge();
	}
	private void initView() {
		item1=(LinearLayout)findViewById(R.id.item1);
		balance=(TextView)findViewById(R.id.balance);
		balance.setText(MyApplication.Clientuser.balance+"元");
		list_1=(ListView) findViewById(R.id.list_1);
//		list_2=(ListView) findViewById(R.id.list_2);
		adapter1=new ConsumerbillListAdapter(ConsumerBillActivity.this, list_consumption);
//		adapter2=new ConsumerbillList2Adapter(ConsumerBillActivity.this, list_consumption);
		list_1.setAdapter(adapter1);
//		list_2.setAdapter(adapter2);
		client_id=MyApplication.Clientuser.id;
	}
	
	private void getconsumer_staff() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffconsumption" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	private void getconsumer(String fun) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", fun };
		String[] name1 = new String[] { "id", client_id };
		params2.add(funParam2);
		params2.add(name1);
		progressDialog1 = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog1.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}
	
	private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (progressDialog1 != null) {
				progressDialog1.dismiss();
				progressDialog1 = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumerListObject = JSON.parseObject(v, ConsumptionListObject.class);
				list_consumption = mConsumerListObject.response;
					if (mConsumerListObject.meta.getMsg().equals("OK")) {
						if (list_consumption.size() > 0) {
							adapter1 = new ConsumerbillListAdapter(ConsumerBillActivity.this, list_consumption);
							list_1.setAdapter(adapter1);
//							adapter2 = new ConsumerbillList2Adapter(ConsumerBillActivity.this, list_consumption);
//							list_2.setAdapter(adapter2);
						} 

					} 
				} 
		}
	};
	private void myonclick() {
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

}






