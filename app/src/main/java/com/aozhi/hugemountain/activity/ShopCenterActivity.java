package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.StaffAccountActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ShopCenterActivity extends Activity {

	Button btn_back;
	View revenue;
	View expenditure;
	private RelativeLayout tx_record,nethuizong,tixian,account;
	LinearLayout store;
	private TextView store_name,daytotal,monthtotal;
	private ImageView store_img;
	private DownloadImage downloadImage = new DownloadImage();
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private StoreListObject mStoreListObject;
	private ArrayList<StoreObject> slist=new ArrayList<StoreObject>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_shopcenter);
		store_img=(ImageView) findViewById(R.id.store_img);
		store_name=(TextView) findViewById(R.id.store_name);
		daytotal=(TextView) findViewById(R.id.daytotal);
		monthtotal=(TextView) findViewById(R.id.monthtotal);
		store=(LinearLayout) findViewById(R.id.store);
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		revenue = (View)findViewById(R.id.revenue);
		revenue.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ShopCenterActivity.this, RevenueInquiryActivity.class);
				startActivity(intent);
			}
		});
		
		expenditure = (View)findViewById(R.id.expenditure);
		expenditure.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ShopCenterActivity.this, ExpenditureInquiryActivity.class);
				startActivity(intent);
			}
		});
		store.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent=new Intent(ShopCenterActivity.this,StoreInfoActivity.class);
				startActivity(mIntent);
			}
		});
		tixian=(RelativeLayout) findViewById(R.id.tixian);
		tixian.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mintent=new Intent(ShopCenterActivity.this,CashRecordActivity.class);
				startActivity(mintent);				
			}
		});
		account=(RelativeLayout) findViewById(R.id.account);
		account.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent mintent=new Intent(ShopCenterActivity.this,StaffAccountActivity.class);
				mintent.putExtra("stats","store");
				startActivity(mintent);
			}
		});
		tx_record=(RelativeLayout) findViewById(R.id.tx_record);
		tx_record.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent mintent=new Intent(ShopCenterActivity.this,TixianRecordActivity.class);
				mintent.putExtra("status", "store");
				startActivity(mintent);
			}
		});
	}


	private void getProjectList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreTatalprice" };
		String[] idParam2 = new String[] { "store_id", MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				list = mProjectListObject.response;
				if (list.size() > 0) {
					daytotal.setText(list.get(0).daymoney+"元");
					monthtotal.setText(list.get(0).monthmoney+"元");
					getStoreInfo();
				}

			} else {
				Toast.makeText(ShopCenterActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	private void setImg(String path){
		if (path.equals("") || path == null) {
			store_img.setBackgroundResource(R.drawable.img_add);
		} else {
			MyApplication.downloadImage.addTasks(path,
					store_img, new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}
					});
		}
		MyApplication.downloadImage.doTask();
		
	}
	
	private void getStoreInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstore" };
		String[] idParam2 = new String[] { "store_id",MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v,
						StoreListObject.class);
				slist = mStoreListObject.response;
				if(mStoreListObject.meta.getMsg().equals("OK")){
					if (slist.size() > 0) {
						store_name.setText("名称　"+slist.get(0).name);
						setImg(slist.get(0).phone);
					}
				}
			} else {
				Toast.makeText(ShopCenterActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
}
