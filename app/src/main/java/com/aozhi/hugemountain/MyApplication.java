package com.aozhi.hugemountain;

import com.aozhi.hugemountain.http.DownloadImage;
import java.util.ArrayList;
import java.util.List;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.RoomObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.GeofenceClient;
import com.baidu.location.LocationClient;
import com.baidu.mapapi.SDKInitializer;
import android.app.Application;
import android.graphics.Bitmap;
import android.os.Vibrator;
import android.util.Log;
import android.widget.TextView;

public class MyApplication extends Application {

	public TextView tv_title;
	public static Boolean ifreg = false;
	public static MyApplication instance;
	private static final String TAG = "JPush";
	public int curPage = 1;
	public static boolean IS_LOGIN = false;// �Ƿ��¼
	public static boolean isbutton = false;
	public static String begintime = "";
	public static boolean isstaff = false;
	public static String endtime = "";
	public String code, name, username, c_id;
	public String pwd, brithryday, constellation, age, carinfo;
	public String adress, adress1, adress2, adress3, goods, other, serverFile;
	public String uyanzheng, userid, face, g_id, x_id;
	public static String num = "";
	public String isadress = "0";
	public static LoginBean user = new LoginBean();
	public static int iscourse = 0;
	// public static long accountId = -1 ;
	public static int screenx = -1;
	public static Bitmap userface;
	public String bg, id;
	public String qianming;
	public String time;
	
	public static String year="";
	public static String intermission = "";
	public static boolean IS_LOGINS = false;
	
	public static boolean search = false;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public static String fuqu_type = "";
	public List<String> list = new ArrayList<String>();;
	public static boolean ismainOpen = false;
	public static boolean isChatActive = false;
	public static String chatActiveTo = "";
	public static boolean isSameUser = true;
	public static boolean isUpdateData = false;
	public static boolean iscome = false;
	public static String preUsername = "";
	public static int unreadcount = 0;
	public static DownloadImage downloadImage = new DownloadImage();
	private String mData;
	public int i = 1, j = 1;
	public Vibrator mVibrator01;
	public static double longitude;
	public static double latitude;
	public static String app_current_city = "";
	public static String app_current_location = null;

	public static double app_longitude1;
	public static double app_latitude1;
	public static String app_current_location1 = null;
	public static int shanc = 0;

	// 百度地图定位功能
	public LocationClient mLocationClient;
	public GeofenceClient mGeofenceClient;
	public MyLocationListener mMyLocationListener;
	public TextView mLocationResult;
	public static double app_longitude = 0l;
	public static double app_latitude = 0l;
	public static String address;
	public String[] member = { "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
			"", "" };
	public int position = 0;

	public static String bank_name = "";
	public static String ka_name = "";
	public static String ka_xingming = "";
	public static int jiedan_qing = 0;

	public static synchronized MyApplication getInstance() {
		return instance;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "[ExampleApplication] onCreate");
		super.onCreate();
		SDKInitializer.initialize(this);
		mLocationClient = new LocationClient(this);
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);
		instance=this;

	}

	public void logMsg(String str) {
		try {
			mData = str;
			Log.e("loc sdk", mData);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String msgreg_id = "";
	public static int pingjia = 0;
	public static LoginBean Clientuser = new LoginBean();
	public static LoginBean Storeuser = new LoginBean();
	public static LoginBean Staffuser = new LoginBean();;
	public static String seller_id;
	public static ArrayList<ServiceObject> list_business_service = new ArrayList<ServiceObject>();
	public static ArrayList<StaffObject> list_business_staff = new ArrayList<StaffObject>();
	public static String service_time;
	public static StoreObject this_store;
	public static String store_id;
	public static String client_storeName;
	public static String Status = "";
	protected static RoomObject mRoomObject = new RoomObject();
	protected static String StaffConsumer;

	/**
	 * 实现实位回调监听
	 */
	public class MyLocationListener implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// Receive Location
			StringBuffer sb = new StringBuffer(256);
			sb.append("time : ");
			sb.append(location.getTime());
			sb.append("\nerror code : ");
			sb.append(location.getLocType());
			sb.append("\nlatitude : ");
			sb.append(location.getLatitude());
			app_latitude = location.getLatitude();
			sb.append("\nlontitude : ");
			sb.append(location.getLongitude());
			app_longitude = location.getLongitude();
			sb.append("\nradius : ");
			app_current_city = location.getCity();
			sb.append(location.getRadius());
			if (location.getLocType() == BDLocation.TypeGpsLocation) {
				sb.append("\nspeed : ");
				sb.append(location.getSpeed());
				sb.append("\nsatellite : ");
				sb.append(location.getSatelliteNumber());
				sb.append("\ndirection : ");
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
				address = location.getAddrStr();
				sb.append(location.getDirection());
			} else if (location.getLocType() == BDLocation.TypeNetWorkLocation) {
				sb.append("\naddr : ");
				sb.append(location.getAddrStr());
				address = location.getAddrStr();
				// 运营商信息
				sb.append("\noperationers : ");
				sb.append(location.getOperators());
			}
			if (mLocationResult != null)
				mLocationResult.setText(location.getCity());
			app_latitude = location.getLatitude();
			app_longitude = location.getLongitude();
			if(location.getCity()!=null)
			app_current_city = location.getCity();
			Log.i("BaiduLocationApiDem",sb.toString());
		}
	}
}
