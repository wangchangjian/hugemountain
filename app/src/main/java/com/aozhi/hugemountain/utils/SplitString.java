package com.aozhi.hugemountain.utils;


public class SplitString {
	
	public static String[] split(String str, String regex) {
		String newStr[] = new String[str.length()];
		String temp = null;
		String[] result = null;
		int start = 0, end = 0;
		int index = 0;
		for (int i = 0; i < str.length(); i++) {
			temp = str.substring(i, i + 1);
			if (temp.equals(regex)) {
				temp = null;
				end = i;
				newStr[index] = str.substring(start, end); 
				index++; 
				start = end + 1; 
			}
		}

//		if (str.lastIndexOf(regex) != str.length() && index < newStr.length) {
//			newStr[index] = str.substring(start, str.length());
//			index++;
//		}
		if (str.lastIndexOf(regex) != str.length()) {
			newStr[index] = str.substring(start, str.length());
			index++;
		}
		result = new String[index];
		System.arraycopy(newStr, 0, result, 0, index);
		return result;
	}
}