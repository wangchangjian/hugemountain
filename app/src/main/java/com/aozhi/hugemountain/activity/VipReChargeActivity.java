package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ReChargeMoneytAdapter;
import com.aozhi.hugemountain.model.VipCenterListObject;
import com.aozhi.hugemountain.model.VipCenterObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class VipReChargeActivity extends Activity implements OnClickListener{

	private TextView count,total,vipcash,nodata,chongzhi;
	private ImageView img1;
	private ListView list_viprecharge;
	private String id="";
	private String balance;
	private String login_id="";
	private ProgressDialog progressDialog = null;
	private VipCenterListObject mVipCenterListObject;
	private ArrayList<VipCenterObject> list = new ArrayList<VipCenterObject>();
	private ReChargeMoneytAdapter adapter;	
	private LinearLayout li_del;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_viprecharge);
		initView();
		initListener();
		getVIPList();
	}

	private void initListener() {
		img1.setOnClickListener(this);
		chongzhi.setOnClickListener(this);
		li_del.setOnClickListener(this);
	}

	private void initView() {
		id=getIntent().getStringExtra("id");
		login_id=getIntent().getStringExtra("login_id");
		balance=getIntent().getStringExtra("balance");
		count=(TextView) findViewById(R.id.count);
		total=(TextView) findViewById(R.id.total);
		vipcash=(TextView) findViewById(R.id.vipcash);
		nodata=(TextView) findViewById(R.id.nodata);
		img1=(ImageView) findViewById(R.id.img1);
		chongzhi=(TextView) findViewById(R.id.chongzhi);
		list_viprecharge=(ListView) findViewById(R.id.list_viprecharge);
		li_del=(LinearLayout) findViewById(R.id.li_del);
		vipcash.setText(balance);
	}
	
	private void getVIPList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getVipRecharge" };
		String[] nameParam1 = new String[] { "client_id",id};
		String[] nameParam2 = new String[] {"store_id", MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						list_viprecharge.setVisibility(View.VISIBLE);
						nodata.setVisibility(View.GONE);
						adapter = new ReChargeMoneytAdapter(getApplicationContext(),
								list);
						list_viprecharge.setAdapter(adapter);
						Double sum=0.00;
						for(int i=0;i<list.size();i++){
							sum=Double.parseDouble(list.get(i).money)+sum;
						}
						total.setText(String.valueOf(sum));
						count.setText(String.valueOf(list.size()));
					}else{
						total.setText("0");
						count.setText("0");
						nodata.setVisibility(View.VISIBLE);
						list_viprecharge.setVisibility(View.GONE);
					}
				}
			} else {
				total.setText("0");
				count.setText("0");
				nodata.setVisibility(View.VISIBLE);
				list_viprecharge.setVisibility(View.GONE);
				Toast.makeText(getApplicationContext(), "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			setResult(61);
			finish();
			break;
		case R.id.chongzhi:
			Intent intent=new Intent(getApplicationContext(),RechargeMoneyActivity.class);
			intent.putExtra("login_id", login_id);
			intent.putExtra("balance", balance);
			startActivityForResult(intent,203);
			break;
		case R.id.li_del:
			if(Double.valueOf(balance)>0.0){
				Toast.makeText(this, "该会员有账户余额，不可以删除", Toast.LENGTH_LONG).show();
			}else{
				delClientVip(id);
			}
			break;
		default:
			break;
		}
		
	}
	
	private void delClientVip(String id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delClientVip" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "client_id", id};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
//				getStaffList("1");
				Toast.makeText(VipReChargeActivity.this, "操作成功", Toast.LENGTH_LONG).show();
				setResult(61);
				finish();
			}
		}
	};
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 203:
			getVIPList();
			break;

		default:
			break;
		}
	}
}
