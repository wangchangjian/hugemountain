package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.VipCenterListObject;
import com.aozhi.hugemountain.model.VipCenterObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class VipAddActivity extends Activity implements OnClickListener {

	private TextView gone, check;
	private ImageView img1;
	private RadioGroup sex;
	private RadioButton sex1, sex2;
	private Spinner sp_type;
	private EditText username, addage;
	private ProgressDialog progressDialog = null;
	private VipCenterListObject mVipCenterListObject;
	private VipCenterListObject mVipCenterListObject1;
	private String client_id="";
	private String level_id="";
	private ArrayList<VipCenterObject> list = new ArrayList<VipCenterObject>();
	private ArrayList<VipCenterObject> list1 = new ArrayList<VipCenterObject>();
	private ArrayList<VipCenterObject> getlist = new ArrayList<VipCenterObject>();
	private RadioButton rb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vipadd);
		initView();
		initListener();
	}

	private void initListener() {
		gone.setOnClickListener(this);
		img1.setOnClickListener(this);
		check.setOnClickListener(this);
		sex.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				// 获取变更后的选中项的ID
				int radioButtonId = group.getCheckedRadioButtonId();
				// 根据ID获取RadioButton的实例
				rb = (RadioButton) VipAddActivity.this
						.findViewById(radioButtonId);
			}
		});
		sp_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
				
			}
		});
	}

	private void initView() {
		gone = (TextView) findViewById(R.id.gone);
		img1 = (ImageView) findViewById(R.id.img1);
		sex = (RadioGroup) findViewById(R.id.sex);
		sex1=(RadioButton) findViewById(R.id.sex1);
		sex2=(RadioButton) findViewById(R.id.sex2);
		addage = (EditText) findViewById(R.id.addage);
		sp_type = (Spinner) findViewById(R.id.sp_type);
		username = (EditText) findViewById(R.id.username);
		check = (TextView) findViewById(R.id.check);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.gone:
			if(username.equals("")){
				Toast.makeText(getApplicationContext(), "您输入账号有误，请检查重新输入", Toast.LENGTH_SHORT).show();
			}else{
				getVIPinfo();
			}
			break;
		case R.id.img1:
			setResult(61);
			finish();
			break;
		case R.id.check:
			getStaffinfo(username.getText().toString());
			break;
		default:
			break;
		}

	}

	private void getStaffLevel() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getClientLevel" };
		params2.add(funParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list1 = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					ArrayList<String> sp_list = new ArrayList<String>();
					if (list1.size() > 0) {
						for (int i = 0; i < list1.size(); i++) {
							sp_list.add(list1.get(i).namelevel);
						}
						ArrayAdapter<String> sp_adapter = new ArrayAdapter<String>(
								VipAddActivity.this, R.layout.item_type,
								sp_list);
						sp_type.setAdapter(sp_adapter);
					}
				}
			} else {
				Toast.makeText(VipAddActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void getStaffinfo(String client_login_id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getVipNotAdd" };
		String[] nameParam1 = new String[] { "photo", client_login_id };
		String[] nameParam2 = new String[] {"store_id", MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					StaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener StaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						addage.setText(list.get(0).age);
						client_id=list.get(0).id;
						if(list.get(0).sex.equals("男")){
							sex1.setChecked(true);
							sex2.setChecked(false);
						}else if(list.get(0).sex.equals("女")){
							sex1.setChecked(false);
							sex2.setChecked(true);
						}else{
							sex1.setChecked(false);
							sex2.setChecked(true);
						}
						
					} else {
						username.setText("");
						Toast.makeText(getApplicationContext(),
								"你输入的账号错误，请检查后再来添加", Toast.LENGTH_SHORT)
								.show();
						// finish();
					}
				}
			} else {
				Toast.makeText(VipAddActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void AddStaffVip() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upVipCenterUser" };
		String[] nameParam1 = new String[] { "client_id",client_id };
//		String[] nameParam2 = new String[] { "level_id",level_id};
		String[] nameParam3 = new String[] { "type", "0" };
		String[] nameParam4 = new String[] {"store_id",MyApplication.Storeuser.id};
		String[] nameParam5 =new String[] {"sex",rb.getText().toString()};
		String[] nameParam6 =new String[] {"age",addage.getText().toString()};
		String[] nameParam7 =new String[] {"photo",username.getText().toString()};
		params2.add(funParam2);
		params2.add(nameParam1);
//		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					upStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener upStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(VipAddActivity.this, "增加会员成功",
							Toast.LENGTH_SHORT).show();
					Intent mIntent = new Intent(VipAddActivity.this,
							VipCenterActivity.class);
					setResult(61, mIntent);
					finish();
				}
			} else {
				Toast.makeText(VipAddActivity.this, "增加会员失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	private void getVIPinfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getVipinfo" };
		String[] nameParam1 = new String[] { "name",username.getText().toString()};
		String[] nameParam3 = new String[] {"level",""};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					vip_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener vip_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mVipCenterListObject1 = JSON.parseObject(v,
						VipCenterListObject.class);
				getlist= mVipCenterListObject1.response;
				if (mVipCenterListObject1.meta.getMsg().equals("OK")) {
					if (getlist.size() > 0) {
						AddStaffVip1();
					} else {
						AddStaffVip();
					}
				}
			} else {
				Toast.makeText(VipAddActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	
	private void AddStaffVip1() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upVipCenterUser1" };
		String[] nameParam1 = new String[] { "client_id",client_id };
//		String[] nameParam2 = new String[] { "level_id",level_id};
		String[] nameParam3 = new String[] { "type", "0" };
		String[] nameParam4 = new String[] {"store_id",MyApplication.Storeuser.id};
		String[] nameParam5 =new String[] {"sex",rb.getText().toString()};
		String[] nameParam6 =new String[] {"age",addage.getText().toString()};
		String[] nameParam7 =new String[] {"photo",username.getText().toString()};
		params2.add(funParam2);
		params2.add(nameParam1);
//		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		params2.add(nameParam6);
		params2.add(nameParam7);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					upStaffList_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener upStaffList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(VipAddActivity.this, "增加会员成功",
							Toast.LENGTH_SHORT).show();
					Intent mIntent = new Intent(VipAddActivity.this,
							VipCenterActivity.class);
					setResult(61, mIntent);
					finish();
				}
			} else {
				Toast.makeText(VipAddActivity.this, "增加会员失败", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
}
