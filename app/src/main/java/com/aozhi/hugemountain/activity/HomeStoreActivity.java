package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.RoomManagementActivity;
import com.aozhi.hugemountain.activity.PublicActivity.LoginsActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HomeStoreActivity extends Activity {
	RelativeLayout project;
	RelativeLayout managementstaff;
	RelativeLayout roommanagement;
	RelativeLayout shopcenter;
	RelativeLayout clearing;
	RelativeLayout orderform,storevip,staffpk;
	LinearLayout store;
	private TextView tv_quitDialog;
	private ImageView store_img,img_avatar;
	private TextView store_name,daytotal,monthtotal;
	private DownloadImage downloadImage = new DownloadImage();
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private StoreListObject mStoreListObject;
	private ArrayList<StoreObject> slist=new ArrayList<StoreObject>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_homestore);
		store_img=(ImageView) findViewById(R.id.store_img);
		store_name=(TextView) findViewById(R.id.store_name);
		daytotal=(TextView) findViewById(R.id.daytotal);
		monthtotal=(TextView) findViewById(R.id.monthtotal);
		store=(LinearLayout) findViewById(R.id.store);
//		if(MyApplication.msgreg_id.equals("")&&MyApplication.Status.equals("store")){
//			setMsgRegId();
//		}
		img_avatar=(ImageView)findViewById(R.id.img_avatar);
		img_avatar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(HomeStoreActivity.this, StorePicActivity.class);
				startActivity(intent);
			}
		});
		project = (RelativeLayout)findViewById(R.id.project);
		project.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(HomeStoreActivity.this, ProjectActivity.class);
				startActivity(intent);
			}
		});
		managementstaff = (RelativeLayout)findViewById(R.id.managementstaff);
		managementstaff.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(HomeStoreActivity.this, ManagementStaffActivity.class);
				startActivity(intent);
			}
		});
		roommanagement = (RelativeLayout)findViewById(R.id.roommanagement);
		roommanagement.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(HomeStoreActivity.this, RoomManagementActivity.class);
				startActivity(intent);
			}
		});
		shopcenter = (RelativeLayout)findViewById(R.id.shopcenter);
		shopcenter.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(HomeStoreActivity.this, ShopCenterActivity.class);
				startActivity(intent);
			}
		});
		orderform = (RelativeLayout)findViewById(R.id.orderform);
		orderform.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(HomeStoreActivity.this, StaffOrderActivity.class);
				startActivity(intent);
			}
		});
		storevip=(RelativeLayout) findViewById(R.id.storevip);
		storevip.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HomeStoreActivity.this, VipCenterActivity.class);
				startActivity(intent);				
			}
		});
		staffpk=(RelativeLayout) findViewById(R.id.staffpk);
		staffpk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(HomeStoreActivity.this, PKHubActivity.class);
				startActivity(intent);		
				
			}
		});
		tv_quitDialog = (TextView)findViewById(R.id.tv_quitDialog);
		tv_quitDialog.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				quitDialog();
			}
		});
		store.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent mIntent=new Intent(HomeStoreActivity.this,StoreInfoActivity.class);
				startActivityForResult(mIntent,964);
			}
		});
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getProjectList();
	}
	protected void quitDialog() {    //确认退出系统
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(getString(R.string.dialog_quit_prompt));
		builder.setTitle(getString(R.string.dialog_prompt));
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						SharedPreferences mySharedPreferences = getSharedPreferences(
								"store_user", Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = mySharedPreferences
								.edit();
						editor.putString("name", "");
						editor.putString("pwd", "");
						editor.putBoolean("ok", false);
						editor.commit();
						
						JPushInterface.setAlias(getApplicationContext(),
								"", mTagsCallback);
//						JPushInterface.stopPush(getApplicationContext());
						
						Intent mIntent = new Intent(HomeStoreActivity.this,LoginsActivity.class);
						startActivity(mIntent);
						finish();
					}
				});
		builder.setNegativeButton(getString(R.string.btn_cancel),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}

	private final TagAliasCallback mTagsCallback = new TagAliasCallback() {

        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs ;
            switch (code) {
            case 0:
                logs = "Set tag and alias success";
//                Log.i(TAG, logs);
                break;
                
            case 6002:
                logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
//                Log.i(TAG, logs);
//                if (ExampleUtil.isConnected(getApplicationContext())) {
//                	mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
//                } else {
////                	Log.i(TAG, "No network");
//                }
                break;
            
            default:
                logs = "Failed with errorCode = " + code;
//                Log.e(TAG, logs);
            }
            
//            ExampleUtil.showToast(logs, getApplicationContext());
        }
        
    };
	
	
	private void getProjectList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreTatalprice" };
		String[] idParam2 = new String[] { "store_id", MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(idParam2);
//		progressDialog = ProgressDialog.show(this,
//				getString(R.string.app_name),
//				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				list = mProjectListObject.response;
				if (list.size() > 0) {
					daytotal.setText(list.get(0).daymoney+"元");
					monthtotal.setText(list.get(0).monthmoney+"元");
					getStoreInfo();
				}

			} else {
				Toast.makeText(HomeStoreActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	private void setImg(String path){
		if (path.equals("") || path == null) {
			store_img.setBackgroundResource(R.drawable.img_add);
		} else {
			MyApplication.downloadImage.addTasks(path,
					store_img, new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}
					});
		}
		MyApplication.downloadImage.doTask();
		
	}
	
	private void getStoreInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstore" };
		String[] idParam2 = new String[] { "store_id",MyApplication.Storeuser.id};
		params2.add(funParam2);
		params2.add(idParam2);
//		progressDialog = ProgressDialog.show(this,
//				getString(R.string.app_name),
//				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
			if (!v.equals("fail")) {
				mStoreListObject = JSON.parseObject(v,
						StoreListObject.class);
				slist = mStoreListObject.response;
				if(mStoreListObject.meta.getMsg().equals("OK")){
					if (slist.size() > 0) {
						store_name.setText("名称　"+slist.get(0).name);
//						getStoreImg();
						
						setImg(slist.get(0).phone);
					}
				}
			} else {
				Toast.makeText(HomeStoreActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 964:
			getStoreInfo();
			break;

		default:
			break;
		}
	};
	
	private void setMsgRegId() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setStoreMsgRegId" };
		String[] name1 = new String[] { "id",MyApplication.Storeuser.id};
		String[] name2=new String[]{"reg_id",JPushInterface.getRegistrationID(getApplicationContext())};
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(HomeStoreActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					typeday_callbackListener);
		} else {
			Toast.makeText(HomeStoreActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
			
			} else {
				
			}
		}
	};
	
	
	private void getStoreImg() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setStoreMsgRegId" };
		String[] name1 = new String[] { "id",MyApplication.Storeuser.id};
		String[] name2=new String[]{"reg_id",JPushInterface.getRegistrationID(getApplicationContext())};
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(HomeStoreActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					getstoreimg_callbackListener);
		} else {
			Toast.makeText(HomeStoreActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getstoreimg_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				setImg(slist.get(0).phone);
			} else {
				Toast.makeText(getApplicationContext(), "失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                System.exit(0);
	            }
	        }
	        return false;
	}
}
