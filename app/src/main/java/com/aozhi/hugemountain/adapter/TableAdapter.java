package com.aozhi.hugemountain.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ConsumptionObject;

import java.util.ArrayList;


public class TableAdapter extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();

	public TableAdapter(Context ctx, ArrayList<ConsumptionObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_table, null);
			mHolder.tv_table=(TextView)convertView.findViewById(R.id.tv_table);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
//		for(int i=0;i<list.size();i++){
		mHolder.tv_table.setText(list.get(position).year+"年");
//		}
		
//		convertView.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				MyApplication.year = list.get(position);.
//			
//				}
//		});
		return convertView;
	}
	class Holder {
		TextView tv_table;
	}
}
