package com.aozhi.hugemountain.activity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegiststoreActivity extends Activity {
	Button btn_ok, btn_yanzhengma, btn_back;
	TextView text_xieyi;

	private String num, str;
	private ProgressDialog progressDialog = null;
	private EditText  et_pwd, et_surepwd, et_yanzhengma,et_storename;
	private TextView et_photo;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private CheckBox cb;
	private String number;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registstore);
		initView();
		initListener();
		
	}

	private void initView() {
		number=getIntent().getStringExtra("store_phonenumber");
		
		btn_back = (Button) findViewById(R.id.btn_back);
		et_photo = (TextView) findViewById(R.id.et_photo);
		et_pwd = (EditText) findViewById(R.id.et_pwd);
		et_surepwd = (EditText) findViewById(R.id.et_surepwd);
		cb = (CheckBox) findViewById(R.id.cb);
		btn_ok = (Button) findViewById(R.id.btn_ok);
		et_storename=(EditText) findViewById(R.id.et_storename);
	}

	private String Split(String String) {
		String[] name=String.split("[,]");
		String result1=name[0].substring(7);
		String result2=name[1].substring(8,10);
		return result1;
	}
	private void initListener() {
		et_photo.setText(Split(number));
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(RegiststoreActivity.this,LoginStoreActivity.class);
				startActivity(intent);
				finish();
			}
		});
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (et_pwd.getText().toString().equals("")) {
					Toast.makeText(RegiststoreActivity.this, "请输入密码！",
							Toast.LENGTH_LONG).show();
				} else if (et_pwd.getText().toString().length() < 6) {
					Toast.makeText(RegiststoreActivity.this, "密码不得少于6位！",
							Toast.LENGTH_LONG).show();
				} else if (!et_pwd.getText().toString()
						.equals(et_surepwd.getText().toString())) {
					Toast.makeText(RegiststoreActivity.this, "密码与确认密码不一致！",
							Toast.LENGTH_LONG).show();
				} else if (!cb.isChecked()) {
					Toast.makeText(RegiststoreActivity.this, "请同意巨岳注册协议！",
							Toast.LENGTH_LONG).show();
				} else if(et_storename.getText().equals("")){
					Toast.makeText(getApplicationContext(), "请输入店铺名称", Toast.LENGTH_SHORT).show();
				}else{
					getuser();
				}
			}
		});

	}

	private void getuser() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "selectstorebyphoto" };
		String[] funParam3 = new String[] { "photo",
				et_photo.getText().toString() };
		params1.add(funParam1);
		params1.add(funParam3);
		Constant.NET_STATUS = Utils.getCurrentNetWork(RegiststoreActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					user_callbackListener);
		} else {
			Toast.makeText(RegiststoreActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}

	}

	private CallbackListener user_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					Toast.makeText(RegiststoreActivity.this, "登录账号已存在，请重新输入",
							Toast.LENGTH_LONG).show();
				} else {
					getregister();
				}
			}
		}
	};

	private void getregister() {
		ArrayList<String[]> params = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "registstore" };
		String[] funParam2 = new String[] { "photo",
				et_photo.getText().toString() };
		 String[] funParam3 = new String[] {"phone","/picture/store11.png"};  //系统默认头像
		String[] funParam4 = new String[] { "pwd",
				encryption(et_pwd.getText().toString()) };
		String[] funParam5=new String[]{"name",et_storename.getText().toString().trim()};
		params.add(funParam1);
		params.add(funParam2);
		params.add(funParam3);
		params.add(funParam4);
		params.add(funParam5);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				if (mLoginListObject.getMeta().msg.equals("OK")) {
					SharedPreferences mySharedPreferences = getSharedPreferences(
							"store_user", Activity.MODE_PRIVATE);
					SharedPreferences.Editor editor = mySharedPreferences
							.edit();
					editor.putString("name", et_photo.getText().toString());
					editor.putString("pwd", et_pwd.getText().toString());
					// editor.putBoolean("ok", true);
					editor.commit();
					Toast.makeText(RegiststoreActivity.this, "注册成功",
							Toast.LENGTH_LONG).show();
					Intent mintent = new Intent(RegiststoreActivity.this,
							LoginStoreActivity.class);
					startActivity(mintent);
					finish();
				} else {
					Toast.makeText(RegiststoreActivity.this, "注册失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(RegiststoreActivity.this, "注册失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

//	private CallbackListener login_callbackListener11 = new HttpConnection.CallbackListener() {
//		@Override
//		public void callBack(String v) {
//			// TODO Auto-generated method stub
//			Log.d("获取验证码返回数据", v);
//			// 对设备注册结果进行解析
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
//			if (!v.equals("fail")) {// 当请求网络返回值正常
//				Toast.makeText(RegiststoreActivity.this, "发送成功!",
//						Toast.LENGTH_SHORT).show();
//			}
//		}
//	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}

			re_md5 = buf.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}
}
