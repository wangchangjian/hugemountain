package com.aozhi.hugemountain.activity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;

import com.aozhi.hugemountain.PersonalStaffActivity;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.StaffStoreListActivity;

public class HomeStaffActivity extends Activity {

	View registratio;
	View office;
	View personalstaff;
	View show;
	View pkhub;
	View withdrawals;
	private RelativeLayout userinfo;
	//Button btn_back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_homestaff);
		registratio = (View)findViewById(R.id.registratio);
		registratio.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, RegistrationActivity.class));
			}
		});
		office = (View)findViewById(R.id.office);
		office.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, StaffStoreListActivity.class));
			}
		});
		personalstaff = (View)findViewById(R.id.personalstaff);
		personalstaff.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, PersonalStaffActivity.class));
			}
		});
		show = (View)findViewById(R.id.show);
		show.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, ShowActivity.class));
			}
		});
		pkhub = (View)findViewById(R.id.pkhub);
		pkhub.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, PKHubActivity.class));
			}
		});
		withdrawals = (View)findViewById(R.id.withdrawals);
		withdrawals.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(HomeStaffActivity.this, WithdrawalsActivity.class));
			}
		});
		userinfo=(RelativeLayout) findViewById(R.id.userinfo);
		userinfo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(HomeStaffActivity.this, StaffUserInfoActivity.class));
				
			}
		});
/*		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});*/
	}

}
