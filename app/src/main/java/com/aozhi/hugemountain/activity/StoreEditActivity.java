package com.aozhi.hugemountain.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.GetLongitudeListObject;
import com.aozhi.hugemountain.model.GetLongitudeObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.StaffsListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class StoreEditActivity extends Activity implements OnClickListener {

	private EditText store_name1, addr1, store_phone1, contents;
	private TextView servertype1;
	private TextView star1, star2, star3, star4, star5, done, times;
	private Spinner worktime1, worktime2;
	private LinearLayout star, positioning, sp;
	private ProgressDialog progressDialog = null;
	private LoginListObject mLoginListObject;
	private LocationClient mLocationClient;
	private LocationMode tempMode = LocationMode.Hight_Accuracy;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private String xinji = "0";
	private String serverFile = "";
	private ImageView store_img, img1;
	private TextView tv_camera, tv_photo;
	private String imgload = "";
	private ArrayList<LoginBean> getlist = new ArrayList<LoginBean>();
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;
	public static final int PHOTOZOOM = 2; 
	public static final int PHOTORESOULT = 3;
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private ArrayList<Integer> myChose = new ArrayList<Integer>();
	private ArrayList<ManageStaffObject> type_list = new ArrayList<ManageStaffObject>();
	private StaffsListObject mStaffsListObject;
	private String time = "";
	private int time_click = 0;
	private GetLongitudeListObject mGetLongitudeListObject;
	private ArrayList<GetLongitudeObject> longlist = new ArrayList<GetLongitudeObject>();
	private String longitude, latitude,address;
	public String classes="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editstore);
		initView();
		initListener();
		setView();
	}

	// String[] methodParam = new String[] { "address", address };
	// String[] idParam = new String[] { "output", "json" };
	// String[] idParam1 = new String[] { "ak", "L59bKB6brc64gF7Mp76gIUH8" };

	private void getLongitude(String addr) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "address", addr };
			String[] idParam = new String[] { "output", "json" };
			String[] idParam1 = new String[] { "ak", "Nhn7NQxTnO2DxuRzjCHblm9e" };
			params.add(methodParam);
			params.add(idParam);
			params.add(idParam1);
			new HttpConnection().get(Constant.BAIDU, params,
					getlongitude_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getlongitude_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mGetLongitudeListObject = JSON.parseObject(v,
						GetLongitudeListObject.class);
				longlist = mGetLongitudeListObject.getResponse();

			} else {
				Toast.makeText(StoreEditActivity.this, "获取失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	private void setView() {
		getlist = (ArrayList<LoginBean>) getIntent().getSerializableExtra("list");
		longitude = getlist.get(0).longitude;
		latitude = getlist.get(0).latitude;
		store_name1.setText(getlist.get(0).name);
		addr1.setText(getlist.get(0).address);
		times.setText(getlist.get(0).business_hours);
		servertype1.setText(getlist.get(0).service);
		store_phone1.setText(getlist.get(0).mobile);
		contents.setText(getlist.get(0).remark);
		getImg(getlist.get(0).phone);
		imgload = getlist.get(0).phone;
		classes=getlist.get(0).getService_id();
		xinji = getlist.get(0).star;
		switch (Integer.parseInt(getlist.get(0).star)) {
		case 0:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate0));
			break;
		case 1:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
			break;
		case 2:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
			break;
		case 3:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
			break;
		case 4:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
			break;
		case 5:
			star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
			break;
		default:
			break;
		}

	}

	private void initView() {
		img1 = (ImageView) findViewById(R.id.img1);
		store_img = (ImageView) findViewById(R.id.store_img);
		store_name1 = (EditText) findViewById(R.id.store_name1);
		addr1 = (EditText) findViewById(R.id.addr1);
		servertype1 = (TextView) findViewById(R.id.servertype1);
		store_phone1 = (EditText) findViewById(R.id.store_phone1);
		contents = (EditText) findViewById(R.id.contents);
		times = (TextView) findViewById(R.id.times);
		star1 = (TextView) findViewById(R.id.star1);
		star2 = (TextView) findViewById(R.id.star2);
		star3 = (TextView) findViewById(R.id.star3);
		star4 = (TextView) findViewById(R.id.star4);
		star5 = (TextView) findViewById(R.id.star5);
		done = (TextView) findViewById(R.id.done);
		worktime1 = (Spinner) findViewById(R.id.worktime1);
		worktime2 = (Spinner) findViewById(R.id.worktime2);
		star = (LinearLayout) findViewById(R.id.star);
		sp = (LinearLayout) findViewById(R.id.sp);
		positioning = (LinearLayout) findViewById(R.id.positioning);
		
		initLoc();
		mLocationClient.start();
	}

	private void initListener() {
		img1.setOnClickListener(this);
		store_img.setOnClickListener(this);
		times.setOnClickListener(this);
		done.setOnClickListener(this);
		star1.setOnClickListener(this);
		star2.setOnClickListener(this);
		star3.setOnClickListener(this);
		star4.setOnClickListener(this);
		star5.setOnClickListener(this);
		servertype1.setOnClickListener(this);
		Spinner(worktime1);
		Spinner(worktime2);
		positioning.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			finish();
			break;
		case R.id.store_img:
			showAtaver();
			break;
		case R.id.servertype1:
			getService();
			break;
		case R.id.positioning:
//			addr1.setText(MyApplication.address);
//			if (!addr1.equals("") || !addr1.equals(null)) {
//				mLocationClient.stop();
//			} else {
//			}
			Intent intent = new Intent(this, LocationActivity.class);
			intent.putExtra("addressname", addr1.getText().toString());
			startActivityForResult(intent,1003);
			break;
		case R.id.star1:
			xinji = "1";
			star.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.rate1));
			break;
		case R.id.star2:
			xinji = "2";
			star.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.rate2));
			break;
		case R.id.star3:
			xinji = "3";
			star.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.rate3));
			break;
		case R.id.star4:
			xinji = "4";
			star.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.rate4));
			break;
		case R.id.star5:
			xinji = "5";
			star.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.rate5));
			break;
		case R.id.done:
			if (store_name1.equals("") || store_name1.equals(null)) {
				Toast.makeText(this, "请检查商铺名称是否输入", Toast.LENGTH_LONG).show();
			} else if (addr1.equals("") || addr1.equals(null)) {
				Toast.makeText(this, "请检查地址是否输入", Toast.LENGTH_LONG).show();
			} else if (servertype1.equals("") || servertype1.equals(null)||classes.equals("")) {
				Toast.makeText(this, "请选择服务类别", Toast.LENGTH_LONG).show();
			} else if (store_phone1.equals("") || store_phone1.equals(null)) {
				Toast.makeText(this, "请检查订餐电话是否输入", Toast.LENGTH_LONG).show();
			} else {
				getLongitude(addr1.getText().toString().trim());
				if (time_click > 0) {
					if (worktime1.getSelectedItem().toString().equals("00:00")
							|| worktime2.getSelectedItem().toString()
									.equals("00:00")) {

						Toast.makeText(StoreEditActivity.this,
								"你没有选择营业时间，请选择后在来保存", Toast.LENGTH_SHORT)
								.show();
						return;
					} else {
						time = worktime1.getSelectedItem().toString() + "-"
								+ worktime2.getSelectedItem().toString();
					}
				} else {
					time = times.getText().toString();
				}
				if (store_name1.getText().toString() == getlist.get(0).name
						&& addr1.getText().toString() == getlist.get(0).address
						&& time == getlist.get(0).business_hours
						&& servertype1.getText().toString() == getlist.get(0).service
						&& store_phone1.getText().toString() == getlist.get(0).mobile) {
					Toast.makeText(this, "你没有任何改动不需要再次上传", Toast.LENGTH_SHORT)
							.show();
					finish();
				} else {
					upStoreInfo();
				}
			}
			break;
		case R.id.times:
			times.setVisibility(View.GONE);
			sp.setVisibility(View.VISIBLE);
			time_click = 1;
			break;
		default:
			break;
		}

	}

	private void Spinner(Spinner sp) {
		ArrayList<String> timelist = new ArrayList<String>();
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		for (int i = 0; i < 24; i++) {
			if (i < 10) {
				timelist.add("0" + i + ":00");
			} else {
				timelist.add(i + ":00");
			}
		}
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_time, timelist);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		sp.setAdapter(adapter);
		// 第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		sp.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO 自动生成的方法存根

			}
		});
	}

	// 更新商铺信息
	private void upStoreInfo() {
		String img="";
		if(serverFile.equals("")){
			img=getlist.get(0).phone;
		}else{
			img=serverFile;
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		// fun=upstoreInfo&address=甘肃省兰州市&store_name=菲利普定点&worktime=08%3A00-20%3A00&remark=菲利普++你的首选&servertype=维修点&star=3&city=兰州市&longitude=103.84026&latitude=36.049231&store_photo=13800000000
//		longitude = String.valueOf(MyApplication.app_longitude);
//		latitude = String.valueOf(MyApplication.app_latitude);
		String[] funParam2 = new String[] { "fun", "upstoreInfo" };
		String[] name2 = new String[] { "store_name",store_name1.getText().toString() };
		String[] name1 = new String[] { "address", addr1.getText().toString() };
		String[] name3 = new String[] { "worktime", time };
		String[] name4 = new String[] { "remark", contents.getText().toString() };// 备注商家简介
		String[] name5 = new String[] { "servertype",servertype1.getText().toString() };
		String[] name6 = new String[] { "star", xinji };
		String[] name7 = new String[] { "city", MyApplication.app_current_city };
		String[] name8 = new String[] { "longitude", longitude};
		String[] name9 = new String[] { "latitude",  latitude};
		String[] name10 = new String[] { "store_photo",MyApplication.Storeuser.getStorephoto() };// 用户登录名
		String[] name11 = new String[] { "mobile",store_phone1.getText().toString() };
		String[] name12 = new String[] { "path",img };
		String[] classn = new String[] { "class",classes };
		params2.add(funParam2);
		params2.add(classn);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		params2.add(name4);
		params2.add(name5);
		params2.add(name6);
		params2.add(name7);
		params2.add(name8);
		params2.add(name9);
		params2.add(name10);
		params2.add(name11);
		params2.add(name12);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				LoginListObject mLoginListObject = JSON.parseObject(v,
						LoginListObject.class);
				list = mLoginListObject.response;
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(StoreEditActivity.this, "店铺信息更新成功！",
							Toast.LENGTH_LONG).show();

					setStoreClass1();
					Intent mIntent = new Intent(StoreEditActivity.this,
							StoreInfoActivity.class);
					setResult(20, mIntent);
					finish();
				} else {
					Toast.makeText(StoreEditActivity.this, "店铺信息更新失败！",
							Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(StoreEditActivity.this, "店铺信息更新失败！",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	private void getImg(String img) {
		if (img.equals("") || img == null) {

		} else {
			MyApplication.downloadImage.addTask(img, store_img,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								store_img.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
	}

	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.activity_xiugai);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						Environment.getExternalStorageDirectory(), "temp.jpg")));
				System.out.println("============="
						+ Environment.getExternalStorageDirectory());
				startActivityForResult(intent, PHOTOHRAPH);
				dlg.cancel();
			}
		});
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
				// intent.setDataAndType(
				// MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				// IMAGE_UNSPECIFIED);
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTOZOOM);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		
		if(resultCode==1003){
			latitude = data.getStringExtra("latitude");
			longitude = data.getStringExtra("longitude");
//			addr1.setText(MyApplication.address);
			addr1.setText(data.getStringExtra("address"));
				
			}
		if (resultCode == NONE)
			return;
		// ����
		if (requestCode == PHOTOHRAPH) {
			// �����ļ�����·��������ڸ�Ŀ¼��
			File picture = new File(Environment.getExternalStorageDirectory()
					+ "/temp.jpg");
			System.out.println("------------------------" + picture.getPath());
			startPhotoZoom(Uri.fromFile(picture));
		}
		if (data == null)
			return;
		// ��ȡ�������ͼƬ
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		// ������
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);// (0
																		// 为压缩率
																		// 不压缩
																		// 75
																		// 为压缩25%

				new DateFormat();
				String name = DateFormat.format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA))
						+ ".jpg";
				Bundle bundle = data.getExtras();
				Bitmap bitmap = (Bitmap) bundle.get("data");// ��ȡ������ص����ݣ���ת��ΪBitmapͼƬ��ʽ
				FileOutputStream b = null;
				// ???????????????????????????????Ϊʲô����ֱ�ӱ�����ϵͳ���λ���أ�����������������������
				File file = new File("/sdcard/myImage/");
				file.mkdirs();//
				String fileName = "/sdcard/myImage/" + name;
				try {
					b = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);// ������д���ļ�
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				File file1 = new File(fileName);
				store_img.setImageBitmap(photo);

				new UploadAsyncTask().execute(file1);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void startPhotoZoom(Uri uri) {
		Intent mintent = new Intent("com.android.camera.action.CROP");
		mintent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		mintent.putExtra("crop", "true");
		mintent.putExtra("aspectX", 1);
		mintent.putExtra("aspectY", 1);
		mintent.putExtra("outputX", 320);
		mintent.putExtra("outputY", 320);
		mintent.putExtra("return-data", true);
		startActivityForResult(mintent, PHOTORESOULT);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
//			setStoreImg(serverFile);
			return serverFile;
		}
	}

	private void initLoc() {
		mLocationClient = ((MyApplication) getApplication()).mLocationClient;
		InitLocation();
		mLocationClient.start();
		// System.out.println("获取到的定位信息是：" + tv_city.getText().toString());
	}

	// 获取地理位置信息
	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("gcj02");// 返回的定位结果是百度经纬度，默认值gcj02
		int span = 1000;
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	private void showMultiChosDia(ArrayList<ManageStaffObject> list) {
		final String[] mList = new String[list.size()];
		boolean mChoseSts[] = new boolean[list.size()];
		String serverStr=this.servertype1.getText().toString();
		for (int i = 0; i < list.size(); i++) {
			mList[i] = list.get(i).name;
			 if(serverStr.indexOf(list.get(i).name)>-1){
				 mChoseSts[i] = true;
			 }else{
				 mChoseSts[i] = false;
			 }
		}
//		myChose.clear();
		AlertDialog.Builder multiChosDia = new AlertDialog.Builder(
				StoreEditActivity.this);
		multiChosDia.setMultiChoiceItems(mList, mChoseSts,
				new DialogInterface.OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							myChose.add(which);
						} else 
							myChose.remove(Integer.valueOf(which));

					}
				});
		multiChosDia.setPositiveButton("确定",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						int size = myChose.size();
						String str = "";
						classes="";
						for (int i = 0; i < size; i++) {
							str += mList[myChose.get(i)] + ",";
							classes += type_list.get(myChose.get(i)).id + ",";
						}
						if(myChose.size()>0){
						str = str.substring(0, str.length() - 1);
						classes= classes.substring(0, classes.length()-1);
						}
						servertype1.setText(str);
					}
				});
		multiChosDia.create().show();
	}

	// 获取店铺服务列表
	private void getService() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "gethome" };
		params2.add(funParam2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					service_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener service_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				type_list = mStaffsListObject.getResponse();
				if (type_list.size() > 0) {
					showMultiChosDia(type_list);
				}

			} else {
				Toast.makeText(StoreEditActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	// 修改店铺图片
	private void setStoreImg(String path) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "setImgPath" };
			String[] paramName = new String[] { "id", getlist.get(0).id }; // 店铺id
			String[] paramName1 = new String[] { "path", path };
			params.add(methodParam);
			params.add(paramName);
			params.add(paramName1);
			new HttpConnection().get(Constant.URL, params,
					getpath_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getpath_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				Toast.makeText(StoreEditActivity.this, "设置成功",
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(StoreEditActivity.this, "设置失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	// 修改店铺分类
	private void setStoreClass(String class_id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "setStroeClass" };
			String[] paramName = new String[] { "store_id",
					MyApplication.Storeuser.id }; // 店铺id
			String[] paramName1 = new String[] { "class_id", class_id };
			params.add(methodParam);
			params.add(paramName);
			params.add(paramName1);
			new HttpConnection().get(Constant.URL, params,
					getclass_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getclass_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
			// Toast.makeText(StoreEditActivity.this, "设置成功",
			// Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(StoreEditActivity.this, "设置失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	// 删除现有店铺分类
	private void setStoreClass1() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "id", MyApplication.Storeuser.id };
			params.add(methodParam);
			new HttpConnection().get(Constant.BURL, params,
					delclass_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener delclass_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				
			} else {
				Toast.makeText(StoreEditActivity.this, "设置失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};
}
