package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.adapter.CityAdapter;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.list.MySideBar;
import com.aozhi.hugemountain.list.MySideBar.OnTouchingLetterChangedListener;
import com.aozhi.hugemountain.model.AddressListObject;
import com.aozhi.hugemountain.model.AddressObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.PinyinUtils;
import com.aozhi.hugemountain.utils.Utils;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CityActivity extends BaseActivity implements OnClickListener, OnTouchingLetterChangedListener {
	private LocationClient mLocationClient;
	private Button btn_back;
	private ArrayList<AddressObject> list = new ArrayList<AddressObject>();
	private AddressListObject mAddressListObject;
	private CityAdapter mCityAdapter;
	private ListView list_city;
	private MySideBar myView;
	private TextView overlay, tv_city, tv_dingwei;
	private ProgressDialog progressDialog = null;
	private OverlayThread overlayThread = new OverlayThread();
	final int RESULT_CODE1=102;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_city);
		initLoc();
		initView();
		initListnner();
	}

	@Override
	protected void initData() {

	}

	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		int span = 60*1000;
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	private void initLoc() {
		mLocationClient = ((MyApplication) getApplication()).mLocationClient;
		tv_city = (TextView) findViewById(R.id.tv_city);
//		((MyApplication) getApplication()).mLocationResult = tv_city;
		InitLocation();
		mLocationClient.start();
		System.out.println("获取到的定位信息是：" + tv_city.getText().toString());
	}
	public void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
		myView = (MySideBar) findViewById(R.id.myView);
		overlay = (TextView) findViewById(R.id.tvLetter);
		tv_city = (TextView) findViewById(R.id.tv_city);
		overlay.setVisibility(View.INVISIBLE);
		myView.setOnTouchingLetterChangedListener(this);
		list_city = (ListView) findViewById(R.id.list_city);
		mCityAdapter = new CityAdapter(this, list);
		list_city.setAdapter(mCityAdapter);
		getcity();
		
		list_city.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
//				MyApplication.app_current_city = list.get(arg2).name;
				Intent intent=new Intent();  
//              intent.putExtra("cityname", MyApplication.app_current_city);  
				intent.putExtra("cityname", list.get(arg2).name);  
                setResult(RESULT_CODE1, intent);  
                finish(); 
			}
		});

		tv_dingwei = (TextView) findViewById(R.id.tv_dingwei);
		tv_dingwei.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tv_city.setVisibility(View.VISIBLE);
				tv_city.setText(MyApplication.address);
			}
		});

//		tv_city.setText(MyApplication.citys);
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
			finish();
		}
			break;

		default:
			break;
		}
	}

	private void getcity() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getcity" };
			params.add(methodParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					getcity_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getcity_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mAddressListObject = JSON.parseObject(result,
						AddressListObject.class);
				list = mAddressListObject.response;

				if (mAddressListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						mCityAdapter = new CityAdapter(CityActivity.this, list);
						list_city.setAdapter(mCityAdapter);
					}

				}

			}
		}
	};

	@Override
	public void onTouchingLetterChanged(String s) {
		// TODO Auto-generated method stub
		overlay.setText(s);
		overlay.setVisibility(View.VISIBLE);
		handler.removeCallbacks(overlayThread);
		handler.postDelayed(overlayThread, 1000);
		if (alphaIndexer(s) > 0) {
			int position = alphaIndexer(s);
			Log.i("coder", "position:" + position);
			list_city.setSelection(position);

		}
	}

	public int alphaIndexer(String s) {
		int position = 0;
		for (int i = 0; i < list.size(); i++) {

			if (PinyinUtils.getAlpha(list.get(i).name).startsWith(s)) {
				position = i;
				break;
			}
		}
		Log.i("coder", "i" + position + list.get(position));
		return position;
	}

	private Handler handler = new Handler() {
	};

	private class OverlayThread implements Runnable {

		public void run() {
			overlay.setVisibility(View.GONE);
		}

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
}
