package com.aozhi.hugemountain.activity;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.aozhi.hugemountain.R;

public class FunctionActivity extends Activity  implements OnClickListener{
	private Button btn_back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_function);
		initView();
		initListnner();
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
			finish();
		}
			break;
		default:
			break;
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.function, menu);
		return true;
	}

}
