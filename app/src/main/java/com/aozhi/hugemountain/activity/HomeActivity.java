package com.aozhi.hugemountain.activity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.StaffStoreDetailsActivity;
import com.aozhi.hugemountain.activity.ConsumerActivity.StoreListActivity;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.adapter.MainAdapter;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.model.ADListObject;
import com.aozhi.hugemountain.model.AdObject;
import com.aozhi.hugemountain.model.MainListObject;
import com.aozhi.hugemountain.model.MainObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MyLocationConfiguration.LocationMode;
import com.baidu.mapapi.model.LatLng;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class HomeActivity extends Activity {
	
	private ArrayList<StoreObject> slist = new ArrayList<StoreObject>();
	private StoreListObject mStoreListObject;
	private ProgressDialog progressDialog = null;
	private ArrayList<MainObject> list = new ArrayList<MainObject>();
	private MainListObject mMainListObject;
	private Button btn_preference;
	private MainAdapter mMainAdapter;
	private GridView grid_view;
	private EditText et_search;
	private int i = 0;
	private int index;
	private DownloadImage downloadImage = new DownloadImage();
	private ViewPager vp_mainadv;
	private int currentItem = 0;// 当前图片的索引号
	private ArrayList<AdObject> list2 = new ArrayList<AdObject>();
	private ADListObject mADListObject;
	private String images = "";
	private MapView mMapView;
	private BaiduMap mBaiduMap;
	private LocationClient mLocClient;
	// 定位相关
	public MyLocationListenner myListener = new MyLocationListenner();
	private LocationMode mCurrentMode;
	BitmapDescriptor mCurrentMarker;
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	// UI相关
	OnCheckedChangeListener radioButtonListener;
	Button requestLocButton;
	boolean isFirstLoc = true;// 是否首次定位
	private ArrayList<ImageView> imgViews = new ArrayList<ImageView>();
	// 切换当前显示图片
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			vp_mainadv.setCurrentItem(currentItem);// 切换当前显示图片
		}
	};
	private Handler updateHandler = new Handler() {
		public void handleMessage(Message msg) {
			// setText();
		};
	};

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(18.0f);// 设置地图的缩放比例
		mBaiduMap.setMapStatus(msu);// 将前面的参数交给BaiduMap类
		grid_view = (GridView) findViewById(R.id.grid_view);
		mMainAdapter = new MainAdapter(this, list);
		grid_view.setAdapter(mMainAdapter);
		et_search = (EditText) findViewById(R.id.et_search);
		et_search = (EditText) findViewById(R.id.et_search);
		et_search.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				String keyword = s.toString();
				if (!keyword.equals("")) {
					Intent mIntent = new Intent(HomeActivity.this,
							StoreListActivity.class);
					mIntent.putExtra("search", keyword);
					startActivity(mIntent);
				}
			}
		});
		grid_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent mIntent = new Intent(HomeActivity.this,StoreListActivity.class);
				mIntent.putExtra("seller_id", list.get(position).id);
				startActivity(mIntent);
			}
		});
		Location();
		GetMainList();
		getad();
		getMapList("兰州市");
	}

	private void Location() {
		requestLocButton = (Button) findViewById(R.id.button1);
		mCurrentMode = LocationMode.NORMAL;
		requestLocButton.setText("普通");
		OnClickListener btnClickListener = new OnClickListener() {
			public void onClick(View v) {
				switch (mCurrentMode) {
				case NORMAL:
					requestLocButton.setText("跟随");
					mCurrentMode = LocationMode.FOLLOWING;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				case COMPASS:
					requestLocButton.setText("普通");
					mCurrentMode = LocationMode.NORMAL;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				case FOLLOWING:
					requestLocButton.setText("罗盘");
					mCurrentMode = LocationMode.COMPASS;
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
					break;
				}
			}
		};
		requestLocButton.setOnClickListener(btnClickListener);

		RadioGroup group = (RadioGroup) this.findViewById(R.id.radioGroup);
		radioButtonListener = new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.defaulticon) {
					// 传入null则，恢复默认图标
					// mCurrentMarker = null;
					// mBaiduMap
					// .setMyLocationConfigeration(new MyLocationConfiguration(
					// mCurrentMode, true, null));
					mCurrentMarker = BitmapDescriptorFactory
							.fromResource(R.drawable.icon_geo);
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
				}
				if (checkedId == R.id.customicon) {
					// 修改为自定义marker
					mCurrentMarker = BitmapDescriptorFactory
							.fromResource(R.drawable.icon_geo);
					mBaiduMap
							.setMyLocationConfigeration(new MyLocationConfiguration(
									mCurrentMode, true, mCurrentMarker));
				}
			}
		};
		group.setOnCheckedChangeListener(radioButtonListener);

		// 地图初始化
		mMapView = (MapView) findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		// 定位初始化
		mLocClient = new LocationClient(this);
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocClient.setLocOption(option);
		mLocClient.start();
	}

	private void GetMainList() {

		// TODO Auto-generated method stub
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "gethome" };
			params.add(methodParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
//			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mMainListObject = JSON
						.parseObject(result, MainListObject.class);
				list = mMainListObject.response;
				if (mMainListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						mMainAdapter = new MainAdapter(HomeActivity.this, list);
						grid_view.setAdapter(mMainAdapter);
					}
				}
			}
		}
	};

	private void getad() {
		// 1.首页，2.订单列表，3.外卖订单详情，4.预定订单详情，5.店内点单详情，6.临时支付订单详情，7.商家列表，8.商家详情，9.收货地址添加，
		// 10.生成外卖订单，11.生成预定订单，12.预约信息添加，13生成点单订单，14.生成临时支付订单，15支付页面
		// TODO Auto-generated method stub
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getadinfo" };
			params.add(methodParam);
			new HttpConnection().get(Constant.URL, params,
					getmain_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接！", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getmain_callbackListener = new HttpConnection.CallbackListener() {

		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mADListObject = JSON.parseObject(v, ADListObject.class);
				list2 = mADListObject.getResponse();
				int n = 0;
				if (list2.size() > 0) {
					for (n = 0; n < list2.size(); n++) {
						final ImageView imgView = new ImageView(
								HomeActivity.this);
						imgView.setClickable(true);
						imgView.setScaleType(ScaleType.FIT_XY);
						downloadImage.addTasks(list2.get(n).pic, imgView,
								new DownloadImage.ImageCallback() {
									public void imageLoaded(Bitmap imageBitmap,
											String imageUrl) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag(imageUrl);
										}
									}

									public void imageLoaded(Bitmap imageBitmap,
											DownloadImageMode callBackTag) {
										// TODO Auto-generated method stub
										if (imageBitmap != null) {
											imgView.setImageBitmap(imageBitmap);
											imgView.setTag("");
										}
									}
								});
						downloadImage.doTask();
						final String url = list2.get(n).url;
						final String title = list2.get(n).name;

						if (images.equals("")) {
							images = list2.get(n).pic;
						} else {
							images = images + "," + list2.get(n).pic;
						}

						imgView.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Intent mIntent = new Intent(HomeActivity.this,
										ImagePagerActivity.class);
								mIntent.putExtra("images", images);
								startActivity(mIntent);
							}
						});
						imgViews.add(imgView);

					}
					vp_mainadv = (ViewPager) findViewById(R.id.viewPager);
					vp_mainadv.setAdapter(new MyAdapter());
					// 设置监听，当ViewPager中的页面改变时调用
					vp_mainadv
							.setOnPageChangeListener(new MyPageChangeListener());
				}
			}
		}

	};

	public class MyAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return list2.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(imgViews.get(arg1));
			return imgViews.get(arg1);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView((View) arg2);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			return null;
		}

		@Override
		public void startUpdate(View arg0) {

		}

		@Override
		public void finishUpdate(View arg0) {

		}
	}

	private class MyPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int position) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
		}

		@Override
		public void onPageSelected(int arg0) {
			// TODO Auto-generated method stub
			// for(int i=0;i<dots.size();i++){
			// if(i!=arg0){
			// dots.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_normal);
			// }else{
			// dots.get(i).findViewById(R.id.AD_dot1).setBackgroundResource(R.drawable.dot_focused);
			// }
			// }
		}
	}

	protected void onStart() {
		scheduleExecutorService = Executors.newSingleThreadScheduledExecutor();
		// 当Activity显示出来后，每两秒切换一次图片显示
		scheduleExecutorService.scheduleAtFixedRate(new ScrollTask(), 1, 7,
				TimeUnit.SECONDS);
		super.onStart();
	}

	protected void onStop() {
		scheduleExecutorService.shutdown();
		super.onStop();
	}

	private ScheduledExecutorService scheduleExecutorService;

	/*
	 * 
	 * 类似切换任务时间：2013.4.22作者：guido
	 */
	private class ScrollTask implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			synchronized (vp_mainadv) {
				System.out.println("currentItem: " + currentItem);
				currentItem = (currentItem + 1) /* % imageViews.size() */;
				if (currentItem >= imgViews.size()) {
					currentItem = 0;
				}
				handler.obtainMessage().sendToTarget(); // 通过Handler切换图片
			}
		}

	}

	/**
	 * 定位SDK监听函数
	 */
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
//			mBaiduMap.setMyLocationData(locData);
			if (isFirstLoc) {
				isFirstLoc = false;
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
			}
			
			mLocClient.stop();
		}

		public void onReceivePoi(BDLocation poiLocation) {
		}
	}
	private void getMapList(String city) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "getMapList" };
			String[] cityParam = new String[] { "city", city };
			params.add(methodParam);
			params.add(cityParam);
			new HttpConnection().get(Constant.URL, params,
					getGoodsList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getGoodsList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStoreListObject = JSON.parseObject(result,
						StoreListObject.class);
				slist = mStoreListObject.response;
				if (mStoreListObject.meta.getMsg().equals("OK")) {
					mBaiduMap.clear();
					
					if (slist.size() > 0) {
						for (i = 0; i < slist.size(); i++) {
							initOverLay(Double.valueOf(slist.get(i).latitude),
									Double.valueOf(slist.get(i).longitude), i);
							index = i;
						}
						mBaiduMap
								.setOnMarkerClickListener(new OnMarkerClickListener() {
									public boolean onMarkerClick(
											final Marker marker) {
										// 创建InfoWindow展示的view
										Button button = new Button(
												getApplicationContext());
										button.setBackgroundResource(R.drawable.popup);
										button.setTextColor(getResources()
												.getColor(R.color.black));
										index = marker.getZIndex();
										button.setText(slist.get(index).name);
										// 定义用于显示该InfoWindow的坐标点
										LatLng pt = new LatLng(
												Double.valueOf(slist.get(index).latitude),
												Double.valueOf(slist.get(index).longitude));
										// 创建InfoWindow , 传入 view， 地理坐标， y
										// 轴偏移量
										InfoWindow mInfoWindow = new InfoWindow(
												button, pt, -77);
										// 显示InfoWindow
										mBaiduMap.showInfoWindow(mInfoWindow);
										button.setOnClickListener(new OnClickListener() {

											@Override
											public void onClick(View arg0) {
												// TODO Auto-generated
												// method stub
												Intent mIntent = new Intent(HomeActivity.this,StaffStoreDetailsActivity.class);
												mIntent.putExtra("store_id", slist.get(index).id);
												startActivity(mIntent);
											}
										});
										return true;
									}
								});
						Location();
					}

				}
			}
		}
	};
	private InfoWindow mInfoWindow;

	public void InfoWindow(int index, double latitude, double longitude) {

		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			public boolean onMarkerClick(final Marker marker) {
				Button button = new Button(getApplicationContext());
				button.setBackgroundResource(R.drawable.popup);
				OnInfoWindowClickListener listener = null;
				int index = marker.getZIndex();
				String btnname = slist.get(index).name;
				button.setText(btnname);
				button.setTextColor(getResources().getColor(R.color.black));
				listener = new OnInfoWindowClickListener() {
					public void onInfoWindowClick() {
						LatLng ll = marker.getPosition();
						LatLng llNew = new LatLng(ll.latitude, ll.longitude);
						marker.setPosition(llNew);
						mBaiduMap.hideInfoWindow();
					}

				};
				LatLng ll = marker.getPosition();
				mInfoWindow = new InfoWindow(BitmapDescriptorFactory
						.fromView(button), ll, -47, listener);
				mBaiduMap.showInfoWindow(mInfoWindow);
				button.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// TODO Au`to-generated method stub
//						Intent mIntent = new Intent(HomeActivity.this,
//								MerchantdetailsActivity.class);
//						mIntent.putExtra("mMerchantObject", mMerchantObject);
//						startActivity(mIntent);
					}
				});
				return true;
			}

		});

	}

	public void initOverLay(double latitude, double longitude, int i) {
		// 定义Maker坐标点
		LatLng ll = new LatLng(latitude, longitude);
		// 构建Marker图标
		BitmapDescriptor bd1 = BitmapDescriptorFactory
				.fromResource(R.drawable.icon_gcoding);
		// 构建MarkerOption，用于在地图上添加Marker\
		MarkerOptions option = new MarkerOptions().position(ll).icon(bd1);
		option.zIndex(i);
		option.title("uiuiu");
		// 在地图上添加Marker，并显示
		mBaiduMap.addOverlay(option);// 添加当前分店信息图层
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
		mBaiduMap.setMapStatus(u);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
//		case 765:
//			orderid=getIntent().getStringExtra("orderid");
//			break;

		default:
			break;
		}
		
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                MyApplication.IS_LOGINS=false;
	                System.exit(0);
	            }
	        }
	        return false;
	}
	
}
