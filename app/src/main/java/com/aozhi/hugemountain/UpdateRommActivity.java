package com.aozhi.hugemountain;

import java.util.ArrayList;

import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class UpdateRommActivity extends Activity implements OnClickListener {

	public static final int RESULT_CODE1 = 102;
	private EditText et_number, et_name;
	private TextView tv_sure;
	private Button btn_back;
	private Spinner tv_state;
	private TextView tv_camera, tv_photo;
	private ProgressDialog progressDialog = null;
	private String[] splist = new String[] { "空闲中", "客已满", "已预订", "维修中" };
	private String id;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_addroom1);
		initView();
		initListnner();
	}

	private void initView() {
		id=getIntent().getStringExtra("id");
		et_number = (EditText) findViewById(R.id.et_number);
		et_name = (EditText) findViewById(R.id.et_name);
		tv_sure = (TextView) findViewById(R.id.tv_sure);
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_state = (Spinner) findViewById(R.id.tv_state);
		et_number.setText(MyApplication.mRoomObject.code);
		et_name.setText(MyApplication.mRoomObject.name);
		ArrayAdapter<String> spadapter = new ArrayAdapter<String>(
				UpdateRommActivity.this, R.layout.item_type, splist);
		tv_state.setAdapter(spadapter);
		
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		tv_sure.setOnClickListener(this);
		btn_back.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.tv_sure: {
			if (et_number.getText().toString().equals("")) {
				Toast.makeText(UpdateRommActivity.this, "请输入房间号",
						Toast.LENGTH_LONG).show();
			} else if (et_name.getText().toString().equals("")) {
				Toast.makeText(UpdateRommActivity.this, "请输入房间名称",
						Toast.LENGTH_LONG).show();
			} else {
				AddProject();
			}
		}
			break;
		default:
			break;
		}
	}

	private void AddProject() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "updateRoom" };
		String[] nameParam2 = new String[] { "name",
				et_name.getText().toString() };
		String[] pwdParam2 = new String[] { "codo",
				et_number.getText().toString() };
		String[] store_idParam2 = new String[] { "id",id};
		String[] storeParam2 = new String[] { "states",String.valueOf(tv_state.getSelectedItemPosition())};
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(pwdParam2);
		params2.add(store_idParam2);
		params2.add(storeParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddStaffInfo_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddStaffInfo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {
					Toast.makeText(UpdateRommActivity.this, "修改成功",
							Toast.LENGTH_LONG).show();
					Intent intent = new Intent();
					setResult(RESULT_CODE1, intent);
					finish();
				} else {
					Toast.makeText(UpdateRommActivity.this, "修改失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(UpdateRommActivity.this, "修改失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

}
