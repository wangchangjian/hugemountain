package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StaffOrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StaffOrderDetailActivity extends Activity {
	Button btn_back;
	private String id="";
	private TextView tv_orderid,tv_tel,tv_time,tv_times,tv_timess,tv_money;
	
	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
	private OrderListObject mOrderListObject;
	private ProgressDialog progressDialog = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_stafforderdetail);
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		id=getIntent().getStringExtra("id");
		tv_orderid=(TextView)findViewById(R.id.tv_orderid);
		tv_tel=(TextView)findViewById(R.id.tv_tel);
		tv_time=(TextView)findViewById(R.id.tv_time);
		tv_times=(TextView)findViewById(R.id.tv_times);
		tv_timess=(TextView)findViewById(R.id.tv_timess);
		tv_money=(TextView)findViewById(R.id.tv_money);
		getorder();
	}
	
	private void getorder() {
		
//		fun=getStaffOrderDetails&id=1
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffOrderDetails" };
		String[] name1 = new String[] { "id", id };
		params2.add(funParam2);
		params2.add(name1);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				if (list.size() > 0) {
					tv_orderid.setText(list.get(0).order_id);
					tv_tel.setText(list.get(0).tel);
					tv_time.setText(list.get(0).create_time);
					tv_times.setText(list.get(0).timebucket);
					tv_timess.setText(list.get(0).pay_time);
					tv_money.setText("￥"+list.get(0).pay_manoy);
				}
			}
		}
	};
}
