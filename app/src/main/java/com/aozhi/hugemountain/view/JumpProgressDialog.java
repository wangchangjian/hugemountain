package com.aozhi.hugemountain.view;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.anmin.JumpShakeAnmin;

import android.content.res.Resources.Theme;

/**
 * Created by ${wangchangjian} on 2016/4/25.
 */
public class JumpProgressDialog extends AlertDialog {
    private View view1;
    private View view2;
    private View view3;
    private ViewGroup view;
    private Animation animation1;
    private Animation animation2;
    private Animation animation3;

    public JumpProgressDialog(Context context) {
        this(context, R.style.loading_dialog);
    }

    public JumpProgressDialog(Context context, int theme) {
        super(context, theme);
        view = (ViewGroup) LayoutInflater.from(MyApplication.getInstance()).inflate(R.layout.progress_dialog, null);
        view1 = view.findViewById(R.id.view1);
        view2 = view.findViewById(R.id.view2);
        view3 = view.findViewById(R.id.view3);

        animation1 = new JumpShakeAnmin(Math.PI / 3);
        animation1.setDuration(500);
        animation2 = new JumpShakeAnmin(Math.PI * 2 / 3);
        animation2.setDuration(500);
        animation3 = new JumpShakeAnmin(Math.PI);
        animation3.setDuration(500);
        animation1.setRepeatMode(Animation.REVERSE);
        animation1.setRepeatCount(100000000);
        animation2.setRepeatMode(Animation.REVERSE);
        animation2.setRepeatCount(100000000);
        animation3.setRepeatMode(Animation.REVERSE);
        animation3.setRepeatCount(100000000);
    }

    public void showDialog() {
        this.show();
        this.setContentView(view);
        this.setCancelable(true);
        view1.startAnimation(animation1);
        view2.startAnimation(animation2);
        view3.startAnimation(animation3);
    }

    public void dimissDialog() {
        this.dismiss();
        view2.clearAnimation();
        view3.clearAnimation();
        view1.clearAnimation();
    }

}
