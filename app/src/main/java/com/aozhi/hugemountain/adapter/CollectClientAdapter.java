package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.CollectObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CollectClientAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<CollectObject> list = new ArrayList<CollectObject>();

	public CollectClientAdapter(Context ctx, ArrayList<CollectObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_collect_client, null);
			mHolder.username = (TextView) convertView
					.findViewById(R.id.username);
			mHolder.create_time = (TextView) convertView
					.findViewById(R.id.create_time);
			mHolder.contents = (TextView) convertView
					.findViewById(R.id.contents);
			mHolder.xiangsidu = (TextView) convertView
					.findViewById(R.id.xiangsidu);
			mHolder.sev = (TextView) convertView.findViewById(R.id.sev);
			mHolder.zl = (TextView) convertView.findViewById(R.id.zl);
			mHolder.avg = (TextView) convertView.findViewById(R.id.avg);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.username.setText("客户：" + list.get(position).clientphoto.substring(0, 3)+" "+"****"+" "+list.get(position).clientphoto.substring(7, 11));
		mHolder.create_time.setText(list.get(position).create_time);
		mHolder.contents.setText(list.get(position).contents);
		mHolder.xiangsidu.setText(list.get(position).quality);
		mHolder.sev.setText(list.get(position).service);
		mHolder.zl.setText(list.get(position).distribution);
		Double a = (Double.parseDouble(list.get(position).quality)
				+ Double.parseDouble(list.get(position).service)
				+ Double.parseDouble(list.get(position).distribution))/3;
		mHolder.avg.setText(String.valueOf(Math.round(a*100)/100.0));
		return convertView;
	}

	class Holder {
		TextView username, create_time, contents, xiangsidu, sev, zl, avg;
	}
}
