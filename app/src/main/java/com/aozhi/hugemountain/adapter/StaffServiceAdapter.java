package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ServiceObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StaffServiceAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ServiceObject> list = new ArrayList<ServiceObject>();
	public StaffServiceAdapter(Context ctx, ArrayList<ServiceObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_staff_service, null);
			mHolder.name=(TextView) convertView.findViewById(R.id.name);
			mHolder.money=(TextView) convertView.findViewById(R.id.money);
			
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(list.get(position).name);
		mHolder.money.setText("￥"+list.get(position).money);
		
		return convertView;
	}
	
	class Holder {
		TextView name,money;
	}
}
