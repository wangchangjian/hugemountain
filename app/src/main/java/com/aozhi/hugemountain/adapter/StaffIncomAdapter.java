package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.StaffIncomeObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StaffIncomAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffIncomeObject> list = new ArrayList<StaffIncomeObject>();

	public StaffIncomAdapter(Context ctx, ArrayList<StaffIncomeObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_pkhub, null);
			mHolder.star1 = (ImageView) convertView.findViewById(R.id.star1);
			mHolder.star2 = (ImageView) convertView.findViewById(R.id.star2);
			mHolder.star3 = (ImageView) convertView.findViewById(R.id.star3);
			mHolder.no = (TextView) convertView.findViewById(R.id.no);
			mHolder.staff_name = (TextView) convertView.findViewById(R.id.staff_name);
			mHolder.money = (TextView) convertView.findViewById(R.id.money);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
			mHolder.no.setText(String.valueOf(position+1));
		
		
		if(position==0){
			mHolder.star1.setVisibility(View.VISIBLE);
			mHolder.star2.setVisibility(View.VISIBLE);
			mHolder.star3.setVisibility(View.VISIBLE);
		}else if(position==1){
			mHolder.star1.setVisibility(View.VISIBLE);
			mHolder.star2.setVisibility(View.VISIBLE);
			mHolder.star3.setVisibility(View.GONE);
		}else if(position==2){
			mHolder.star1.setVisibility(View.VISIBLE);
			mHolder.star2.setVisibility(View.GONE);
			mHolder.star3.setVisibility(View.GONE);
		}else{
			mHolder.star1.setVisibility(View.GONE);
			mHolder.star2.setVisibility(View.GONE);
			mHolder.star3.setVisibility(View.GONE);
		}
		mHolder.staff_name.setText(list.get(position).name);
		mHolder.money.setText(list.get(position).code_id);
		return convertView;
	}

	class Holder {
		ImageView star1, star2, star3;
		TextView no, staff_name, money;
	}
}
