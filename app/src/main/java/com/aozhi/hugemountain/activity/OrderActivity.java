package com.aozhi.hugemountain.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
//import com.aozhi.hugemountain.adapter.OrderAdapter;
//import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.adapter.OrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
//import com.aozhi.hugemountain.model.OrderFormObject;
import java.util.ArrayList;

public class OrderActivity extends Activity implements OnClickListener {

	private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
 
	private OrderListObject mOrderListObject;
	private ListView list_order;
	private OrderAdapter adapter;
	private String Status;
	private TextView tv_order1, tv_order2, tv_line1, tv_line2, tv_daysum,
			tv_monthsum;
	private Double sum;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_order);
		initView();
		initListener();
		initListener_staffofmonth();
		initListener_staffofday();
	}

	private void initView() {
		list_order = (ListView) findViewById(R.id.list_order);
		tv_order1 = (TextView) findViewById(R.id.tv_today_order);
		tv_order2 = (TextView) findViewById(R.id.tv_thismonth_order);
		tv_line1 = (TextView) findViewById(R.id.tv_line1);
		tv_line2 = (TextView) findViewById(R.id.tv_line2);
		tv_daysum = (TextView) findViewById(R.id.tv_daysum);
		tv_monthsum = (TextView) findViewById(R.id.tv_monthsum);
		adapter = new OrderAdapter(this, list);
		list_order.setAdapter(adapter);
		Status = MyApplication.Status;
		tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
		tv_order1.setTextColor(getResources().getColor(R.color.linebg));
		tv_line2.setBackgroundColor(getResources().getColor(R.color.white)); 
		tv_order2.setTextColor(getResources().getColor(R.color.black));
		setTitleData(tv_daysum);
		setTitleData(tv_monthsum);
	}

	private void initListener() {
		tv_order1.setOnClickListener(this);
		tv_order2.setOnClickListener(this);
		if (Status != "" && !Status.equals("")) {
			if (Status == "client") {
//				initListener_client();
			}
			if (Status == "staff") {
//				initListener_staff();
			}
			if (Status == "store") {
				// initListener_store();
			}
		}
	}

	private void initListener_staff() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbystaff" };
		String[] name1 = new String[] { "id", MyApplication.Staffuser.id };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(OrderActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					type_callbackListener);
		} else {
			Toast.makeText(OrderActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
					.show();
		}
	}

	private void initListener_staffofday() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbystaff1" };
		String[] name1 = new String[] { "id", MyApplication.Staffuser.id };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(OrderActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					typeday_callbackListener);
		} else {
			Toast.makeText(OrderActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
					.show();
		}
	}

	private CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				adapter = new OrderAdapter(OrderActivity.this, list);
				list_order.setAdapter(adapter);
				Double sum1 = 0.0;
				for (int i = 0; i < list.size(); i++) {
					sum1 = sum1 + Double.parseDouble(list.get(i).pay_manoy);
				}
				tv_daysum.setText("￥"+String.valueOf(sum1)+"元");
			}
		}
	};

	private void initListener_staffofmonth() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbystaff2" };
		String[] name1 = new String[] { "id", MyApplication.Staffuser.id };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(OrderActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					typemonth_callbackListener);
		} else {
			Toast.makeText(OrderActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
					.show();
		}
	}

	private CallbackListener typemonth_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				adapter = new OrderAdapter(OrderActivity.this, list);
				list_order.setAdapter(adapter);
				Double sum2 = 0.0;
				for (int i = 0; i < list.size(); i++) {
					sum2 = sum2 + Double.parseDouble(list.get(i).pay_manoy);
				}
				tv_monthsum.setText("￥"+String.valueOf(sum2)+"元");
			}
		}
	};

	private void initListener_client() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "getorderformbyclient" };
		String[] name1 = new String[] { "client_id",
				MyApplication.Clientuser.id };
		params1.add(funParam1);
		params1.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(OrderActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					type_callbackListener);
		} else {
			Toast.makeText(OrderActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
					.show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				adapter = new OrderAdapter(OrderActivity.this, list);
				list_order.setAdapter(adapter);
			}
		}
	};

	private void setTitleData(TextView tv) {
		Double money = 0.0;
		for (int i = 0; i < list.size(); i++) {
			money = Double.parseDouble(list.get(i).pay_manoy) + money;
		}
		tv.setText(String.valueOf(money));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_today_order:
			tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
			tv_order1.setTextColor(getResources().getColor(R.color.linebg));
			tv_line2.setBackgroundColor(getResources().getColor(R.color.white)); 
			tv_order2.setTextColor(getResources().getColor(R.color.black));
			initListener_staffofday();
			break;
		case R.id.tv_thismonth_order:
			tv_line2.setBackgroundColor(getResources().getColor(R.color.linebg));
			tv_order2.setTextColor(getResources().getColor(R.color.linebg));
			tv_line1.setBackgroundColor(getResources().getColor(R.color.white));
			tv_order1.setTextColor(getResources().getColor(R.color.black));
			initListener_staffofmonth();
			break;
		default:
			break;
		}

	}
}
