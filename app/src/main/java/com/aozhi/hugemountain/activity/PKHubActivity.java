package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StaffIncomAdapter;
import com.aozhi.hugemountain.model.StaffIncomeListObject;
import com.aozhi.hugemountain.model.StaffIncomeObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PKHubActivity extends Activity{

	private ArrayList<StaffIncomeObject> list = new ArrayList<StaffIncomeObject>();
	private StaffIncomAdapter adapter;
	private ListView list_pkhub;
	private StaffIncomeListObject mStaffIncomeListObject;
	private Spinner sp_which;
	private TextView tv_show,exit;
	private ProgressDialog progressDialog = null;
	private ArrayList<String> splist = new ArrayList<String>();

	private Button btn_back;
 	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_pkdetails);
		initView();
		initListener();
		getMainList();
	}

	private void initListener() {
//		sp_which.setOnItemSelectedListener(new OnItemSelectedListener() {
//
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view,
//					int position, long id) {
//				if (sp_which.getSelectedItem().equals("按区域")) {
//					getMainOrderList("city_name");
//				} else if (sp_which.getSelectedItem().equals("按店铺")) {
//					getMainOrderList("store_name");
//				} else if (sp_which.getSelectedItem().equals("按员工")) {
//					getMainOrderList("user_name");
//				} else if (sp_which.getSelectedItem().equals("全部")) {
//					getMainList();
//				}
//
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent) {
//
//			}
//		});
		list_pkhub.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
//				Intent mIntent=new Intent(PKHubActivity.this,PKDetails.class);
//				mIntent.putExtra("staff_name",list.get(position).staff_name);
//				mIntent.putExtra("money", list.get(position).money);
//				startActivity(mIntent);
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		
	}

	private void initView() {
		list_pkhub = (ListView) findViewById(R.id.list_pkhub);
		tv_show = (TextView) findViewById(R.id.tv_show);
		btn_back=(Button)findViewById(R.id.btn_back);
//		sp_which = (Spinner) findViewById(R.id.sp_which);
//		splist.add("全部");
//		splist.add("按区域");
//		splist.add("按店铺");
//		splist.add("按员工");
//		ArrayAdapter<String> spadapter = new ArrayAdapter<String>(this,
//				R.layout.item_time, splist);
//		sp_which.setAdapter(spadapter);
		adapter = new StaffIncomAdapter(this, list);
		list_pkhub.setAdapter(adapter);
	}

	private void getMainList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getPk" };
		String[] idParam2 = new String[] { "id", MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this, null, "正在加载", false);
		progressDialog.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (progressDialog!= null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mStaffIncomeListObject = JSON.parseObject(v,
						StaffIncomeListObject.class);
				list = mStaffIncomeListObject.response;
				tv_show.setVisibility(View.GONE);
				list_pkhub.setVisibility(View.VISIBLE);
				adapter = new StaffIncomAdapter(PKHubActivity.this, list);
				list_pkhub.setAdapter(adapter);
			} else {
				tv_show.setVisibility(View.VISIBLE);
				list_pkhub.setVisibility(View.GONE);
				Toast.makeText(PKHubActivity.this, "没有显示的数据",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

//	// 按条件查询显示结果
//	private void getMainOrderList(String order) {
//		ArrayList<String[]> params2 = new ArrayList<String[]>();
//		String[] funParam2 = new String[] { "fun", "getOrderbyPK" };
//		String[] nameParam = new String[] { "order", order };
//		params2.add(funParam2);
//		params2.add(nameParam);
//		progressDialog = ProgressDialog.show(this, null, "正在加载", false);
//		progressDialog.setCancelable(true);
//		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
//		if (Constant.NET_STATUS) {
//			new HttpConnection().get(Constant.URL, params2,
//					getOrderList_callbackListener);
//		} else {
//			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
//		}
//	}
//
//	private CallbackListener getOrderList_callbackListener = new HttpConnection.CallbackListener() {
//		@Override
//		public void callBack(String v) {
//			// TODO Auto-generated method stub
//			Log.d("返回数据", v);
//			// 对设备注册结果进行解析
//			if (progressDialog!= null) {
//				progressDialog.dismiss();
//				progressDialog = null;
//			}
//			if (!v.equals("fail")) {// 当请求网络返回值正常
//				mStaffIncomeListObject = JSON.parseObject(v,
//						StaffIncomeListObject.class);
//				list = mStaffIncomeListObject.response;
//				if (mStaffIncomeListObject.meta.getMsg().equals("OK")) {
//					tv_show.setVisibility(View.GONE);
//					list_pkhub.setVisibility(View.VISIBLE);
//					adapter = new StaffIncomAdapter(PKHubActivity.this, list);
//					list_pkhub.setAdapter(adapter);
//				} else {
//					tv_show.setVisibility(View.VISIBLE);
//					list_pkhub.setVisibility(View.GONE);
//					Toast.makeText(PKHubActivity.this, "无数据",
//							Toast.LENGTH_SHORT).show();
//				}
//			} else {
//				tv_show.setVisibility(View.VISIBLE);
//				list_pkhub.setVisibility(View.GONE);
//				Toast.makeText(PKHubActivity.this, "无数据", Toast.LENGTH_SHORT)
//						.show();
//			}
//		}
//	};


}
