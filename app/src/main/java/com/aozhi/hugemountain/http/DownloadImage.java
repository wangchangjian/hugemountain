package com.aozhi.hugemountain.http;


import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.aozhi.hugemountain.utils.Constant;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class DownloadImage {

    private ExecutorService executorService;
    private ImageMemoryCache imageMemoryCache;
    private ImageFileCache imageFileCache;
    private DownloadImageMode downloadImageMode;
    private Map<String, View> taskMap;

    private int POOL_SIZE = 6;

    public DownloadImage() {
        final int cpuNums = Runtime.getRuntime().availableProcessors();
        executorService = Executors.newFixedThreadPool(cpuNums * POOL_SIZE);

        imageMemoryCache = new ImageMemoryCache();
        imageFileCache = new ImageFileCache();
        downloadImageMode = new DownloadImageMode();
        taskMap = new HashMap<String, View>();
    }

//	public void addTask(String url, ImageView img) {
//		addTask(url, img, null);
//	}

    public void addTask(String url, View img, ImageCallback callback) {
        url = Constant.DOWNLOAD + url;

        Log.i("imageLoaded", url);
        if (img == null) {
            return;
        }

        if (TextUtils.isEmpty(url)) {
            return;
        }

        if (callback != null) {
            downloadImageMode = new DownloadImageMode();
            downloadImageMode.setCallback(callback);
            downloadImageMode.setImageUrl(url);
            Log.v("DownloadImage", "downloadImageMode =====" + downloadImageMode + ", url "
                    + url);
            img.setTag(downloadImageMode);
        } else {
            img.setTag(url);
        }

        final Bitmap bitmap = imageMemoryCache.getBitmapFromCache(url);
        if (bitmap != null) {
            if (callback != null) {
                callback.imageLoaded(bitmap, downloadImageMode);
            } else {
                if (img instanceof ImageView/* || view instanceof ImageZoomView */) {
                    ((ImageView) img).setImageBitmap(bitmap);
                }
            }
        } else {
            if (taskMap != null) {
                synchronized (taskMap) {
                    final String mapKey = Integer.toString(img.hashCode());
                    if (!taskMap.containsKey(mapKey)) {
                        taskMap.put(mapKey, img);
                    }
                }
            }
        }
    }

    public void addTask1(String url, View img, ImageCallback callback) {
        url = Constant.DOWNLOAD + url;
        if (img == null) {
            return;
        }

        if (TextUtils.isEmpty(url)) {
            return;
        }

        if (callback != null) {
            downloadImageMode = new DownloadImageMode();
            downloadImageMode.setCallback(callback);
            downloadImageMode.setImageUrl(url);
            Log.v("DownloadImage", "downloadImageMode =====" + downloadImageMode + ", url " + url);
            img.setTag(downloadImageMode);
        } else {
            img.setTag(url);
        }

        final Bitmap bitmap = imageMemoryCache.getBitmapFromCache(url);
        if (bitmap != null) {
            if (callback != null) {
                callback.imageLoaded(bitmap, downloadImageMode);
            } else {
                if (img instanceof ImageView/* || view instanceof ImageZoomView */) {
                    ((ImageView) img).setImageBitmap(bitmap);
                }
            }
        } else {
            if (taskMap != null) {
                synchronized (taskMap) {
                    final String mapKey = Integer.toString(img.hashCode());
                    if (!taskMap.containsKey(mapKey)) {
                        taskMap.put(mapKey, img);
                    }
                }
            }
        }
    }

    public void addTaskLocalhost(String file, View img) {
        if (file == null) {
            return;
        }

        if (!new File(file).exists()) {
            return;
        }

        final Bitmap bitmap = imageMemoryCache.getBitmapFromCache(file);
        if (bitmap != null) {
            if (img instanceof ImageView/* || view instanceof ImageZoomView */) {
                ((ImageView) img).setImageBitmap(bitmap);
            }
        } else {
            Bitmap bitmap2 = BitmapFactory.decodeFile(file);
            imageMemoryCache.addBitmapToCache(file, bitmap2);
            if (img instanceof ImageView/* || view instanceof ImageZoomView */) {
                ((ImageView) img).setImageBitmap(bitmap2);
            }
        }
    }

    public void doTask() {
        if (taskMap == null) {
            return;
        } else {
            synchronized (taskMap) {
                Collection<View> collection = taskMap.values();

                for (View view : collection) {
                    if (view != null) {
                        Object object = view.getTag();
                        String url = "";
                        if (object instanceof DownloadImageMode) {
                            url = ((DownloadImageMode) object).getImageUrl();
                        } else {
                            url = (String) object;
                        }

                        if (!TextUtils.isEmpty(url)) {
                            loadImage(url, view);
                        }
                    }
                }

            }
        }
    }

    public void addTasks(String url, View img, ImageCallback callback) {
        url = Constant.DOWNLOAD + url;
        if (img == null) {
            return;
        }

        if (TextUtils.isEmpty(url)) {
            return;
        }

        if (callback != null) {
            downloadImageMode = new DownloadImageMode();
            downloadImageMode.setCallback(callback);
            downloadImageMode.setImageUrl(url);
            Log.v("DownloadImage", "downloadImageMode =====" + downloadImageMode + ", url "
                    + url);
            img.setTag(downloadImageMode);
        } else {
            img.setTag(url);
        }

        final Bitmap bitmap = imageMemoryCache.getBitmapFromCache(url);
        if (bitmap != null) {
            if (callback != null) {
                callback.imageLoaded(bitmap, downloadImageMode);
            } else {
                if (img instanceof ImageView/* || view instanceof ImageZoomView */) {
                    ((ImageView) img).setImageBitmap(bitmap);
                }
            }
        } else {
            if (taskMap != null) {
                synchronized (taskMap) {
                    final String mapKey = Integer.toString(img.hashCode());
                    if (!taskMap.containsKey(mapKey)) {
                        taskMap.put(mapKey, img);
                    }
                }
            }
        }
    }

    private void loadImage(final String url, final View img) {
        loadImage(url, img, null);
    }

    private void loadImage(final String url, final View img, ImageCallback callback) {
        executorService.submit(new TaskWithResult(new TaskHandler(url, img, callback), url));
    }

    private class TaskWithResult implements Callable<String> {

        private String url;
        private Handler handler;

        public TaskWithResult(Handler handler, String url) {
            this.url = url;
            this.handler = handler;
        }

        @Override
        public String call() throws Exception {
            final Message message = handler.obtainMessage(0, getBitmap(url));
            handler.sendMessage(message);
            return url;
        }

    }

    private class TaskHandler extends Handler {
        private String url;
        private View img;
        private ImageCallback callback;

        public TaskHandler(String url, View img, ImageCallback callback) {
            Log.i("TaskHandler", url);
            this.url = url;
            this.img = img;
            this.callback = callback;
        }

        @Override
        public void handleMessage(Message msg) {
            final Object object = img.getTag();

            if (object instanceof DownloadImageMode) {
                final DownloadImageMode imageMode = (DownloadImageMode) object;
                imageMode.getCallback().imageLoaded((Bitmap) msg.obj, imageMode);

                if (taskMap != null) {
                    taskMap.remove(Integer.toString(img.hashCode()));
                }
            } else if (object instanceof String) {
                if (callback != null) {
                    callback.imageLoaded((Bitmap) msg.obj, url);
                } else {
                    if (object.equals(url) && msg.obj != null) {
                        final Bitmap bitmap = (Bitmap) msg.obj;
                        if (bitmap != null) {
                            if (img instanceof ImageView) {
                                ((ImageView) img).setImageBitmap(bitmap);
                            }
                        }
                    }
                }
                if (taskMap != null) {
                    taskMap.remove(Integer.toString(img.hashCode()));
                }
            }
        }
    }

    private Bitmap getBitmap(String url) {
        Bitmap bitmap = imageMemoryCache.getBitmapFromCache(url);
        if (bitmap == null) {
            bitmap = imageFileCache.getImage(url);
            if (bitmap == null) {
                bitmap = ImageGetForHttp.downloadBitmap(url);
                if (bitmap != null) {
                    imageMemoryCache.addBitmapToCache(url, bitmap);
                    imageFileCache.saveBmpToSd(bitmap, url);
                }
            } else {
                imageMemoryCache.addBitmapToCache(url, bitmap);
            }
        } else {
            Log.d("DownloadImage", url);
        }
        return bitmap;
    }

    public void deleteImageCache(String url) {
        imageMemoryCache.deleteBitmapFromCache(url);
        imageFileCache.deleteImage(url);
    }

    public interface ImageCallback {
        public void imageLoaded(Bitmap imageBitmap,
                                DownloadImageMode callBackTag);

        public void imageLoaded(Bitmap imageBitmap, String imageUrl);
    }
}
