package com.aozhi.hugemountain.activity.PublicActivity;

import com.aozhi.hugemountain.R;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class AboutActivity extends BaseActivity {

	private Button btn_back;
	private TextView tv_version;
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_about);
		tv_version=(TextView)findViewById(R.id.tv_version);
		btn_back=(Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		try {
			PackageManager manager = this.getPackageManager();
			PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
			String version = info.versionName;
			tv_version.setText("版本号:"+version);
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {

	}

}
