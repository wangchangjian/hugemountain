package com.aozhi.hugemountain.activity.StaffActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
//import com.aozhi.hugemountain.adapter.OrderAdapter;
//import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;
import com.aozhi.hugemountain.adapter.OrderStaffAdapter;
import com.aozhi.hugemountain.adapter.ViewPagerAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.aozhi.hugemountain.view.OrderView;
import com.aozhi.hugemountain.view.StaffThisMonthOrderView;
import com.aozhi.hugemountain.view.StaffTodayOrderView;
//import com.aozhi.hugemountain.model.OrderFormObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class OrderStaffActivity extends BaseActivity implements OnClickListener {

    private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
    private ArrayList<OrderFormObject> je = new ArrayList<OrderFormObject>();
    private OrderListObject mOrderListObject;
    private TextView tv_today_order, tv_thismonth_order, tv_line1, tv_line2, tv_daysum, tv_monthsum;
    private JumpProgressDialog progressDialog;
    private ViewPager vp_order;
    private List<OrderView> vp_item_views;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_order);
        initView();
        initListener();
        getStaffTotal();
    }

    @Override
    protected void initData() {

    }

    public void initView() {

        progressDialog = new JumpProgressDialog(this);
        tv_today_order = (TextView) findViewById(R.id.tv_today_order);
        tv_thismonth_order = (TextView) findViewById(R.id.tv_thismonth_order);
        tv_line1 = (TextView) findViewById(R.id.tv_line1);
        tv_line2 = (TextView) findViewById(R.id.tv_line2);
        tv_daysum = (TextView) findViewById(R.id.tv_daysum);
        tv_monthsum = (TextView) findViewById(R.id.tv_monthsum);
        tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
        tv_today_order.setTextColor(getResources().getColor(R.color.linebg));
        tv_line2.setBackgroundColor(getResources().getColor(R.color.white));
        tv_thismonth_order.setTextColor(getResources().getColor(R.color.black));

        vp_order = (ViewPager) findViewById(R.id.vp_order);
        vp_item_views = new ArrayList<OrderView>();
        vp_item_views.add(new StaffTodayOrderView(this));
        vp_item_views.add(new StaffThisMonthOrderView(this));
        vp_order.setAdapter(new ViewPagerAdapter(vp_item_views));


    }

    private void initListener() {
        tv_today_order.setOnClickListener(this);
        tv_thismonth_order.setOnClickListener(this);
        vp_order.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 0) {
                    tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
                    tv_today_order.setTextColor(getResources().getColor(R.color.linebg));
                    tv_line2.setBackgroundColor(getResources().getColor(R.color.white));
                    tv_thismonth_order.setTextColor(getResources().getColor(R.color.black));
                } else {
                    tv_line2.setBackgroundColor(getResources().getColor(R.color.linebg));
                    tv_thismonth_order.setTextColor(getResources().getColor(R.color.linebg));
                    tv_line1.setBackgroundColor(getResources().getColor(R.color.white));
                    tv_today_order.setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_today_order:
                tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
                tv_today_order.setTextColor(getResources().getColor(R.color.linebg));
                tv_line2.setBackgroundColor(getResources().getColor(R.color.white));
                tv_thismonth_order.setTextColor(getResources().getColor(R.color.black));
                vp_order.setCurrentItem(0);
                break;
            case R.id.tv_thismonth_order:
                tv_line2.setBackgroundColor(getResources().getColor(R.color.linebg));
                tv_thismonth_order.setTextColor(getResources().getColor(R.color.linebg));
                tv_line1.setBackgroundColor(getResources().getColor(R.color.white));
                tv_today_order.setTextColor(getResources().getColor(R.color.black));
                vp_order.setCurrentItem(1);
                break;
            default:
                break;
        }

    }


    private void getStaffTotal() {
        ArrayList<String[]> params1 = new ArrayList<String[]>();
        String[] funParam1 = new String[]{"fun", "getTotalmoney1"};
        String[] name1 = new String[]{"id", MyApplication.Staffuser.id};
        params1.add(funParam1);
        params1.add(name1);
        progressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(OrderStaffActivity.this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params1,
                    total_callbackListener);
        } else {
            Toast.makeText(OrderStaffActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private CallbackListener total_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (progressDialog != null) {
                progressDialog.dimissDialog();
            }
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mOrderListObject = JSON.parseObject(v, OrderListObject.class);
                je = mOrderListObject.response;
                if (mOrderListObject.meta.getMsg().equals("OK")) {
                    if (je.size() > 0) {
                        tv_daysum.setText("￥" + je.get(0).money + "元");
                    } else {
                        tv_daysum.setText("￥0元");
                    }
                    getStaffTotal1();
                } else {
                    Toast.makeText(getApplication(), "获取统计数据失败", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    private void getStaffTotal1() {
        ArrayList<String[]> params1 = new ArrayList<String[]>();
        String[] funParam1 = new String[]{"fun", "getTotalmoney2"};
        String[] name1 = new String[]{"id", MyApplication.Staffuser.id};
        params1.add(funParam1);
        params1.add(name1);
        progressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(OrderStaffActivity.this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params1,
                    total_callbackListener1);
        } else {
            Toast.makeText(OrderStaffActivity.this, "请检查网络连接", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private CallbackListener total_callbackListener1 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对设备注册结果进行解析
            if (progressDialog != null) {
                progressDialog.dimissDialog();
            }
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mOrderListObject = JSON.parseObject(v, OrderListObject.class);
                je = mOrderListObject.response;
                if (mOrderListObject.meta.getMsg().equals("OK")) {
                    if (je.size() > 0) {
                        tv_monthsum.setText("￥" + je.get(0).money + "元");
                    } else {
                        tv_monthsum.setText("￥0元");
                    }
                } else {
                    Toast.makeText(getApplication(), "获取统计数据失败", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 112:
                tv_line1.setBackgroundColor(getResources().getColor(R.color.linebg));
                tv_today_order.setTextColor(getResources().getColor(R.color.linebg));
                tv_line2.setBackgroundColor(getResources().getColor(R.color.white));
                tv_thismonth_order.setTextColor(getResources().getColor(R.color.black));
                break;

            default:
                break;
        }
    }

    ;

    private static Boolean isQuit = false;
    Timer timer = new Timer();

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isQuit == false) {
                isQuit = true;
                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
                TimerTask task = null;
                task = new TimerTask() {
                    @Override
                    public void run() {
                        isQuit = false;
                    }
                };
                timer.schedule(task, 2000);
            } else {
                finish();
                System.exit(0);
            }
        }
        return false;
    }
}
