ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:19.1.0
gson-2.4.jar => com.google.code.gson:gson:2.4

Potentially Missing Dependency:
-------------------------------
When we replaced the following .jar files with a Gradle dependency, we
inferred the dependency version number from the filename. This
specific version may not actually be available from the repository.
If you get a build error stating that the dependency is missing, edit
the version number to for example "+" to pick up the latest version
instead. (This may require you to update your code if the library APIs
have changed.)

gson-2.4.jar => version 2.4 in com.google.code.gson:gson:2.4

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* libs\alipaysdk.jar => app\libs\alipaysdk.jar
* libs\alipaysecsdk.jar => app\libs\alipaysecsdk.jar
* libs\alipayutdid.jar => app\libs\alipayutdid.jar
* libs\armeabi\libapp_BaiduNaviApplib.so => app\src\main\jniLibs\armeabi\libapp_BaiduNaviApplib.so
* libs\armeabi\libapp_BaiduVIlib.so => app\src\main\jniLibs\armeabi\libapp_BaiduVIlib.so
* libs\armeabi\libBaiduMapSDK_v3_5_0_31.so => app\src\main\jniLibs\armeabi\libBaiduMapSDK_v3_5_0_31.so
* libs\armeabi\libbd_etts.so => app\src\main\jniLibs\armeabi\libbd_etts.so
* libs\armeabi\libbd_wsp_v1_0.so => app\src\main\jniLibs\armeabi\libbd_wsp_v1_0.so
* libs\armeabi\libbds.so => app\src\main\jniLibs\armeabi\libbds.so
* libs\armeabi\libBDSpeechDecoder_V1.so => app\src\main\jniLibs\armeabi\libBDSpeechDecoder_V1.so
* libs\armeabi\libcurl.so => app\src\main\jniLibs\armeabi\libcurl.so
* libs\armeabi\libgnustl_shared.so => app\src\main\jniLibs\armeabi\libgnustl_shared.so
* libs\armeabi\libjpush182.so => app\src\main\jniLibs\armeabi\libjpush182.so
* libs\armeabi\liblocSDK5.so => app\src\main\jniLibs\armeabi\liblocSDK5.so
* libs\armeabi\libsmssdk.so => app\src\main\jniLibs\armeabi\libsmssdk.so
* libs\BaiduLBS_Android.jar => app\libs\BaiduLBS_Android.jar
* libs\BaiduNaviSDK_3.0.0.jar => app\libs\BaiduNaviSDK_3.0.0.jar
* libs\beecloud-2.1.1.jar => app\libs\beecloud-2.1.1.jar
* libs\calligraphy-1.2.0.jar => app\libs\calligraphy-1.2.0.jar
* libs\Cashier_SDK-v4.2.0.jar => app\libs\Cashier_SDK-v4.2.0.jar
* libs\fastjson-1.1.26-android.jar => app\libs\fastjson-1.1.26-android.jar
* libs\httpmime-4.1.2.jar => app\libs\httpmime-4.1.2.jar
* libs\jpush-sdk-release1.8.2.jar => app\libs\jpush-sdk-release1.8.2.jar
* libs\libammsdk.jar => app\libs\libammsdk.jar
* libs\locSDK_5.2.jar => app\libs\locSDK_5.2.jar
* libs\MobLogCollector.jar => app\libs\MobLogCollector.jar
* libs\MobTools.jar => app\libs\MobTools.jar
* libs\PayPalAndroidSDK-2.11.2.jar => app\libs\PayPalAndroidSDK-2.11.2.jar
* libs\pinyin4j-2.5.0.jar => app\libs\pinyin4j-2.5.0.jar
* libs\SMSSDK-1.3.0.jar => app\libs\SMSSDK-1.3.0.jar
* libs\universal-image-loader-1.9.1.jar => app\libs\universal-image-loader-1.9.1.jar
* libs\UPPayAssistEx.jar => app\libs\UPPayAssistEx.jar
* libs\UPPayPluginEx.jar => app\libs\UPPayPluginEx.jar
* libs\zxing-3.2.0.jar => app\libs\zxing-3.2.0.jar
* lint.xml => app\lint.xml
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
