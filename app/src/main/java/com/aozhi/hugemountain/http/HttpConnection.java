package com.aozhi.hugemountain.http;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.aozhi.hugemountain.utils.ConnectionManager;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Asynchronous HTTP connections
 * 
 * @author Greg Zavitz & Joseph Roth
 */
public class HttpConnection implements Runnable {

	public static final int DID_START = 0;
	public static final int DID_ERROR = 1;
	public static final int DID_SUCCEED = 2;

	private static final int GET = 0;
	private static final int POST = 1;
	private static final int PUT = 2;
	private static final int DELETE = 3;
	private static final int BITMAP = 4;
	private static final int GET_HTTPCONNECTION = 5;

	private String url;
	private int method;
	private String data;
	private ArrayList<String[]> arrayStr;
	private CallbackListener listener;

	private HttpClient httpClient;

	// public HttpConnection() {
	// this(new Handler());
	// }

	public void create(int method, String url, ArrayList<String[]> arrayStr,
			CallbackListener listener) {
		this.method = method;
		this.url = url;
		this.arrayStr = arrayStr;
		this.listener = listener;
		ConnectionManager.getInstance().push(this);
	}

	public void get(String url, ArrayList<String[]> arrayStr,
			CallbackListener listener) {
		create(GET, url, arrayStr, listener);
	}

	public void post(String url, ArrayList<String[]> arrayStr,
			CallbackListener listener) {
		create(POST, url, arrayStr, listener);
	}

	public void get_httpconnection(String url, ArrayList<String[]> arrayStr,
			CallbackListener listener) {
		create(GET_HTTPCONNECTION, url, arrayStr, listener);
	}

	public void put(String url, ArrayList<String[]> arrayStr) {
		create(PUT, url, arrayStr, listener);
	}

	public void delete(String url) {
		create(DELETE, url, null, listener);
	}

	public void bitmap(String url) {
		create(BITMAP, url, null, listener);
	}

	public interface CallbackListener {
		public void callBack(String result);
	}

	private static final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message message) {
			switch (message.what) {
			case HttpConnection.DID_START: {
				break;
			}
			case HttpConnection.DID_SUCCEED: {
				CallbackListener listener = (CallbackListener) message.obj;
				Object data = message.getData();
				if (listener != null) {
					if (data != null) {
						Bundle bundle = (Bundle) data;
						String result = bundle.getString("callbackkey");
						listener.callBack(result);
					}
				}
				break;
			}
			case HttpConnection.DID_ERROR: {
				break;
			}
			}
		}
	};

	public void run() {

		httpClient = getHttpClient();
		try {
			HttpResponse httpResponse = null;
			switch (method) {
			case GET:
				for (int i = 0; i < arrayStr.size(); i++) {
					String[] str = arrayStr.get(i);
					if (i == 0) {
						str[1] = URLEncoder.encode(str[1]);
						url = url + "?" + str[0] + "=" + str[1];
					} else if (i != 0) {
						str[1] = URLEncoder.encode(str[1]);
						url = url + "&" + str[0] + "=" + str[1];
					}

				}
				Log.e("httpconnection", url);
				httpResponse = httpClient.execute(new HttpGet(url));
				if (isHttpSuccessExecuted(httpResponse)) {
					String result = EntityUtils.toString(httpResponse
							.getEntity());
					this.sendMessage(result);
				} else {
					this.sendMessage("fail");
				}
				break;
			case POST:
				HttpPost httpPost = new HttpPost(url);
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				if (arrayStr != null) {
					for (int i = 0; i < arrayStr.size(); i++) {
						String[] str = arrayStr.get(i);
						params.add(new BasicNameValuePair(str[0], str[1]));
					}
				}
				BasicNameValuePair valuesPair = new BasicNameValuePair("args",
						data);
				params.add(valuesPair);
				httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
				httpResponse = httpClient.execute(httpPost);
				if (isHttpSuccessExecuted(httpResponse)) {
					String result = EntityUtils.toString(httpResponse
							.getEntity());
					this.sendMessage(result);
				} else {
					this.sendMessage("fail");
				}
				break;
			case GET_HTTPCONNECTION:
				for (int i = 0; i < arrayStr.size(); i++) {
					String[] str = arrayStr.get(i);
					if (i == 0) {
						url = url + "?" + str[0] + "=" + str[1];
					} else if (i != 0) {
						url = url + "&" + str[0] + "=" + str[1];
					}

				}
				String html = null;
				InputStream stream = null;
				StringBuffer output = new StringBuffer("");
				URL urlstr = new URL(url);
				HttpURLConnection conn = (HttpURLConnection) urlstr
						.openConnection();
				conn.setRequestMethod("GET");
				conn.setUseCaches(false);
				// conn.set
				conn.setConnectTimeout(5000);
				conn.connect();
				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					stream = conn.getInputStream();
				}
				// BufferedReader buffer = new BufferedReader(new
				// InputStreamReader(stream));
				// String s = "";
				// while ((s = buffer.readLine()) != null)
				// output.append(s);

				byte[] data = readStream(stream);
				html = new String(data);
				// this.sendMessage(output.toString());
				this.sendMessage(html);
				// .close();
				stream.close();
				conn.disconnect();
				break;
			}
		} catch (Exception e) {
			this.sendMessage("fail");
		}
		ConnectionManager.getInstance().didComplete(this);
	}

	public byte[] readStream(InputStream inputStream) throws Exception {
		byte[] buffer = new byte[1024];
		int len = -1;
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		while ((len = inputStream.read(buffer)) != -1) {
			byteArrayOutputStream.write(buffer, 0, len);
		}

		inputStream.close();
		byteArrayOutputStream.close();
		return byteArrayOutputStream.toByteArray();
	}

	// private void processBitmapEntity(HttpEntity entity) throws IOException {
	// BufferedHttpEntity bufHttpEntity = new BufferedHttpEntity(entity);
	// Bitmap bm = BitmapFactory.decodeStream(bufHttpEntity.getContent());
	// handler.sendMessage(Message.obtain(handler, DID_SUCCEED, bm));
	// }

	private void sendMessage(String result) {
		Message message = Message.obtain(handler, DID_SUCCEED, listener);
		Bundle data = new Bundle();
		data.putString("callbackkey", result);
		message.setData(data);
		handler.sendMessage(message);

	}

	public static DefaultHttpClient getHttpClient() {
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 20000);
		HttpConnectionParams.setSoTimeout(httpParams, 20000);
		// HttpConnectionParams.setSocketBufferSize(httpParams, 8192);

		DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
		return httpClient;
	}

	public static boolean isHttpSuccessExecuted(HttpResponse response) {
		int statusCode = response.getStatusLine().getStatusCode();
		return (statusCode > 199) && (statusCode < 400);
	}

}