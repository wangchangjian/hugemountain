package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.VipCenterListObject;
import com.aozhi.hugemountain.model.VipCenterObject;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReChargeMoneytAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<VipCenterObject> list = new ArrayList<VipCenterObject>();

	public ReChargeMoneytAdapter(Context ctx, ArrayList<VipCenterObject> list2) {
		mContext = ctx;
		list = list2;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_recharge_money, null);
			mHolder.time = (TextView) convertView
					.findViewById(R.id.time);
			mHolder.recharge = (TextView) convertView.findViewById(R.id.recharge);
			mHolder.balance = (TextView) convertView.findViewById(R.id.balance);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.time.setText(list.get(position).create_time);
		mHolder.recharge.setText("￥"+list.get(position).money);
		mHolder.balance.setText("￥"+list.get(position).recharge_money);
		return convertView;
	}

	class Holder {
		TextView time, recharge, balance;
	}
}
