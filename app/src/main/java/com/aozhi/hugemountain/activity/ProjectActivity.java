package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ProjectAdapter;
import com.aozhi.hugemountain.model.ProjectListObject;
import com.aozhi.hugemountain.model.ProjectObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ProjectActivity extends Activity {
	private Button btn_back;
	private TextView releases;
	private LinearLayout details;
	final int RESULT_CODE = 101;
	final int REQUEST_CODE = 1;
	private ProgressDialog progressDialog = null;
	private ArrayList<ProjectObject> list = new ArrayList<ProjectObject>();
	private ProjectListObject mProjectListObject;
	private ProjectAdapter mProjectAdapter;
	private ListView list_project;
	private GridView grid_project1;
	private String id = "";
	private TextView nodata;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_project);
		list_project = (ListView) findViewById(R.id.list_project);
		grid_project1 = (GridView) findViewById(R.id.list_project1);
		nodata=(TextView) findViewById(R.id.nodata);
		mProjectAdapter = new ProjectAdapter(this, list);
		grid_project1.setAdapter(mProjectAdapter);
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		releases = (TextView) findViewById(R.id.releases);
		releases.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(ProjectActivity.this,
						ProjectReleasesActivity.class);
				startActivityForResult(intent, REQUEST_CODE);
			}
		});
		getProjectList();
//		list_project.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//					long arg3) {
//				// TODO Auto-generated method stub
//				ProjectObject mProjectObject = list.get(arg2);
//				Intent intent = new Intent(ProjectActivity.this,
//						ProjectDetailsActivity.class);
//				intent.putExtra("mProjectObject", mProjectObject);
//				startActivity(intent);
//			}
//		});
//		list_project.setOnItemLongClickListener(new OnItemLongClickListener() {
//			@Override
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
//					int arg2, long arg3) {
//				// TODO Auto-generated method stub
//				id = list.get(arg2).store_service_id;
//				Deldialog();
//				return true;
//			}
//		});
		grid_project1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long ids) {
				ProjectObject mProjectObject = list.get(position);
				Intent intent = new Intent(ProjectActivity.this,
						ProjectDetailsActivity.class);
				intent.putExtra("mProjectObject", mProjectObject);
				startActivityForResult(intent,152);
			}
		});
		grid_project1.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long ids) {
				id = list.get(position).store_service_id;
				Deldialog();
				return true;
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("", "onActivityResult" + "requestCode" + requestCode
				+ "\n resultCode=" + resultCode);
		if (requestCode == REQUEST_CODE) {
			if (resultCode == RESULT_CODE) {
				getProjectList();
			}
		}
		switch (resultCode) {
		case 152:
			getProjectList();
			break;
		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void getProjectList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getProjectList" };
		String[] idParam2 = new String[] { "sid",
				MyApplication.Storeuser.getId() };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mProjectListObject = JSON.parseObject(v,
						ProjectListObject.class);
				list = mProjectListObject.response;
				if (list.size() > 0) {
					nodata.setVisibility(View.GONE);
					grid_project1.setVisibility(View.VISIBLE);
					mProjectAdapter = new ProjectAdapter(ProjectActivity.this,
							list);
					grid_project1.setAdapter(mProjectAdapter);
				}else{
					nodata.setVisibility(View.VISIBLE);
					grid_project1.setVisibility(View.GONE);
				}

			} else {
				Toast.makeText(ProjectActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void Deldialog() {
		Dialog alertDialog = new AlertDialog.Builder(this).setTitle("确定删除吗？")
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						upOrderList(id);
					}
				})
				.setNegativeButton("取消", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
					}
				}).create();
		alertDialog.show();
	}

	private void upOrderList(String id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upProject" };
			String[] pageParam = new String[] { "id", id };
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					upOrderList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener upOrderList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {

				if (result.indexOf("OK") != -1) {
					Toast.makeText(ProjectActivity.this, "删除成功",
							Toast.LENGTH_LONG).show();
					getProjectList();

				} else {
					Toast.makeText(ProjectActivity.this, "删除失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(ProjectActivity.this, "删除失败", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
}
