package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.StaffObject;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PreordainStaffAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();

	public PreordainStaffAdapter(Context ctx, ArrayList<StaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_preordainstaff, null);
			mHolder.phone=(ImageView)convertView.findViewById(R.id.photo);
			mHolder.name=(TextView)convertView.findViewById(R.id.name);
			mHolder.age=(TextView)convertView.findViewById(R.id.age);
			mHolder.tv_ty=(TextView)convertView.findViewById(R.id.tv_ty);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText("员工编号："+list.get(position).code_id);
		mHolder.age.setText("员工年龄："+list.get(position).age);
		mHolder.tv_ty.setText("特约费用：￥"+list.get(position).surchange);
		if (list.get(position).avatar.equals("")|| list.get(position).avatar==null ) {
			
		} else {
			MyApplication.downloadImage.addTask(
					list.get(position).avatar, mHolder.phone,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.phone.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		return convertView;
	}
	
	class Holder {
		ImageView phone;
		TextView name,age,tv_ty;
	}
}
