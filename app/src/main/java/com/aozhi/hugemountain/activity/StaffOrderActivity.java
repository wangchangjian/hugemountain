package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StaffOrderAdapter;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Utils;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class StaffOrderActivity extends Activity {
	Button btn_back;
	TextView t1, t2, t3, b1, b2, b3;
	private ArrayList<OrderFormObject> list= new ArrayList<OrderFormObject>();
	private StaffOrderAdapter adapter1;
	private ListView list_1;
	private String type = "1";
	private OrderListObject mOrderListObject;
	private ProgressDialog progressDialog = null;
	private TextView nodata;
	private Spinner sp;
	private String fun="";
	private int requestCode=0;
	private int requestCode1=1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_stafforder);
		initView();
		initListener();
	    if(MyApplication.isstaff){
	    	fun="getStaffOrderList1";
	    }else{
	    	fun="getStaffOrderList1";
	    }
	    getorder(fun,"10");
	}

	private void initListener() {
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});

		t1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t1.setTextColor(getResources().getColor(R.color.y));
				t2.setTextColor(getResources().getColor(R.color.n));
				t3.setTextColor(getResources().getColor(R.color.n));
				b1.setBackgroundColor(getResources().getColor(R.color.y));
				b2.setBackgroundColor(getResources().getColor(R.color.bg));
				b3.setBackgroundColor(getResources().getColor(R.color.bg));
				getorder(fun,"10");
			}
		});

		t2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t2.setTextColor(getResources().getColor(R.color.y));
				t1.setTextColor(getResources().getColor(R.color.n));
				t3.setTextColor(getResources().getColor(R.color.n));
				b2.setBackgroundColor(getResources().getColor(R.color.y));
				b1.setBackgroundColor(getResources().getColor(R.color.bg));
				b3.setBackgroundColor(getResources().getColor(R.color.bg));
				getorder(fun,"20");
			}
		});

		t3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				t3.setTextColor(getResources().getColor(R.color.y));
				t1.setTextColor(getResources().getColor(R.color.n));
				t2.setTextColor(getResources().getColor(R.color.n));
				b3.setBackgroundColor(getResources().getColor(R.color.y));
				b1.setBackgroundColor(getResources().getColor(R.color.bg));
				b2.setBackgroundColor(getResources().getColor(R.color.bg));
				getorder(fun,"30");
			}
		});
//		ArrayList<String> sp_list = new ArrayList<String>();
//		sp_list.add("全部");
//		sp_list.add("当日");
//		sp_list.add("当月");
//		ArrayAdapter<String> spadapter = new ArrayAdapter<String>(
//				getApplicationContext(), R.layout.item_time, sp_list);
//		sp.setAdapter(spadapter);
		
		list_1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if(list.get(arg2).type_mark.equals("1")){
					Intent mIntent = new Intent(StaffOrderActivity.this,OrderFormDetailClientActivity.class);
					mIntent.putExtra("order_id", list.get(arg2).order_id);
					mIntent.putExtra("status", list.get(arg2).orderstatus);
					mIntent.putExtra("type", "");
					startActivityForResult(mIntent, 112); 
				}
				if(list.get(arg2).type_mark.equals("2")){
					Intent mIntent = new Intent(StaffOrderActivity.this,PreordainPayActivity.class);
					mIntent.putExtra("order_id", list.get(arg2).order_id);
					mIntent.putExtra("status", list.get(arg2).orderstatus);
					mIntent.putExtra("paytype", list.get(arg2).paytype);
					mIntent.putExtra("orderstatus1", list.get(arg2).orderstatus1);
					mIntent.putExtra("type", "store");
//					startActivity(mIntent);
					startActivityForResult(mIntent, requestCode);  
				}
			}
		});
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		t1 = (TextView) findViewById(R.id.tv_service);
		t2 = (TextView) findViewById(R.id.tv_staff);
		t3 = (TextView) findViewById(R.id.tv_store_detail);
		b1 = (TextView) findViewById(R.id.b1);
		b2 = (TextView) findViewById(R.id.b2);
		b3 = (TextView) findViewById(R.id.b3);
		nodata = (TextView) findViewById(R.id.nodata);
//		sp = (Spinner) findViewById(R.id.sp);
		list_1 = (ListView) findViewById(R.id.list_1);
		adapter1 = new StaffOrderAdapter(this, list);
		list_1.setAdapter(adapter1);

	}

	// getStaffOrderList&id=1&states=10
	private void getorder(String fun,String status) {
		list.clear();
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", fun };
		String[] name1 = new String[] { "store_id", MyApplication.Storeuser.getId() };
		String[] name2 = new String[] { "orderstatus", status };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备注册结果进行解析

			if (!v.equals("fail")) {// 当请求网络返回值正常
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				mOrderListObject = JSON.parseObject(v, OrderListObject.class);
				list = mOrderListObject.response;
				if (mOrderListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						nodata.setVisibility(View.GONE);
						list_1.setVisibility(View.VISIBLE);
						adapter1 = new StaffOrderAdapter(
								StaffOrderActivity.this, list);
						list_1.setAdapter(adapter1);
					} else {
						nodata.setVisibility(View.VISIBLE);
						list_1.setVisibility(View.GONE);
					}
				} else {
					nodata.setVisibility(View.VISIBLE);
					list_1.setVisibility(View.GONE);
					Toast.makeText(getApplicationContext(), "获取失败",
							Toast.LENGTH_SHORT).show();
				}
			}
		}
	};
	
	 // 回调方法，从第二个页面回来的时候会执行这个方法  
    @Override  
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
    	if(data!=null){
    	 String issure = data.getStringExtra("issure");  
        switch (requestCode) {  
        case 0:  
        	if(issure.equals("1")){
        		getorder(fun,"10");
        	}else{
        		getorder(fun,"20");
        	}
        	
            break;  
          
        default:  
            break;  
        }  }
    }  
}
