package com.aozhi.hugemountain.activity;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.adapter.StorepicAdapter;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class StorePicActivity extends Activity {
	private ProgressDialog progressDialog = null;
	private Button btn_back;
	private TextView tv_version,yv_add;
	private StorepicAdapter mStorepicAdapter;
	private ArrayList<ServiceObject> list=new ArrayList<ServiceObject>();
	private ServiceListObject mServiceListObject;
	private GridView grid_view;
	private TextView tv_camera, tv_photo;
	private LinearLayout img_liner;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// 拍照
	public static final int PHOTOZOOM = 2; // 缩放
	public static final int PHOTORESOULT = 3;// 结果
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private Bitmap bitmap;
	private String serverFile = "";
	private String ids="";
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_storepic);
		
		btn_back=(Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		yv_add=(TextView)findViewById(R.id.yv_add);
		yv_add.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showAtaver();
			}
		});
		grid_view=(GridView)findViewById(R.id.grid_view);
		
		mStorepicAdapter=new StorepicAdapter(this, list);
		grid_view.setAdapter(mStorepicAdapter);
		getStorePic();
		
		grid_view.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				ids=list.get(arg2).id;
				quitDialog();
				return false;
			}
		});
		
		grid_view.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent mIntent=new Intent(StorePicActivity.this,ImagePagerActivity.class);
				mIntent.putExtra("images", list.get(arg2).src);
				startActivity(mIntent);
			}
		});
	}
	
	
	private void getStorePic() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun",
					"getStorePic" };
			String[] geotable_idParam = new String[] { "id", MyApplication.Storeuser.id };
			
			params.add(methodParam);
			params.add(geotable_idParam);
			
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);

			new HttpConnection().get(Constant.URL, params,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

	}
	protected void quitDialog() {   
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage("是否删除？");
		builder.setTitle(getString(R.string.dialog_prompt));
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						DelStorePic(ids);
					}
				});
		builder.setNegativeButton(getString(R.string.btn_cancel),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}
	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}

			if (!v.equals("fail")) {
				mServiceListObject = JSON.parseObject(v, ServiceListObject.class);
				list = mServiceListObject.response;
				if (list.size() > 0) {
					mStorepicAdapter=new StorepicAdapter(StorePicActivity.this, list);
					grid_view.setAdapter(mStorepicAdapter);
				}
			}
		}
	};
	
	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		// *** 主要就是在这里实现这种效果的.
		// 设置窗口的内容页面,activity_warning.xml文件中定义view内容
		window.setContentView(R.layout.renwufabu_shooting);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, 11);
				dlg.cancel();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT,
						Uri.fromFile(new File(Environment
								.getExternalStorageDirectory(), "juyue.jpg")));
				startActivityForResult(intent, 12);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode != RESULT_OK) {
			return;
		}
		if (data != null) {
			switch (requestCode) {
			case 11:
				startPhotoZoom(data.getData());
				break;
			case 12:
				Log.i("OnActivityResult", "**********************case camera!");
				File temp = new File(Environment.getExternalStorageDirectory()
						+ "/juyue.jpg");
				startPhotoZoom(Uri.fromFile(temp));
				break;

			case 13: {
				setPicToView(data);
			}
				break;

			default:
				break;
			}

		} else if (Build.MODEL.equals("MI 2C")) {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			startPhotoZoom(Uri.fromFile(temp));
		} else {
			File temp = new File(Environment.getExternalStorageDirectory()
					+ "/juyue.jpg");
			if (temp.exists()) {
				startPhotoZoom(Uri.fromFile(temp));
			}

		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			setStorePic();
			return serverFile;
		}
	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("outputX", 320);
		intent.putExtra("outputY", 320);
		intent.putExtra("return-data", true);
		startActivityForResult(intent, 13);
	}

	/**
	 * 保存裁剪之后的图片数据
	 * 
	 * @param picdata
	 */
	@SuppressLint({ "NewApi", "SdCardPath" })
	private void setPicToView(Intent picdata) {
		Bundle extras = picdata.getExtras();
		if (extras != null) {
			bitmap = extras.getParcelable("data");
			String sdStatus = Environment.getExternalStorageState();
			if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
				// Log.i("TestFile"
				// "SD card is not avaiable/writeable right now.");
				return;
			}
			@SuppressWarnings("static-access")
			String fileName = new DateFormat().format("yyyyMMdd_hhmmss",
					Calendar.getInstance(Locale.CHINA)) + ".jpg";
			File dir = new File("/sdcard/juyue/cache/");
			dir.mkdirs();// 创建文件夹

			fileName = "/sdcard/juyue/cache/" + fileName;
			String result = Utils.saveBitmapToFile(bitmap, fileName);
			if (result == null || result.trim().equals("")) {
				Utils.DisplayToast(this, "存储空间不足");
			}
			// @SuppressWarnings("unused")
			File file = new File(fileName);
			// img_add.setImageBitmap(bitmap);
			new UploadAsyncTask().execute(file);
		}
	}
	
	private void setStorePic() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun",
					"setStorePic" };
			String[] geotable_idParam = new String[] { "store_id", MyApplication.Storeuser.id };
			String[] geotable_idParam1 = new String[] { "src", serverFile };
			params.add(methodParam);
			params.add(geotable_idParam);
			params.add(geotable_idParam1);
			
			new HttpConnection().get(Constant.URL, params,
					setStorePic_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

	}

	private CallbackListener setStorePic_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mServiceListObject = JSON.parseObject(v, ServiceListObject.class);
				if(mServiceListObject.getMeta().getMsg().equals("OK")){
					Toast.makeText(StorePicActivity.this, "上传成功", Toast.LENGTH_LONG).show();
					getStorePic();
				}else{
					Toast.makeText(StorePicActivity.this, "上传失败", Toast.LENGTH_LONG).show();
				}
			}
		}
	};
	
	private void DelStorePic(String id) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun",
					"DelStorePic" };
			String[] geotable_idParam = new String[] { "id", id };
			params.add(methodParam);
			params.add(geotable_idParam);
			
			new HttpConnection().get(Constant.URL, params,
					DelStorePic_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}

	}

	private CallbackListener DelStorePic_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mServiceListObject = JSON.parseObject(v, ServiceListObject.class);
				if(mServiceListObject.getMeta().getMsg().equals("OK")){
					Toast.makeText(StorePicActivity.this, "删除成功", Toast.LENGTH_LONG).show();
					getStorePic();
				}else{
					Toast.makeText(StorePicActivity.this, "删除失败", Toast.LENGTH_LONG).show();
				}
			}
		}
	};
}
