package com.aozhi.hugemountain.activity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.StoreListsAdapter;
import com.aozhi.hugemountain.model.ConsumptionListObject;
import com.aozhi.hugemountain.model.ConsumptionObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class StaffMoneyActivity extends Activity {

	private ListView list_city;
	private ProgressDialog progressDialog = null;
	private StaffListObject mStaffListObject;
	// private ArrayList<StaffObject> list=new ArrayList<StaffObject>();
	// private StaffAdapter adapter;
	private Button btn_back;

	private TextView et_peoper, monthrevenue, tv_cash, tv_yue, tv_wangluo,
			tv_show;
	private LinearLayout li_query;
	private ListView list_shopcenter;
	private StoreListsAdapter adapter;
	private ArrayList<ConsumptionObject> list = new ArrayList<ConsumptionObject>();
	ConsumptionObject mConsumptionObject;
	ConsumptionListObject mConsumptionListObject;
	private String date = "",date2="";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_smoney);
		initView();
		initListener();
		getlist(date);
	}

	private void initListener() {
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		et_peoper.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				showMonPicker();
			}
		});
		li_query.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				if(et_peoper.getText().toString().equals("请选择时间(年月)")||et_peoper.getText().toString().equals("")){
					Toast.makeText(StaffMoneyActivity.this, "请选择时间(年月)", Toast.LENGTH_LONG).show();
				}else{
				String s = et_peoper.getText().toString().replaceAll("-", "");
				s = s.substring(2,6);
				getlist(s);
				}
			}
		});
	}

	private void initView() {
		list_city = (ListView) findViewById(R.id.list_city);
		list_shopcenter = (ListView) findViewById(R.id.list_shopcenter);
		et_peoper = (TextView) findViewById(R.id.et_peoper);
		monthrevenue = (TextView) findViewById(R.id.monthrevenue);
		tv_cash = (TextView) findViewById(R.id.tv_cash);
		tv_yue = (TextView) findViewById(R.id.tv_yue);
		tv_wangluo = (TextView) findViewById(R.id.tv_wangluo);
		tv_show = (TextView) findViewById(R.id.tv_show);
		li_query = (LinearLayout) findViewById(R.id.li_query);
		adapter = new StoreListsAdapter(this, list);
		list_shopcenter.setAdapter(adapter);
		// adapter=new StaffAdapter(StaffMoneyActivity.this, list);
		// list_city.setAdapter(adapter);

		// 计算当前年份和月份
		SimpleDateFormat df = new SimpleDateFormat("yyMM");// 设置日期格式
		date = df.format(new Date());

		SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM");// 设置日期格式
		date2 = df2.format(new Date());
		et_peoper.setText(date2);
	}

	public void getlist(String date) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getstaffmoney" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		String[] dates = new String[] { "date", date };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(dates);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getlist_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getlist_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mConsumptionListObject = JSON.parseObject(v,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (list.size() > 0) {
					if(!list.get(0).m.equals("")&&list.get(0).m!=null){
						monthrevenue.setText("￥"+list.get(0).m);
						if(!list.get(0).m1.equals("")&&list.get(0).m1!=null){
							tv_cash.setText("￥"+list.get(0).m1);
						}
						if(!list.get(0).m2.equals("")&&list.get(0).m2!=null){
							tv_yue.setText("￥"+list.get(0).m2);
						}
						if(!list.get(0).m3.equals("")&&list.get(0).m3!=null){
							tv_wangluo.setText("￥"+list.get(0).m3);
						}
						getStaffCollect();
					}else{
						monthrevenue.setText("");
						tv_cash.setText("");
						tv_yue.setText("");
						tv_wangluo.setText("");
						list.clear();
						tv_show.setVisibility(View.VISIBLE);
						list_shopcenter.setVisibility(View.GONE);
						Toast.makeText(StaffMoneyActivity.this, "当前月没有数据",
								Toast.LENGTH_LONG).show();
					}
				} 
			}else{
				Toast.makeText(StaffMoneyActivity.this, "获取数据失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};
	
	
	private void getStaffCollect() {
		// fun=getCollect&staff_id=20151106093853
		ArrayList<String[]> params = new ArrayList<String[]>();
		String[] methodParam = new String[] { "fun", "getstaffmoenybydate" };
		String[] pageParam = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		String[] dates = new String[] { "date", date };
		params.add(methodParam);
		params.add(pageParam);
		params.add(dates);
		progressDialog = ProgressDialog.show(this, null, "正在加载中。。。", false);
		progressDialog.setCancelable(true);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params,
					getGoodsList_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
	}

	private CallbackListener getGoodsList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mConsumptionListObject = JSON.parseObject(result,
						ConsumptionListObject.class);
				list = mConsumptionListObject.response;
				if (mConsumptionListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						adapter = new StoreListsAdapter(
								StaffMoneyActivity.this, list);
						list_shopcenter.setAdapter(adapter);
						list_shopcenter.setVisibility(View.VISIBLE);
						tv_show.setVisibility(View.GONE);
					} else {

						Toast.makeText(StaffMoneyActivity.this, "无数据",
								Toast.LENGTH_SHORT).show();
					}
				}
			} else {

			}
		}
	};

	/**
	 * 重写datePicker 1.只显示 年-月 2.title 只显示 年-月
	 * 
	 * @author lmw
	 */
	public class MonPickerDialog extends DatePickerDialog {
		public MonPickerDialog(Context context, OnDateSetListener callBack,
				int year, int monthOfYear, int dayOfMonth) {
			super(context, callBack, year, monthOfYear, dayOfMonth);
			this.setTitle(year + "年" + (monthOfYear + 1) + "月");
			((ViewGroup) ((ViewGroup) this.getDatePicker().getChildAt(0))
					.getChildAt(0)).getChildAt(2).setVisibility(View.GONE);
		}

		@Override
		public void onDateChanged(DatePicker view, int year, int month, int day) {
			super.onDateChanged(view, year, month, day);
			this.setTitle(year + "年" + (month + 1) + "月");
		}

	}

	public void showMonPicker() {
		final Calendar localCalendar = Calendar.getInstance();
		localCalendar.setTime(strToDate("yyyy-MM", et_peoper.getText()
				.toString()));
		new MonPickerDialog(this, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				localCalendar.set(1, year);
				localCalendar.set(2, monthOfYear);
				et_peoper.setText(clanderTodatetime(localCalendar, "yyyy-MM"));
			}
		}, localCalendar.get(1), localCalendar.get(2), localCalendar.get(5))
				.show();
	}

	// 字符串类型日期转化成date类型
	public static Date strToDate(String style, String date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	public static String dateToStr(String style, Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(date);
	}

	public static String clanderTodatetime(Calendar calendar, String style) {
		SimpleDateFormat formatter = new SimpleDateFormat(style);
		return formatter.format(calendar.getTime());
	}
}
