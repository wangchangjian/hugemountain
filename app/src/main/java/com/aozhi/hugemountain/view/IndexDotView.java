package com.aozhi.hugemountain.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${wangchangjian} on 2016/4/8.
 */
public class IndexDotView extends View {
    private List<PointF> pointFs;
    private PointF mMovePoint;
    private int nums = 0;
    private static final float RADIUS = 10;
    private Paint mStaticDotPaint;
    private Paint mMoveDotPaint;

    public void setNums(int nums) {
        this.nums = nums;
        this.requestLayout();
    }

    public IndexDotView(Context context) {
        this(context, null);
    }

    public IndexDotView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IndexDotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mStaticDotPaint = new Paint();
        mStaticDotPaint.setColor(Color.GRAY);
        mStaticDotPaint.setAntiAlias(true);
        mStaticDotPaint.setStyle(Paint.Style.FILL);

        mMoveDotPaint = new Paint();
        mMoveDotPaint.setColor(Color.WHITE);
        mMoveDotPaint.setAntiAlias(true);
        mMoveDotPaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < nums; i++) {
            canvas.drawCircle(pointFs.get(i).x, pointFs.get(i).y, RADIUS, mStaticDotPaint);
        }
        if (mMovePoint != null)
            canvas.drawCircle(mMovePoint.x, mMovePoint.y, RADIUS, mMoveDotPaint);
    }

    private float devide;

    public void setMoveDotPercent(float percent) {
        if (pointFs.size() != 0)
            mMovePoint.x = pointFs.get(0).x + percent * devide * nums;
        invalidate();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        devide = Math.abs(right - left) / (nums + 1);
        float verticalCenter = Math.abs(bottom - top) / 2;
        pointFs = new ArrayList<PointF>();
        for (int i = 0; i < nums; i++) {
            PointF pointF = new PointF(devide * (i + 1), verticalCenter);
            pointFs.add(pointF);
        }
        if (pointFs.size() > 0)
            mMovePoint = new PointF(pointFs.get(0).x, pointFs.get(0).y);
    }
}
