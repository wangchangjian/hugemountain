package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.AddOrderActivity;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.activity.LocationsActivity;
import com.aozhi.hugemountain.adapter.BusinessServiceAdapter;
import com.aozhi.hugemountain.adapter.BusinessStaffAdapter;
import com.aozhi.hugemountain.adapter.StorepicAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.list.HorizontalListView;
import com.aozhi.hugemountain.model.OrderFormObject;
import com.aozhi.hugemountain.model.OrderListObject;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StaffListObject;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.aozhi.hugemountain.view.OrderSelectedListener;
import com.aozhi.hugemountain.view.ServiceDetaileView;
import com.aozhi.hugemountain.view.StaffDetaileView;

public class BusinessActivity extends Activity {

    private Button btn_back, btn_ok;
    private TextView tv_service;
    TextView tv_staff;
    TextView tv_store_detail, star;
    LinearLayout item1;
    LinearLayout item2;
    LinearLayout item3;
    private HorizontalListView horizon_listview;
    private ArrayList<ServiceObject> serviceObjects = new ArrayList<ServiceObject>();
    private ArrayList<StaffObject> staffObjects = new ArrayList<StaffObject>();
    private ArrayList<StoreObject> list_store = new ArrayList<StoreObject>();
    private ArrayList<OrderFormObject> list = new ArrayList<OrderFormObject>();
    private OrderListObject mOrderListObject;
    private BusinessServiceAdapter serviceAdapter;
    private BusinessStaffAdapter staffAdapter;
    private GridView list_service;
    private GridView list_staff;
    private RelativeLayout rl_address;
    private String store_id = "";
    ServiceListObject mServiceListObject;
    StaffListObject mStaffListObject;
    StoreListObject mStoreListObject;
    StoreObject mStoreObject;
    private TextView serviceprice, staffprice, total, servicename, staffcode;
    ImageView storePhoto;
    TextView name, tv_title, business_houre, remark, address, phone, service;
    private String service_money = "0", service_id = "";
    private TextView nodata1, nodata2;
    private String type = "";
    private StorepicAdapter mStorepicAdapter;
    private ArrayList<ServiceObject> piclist = new ArrayList<ServiceObject>();
    private ServiceListObject mServiceListObject1;
    private ImageView img1, img2, img3, tv_call;
    private JumpProgressDialog jumpProgressDialog;
    private ServiceDetaileView serviceDetaileView;
    private StaffDetaileView staffDetaileView;
    private int currentServiceIndex;
    private int currentStaffIndex;
    private int lastServiceIndex;
    private int lastStaffIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_business);
        initView();
        bindClickListener();
        getBusinessServicelist();
        isFirstGetBusiness = false;
        if (!MyApplication.Status.equals("client")) {
            item1.setVisibility(View.GONE);
            item2.setVisibility(View.GONE);
            tv_service.setVisibility(View.GONE);
            tv_staff.setVisibility(View.GONE);
            list_service.setVisibility(View.GONE);
            list_staff.setVisibility(View.GONE);
            item3.setVisibility(View.GONE);
            tv_store_detail.setVisibility(View.GONE);
            tv_store_detail.setTextColor(getResources().getColor(R.color.y1));
            tv_service.setTextColor(getResources().getColor(R.color.n1));
            tv_staff.setTextColor(getResources().getColor(R.color.n1));
            tv_store_detail.setBackgroundColor(getResources().getColor(R.color.bg1));
            tv_service.setBackgroundColor(getResources().getColor(R.color.bg2));
            tv_staff.setBackgroundColor(getResources().getColor(R.color.bg2));
            item3.setVisibility(View.VISIBLE);
            item1.setVisibility(View.GONE);
            item2.setVisibility(View.GONE);
        }
    }

    private void initView() {
        serviceDetaileView = new ServiceDetaileView(this);
        staffDetaileView = new StaffDetaileView(this);
        jumpProgressDialog = new JumpProgressDialog(this);
        tv_service = (TextView) findViewById(R.id.tv_service);
        tv_staff = (TextView) findViewById(R.id.tv_staff);
        tv_store_detail = (TextView) findViewById(R.id.tv_store_detail);
        item1 = (LinearLayout) findViewById(R.id.item1);
        item2 = (LinearLayout) findViewById(R.id.item2);
        item3 = (LinearLayout) findViewById(R.id.item3);
        star = (TextView) findViewById(R.id.star);
        tv_call = (ImageView) findViewById(R.id.tv_call);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        store_id = getIntent().getStringExtra("store_id");
        list_service = (GridView) findViewById(R.id.gridlist_busness1);
        list_staff = (GridView) findViewById(R.id.gridlist_busness2);
        serviceAdapter = new BusinessServiceAdapter(this, serviceObjects);
        staffAdapter = new BusinessStaffAdapter(this, staffObjects);
        list_service.setAdapter(serviceAdapter);
        list_staff.setAdapter(staffAdapter);
        storePhoto = (ImageView) findViewById(R.id.photo);
        business_houre = (TextView) findViewById(R.id.business_houre);
        remark = (TextView) findViewById(R.id.remark);
        address = (TextView) findViewById(R.id.address_name);
        phone = (TextView) findViewById(R.id.phone);
        service = (TextView) findViewById(R.id.service);
        name = (TextView) findViewById(R.id.name);
        rl_address = (RelativeLayout) findViewById(R.id.rl_address);
        tv_title = (TextView) findViewById(R.id.tv_title);
        btn_ok = (Button) findViewById(R.id.btn_ok);
        serviceprice = (TextView) findViewById(R.id.serviceprice);
        staffprice = (TextView) findViewById(R.id.staffprice);
        total = (TextView) findViewById(R.id.total);
        servicename = (TextView) findViewById(R.id.servicename);
        staffcode = (TextView) findViewById(R.id.staffcode);
        nodata1 = (TextView) findViewById(R.id.nodata1);
        nodata2 = (TextView) findViewById(R.id.nodata2);
        MyApplication.list_business_service.clear();
        MyApplication.list_business_staff.clear();
        horizon_listview = (HorizontalListView) findViewById(R.id.horizon_listview);
        mStorepicAdapter = new StorepicAdapter(this, piclist);
        horizon_listview.setAdapter(mStorepicAdapter);
        getbusinesss();
        getStorePic();
        serviceDetaileView.setOnSelectedListener(new OrderSelectedListener() {
            @Override
            public void onBookPressed() {
                isFirstGetStaff = true;
                type="预订";

                MyApplication.list_business_service.clear();
                ServiceObject str =serviceObjects.get(currentServiceIndex);// str即为回传的值
                MyApplication.list_business_service.add(serviceObjects.get(currentServiceIndex));
                service_id = str.id;

                list_service.getChildAt(lastServiceIndex).setBackgroundResource(R.drawable.bg_item_progect_selector);
                list_service.getChildAt(currentServiceIndex).setBackgroundResource(R.drawable.bg_item_project_pressed_shape);
                lastServiceIndex = currentServiceIndex;

                service_id = serviceObjects.get(currentServiceIndex).id;
                service_money = serviceObjects.get(currentServiceIndex).money;
                servicename.setText(serviceObjects.get(currentServiceIndex).name);
                serviceprice.setText("￥" + service_money);
                total.setText("￥" + String.valueOf(Double.parseDouble(service_money)));
                tv_staff.performClick();
            }

            @Override
            public void onRightNowPressed() {
                isFirstGetStaff = true;
                type="现场";

                MyApplication.list_business_service.clear();
                ServiceObject str =serviceObjects.get(currentServiceIndex);// str即为回传的值
                MyApplication.list_business_service.add(serviceObjects.get(currentServiceIndex));
                service_id = str.id;

                list_service.getChildAt(lastServiceIndex).setBackgroundResource(R.drawable.bg_item_progect_selector);
                list_service.getChildAt(currentServiceIndex).setBackgroundResource(R.drawable.bg_item_project_pressed_shape);
                lastServiceIndex = currentServiceIndex;

                service_id = serviceObjects.get(currentServiceIndex).id;
                service_money = serviceObjects.get(currentServiceIndex).money;
                servicename.setText(serviceObjects.get(currentServiceIndex).name);
                serviceprice.setText("￥" + service_money);
                total.setText("￥" + String.valueOf(Double.parseDouble(service_money)));
                tv_staff.performClick();




            }
        });
        staffDetaileView.setOnSelectedListener(new OrderSelectedListener() {
            @Override
            public void onBookPressed() {
                StaffObject staff = staffObjects.get(currentStaffIndex);
                list_staff.getChildAt(lastStaffIndex).setBackgroundResource(R.drawable.bg_item_progect_selector);
                list_staff.getChildAt(currentStaffIndex).setBackgroundResource(R.drawable.bg_item_project_pressed_shape);
                lastStaffIndex = currentStaffIndex;
                staffprice.setText("￥" + staff.surchange);
                staffcode.setText(staff.code_id);
                total.setText("￥" + (Double.parseDouble(service_money) + Double.parseDouble(staff.surchange)));


                MyApplication.list_business_staff.clear();
                MyApplication.list_business_staff.add(staff);
            }

            @Override
            public void onRightNowPressed() {

            }
        });

        horizon_listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                Intent mIntent = new Intent(BusinessActivity.this, ImagePagerActivity.class);
                String imageslist = "";
                for (int i = 0; i < piclist.size(); i++) {
                    imageslist = imageslist + piclist.get(i).src + ",";
                }
                mIntent.putExtra("arg2", String.valueOf(arg2));
                mIntent.putExtra("imageslist", imageslist);
                startActivity(mIntent);
            }
        });
    }

    private void getStorePic() {
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] methodParam = new String[]{"fun",
                    "getStorePic"};
            String[] geotable_idParam = new String[]{"id", store_id};
            params.add(methodParam);
            params.add(geotable_idParam);
            new HttpConnection().get(Constant.URL, params,
                    type_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }

    }

    private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (!v.equals("fail")) {
                mServiceListObject1 = JSON.parseObject(v, ServiceListObject.class);
                piclist = mServiceListObject1.response;
                if (piclist.size() > 0) {
                    mStorepicAdapter = new StorepicAdapter(BusinessActivity.this, piclist);
                    horizon_listview.setAdapter(mStorepicAdapter);
                } else {
                    horizon_listview.setVisibility(View.GONE);
                }
            }
        }
    };

    private void getBusinessServicelist() {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getservicebystore"};
        String[] name1 = new String[]{"store_id", store_id};
        params2.add(funParam2);
        params2.add(name1);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    type_callbackListener1);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private void getSceneOrder() {
        if (service_id.equals("")) {
            Toast.makeText(getApplicationContext(), "请您先选择您所需的服务项目。", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getstaffbystore"};
        String[] name1 = new String[]{"user_store_id", store_id};
        String[] name2 = new String[]{"service_id", service_id};
        // String[] name3 = new String[] { "states", "2" };
        params2.add(funParam2);
        params2.add(name1);
        params2.add(name2);
        // params2.add(name3);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2, type_callbackListener2);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private void getStaffWorkstates() {
        if (service_id.equals("")) {
            Toast.makeText(getApplicationContext(), "请您先选择您所需的服务项目。", Toast.LENGTH_SHORT).show();
            return;
        }
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getStaffWorkstates"};
        String[] name1 = new String[]{"user_store_id", store_id};
        String[] name2 = new String[]{"service_id", service_id};
        params2.add(funParam2);
        params2.add(name1);
        params2.add(name2);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2, type_callbackListener2);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private void getbusinesss() {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getstore"};
        String[] name1 = new String[]{"store_id", store_id};
        params2.add(funParam2);
        params2.add(name1);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2, type_callbackListener3);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener type_callbackListener1 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            jumpProgressDialog.dimissDialog();
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mServiceListObject = JSON.parseObject(v, ServiceListObject.class);
                serviceObjects = mServiceListObject.response;
                if (mServiceListObject.meta.getMsg().equals("OK")) {
                    if (serviceObjects.size() > 0) {
                        nodata1.setVisibility(View.GONE);
                        list_service.setVisibility(View.VISIBLE);
                        serviceAdapter = new BusinessServiceAdapter(BusinessActivity.this, serviceObjects);
                        list_service.setAdapter(serviceAdapter);
                    } else {
                        nodata1.setVisibility(View.VISIBLE);
                        list_service.setVisibility(View.GONE);
                        Toast.makeText(BusinessActivity.this, "无服务", Toast.LENGTH_LONG).show();
                    }
                } else {
                    nodata1.setVisibility(View.VISIBLE);
                    list_service.setVisibility(View.GONE);
                    Toast.makeText(BusinessActivity.this, "无服务", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(BusinessActivity.this, "无服务", Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setImg(String path) {
        if (path.equals("") || path == null) {
        } else {
            MyApplication.downloadImage.addTask(path, storePhoto,
                    new DownloadImage.ImageCallback() {

                        @Override
                        public void imageLoaded(Bitmap imageBitmap,
                                                String imageUrl) {
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                storePhoto.setBackgroundDrawable(drawable);
                            }
                        }

                        @Override
                        public void imageLoaded(Bitmap imageBitmap,
                                                DownloadImageMode callBackTag) {
                            // TODO 自动生成的方法存根
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                storePhoto.setBackgroundDrawable(drawable);
                            }
                        }

                    });
        }
        MyApplication.downloadImage.doTask();
    }

    private CallbackListener type_callbackListener2 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            jumpProgressDialog.dimissDialog();
            // 对结果进行解析
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mStaffListObject = JSON.parseObject(v, StaffListObject.class);
                staffObjects = mStaffListObject.response;
                if (mStaffListObject.meta.getMsg().equals("OK")) {
                    if (staffObjects.size() > 0) {

                        nodata2.setVisibility(View.GONE);
                        list_staff.setVisibility(View.VISIBLE);
                        staffAdapter = new BusinessStaffAdapter(BusinessActivity.this, staffObjects);
                        list_staff.setAdapter(staffAdapter);
                    } else {
                        nodata2.setVisibility(View.VISIBLE);
                        list_staff.setVisibility(View.GONE);
                        Toast.makeText(BusinessActivity.this, "无员工", Toast.LENGTH_LONG).show();
                    }
                } else {
                    nodata1.setVisibility(View.VISIBLE);
                    list_service.setVisibility(View.GONE);
                    Toast.makeText(BusinessActivity.this, "无员工", Toast.LENGTH_LONG).show();
                }
            }
        }
    };
    private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            jumpProgressDialog.dimissDialog();
            // 对结果进行解析
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mStoreListObject = JSON.parseObject(v, StoreListObject.class);
                list_store = mStoreListObject.response;
                if (mStoreListObject.meta.getMsg().equals("OK")) {
                    if (list_store.size() > 0) {
                        mStoreObject = list_store.get(0);
                        MyApplication.this_store = mStoreObject;
                        business_houre.setText(mStoreObject.business_hours);
                        remark.setText(mStoreObject.remark);
                        name.setText(mStoreObject.name);
                        tv_title.setText(mStoreObject.name);
                        address.setText("地址：" + mStoreObject.address);
                        phone.setText("电话：" + mStoreObject.mobile);
                        service.setText(mStoreObject.service);
                        setImg(mStoreObject.phone);
                        getStar(list_store.get(0).star);
                    } else {
                        Toast.makeText(BusinessActivity.this, "无数据", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(BusinessActivity.this, "无数据", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(BusinessActivity.this, "无数据", Toast.LENGTH_LONG).show();
            }
        }
    };

    private boolean isFirstGetStaff = true;
    private boolean isFirstGetBusiness = true;

    private void bindClickListener() {
        tv_service.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                tv_service.setTextColor(getResources().getColor(R.color.y1));
                tv_staff.setTextColor(getResources().getColor(R.color.n1));
                tv_store_detail.setTextColor(getResources().getColor(R.color.n1));
                tv_service.setBackgroundColor(getResources().getColor(R.color.bg1));
                tv_staff.setBackgroundColor(getResources().getColor(R.color.bg2));
                tv_store_detail.setBackgroundColor(getResources().getColor(R.color.bg2));
                item1.setVisibility(View.VISIBLE);
                item2.setVisibility(View.GONE);
                item3.setVisibility(View.GONE);
                if (isFirstGetBusiness) {
                    getBusinessServicelist();
                    isFirstGetBusiness = false;
                }
            }
        });

        tv_staff.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                tv_staff.setTextColor(getResources().getColor(R.color.y1));
                tv_service.setTextColor(getResources().getColor(R.color.n1));
                tv_store_detail.setTextColor(getResources().getColor(R.color.n1));
                tv_staff.setBackgroundColor(getResources().getColor(R.color.bg1));
                tv_service.setBackgroundColor(getResources().getColor(R.color.bg2));
                tv_store_detail.setBackgroundColor(getResources().getColor(R.color.bg2));
                item2.setVisibility(View.VISIBLE);
                item1.setVisibility(View.GONE);
                item3.setVisibility(View.GONE);
                if (isFirstGetStaff) {
                    isFirstGetStaff = false;
                    if (type.equals("预定")) {
                        getSceneOrder();
                    } else {
                        getStaffWorkstates();
                    }
                }
            }
        });

        tv_store_detail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                tv_store_detail.setTextColor(getResources().getColor(R.color.y1));
                tv_service.setTextColor(getResources().getColor(R.color.n1));
                tv_staff.setTextColor(getResources().getColor(R.color.n1));
                tv_store_detail.setBackgroundColor(getResources().getColor(R.color.bg1));
                tv_service.setBackgroundColor(getResources().getColor(R.color.bg2));
                tv_staff.setBackgroundColor(getResources().getColor(R.color.bg2));
                item3.setVisibility(View.VISIBLE);
                item1.setVisibility(View.GONE);
                item2.setVisibility(View.GONE);
                getbusinesss();
            }
        });
        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        btn_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if (type.equals("预订")) {
                    Log.i("btn_ok", "btn_ok-1"+type);
                    if (MyApplication.list_business_service.size() == 0) {

                        Log.i("btn_ok", "btn_ok0"+type);
                        Toast.makeText(BusinessActivity.this, "请选择服务！", Toast.LENGTH_LONG).show();


                    } else if (MyApplication.list_business_staff.size() == 0) {


                        Log.i("btn_ok", "btn_ok1"+type);

                        Toast.makeText(BusinessActivity.this, "请选择员工！", Toast.LENGTH_LONG).show();
                    } else {

                        Log.i("btn_ok", "btn_ok2"+type);

                        Intent intent = new Intent(BusinessActivity.this, ChooseDatetimeActivity.class);
                        intent.putExtra("store_id", store_id);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    }
                } else if (type.equals("现场")) {
                    if (MyApplication.list_business_staff.size() == 0) {
                        Toast.makeText(BusinessActivity.this, "请选择员工！", Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(BusinessActivity.this, ChooseDatetimeActivity.class);
                        intent.putExtra("store_id", store_id);
                        intent.putExtra("type", type);
                        startActivity(intent);
                    }
                }
            }
        });
        list_service.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                currentServiceIndex = arg2;
                serviceDetaileView.setData(serviceObjects.get(arg2));
            }
        });
        list_staff.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                currentStaffIndex = arg2;
                staffDetaileView.setData(staffObjects.get(arg2));
            }
        });

        rl_address.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent mIntent = new Intent(BusinessActivity.this, LocationsActivity.class);
                mIntent.putExtra("lat", mStoreObject.latitude);
                mIntent.putExtra("lng", mStoreObject.longitude);
                mIntent.putExtra("address", mStoreObject.address);
                startActivity(mIntent);
            }
        });

        tv_call.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mStoreObject.storephoto));
                startActivity(intent);
            }
        });

    }

    protected void dialog() {
        AlertDialog.Builder builder = new Builder(BusinessActivity.this);
        builder.setMessage("确认要追加订单吗？");
        builder.setPositiveButton("确认", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getAddOrder();
            }
        });
        builder.setNegativeButton("取消", new AlertDialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            finish();
        }
        return false;
    }

    // 获取可追加的订单 订单状态必须为未付款 已付款的订单不能追加只能生成新的订单
    private void getAddOrder() {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getAddorder"};
        String[] name1 = new String[]{"client_id",
                MyApplication.Clientuser.id};
        params2.add(funParam2);
        params2.add(name1);
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    addorder_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener addorder_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对结果进行解析
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mOrderListObject = JSON.parseObject(v, OrderListObject.class);
                list = mOrderListObject.response;
                if (mOrderListObject.meta.getMsg().equals("OK")) {
                    if (list.size() > 0) {
                        Intent mintent = new Intent(BusinessActivity.this,
                                AddOrderActivity.class);
                        mintent.putExtra("list", list);
                        startActivity(mintent);
                    } else {

                        Toast.makeText(BusinessActivity.this, "您没有可追加的订单了",
                                Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(BusinessActivity.this, "获取失败",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(BusinessActivity.this, "获取失败", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle b = data.getExtras(); // data为B中回传的Intent
        switch (resultCode) {
            case 503:
                MyApplication.list_business_service.clear();
                ServiceObject str = (ServiceObject) data
                        .getSerializableExtra("service");// str即为回传的值
                type = b.getString("type");
                MyApplication.list_business_service.add(str);
                service_id = str.id;
                tv_staff.setTextColor(getResources().getColor(R.color.y1));
                tv_service.setTextColor(getResources().getColor(R.color.n1));
                tv_store_detail.setTextColor(getResources().getColor(R.color.n1));
                tv_staff.setBackgroundColor(getResources().getColor(R.color.bg1));
                tv_service.setBackgroundColor(getResources().getColor(R.color.bg2));
                tv_store_detail.setBackgroundColor(getResources().getColor(R.color.bg2));
                item2.setVisibility(View.VISIBLE);
                item1.setVisibility(View.GONE);
                item3.setVisibility(View.GONE);

                if (type.equals("现场")) {
                    getStaffWorkstates();
                } else {
                    getSceneOrder();
                }
                break;
            case 504:
                MyApplication.list_business_staff.clear();
                StaffObject str1 = (StaffObject) data.getSerializableExtra("staff");
                MyApplication.list_business_staff.add(str1);
                // Intent intent=new
                // Intent(BusinessActivity.this,ChooseDatetimeActivity.class);
                // intent.putExtra("store_id", store_id);
                // startActivity(intent);
                break;
            case 505:
//			getbusinessstafflist();
                break;
            default:
                break;
        }
    }

    ;

    public void getStar(String stars) {
        switch (Integer.parseInt(stars)) {
            case 0:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate0));
                break;
            case 1:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate1));
                break;
            case 2:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate2));
                break;
            case 3:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate3));
                break;
            case 4:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate4));
                break;
            case 5:
                star.setBackgroundDrawable(getResources().getDrawable(R.drawable.rate5));
                break;
            default:
                break;
        }
    }
}
