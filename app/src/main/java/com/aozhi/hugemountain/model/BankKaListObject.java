package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class BankKaListObject {
	public Meta meta;
	public ArrayList<BankAccountBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<BankAccountBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<BankAccountBean> response) {
		this.response = response;
	}
}
