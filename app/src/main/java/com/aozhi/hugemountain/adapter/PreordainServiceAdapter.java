package com.aozhi.hugemountain.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ServiceObject;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PreordainServiceAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ServiceObject> list = new ArrayList<ServiceObject>();

	public PreordainServiceAdapter(Context ctx, ArrayList<ServiceObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_preordainservice, null);
			mHolder.servername=(TextView)convertView.findViewById(R.id.servername);
			mHolder.serverprice=(TextView)convertView.findViewById(R.id.serverprice);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.servername.setText(list.get(position).name);
		
		double s=Double.parseDouble(list.get(position).pay_manoy)-Double.parseDouble(list.get(position).surchange);
		BigDecimal bigDecimal =  new BigDecimal(s);
		DecimalFormat df=new DecimalFormat("#.##");  
		mHolder.serverprice.setText("项目价格：￥"+df.format(bigDecimal));
		return convertView;
	}
	
	class Holder {
		TextView servername,serverprice;
	}
}
