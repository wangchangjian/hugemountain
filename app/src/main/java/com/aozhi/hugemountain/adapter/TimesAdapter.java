package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class TimesAdapter extends BaseAdapter {

	private Activity mContext;
	private LayoutInflater inflater;
	private ArrayList<String> list = new ArrayList<String>();
	private int temp = -1;
	private Boolean[] ifchecked = new Boolean[] {};

	public TimesAdapter(Activity ctx, ArrayList<String> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView,
			final ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_times, null);
			mHolder.item_rl_time = (RelativeLayout) convertView
					.findViewById(R.id.item_rl_time);
			mHolder.time = (TextView) convertView.findViewById(R.id.tv_time);
			mHolder.radiobutton_1 = (RadioButton) convertView
					.findViewById(R.id.radiobutton_1);
			// mHolder.cbox_1 = (CheckBox)
			// convertView.findViewById(R.id.cbox_1);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.time.setText(list.get(position));
		// 以下方法是单选
		mHolder.radiobutton_1.setId(position);
		mHolder.radiobutton_1
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {

						if (isChecked) {
							MyApplication.begintime=list.get(position);
							if (temp != -1) {
								RadioButton tempButton = (RadioButton) mContext
										.findViewById(temp);
								if (tempButton != null) {
									tempButton.setChecked(false);
								}
							}
							temp = buttonView.getId();
						}
					}
				});

		if (position == temp) {
			mHolder.radiobutton_1.setChecked(true);
			MyApplication.getInstance().setTime(list.get(position));
		} else {
			mHolder.radiobutton_1.setChecked(false);
			MyApplication.getInstance().setTime("");
		}

		mHolder.time.setText(list.get(position));
		return convertView;
	}

	class Holder {
		TextView time;
		RadioButton radiobutton_1;
		CheckBox cbox_1;
		RelativeLayout item_rl_time;
	}
}
