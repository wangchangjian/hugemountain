/*
 * Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 */

package com.alipay.android.appDemo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.JSONObject;

import com.aozhi.hugemountain.R;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * 鐎规悶鍎遍崣璺ㄧ尵閿燂拷 * 
 */
public class BaseHelper {

	/**
	 * 婵炵繝娴囧ù鍡欙拷濡ゅ喚鍎婂☉鎾冲级閺岀喎鈻旈敓锟� * 
	 * @param is
	 * @return
	 */
	public static String convertStreamToString(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	/**
	 * 闁哄嫬澧介妵姝瀒alog
	 * 
	 * @param context
	 *            闁绘粠鍨伴。锟�	 * @param strTitle
	 *            闁哄秴娲。锟�	 * @param strText
	 *            闁告劕鎳庨锟�	 * @param icon
	 *            闁搞儳鍋撻悥锟�	 */
	public static void showDialog(Activity context, String strTitle,
			String strText, int icon) {
		AlertDialog.Builder tDialog = new AlertDialog.Builder(context);
		tDialog.setIcon(icon);
		tDialog.setTitle(strTitle);
		tDialog.setMessage(strText);
		tDialog.setPositiveButton(R.string.btn_confirm, null);
		tDialog.show();
	}

	/**
	 * 闁瑰灚鎸稿畵鍐╃┍閳╁啩绱�
	 * 
	 * @param tag
	 *            闁哄秴娲ㄩ锟�	 * @param info
	 *            濞ｅ洠鍓濇导锟�	 */
	public static void log(String tag, String info) {
		// Log.d(tag, info);
	}

	/**
	 * 闁兼儳鍢茶ぐ鍥级閸愵喗顎�
	 * 
	 * @param permission
	 *            闁哄鍟村锟�	 * @param path
	 *            閻犱警鍨扮欢锟�	 */
	public static void chmod(String permission, String path) {
		try {
			String command = "chmod " + permission + " " + path;
			Runtime runtime = Runtime.getRuntime();
			runtime.exec(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//
	// show the progress bar.
	/**
	 * 闁哄嫬澧介妵姘交濞戞ê顔婇柡澶涙嫹
	 * 
	 * @param context
	 *            闁绘粠鍨伴。锟�	 * @param title
	 *            闁哄秴娲。锟�	 * @param message
	 *            濞ｅ洠鍓濇导锟�	 * @param indeterminate
	 *            缁绢収鍠栭悾楣冨箑閿燂拷	 * @param cancelable
	 *            闁告瑯鍨遍幐娆撴煥閿燂拷	 * @return
	 */
	public static ProgressDialog showProgress(Context context,
			CharSequence title, CharSequence message, boolean indeterminate,
			boolean cancelable) {
		ProgressDialog dialog = new ProgressDialog(context);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setIndeterminate(indeterminate);
		dialog.setCancelable(false);
		// dialog.setDefaultButton(false);
//		dialog.setOnCancelListener(new RechargeSureActivity.AlixOnCancelListener(
//				(Activity) context));

		dialog.show();
		return dialog;
	}

	/**
	 * 閻庢稒顨堥浣圭▔閼艰埖绁甹son閻庣數顢婇挅锟�	 * @param str
	 * @param split
	 * @return
	 */
	public static JSONObject string2JSON(String str, String split) {
		JSONObject json = new JSONObject();
		try {
			String[] arrStr = str.split(split);
			for (int i = 0; i < arrStr.length; i++) {
				String[] arrKeyValue = arrStr[i].split("=");
				json.put(arrKeyValue[0],
						arrStr[i].substring(arrKeyValue[0].length() + 1));
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return json;
	}
}