package com.aozhi.hugemountain.activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.BaseActivity;

public class ChooseDateActivity extends BaseActivity {
	private GridView gridView;
	private BaseAdapter adapter;
	private JSONArray datas = new JSONArray();// 时间段data
	private JSONArray headDatas = new JSONArray();// 头部时间
	private LinearLayout container;
	private String week;

	// state 说明 // 0 normal 1 choose 2 disable
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choosedate);
		container = (LinearLayout) findViewById(R.id.week_days);
		gridView = (GridView) findViewById(R.id.gridView);
		try {
			getOtherDay();
			getHeadDates();
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int mScreenWidth = dm.widthPixels;
		for (int i = 0; i < headDatas.length(); i++) {
			JSONObject item = headDatas.optJSONObject(i);
			try {
				Calendar ca = (Calendar) item.get("time");
				TextView text = new TextView(this);
				text.setClickable(true);
				text.setId(i);
				text.setOnClickListener(clickListener);
				text.setGravity(Gravity.CENTER);
				text.setBackgroundColor(getResources().getColor(R.color.white));
				text.setLayoutParams(new LayoutParams(mScreenWidth / 7,
						LayoutParams.MATCH_PARENT));
				container.addView(text);
				text.setText(getWeek(ca));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		adapter = new BaseAdapter() {

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				ViewHolder holder = null;
				JSONObject item = datas.optJSONObject(position);
				if (convertView == null) {
					holder = new ViewHolder();
					convertView = LayoutInflater.from(ChooseDateActivity.this)
							.inflate(R.layout.cell_time, null);
					holder.timeTextView = (TextView) convertView
							.findViewById(R.id.timeTextView);
					convertView.setTag(holder);
				} else {
					holder = (ViewHolder) convertView.getTag();

				}
				try {
					Calendar calendar = (Calendar) item.get("time");
					SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
					holder.timeTextView.setText(formatter.format(calendar
							.getTime()));
				} catch (JSONException e) {
					e.printStackTrace();
				}

				int state = item.optInt("state");
				switch (state) {
				case 0:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.white));
					break;
				case 1:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.red));
					MyApplication.begintime=holder.timeTextView.getText().toString();
					break;
				case 2:
					holder.timeTextView.setBackgroundColor(getResources()
							.getColor(R.color.gray));
					break;

				default:
					break;
				}
				return convertView;
			}

			@Override
			public long getItemId(int position) {
				return position;
			}

			@Override
			public Object getItem(int position) {
				return position;
			}

			@Override
			public int getCount() {
				return datas.length();
			}
		};
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				JSONObject item = datas.optJSONObject(position);
				if (item.optInt("state") == 2) {
					return;
				}
				for (int i = 0; i < datas.length(); i++) {
					JSONObject child = datas.optJSONObject(i);
					if (child.optInt("state") == 1) {
						try {
							child.put("state", 0);
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}
				try {
					item.put("state", 1);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				adapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {

	}

	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			for (int i = 0; i < 31; i++) {
				View view = container.getChildAt(i);
				view.setBackgroundColor(getResources().getColor(R.color.white));
			}
			JSONObject item= headDatas.optJSONObject(v.getId());
			View view = container.getChildAt(v.getId());
			view.setBackgroundColor(getResources().getColor(R.color.red));
			Log.v("Data",v.getTag(v.getId()).toString());
		}
	};

	class ViewHolder {
		public TextView timeTextView;

	}

	// 头部构造7天时间
	public void getHeadDates() throws JSONException {
		for (int i = 0; i < 7; i++) {
			JSONObject item = new JSONObject();
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, i);
			item.put("state", 0);
			item.put("time", calendar);
			headDatas.put(item);
		}

	}

	// 构造时间段
	public void getOtherDay() throws JSONException {
		int hour = 13;
		for (int i = 0; i < 24; i++) {
			JSONObject item = new JSONObject();
			Calendar c = Calendar.getInstance();
			if (i % 2 == 0) {
				hour++;
				c.set(Calendar.MINUTE, 00);
			} else {
				c.set(Calendar.MINUTE, 30);
			}
			c.set(Calendar.HOUR_OF_DAY, hour);
			item.putOpt("time", c);
			if (i < 4) {
				item.put("state", 2);
			} else {
				item.put("state", 0);// 0 normal 1 choose 2 disable
			}
			datas.put(item);
		}

	}

	// 根据claendat获取星期几
	private String getWeek(Calendar c) {
		String Week = "周";
		if (c.get(Calendar.DAY_OF_WEEK) == 1) {
			Week += "天";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 2) {
			Week += "一";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 3) {
			Week += "二";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 4) {
			Week += "三";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 5) {
			Week += "四";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 6) {
			Week += "五";
		}
		if (c.get(Calendar.DAY_OF_WEEK) == 7) {
			Week += "六";
		}
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd");
		String result = Week + "\n" + formatter.format(c.getTime());
		return result;
	}

}
