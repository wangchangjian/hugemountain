package com.aozhi.hugemountain.activity.PublicActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.Receiver.MyReceiver;
import com.aozhi.hugemountain.activity.ConsumerActivity.ConsumerMainActivity;
import com.aozhi.hugemountain.activity.FindPwd1Activity;
import com.aozhi.hugemountain.activity.HomeStoreActivity;
import com.aozhi.hugemountain.activity.StaffActivity.MainStaffActivity;
import com.aozhi.hugemountain.activity.RegisterActivity;
import com.aozhi.hugemountain.model.GetVersionObject;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.ConfirmDialog;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginsActivity extends BaseActivity {
    private View img_store;// 客铺
    private View img_client;// 客户
    private View img_staff;// 员工
    private LoginListObject mLoginListObject;
    private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
    private ArrayList<LoginBean> reg_list = new ArrayList<LoginBean>();
    private MyReceiver receiveBroadCast;
    private IntentFilter intentFilter;
    private LocationClient mLocationClient;
    private int requestCode;
    private JumpProgressDialog jumpProgressDialog;
    private TextView tv_type, tv_kehu, tv_shanghu, tv_yuangong, tv_findpwd,
            tv_ts;
    private EditText login_edit_account, login_edit_pwd;
    private CheckBox cb;
    private Button btn_login, btn_regist;
    private boolean ok;
    private String types = "1";
    private String typess = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_logins);
        initLoc();
        getNewVersion();
        // getsoft();
        // 初始化 JPush。如果已经初始化，但没有登录成功，则执行重新登录。

        receiveBroadCast = new MyReceiver();
        intentFilter = new IntentFilter("com.aozhi.hugemountain.android.USER_ACTION");
        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_ts = (TextView) findViewById(R.id.tv_ts);
        login_edit_account = (EditText) findViewById(R.id.login_edit_account);
        login_edit_pwd = (EditText) findViewById(R.id.login_edit_pwd);
        cb = (CheckBox) findViewById(R.id.cb);
        tv_findpwd = (TextView) findViewById(R.id.tv_findpwd);
        jumpProgressDialog=new JumpProgressDialog(this);

        tv_findpwd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(LoginsActivity.this,
                        FindPwd1Activity.class);
                startActivity(intent);
            }
        });

        SharedPreferences clientPreferences = getSharedPreferences(
                "client_user", Activity.MODE_PRIVATE);
        String name = clientPreferences.getString("name", "");
        String pwd = clientPreferences.getString("pwd", "");
        if (!name.equals("") && !name.equals(null)) {
            types = "1";
            tv_type.setText("客户登录");
            login_edit_account.setText(name);
            login_edit_pwd.setText(pwd);
            getlogin("getclient_login", name, pwd);
        }

        SharedPreferences storePreferences1 = getSharedPreferences(
                "store_user", Activity.MODE_PRIVATE);
        String name1 = storePreferences1.getString("name", "");
        String pwd1 = storePreferences1.getString("pwd", "");
        if (!name1.equals("") && !name1.equals(null)) {
            types = "2";
            tv_type.setText("商户登录");
            login_edit_account.setText(name1);
            login_edit_pwd.setText(pwd1);
            getlogin("getstore_login", name1, pwd1);
        }

        SharedPreferences staffPreferences2 = getSharedPreferences(
                "staff_user", Activity.MODE_PRIVATE);
        String name2 = staffPreferences2.getString("name", "");
        String pwd2 = staffPreferences2.getString("pwd", "");
        if (!name2.equals("") && !name2.equals(null)) {
            types = "3";
            tv_type.setText("员工登录");
            login_edit_account.setText(name2);
            login_edit_pwd.setText(pwd2);
            getlogin("getstaff_login", name2, pwd2);
        }
        btn_login = (Button) findViewById(R.id.btn_login);
        tv_type.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                showAtaver();
                tv_ts.setVisibility(View.GONE);
            }
        });
        btn_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (login_edit_account.getText().toString().equals("")) {
                    Toast.makeText(LoginsActivity.this, "请输入手机号！",
                            Toast.LENGTH_LONG).show();
                } else if (login_edit_account.getText().toString().trim()
                        .length() != 11) {
                    Toast.makeText(LoginsActivity.this, "请输入正确格式的电话号码！",
                            Toast.LENGTH_LONG).show();
                } else if (login_edit_pwd.getText().toString().equals("")) {
                    Toast.makeText(LoginsActivity.this, "请输入密码！",
                            Toast.LENGTH_LONG).show();
                }
                // else if (!MyApplication.ifreg) {
                // Toast.makeText(LoginsActivity.this, "对不起，功能受限，请联系服务商",
                // Toast.LENGTH_LONG).show();
                // }
                else {
                    if (types.equals("1")) {
                        getlogin("getclient_login", login_edit_account
                                .getText().toString(), login_edit_pwd.getText()
                                .toString());
                    } else if (types.equals("2")) {
                        getlogin("getstore_login", login_edit_account.getText()
                                .toString(), login_edit_pwd.getText()
                                .toString());
                    } else {
                        getlogin("getstaff_login", login_edit_account.getText()
                                .toString(), login_edit_pwd.getText()
                                .toString());
                    }
                }

                // Intent intent = new Intent(LoginsActivity.this,
                // MainStaffActivity.class);
                // startActivity(intent);
            }
        });

        btn_regist = (Button) findViewById(R.id.btn_regist);
        btn_regist.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent mIntent = new Intent(LoginsActivity.this,
                        RegisterActivity.class);
                requestCode = 0;
                startActivityForResult(mIntent, requestCode);
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initView() {

    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        types = data.getStringExtra("types");
        // 根据上面发送过去的请求吗来区别
        switch (requestCode) {
            case 0:

                if (types.equals("1")) {
                    tv_type.setText("客户登录");
                    SharedPreferences sharedPreferences = getSharedPreferences(
                            "client_user", Activity.MODE_PRIVATE);
                    String name = sharedPreferences.getString("name", "");
                    String pwd = sharedPreferences.getString("pwd", "");
                    if (!name.equals("") && !name.equals(null)) {
                        login_edit_account.setText(name);
                        login_edit_pwd.setText(pwd);
                        getlogin("getclient_login", name, pwd);
                    }
                } else if (types.equals("2")) {
                    tv_type.setText("商户登录");
                    SharedPreferences sharedPreferences1 = getSharedPreferences(
                            "store_user", Activity.MODE_PRIVATE);
                    String name1 = sharedPreferences1.getString("name", "");
                    String pwd1 = sharedPreferences1.getString("pwd", "");
                    if (!name1.equals("") && !name1.equals(null)) {
                        login_edit_account.setText(name1);
                        login_edit_pwd.setText(pwd1);
                        getlogin("getstaff_login", name1, pwd1);
                    }
                } else if (types.equals("3")) {
                    tv_type.setText("员工登录");
                    SharedPreferences sharedPreferences2 = getSharedPreferences(
                            "staff_user", Activity.MODE_PRIVATE);
                    String name2 = sharedPreferences2.getString("name", "");
                    String pwd2 = sharedPreferences2.getString("pwd", "");
                    if (!name2.equals("") && !name2.equals(null)) {
                        login_edit_account.setText(name2);
                        login_edit_pwd.setText(pwd2);
                        getlogin("getstaff_login", name2, pwd2);
                    }
                }

                break;

            default:
                break;
        }
    }

    private void InitLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
        option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
        int span = 1000;
        option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    private void initLoc() {
        mLocationClient = ((MyApplication) getApplication()).mLocationClient;
        InitLocation();
        mLocationClient.start();
    }

    private void getlogin(String fun, String username, String password) {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", fun};
        String[] name = new String[]{"name", username};
        String[] pwd = new String[]{"pwd", encryption(password)};
        params2.add(funParam2);
        params2.add(name);
        params2.add(pwd);
        jumpProgressDialog.showDialog();
//		progressDialog = ProgressDialog.show(this,
//				getString(R.string.app_name),
//				getString(R.string.tv_dialog_context), false);
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2, Logins_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener Logins_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            jumpProgressDialog.dimissDialog();
            if (!v.equals("fail")) {
                if (types.equals("1")) {
                    mLoginListObject = JSON.parseObject(v, LoginListObject.class);
                    list = mLoginListObject.getResponse();
                    if (list.size() > 0) {
                        // if (cb.isChecked()) {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "client_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", login_edit_account.getText()
                                .toString());
                        editor.putString("pwd", login_edit_pwd.getText()
                                .toString());
                        editor.putBoolean("ok", true);
                        editor.commit();

                        SharedPreferences mySharedPreferences1 = getSharedPreferences(
                                "store_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = mySharedPreferences1
                                .edit();
                        editor1.putString("name", "");
                        editor1.putString("pwd", "");
                        editor1.putBoolean("ok", false);
                        editor1.commit();

                        SharedPreferences mySharedPreferences2 = getSharedPreferences(
                                "staff_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor2 = mySharedPreferences2
                                .edit();
                        editor2.putString("name", "");
                        editor2.putString("pwd", "");
                        editor2.putBoolean("ok", false);
                        editor2.commit();
                        MyApplication.IS_LOGINS = true;
                        MyApplication.Clientuser = list.get(0);
                        MyApplication.Status = "client";

                        JPushInterface.setAlias(getApplicationContext(), MyApplication.Clientuser.getClientphoto(), mTagsCallback);

                        Intent mintent = new Intent(LoginsActivity.this,
                                ConsumerMainActivity.class);
                        startActivity(mintent);
                        finish();
                    } else {
                        Toast.makeText(LoginsActivity.this, "登录失败！",
                                Toast.LENGTH_LONG).show();
                    }
                } else if (types.equals("2")) {
                    mLoginListObject = JSON.parseObject(v,
                            LoginListObject.class);
                    list = mLoginListObject.getResponse();
                    if (list.size() > 0) {
                        // if (cb.isChecked()) {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "client_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", "");
                        editor.putString("pwd", "");
                        editor.putBoolean("ok", false);
                        editor.commit();

                        SharedPreferences mySharedPreferences1 = getSharedPreferences(
                                "store_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = mySharedPreferences1
                                .edit();
                        editor1.putString("name", login_edit_account.getText()
                                .toString());
                        editor1.putString("pwd", login_edit_pwd.getText()
                                .toString());
                        editor1.putBoolean("ok", true);
                        editor1.commit();

                        SharedPreferences mySharedPreferences2 = getSharedPreferences(
                                "staff_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor2 = mySharedPreferences2
                                .edit();
                        editor2.putString("name", "");
                        editor2.putString("pwd", "");
                        editor2.putBoolean("ok", false);
                        editor2.commit();

                        if (list.get(0).del_flag.equals("2")) {
                            Toast.makeText(LoginsActivity.this,
                                    "您申请的账号正在审核中，请联系服务商！", Toast.LENGTH_LONG)
                                    .show();
                        } else if (list.get(0).del_flag.equals("1")) {
                            Toast.makeText(LoginsActivity.this, "您登录的账号不存在！",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            MyApplication.Storeuser = list.get(0);
                            MyApplication.Status = "store";
                            MyApplication.isstaff = true;

                            JPushInterface.setAlias(getApplicationContext(), MyApplication.Storeuser.getStorephoto(), mTagsCallback);

                            Intent mintent = new Intent(LoginsActivity.this,
                                    HomeStoreActivity.class);
                            startActivity(mintent);
                            finish();
                        }
                    } else {
                        Toast.makeText(LoginsActivity.this, "登录失败！",
                                Toast.LENGTH_LONG).show();
                    }
                } else if (types.equals("3")) {
                    mLoginListObject = JSON.parseObject(v,
                            LoginListObject.class);
                    list = mLoginListObject.getResponse();
                    if (list.size() > 0) {
                        // if (cb.isChecked()) {
                        SharedPreferences mySharedPreferences = getSharedPreferences(
                                "client_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = mySharedPreferences
                                .edit();
                        editor.putString("name", "");
                        editor.putString("pwd", "");
                        editor.putBoolean("ok", false);
                        editor.commit();
                        SharedPreferences mySharedPreferences1 = getSharedPreferences(
                                "store_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor1 = mySharedPreferences1
                                .edit();
                        editor1.putString("name", "");
                        editor1.putString("pwd", "");
                        editor1.putBoolean("ok", false);
                        editor1.commit();
                        SharedPreferences mySharedPreferences2 = getSharedPreferences(
                                "staff_user", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor2 = mySharedPreferences2
                                .edit();
                        editor2.putString("name", login_edit_account.getText()
                                .toString());
                        editor2.putString("pwd", login_edit_pwd.getText()
                                .toString());
                        editor2.putBoolean("ok", true);
                        editor2.commit();
                        if (list.get(0).del_flag.equals("2")) {
                            Toast.makeText(LoginsActivity.this,
                                    "您申请的账号正在审核中，请联系服务商！", Toast.LENGTH_LONG)
                                    .show();
                        } else if (list.get(0).del_flag.equals("1")) {
                            Toast.makeText(LoginsActivity.this, "您登录的账号不存在！",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            MyApplication.Staffuser = list.get(0);
                            MyApplication.Status = "staff";

                            JPushInterface.setAlias(getApplicationContext(), MyApplication.Staffuser.getStaffphoto(), mTagsCallback);

                            Intent mintent = new Intent(LoginsActivity.this, MainStaffActivity.class);
                            startActivity(mintent);
                            finish();
                        }
                    } else {
                        Toast.makeText(LoginsActivity.this, "登录失败！",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(LoginsActivity.this, "登录失败！", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    private final TagAliasCallback mTagsCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
//                Log.i(TAG, logs);
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
//                Log.i(TAG, logs);
//                if (ExampleUtil.isConnected(getApplicationContext())) {
//                	mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
//                } else {
////                	Log.i(TAG, "No network");
//                }
                    break;
                default:
                    logs = "Failed with errorCode = " + code;
//                Log.e(TAG, logs);
            }
//            ExampleUtil.showToast(logs, getApplicationContext());
        }
    };

    private void showAtaver() {
        final AlertDialog dlg = new AlertDialog.Builder(this).create();
        dlg.show();
        Window window = dlg.getWindow();
        window.setContentView(R.layout.item_types);
        tv_kehu = (TextView) window.findViewById(R.id.tv_kehu);
        tv_shanghu = (TextView) window.findViewById(R.id.tv_shanghu);
        tv_yuangong = (TextView) window.findViewById(R.id.tv_yuangong);
        tv_kehu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                types = "1";
                tv_type.setText("客户登录");
                dlg.cancel();
            }
        });
        tv_shanghu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                types = "2";
                tv_type.setText("商户登录");
                dlg.cancel();
            }
        });
        tv_yuangong.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                types = "3";
                tv_type.setText("员工登录");
                dlg.cancel();
            }
        });
    }

    public static String encryption(String plainText) {
        String re_md5 = new String();
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            re_md5 = buf.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return re_md5;
    }

    protected void onResume() {
        JPushInterface.onResume(this);
        super.onResume();
    }

    ;

    @Override
    protected void onPause() {
        JPushInterface.onPause(this);
        super.onPause();
    }

    private void getsoft() {
        // http://112.126.67.4:10009/json.aspx?fun=getStates
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getStates"};
        params2.add(funParam2);
        jumpProgressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            // new HttpConnection().get(Constant.URL2, params2,
            // getStates_callbackListener);
            new HttpConnection().get(Constant.URL, params2,
                    getStates_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener getStates_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            jumpProgressDialog.dimissDialog();
            if (!v.equals("fail")) {
                mLoginListObject = JSON.parseObject(v, LoginListObject.class);
                reg_list = mLoginListObject.response;
                if (mLoginListObject.meta.getMsg().equals("OK")) {
                    if (reg_list.get(0).states.equals("0")) {
                        MyApplication.ifreg = true;
                    } else if (reg_list.get(0).states.equals("1")) {
                        MyApplication.ifreg = false;
                        Toast.makeText(LoginsActivity.this, "对不起，功能受限，请联系服务商",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                Toast.makeText(LoginsActivity.this, "系统错误", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    private void getNewVersion() {
        ArrayList<String[]> params = new ArrayList<String[]>();
        String[] funParam = new String[]{"fun", "getnewversion"};
        String[] Param1 = new String[]{"os", "1"};
        params.add(funParam);
        params.add(Param1);
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params, callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (!v.equals("fail")) {
                GetVersionObject gvo = JSON.parseObject(v,
                        GetVersionObject.class);
                if (gvo != null && gvo.response != null
                        && gvo.response.size() > 0) {
                    getVersion(gvo.response.get(0).ver,
                            gvo.response.get(0).link);
                } else {
                }
            } else {
                Toast.makeText(LoginsActivity.this, "服务器无响应", Toast.LENGTH_LONG)
                        .show();
            }
        }
    };

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public void getVersion(String ver, String upgrade_url) {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            String version = info.versionName;
            if (version.equals(ver)) {
                // newversionDialog();
            } else {
                if (upgrade_url.indexOf("http://") == -1) {
                    upgrade_url = "http://" + upgrade_url;
                }
                updateDialog(upgrade_url);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void newversionDialog() {

        final ConfirmDialog confirmDialog = new ConfirmDialog(this, "已经是最新版本");
        confirmDialog.showOnlyConfirm();
        confirmDialog.confirmListener(new ConfirmDialog.ConfirmListener() {
            @Override
            public void confirm() {
                confirmDialog.dismiss();
            }
        });
    }

    public void updateDialog(final String upgrade_url) {

        final ConfirmDialog confirmDialog = new ConfirmDialog(this, "发现新版本，现在更新？");
        confirmDialog.showConfirmAndCancel();
        confirmDialog.confirmListener(new ConfirmDialog.ConfirmListener() {
            @Override
            public void confirm() {
                confirmDialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(upgrade_url));
                startActivity(intent);

            }
        });
    }
}
