package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.CodoActivity;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Code;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
/*
import com.aozhi.olehui.CodoActivity;
import com.aozhi.olehui.FindPwdActivity;
import com.aozhi.olehui.http.HttpConnection;
import com.aozhi.olehui.http.HttpConnection.CallbackListener;
import com.aozhi.olehui.model.MemberListObject;
import com.aozhi.olehui.model.MemberObject;
import com.aozhi.olehui.utils.Code;
import com.aozhi.olehui.utils.Constant;
import com.aozhi.olehui.utils.Utils;
*/
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FindPwd1Activity extends  Activity implements OnClickListener {

	private Button btn_back, btn_login;
	private ImageView iv_showCode;
	// 产生的验证码
	private String realCode;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LoginListObject mMemberListObject;
	private ProgressDialog progressDialog = null;
	private EditText et_tel, et_pwd;
	private TextView tv_type,tv_ts,tv_kehu,tv_shanghu, tv_yuangong;
	private String types = "1";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findpwd1);
		initView();
		initListnner();
	}

	private void initView() {
		// TODO Auto-generated method stub
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_login = (Button) findViewById(R.id.btn_login);
		et_tel = (EditText) findViewById(R.id.et_tel);
		et_pwd = (EditText) findViewById(R.id.et_pwd);
		tv_type = (TextView) findViewById(R.id.tv_type);
		tv_ts = (TextView) findViewById(R.id.tv_ts);
		iv_showCode = (ImageView) findViewById(R.id.iv_showCode);
		// 将验证码用图片的形式显示出来
		iv_showCode.setImageBitmap(Code.getInstance().createBitmap());
		realCode = Code.getInstance().getCode();
//		iv_showCode.setOnClickListener(this);
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		btn_login.setOnClickListener(this);
		iv_showCode.setOnClickListener(this);
		tv_type.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
			case R.id.btn_back: {
				finish();
			}
			break;
			case R.id.tv_type:
				showAtaver();
				tv_ts.setVisibility(View.GONE);
				break;
			case R.id.iv_showCode:
				iv_showCode.setImageBitmap(Code.getInstance().createBitmap());
				realCode = Code.getInstance().getCode();
				break;
			case R.id.btn_login: {
				if (et_tel.getText().toString() == null
						|| et_tel.getText().toString().equals("")) {
					Toast.makeText(FindPwd1Activity.this, "请输入手机号",
							Toast.LENGTH_SHORT).show();
				} else if (et_tel.getText().toString().length() > 11
						|| et_tel.getText().toString().length() < 11) {
					Toast.makeText(FindPwd1Activity.this, "手机号输入有误，请重新输入",
							Toast.LENGTH_SHORT).show();
				} else if (et_pwd.getText().toString() == null
						|| et_pwd.getText().toString().equals("")) {
					Toast.makeText(FindPwd1Activity.this, "请输入验证码",
							Toast.LENGTH_SHORT).show();
				} else if (!et_pwd.getText().toString().toLowerCase().equals(realCode.toLowerCase())) {
					Toast.makeText(FindPwd1Activity.this, "验证码输入有误，请重新输入",
							Toast.LENGTH_SHORT).show();
				} else {
					GetUsername(types);

				}
			}
			default:
				break;
		}
	}

	private void GetUsername(String types) {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam=null;
			if(types.equals("1")){
				methodParam = new String[] { "fun", "findClientname" };
			}else if(types.equals("2")){
				methodParam = new String[] { "fun", "findStorename" };
			}else{
				methodParam = new String[] { "fun", "findStaffname" };
			}
			String[] pageParam = new String[] { "username",et_tel.getText().toString() };
			params.add(methodParam);
			params.add(pageParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetInFo_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetInFo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mMemberListObject = JSON.parseObject(result,
						LoginListObject.class);
				list = mMemberListObject.response;
				if (mMemberListObject.meta.getMsg().equals("OK")) {
					if (Integer.valueOf(list.get(0).count) > 0) {
						Intent mIntent=new Intent(FindPwd1Activity.this,CodoActivity.class);
						mIntent.putExtra("tel", et_tel.getText().toString());
						mIntent.putExtra("types", types);
						startActivity(mIntent);

					} else {
						Toast.makeText(FindPwd1Activity.this,"该手机号不存在，请重新输入手机号码", Toast.LENGTH_LONG).show();
						et_tel.setText("");
					}
				}
			}
		}
	};

	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.item_types);
		tv_kehu = (TextView) window.findViewById(R.id.tv_kehu);
		tv_shanghu = (TextView) window.findViewById(R.id.tv_shanghu);
		tv_yuangong = (TextView) window.findViewById(R.id.tv_yuangong);
		tv_kehu.setText("我是客户");
		tv_shanghu.setText("我是商户");
		tv_yuangong.setText("我是员工");
		tv_kehu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				types = "1";
				tv_type.setText("我是客户");
				dlg.cancel();
			}
		});
		tv_shanghu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				types = "2";
				tv_type.setText("我是商户");
				dlg.cancel();
			}
		});
		tv_yuangong.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				types = "3";
				tv_type.setText("我是员工");
				dlg.cancel();
			}
		});
	}
}
/*
Activity {
	 Button btn_next;
	 Button btn_back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_findpwd1);
		
		btn_next = (Button)findViewById(R.id.btn_next);
		btn_next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(FindPwd1Activity.this, FindPwd2Activity.class);
				startActivity(intent);
			}
		});
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_pwd1, menu);
		return true;
	}
*/