package com.aozhi.hugemountain.activity;

import java.math.BigDecimal;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.VipCenterAdapter;
import com.aozhi.hugemountain.model.VipCenterListObject;
import com.aozhi.hugemountain.model.VipCenterObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class VipCenterActivity extends Activity implements OnClickListener {
	
	private ImageView img1;
	private Spinner p_type, p_store;
	private TextView tv_ppsum,tv_nodata,totalprice,addvip;
	private ListView per_list;
	private ArrayList<VipCenterObject> list = new ArrayList<VipCenterObject>();
	private VipCenterListObject mVipCenterListObject;
	private VipCenterAdapter adapter;
	private ProgressDialog progressDialog = null;
	private String[] types = new String[] { "全部", "本商铺会员"};
	private String[] order = new String[] { "按注册时间升序", "按注册时间降序", "按账户余额升序","按账户余额降序" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vipcenter);
		initView();
		initListener();
	}

	private void initView() {
		img1=(ImageView) findViewById(R.id.img1);
		p_type = (Spinner) findViewById(R.id.p_type);
		p_store = (Spinner) findViewById(R.id.p_store);
		tv_ppsum = (TextView) findViewById(R.id.tv_ppsum);
		per_list = (ListView) findViewById(R.id.per_list);
		tv_nodata=(TextView) findViewById(R.id.tv_nodata);
		totalprice=(TextView) findViewById(R.id.totalprice);
		addvip=(TextView) findViewById(R.id.addvip);
	}

	private void initListener() {
		img1.setOnClickListener(this);
		Spinner(p_type,types);
		p_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(p_type.getSelectedItem().equals("全部")){
					
				}else if(p_type.getSelectedItem().equals("本商铺会员")){
					getStaffList("a.id");
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				getStaffList("1");
				
			}
		
		});
		Spinner(p_store,order);
		p_store.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(p_store.getSelectedItem().equals("按注册时间升序")){
					getStaffList("c.create_time");
				}else if(p_store.getSelectedItem().equals("按注册时间降序")){
					getStaffList("c.create_time desc");
				}else if(p_store.getSelectedItem().equals("按账户余额升序")){
					getStaffList("c.balance");
				}else if(p_store.getSelectedItem().equals("按账户余额降序")){
					getStaffList("c.balance desc");
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});
		adapter = new VipCenterAdapter(VipCenterActivity.this, list);
		per_list.setAdapter(adapter);
		addvip.setOnClickListener(this);
		per_list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long ids) {
				Intent mintent=new Intent(getApplicationContext(),VipReChargeActivity.class);
				mintent.putExtra("id",list.get(position).id);
				mintent.putExtra("login_id",list.get(position).clientphoto);
				mintent.putExtra("balance", list.get(position).balance);
				startActivityForResult(mintent,61);
//				startActivity(mintent);
			}	
		});
//		per_list.setOnItemLongClickListener(new OnItemLongClickListener() {
//
//			@Override
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
//					final int arg2, long arg3) {
//				AlertDialog.Builder builder=new Builder(VipCenterActivity.this);
//				builder.setMessage("确认删除该会员吗");
//				builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface arg0, int arg1) {
//						delClientVip(list.get(arg2).id);
//						arg0.dismiss();
//					}
//				});
//				builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
//					
//					@Override
//					public void onClick(DialogInterface arg0, int arg1) {
//						arg0.dismiss();						
//					}
//				});
//				return true;
//			}
//		});
	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.img1:
			finish();
			break;
		case R.id.addvip:
			Intent mintent=new Intent(this,VipAddActivity.class);
			startActivityForResult(mintent,61);
			break;
		default:
			break;
		}
	}

	private void getStaffList(String orderby) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreVipcenter" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "order", orderby};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mVipCenterListObject = JSON.parseObject(v,
						VipCenterListObject.class);
				list = mVipCenterListObject.response;
				if (mVipCenterListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						tv_ppsum.setText(String.valueOf(list.size()));
						double sum=0.0;
						for(int i=0;i<list.size();i++){
							double balance=Double.parseDouble(list.get(i).balance);
							sum=sum+balance;
						}
						double bsum=sum/10000;
						totalprice.setText(String.valueOf(rounds(bsum,2)));
						tv_nodata.setVisibility(View.GONE);
						per_list.setVisibility(View.VISIBLE);
						adapter = new VipCenterAdapter(VipCenterActivity.this,
								list);
						per_list.setAdapter(adapter);
					}else{
						tv_ppsum.setText("0");
						totalprice.setText("0");
						tv_nodata.setVisibility(View.VISIBLE);
						per_list.setVisibility(View.GONE);
					}
				}
			} else {
				tv_ppsum.setText("0");
				totalprice.setText("0");
				tv_nodata.setVisibility(View.VISIBLE);
				per_list.setVisibility(View.GONE);
				Toast.makeText(VipCenterActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	
	private void Spinner(Spinner sp,String[] data) {
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		ArrayList<String> timelist = new ArrayList<String>();
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type,data);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		sp.setAdapter(adapter);
	}
	
	//实现小数的保留两位后四舍五入
	public static double rounds(Double v, int scale) {
		if (scale < 0) {
		throw new IllegalArgumentException("The scale must be a positive integer or zero");
		}
		 BigDecimal b = null == v ? new BigDecimal("0.0") : new BigDecimal(Double.toString(v));
	     BigDecimal one = new BigDecimal("1");
	     return b.divide(one, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
		}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 61:
			getStaffList("1");
			break;
		default:
			break;
		}
		
	}
	
	private void delClientVip(String id) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delClientVip" };
		String[] nameParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam2 = new String[] { "client_id", id};
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				getStaffList("1");
			}
		}
	};
}
