package com.aozhi.hugemountain.activity.ConsumerActivity;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cn.jpush.android.api.JPushInterface;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.StaffActivity.SettingActivity;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.Toast;

public class ConsumerMainActivity extends TabActivity {

	TabHost tabhost;
	private boolean isAppExit;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main1);
		initView();
		if(MyApplication.msgreg_id.equals("")&&MyApplication.Status.equals("client")){
			setMsgRegId();
		}
	}

	private void initView() {
		// TODO Auto-generated method stub
		tabhost = this.getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		intent = new Intent().setClass(this, StoreListActivity.class);
		spec = tabhost.newTabSpec("home").setIndicator("home")
				.setContent(intent);
		tabhost.addTab(spec);
		intent = new Intent().setClass(this, ConsumerOrderActivity.class);
		spec = tabhost.newTabSpec("order").setIndicator("order")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, ConsumerAccountActivity.class);
		spec = tabhost.newTabSpec("account").setIndicator("account")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, SettingActivity.class);
		spec = tabhost.newTabSpec("setting").setIndicator("setting")
				.setContent(intent);
		tabhost.addTab(spec);
		tabhost.setCurrentTab(0);

		RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.main_tab_group);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
					case R.id.staff_order:
						tabhost.setCurrentTabByTag("home");
						break;
					case R.id.staff_store:
						tabhost.setCurrentTabByTag("order");
						break;
					case R.id.staff_account:
						tabhost.setCurrentTabByTag("account");
						break;
					case R.id.staff_state:
						tabhost.setCurrentTabByTag("setting");
						break;
					default:
						;
						break;
				}
			}
		});
	}

	private void setMsgRegId() {
		ArrayList<String[]> params1 = new ArrayList<String[]>();
		String[] funParam1 = new String[] { "fun", "setClientMsgRegId" };
		String[] name1 = new String[] { "id",MyApplication.Clientuser.id};
		String[] name2=new String[]{"reg_id",JPushInterface.getRegistrationID(getApplicationContext())};
		params1.add(funParam1);
		params1.add(name1);
		params1.add(name2);
		Constant.NET_STATUS = Utils.getCurrentNetWork(ConsumerMainActivity.this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params1,
					typeday_callbackListener);
		} else {
			Toast.makeText(ConsumerMainActivity.this, "请检查网络连接",
					Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener typeday_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备注册结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
//				Toast.makeText(getApplicationContext(), "更新成功",
//						Toast.LENGTH_SHORT).show();
			} else {
//				Toast.makeText(getApplicationContext(), "更新失败",
//						Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	private static Boolean isQuit = false;
	Timer timer = new Timer();
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	        if (keyCode == KeyEvent.KEYCODE_BACK) {
	            if (isQuit == false) {
	                isQuit = true;
	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
	                TimerTask task = null;
	                task = new TimerTask() {
	                    @Override
	                    public void run() {
	                        isQuit = false;
	                    }
	                };
	                timer.schedule(task, 2000);
	            } else {
	                finish();
	                MyApplication.IS_LOGINS=false;
	                System.exit(0);
	            }
	        }
	        return false;
	}
}
