package com.aozhi.hugemountain.model;

import java.util.ArrayList;


public class FuWuTypeListObject {
	public Meta meta;
	public ArrayList<FuWuTypeBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<FuWuTypeBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<FuWuTypeBean> response) {
		this.response = response;
	}
	
}
