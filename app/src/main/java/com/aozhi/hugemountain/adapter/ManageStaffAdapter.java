package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.StaffObject;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.StaffObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ManageStaffAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<StaffObject> list = new ArrayList<StaffObject>();

	public ManageStaffAdapter(Context ctx, ArrayList<StaffObject> li) {
		mContext = ctx;
		list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_managestaff1, null);
			mHolder.tv_state = (TextView) convertView
					.findViewById(R.id.tv_state);
			mHolder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
			mHolder.img_avatar = (ImageView) convertView
					.findViewById(R.id.img_avatar);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		if (list.get(position).workstatus.equals("1")) {
			mHolder.tv_state.setText("忙碌");
			mHolder.tv_state.setBackgroundColor(Color.parseColor("#76CEDA"));
		}else if (list.get(position).workstatus.equals("2")) {
			mHolder.tv_state.setText("空闲");
			mHolder.tv_state.setBackgroundColor(Color.parseColor("#D6D6D6"));
		}else if (list.get(position).workstatus.equals("3")) {
			mHolder.tv_state.setText("休息");
			mHolder.tv_state.setBackgroundColor(Color.parseColor("#F77172"));
		}else{
			mHolder.tv_state.setText("休息");
			mHolder.tv_state.setBackgroundColor(Color.parseColor("#F77172"));
		}
		mHolder.tv_name.setText("编号"+list.get(position).code_id);
		if (list.get(position).getAvatar().equals("")
				|| list.get(position).getAvatar() == null) {
			mHolder.img_avatar.setBackgroundResource(R.drawable.head);
		} else {
			MyApplication.downloadImage.addTasks(
					list.get(position).getAvatar(), mHolder.img_avatar,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_avatar
										.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_avatar
										.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		return convertView;
	}

	class Holder {
		TextView tv_state, tv_name, tv_age, tv_server;
		ImageView img_avatar;
	}
}
