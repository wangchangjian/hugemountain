package com.aozhi.hugemountain.model;

public class ClientObject {
	 public String id;
     public String clientphoto;
     public String clientpwd;
     public String clientname;
     public String name;
     public String sex;
     public String age;
     public String remark;
     public String create_time;
     public String update_time;
     public String mark;
     public String del_flag;
     public String phone;
     public String balance;
     public String msgreg_id;
     public String levelname;
     public String percent;
     public String client_id;
}
