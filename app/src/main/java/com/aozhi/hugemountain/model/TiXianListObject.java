package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class TiXianListObject {
	public Meta meta;
	public ArrayList<TiXianObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<TiXianObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<TiXianObject> response) {
		this.response = response;
	}
}
