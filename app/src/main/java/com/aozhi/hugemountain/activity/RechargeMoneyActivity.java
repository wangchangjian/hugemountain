package com.aozhi.hugemountain.activity;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.model.ClientListObject;
import com.aozhi.hugemountain.model.ClientObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import cn.beecloud.demo.ShoppingCartActivity;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class RechargeMoneyActivity extends Activity {

	private Button ok;
	private TextView et_login_id,et_money1;
	private EditText et_money;
	private ImageView img1;
	private String created,balane;
	private TextView tv_balane,tv_login,et_percent;
	private ProgressDialog progressDialog = null;
	private ClientListObject mClientListObject;
	private ArrayList<ClientObject> list = new ArrayList<ClientObject>();
	private SeekBar seekbar;
	private String login_id="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recharge_money);
		initView();
		initListener();
	}

	private void initListener() {
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!et_login_id.getText().toString().equals("")
						&& !et_money.getText().toString().equals("")) {
					setOrderStatus();
				}
			}
		});
		
		img1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(203);
				finish();
			}
		});
		seekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
			}
			
			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if(et_money.getText().toString().equals("")){
//					Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
				}else{
				et_percent.setText(String.valueOf(arg1));
				Double remoney=Double.parseDouble(et_money.getText().toString())*(1+Double.parseDouble(et_percent.getText().toString())/100);
				DecimalFormat df = new DecimalFormat("###.00"); 
				et_money1.setText(String.valueOf(df.format(remoney)));
				}
			}
		});
		
	}

	private void initView() {
		balane=getIntent().getStringExtra("balance");
		login_id=getIntent().getStringExtra("login_id");
		ok = (Button) findViewById(R.id.ok);
		et_percent=(TextView) findViewById(R.id.et_percent);
		seekbar=(SeekBar) findViewById(R.id.seekbar);
		tv_login=(TextView) findViewById(R.id.tv_login);
		tv_balane=(TextView) findViewById(R.id.balane);
		et_login_id = (TextView) findViewById(R.id.et_login_id);
		et_money = (EditText) findViewById(R.id.et_money);
		img1 = (ImageView) findViewById(R.id.img1);
		tv_login.setText(MyApplication.Storeuser.storephoto);
		et_money1=(TextView) findViewById(R.id.et_money1);
		et_login_id.setText(login_id);
		tv_balane.setText(balane);
		Calendar calendar = Calendar.getInstance();
		created = String.valueOf(calendar.get(Calendar.YEAR))
				+ String.valueOf((calendar.get(Calendar.MONTH) + 1))
				+ String.valueOf(calendar.get(Calendar.DAY_OF_MONTH))
				+ String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))
				+ String.valueOf(calendar.get(Calendar.MINUTE))
				+ String.valueOf(calendar.get(Calendar.SECOND));
		
	}

	private void setOrderStatus() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "selectclientbyphoto" };
		String[] nameParam1 = new String[] { "photo",
				et_login_id.getText().toString().trim() };
		String[] nameParam2 = new String[] {"store_id",MyApplication.Storeuser.id}; 
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getOrder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getOrder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mClientListObject = JSON.parseObject(v, ClientListObject.class);
				list = mClientListObject.response;
				if (mClientListObject.meta.getMsg().equals("OK")) {
					Intent intent = new Intent(getApplicationContext(),
							ShoppingCartActivity.class);
					intent.putExtra("login_id",list.get(0).client_id);
					intent.putExtra("store_id",MyApplication.Storeuser.id);
					intent.putExtra("cash", et_money.getText().toString()
							.trim());
					intent.putExtra("storename", "会员中心");
					intent.putExtra("services", "充值");
					intent.putExtra("orderid", created);
					intent.putExtra("balance",list.get(0).balance);
					intent.putExtra("percent",et_percent.getText().toString());
					startActivityForResult(intent,23);
				}else{
					Toast.makeText(getApplicationContext(), "账号不存在", Toast.LENGTH_SHORT).show();
				}
			}else{
				Toast.makeText(getApplicationContext(), "获取失败", Toast.LENGTH_SHORT).show();
			}
		}
	};
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case 23:
			setResult(203);
			finish();
			break;
		default:
			break;
		}
	};
}
