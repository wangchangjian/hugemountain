package com.aozhi.hugemountain.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ShowPhotoAdapter;
import com.aozhi.hugemountain.adapter.ShowVideoAdapter;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;

import com.aozhi.hugemountain.model.ImageListObject;
import com.aozhi.hugemountain.model.ImageObject;
import com.aozhi.hugemountain.model.StaffPhotoListObject;
import com.aozhi.hugemountain.model.StaffPhotoObject;
import com.aozhi.hugemountain.model.StaffVideoListObject;
import com.aozhi.hugemountain.model.StaffVideoObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ShowEditActivity extends Activity implements OnClickListener {
	
	private Button upload,btn_back;
	private ImageView addpj;
	private TextView gone,tv_1,tv_2;
	private GridView imglist1,imglist2;
	private EditText name;
	private ImageListObject mImageListObject;
	private ArrayList<ImageObject> list=new ArrayList<ImageObject>();
	private ProgressDialog progressDialog = null;
	private ShowPhotoAdapter mShowphotoAdapter;
	private StaffPhotoObject mStaffPhotoObject;
	private StaffPhotoListObject mStaffPhotoListObject;
	private ArrayList<StaffPhotoObject> list_photo = new ArrayList<StaffPhotoObject>();
	private ShowVideoAdapter mShowVideoAdapter;
	private StaffVideoObject mStaffVideoObject;
	private StaffVideoListObject mStaffVideoListObject;
	private ArrayList<StaffVideoObject> list_video = new ArrayList<StaffVideoObject>();
	private int status=0;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// ����
	public static final int PHOTOZOOM = 2; // ����
	public static final int PHOTORESOULT = 3;// ���
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private TextView	tv_camera,tv_photo;
	private String imgload="";
	private String serverFile = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_showedit);
		initView();
		initListener();
	}
	
	private void initListener() {
		upload.setOnClickListener(this);
		gone.setOnClickListener(this);
		btn_back.setOnClickListener(this);
	}

	private void initView() {
		upload=(Button) findViewById(R.id.upload);
		gone=(TextView) findViewById(R.id.gone);
		tv_1=(TextView) findViewById(R.id.tv_photo);
		tv_2=(TextView) findViewById(R.id.tv_vedio);
		imglist1=(GridView) findViewById(R.id.imglist1);
		imglist2=(GridView) findViewById(R.id.imglist2);
		addpj=(ImageView) findViewById(R.id.addpj);
		name=(EditText) findViewById(R.id.name);
		btn_back=(Button) findViewById(R.id.btn_back);
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_photo:
			status=0;
			tv_1.setTextColor(getResources().getColor(Color.parseColor("#ffffff")));
			tv_1.setBackgroundColor(getResources().getColor(Color.parseColor("#FF9201")));
			tv_2.setTextColor(getResources().getColor(Color.parseColor("#000000")));
			tv_2.setBackgroundColor(getResources().getColor(Color.parseColor("#ffffff")));
			break;
		case R.id.tv_vedio:
			status=1;
			tv_1.setTextColor(getResources().getColor(Color.parseColor("#000000")));
			tv_1.setBackgroundColor(getResources().getColor(Color.parseColor("#ffffff")));
			tv_2.setTextColor(getResources().getColor(Color.parseColor("#ffffff")));
			tv_2.setBackgroundColor(getResources().getColor(Color.parseColor("#FF9201")));
			break;
		case R.id.upload:
			if(status==0){
				showAtaver();
			}else if(status==1){
//				upVideoList();
			}
			break;
		case R.id.gone:
			if(status==0){
				upPhotoList();
			}else if(status==1){
//				upVideoList();
			}
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}

	}

	private void upPhotoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upStaffPhoto" };
			String[] name0=new String[] {"name",name.getText().toString().trim()};
			String[] name1=new String[] {"path",serverFile};
			String[] name2 = new String[] { "staff_id", MyApplication.Staffuser.id };
			params.add(methodParam);
			params.add(name0);
			params.add(name1);
			params.add(name2);
			progressDialog = ProgressDialog.show(this, null, "正在加载。。。", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener1);
		} else
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffPhotoListObject = JSON.parseObject(result,
						StaffPhotoListObject.class);
				list_photo = mStaffPhotoListObject.response;
				if (mStaffPhotoListObject.meta.getMsg().equals("OK")) {
						Intent mintent=new Intent(ShowEditActivity.this,ShowActivity.class);
						setResult(136,mintent);
						finish();

				} else {
					Toast.makeText(ShowEditActivity.this, "无数据", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(ShowEditActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};

	private void upVideoList() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam = new String[] { "fun", "upStaffVideo" };
			String[] name=new String[] {"name",""};
			String[] name1=new String[] {"path",};//鏄剧ず鍥剧墖
			String[] name2= new String[] { "staff_id",MyApplication.Staffuser.id };
			String[] name3=new String[] {"link",""}; //瑙嗛璺緞
			params.add(methodParam);
			params.add(name);
			params.add(name1);
			params.add(name2);
			params.add(name3);
			progressDialog = ProgressDialog.show(this, null, "正在加载。。。", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetMainList_callbackListener2);
		} else
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetMainList_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mStaffVideoListObject = JSON.parseObject(result,
						StaffVideoListObject.class);
				list_video = mStaffVideoListObject.response;
				if (mStaffVideoListObject.meta.getMsg().equals("OK")) {
					
						Intent mintent=new Intent(ShowEditActivity.this,ShowActivity.class);
						setResult(136,mintent);
						finish();
				

				} else {
					Toast.makeText(ShowEditActivity.this,"无数据", Toast.LENGTH_LONG)
							.show();
				}
			} else {
				Toast.makeText(ShowEditActivity.this,"无数据", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	
	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.activity_xiugai);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						Environment.getExternalStorageDirectory(), "temp.jpg")));
				System.out.println("============="
						+ Environment.getExternalStorageDirectory());
				startActivityForResult(intent, PHOTOHRAPH);
				dlg.cancel();
			}
		});
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
//				Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
//				intent.setDataAndType(
//						MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//						IMAGE_UNSPECIFIED);
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTOZOOM);
				dlg.cancel();
			}
		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode == NONE)
			return;
		if (requestCode == PHOTOHRAPH) {
			File picture = new File(Environment.getExternalStorageDirectory()
					+ "/temp.jpg");
			System.out.println("------------------------" + picture.getPath());
			startPhotoZoom(Uri.fromFile(picture));
		}
		if (data == null)
			return;
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 -

				new DateFormat();
				String name = DateFormat.format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA)) + ".jpg";
				Bundle bundle = data.getExtras();
				Bitmap bitmap = (Bitmap) bundle.get("data");
				FileOutputStream b = null;
				File file = new File("/sdcard/myImage/");
				file.mkdirs();
				String fileName = "/sdcard/myImage/" + name;
				try {
					b = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				File file1 = new File(fileName);
				
				addpj.setImageBitmap(photo);

				new UploadAsyncTask().execute(file1);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	public void startPhotoZoom(Uri uri) {
		Intent mintent = new Intent("com.android.camera.action.CROP");
		mintent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		mintent.putExtra("crop", "true");
		mintent.putExtra("aspectX", 1);
		mintent.putExtra("aspectY", 1);
		mintent.putExtra("outputX", 320);
		mintent.putExtra("outputY", 320);
		mintent.putExtra("return-data", true);
		startActivityForResult(mintent, PHOTORESOULT);
	}
	
	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			imgload=serverFile;
			return serverFile;
		}
	}
}
