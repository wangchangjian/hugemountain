package com.aozhi.hugemountain.activity.PublicActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.jpush.android.api.JPushInterface;
import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ConsumerActivity.ConsumerMainActivity;
import com.aozhi.hugemountain.activity.HomeStoreActivity;
import com.aozhi.hugemountain.activity.StaffActivity.MainStaffActivity;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SplashActivity extends BaseActivity {
	private static final int STOPSPLASH = 0;
	// time in milliseconds
	private static final long SPLASHTIME = 2000;

	@Bind(R.id.main)
	RelativeLayout main;
	@Bind(R.id.tv_version)
	TextView tv_version;

	private Animation myAnimation_Alpha;
	private Animation animatinoGone;
	private LoginListObject mLoginListObject;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	public static final String KEY_TITLE = "title";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_EXTRAS = "extras";
	public static boolean isForeground = false;
	private LocationClient mLocationClient;
	private String types = "1";
	public static final String MESSAGE_RECEIVED_ACTION = "com.aozhi.zwty.MESSAGE_RECEIVED_ACTION";

	private Handler splashHandler = new Handler() {
		public void handleMessage(Message msg) {
			SharedPreferences sharedPreferences = getSharedPreferences(
					"client_user", Activity.MODE_PRIVATE);
			String name = sharedPreferences.getString("name", "");
			String pwd = sharedPreferences.getString("pwd", "");

			SharedPreferences sharedPreferences1 = getSharedPreferences(
					"store_user", Activity.MODE_PRIVATE);
			String name1 = sharedPreferences1.getString("name", "");
			String pwd1 = sharedPreferences1.getString("pwd", "");

			SharedPreferences sharedPreferences2 = getSharedPreferences(
					"staff_user", Activity.MODE_PRIVATE);
			String name2 = sharedPreferences2.getString("name", "");
			String pwd2 = sharedPreferences2.getString("pwd", "");
			if (!name.equals("") && !name.equals(null)) {
				types = "1";
				getlogin("getclient_login", name, pwd);
			} else if (!name1.equals("") && !name1.equals(null)) {
				types = "2";
				getlogin("getstore_login", name1, pwd1);
			} else if (!name2.equals("") && !name2.equals(null)) {
				types = "3";
				getlogin("getstaff_login", name2, pwd2);
			} else {
				startActivity(new Intent(SplashActivity.this, LoginsActivity.class));
				SplashActivity.this.finish();
			}
			super.handleMessage(msg);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		ButterKnife.bind(this);
		initLoc();
		JPushInterface.init(getApplicationContext());
		JPushInterface.setDebugMode(true);
		myAnimation_Alpha = AnimationUtils.loadAnimation(this, R.anim.aozhi_alpha_action);
		main.startAnimation(myAnimation_Alpha);
		Message msg = new Message();
		msg.what = STOPSPLASH;
		splashHandler.sendMessageDelayed(msg, SPLASHTIME);
		PackageManager manager = this.getPackageManager();
		PackageInfo info = null;
		try {
			info = manager.getPackageInfo(this.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		String version = info.versionName;
		tv_version.setText("版本号" + version);

	}

	@Override
	protected void initData() {

	}

	@Override
	protected void initView() {

	}

	private void getlogin(String fun, String username, String password) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[]{"fun", fun};
		String[] name = new String[]{"name", username};
		String[] pwd = new String[]{"pwd", encryption(password)};
		params2.add(funParam2);
		params2.add(name);
		params2.add(pwd);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					Logins_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener Logins_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {

				if (types.equals("1")) {
					mLoginListObject = JSON.parseObject(v,
							LoginListObject.class);
					list = mLoginListObject.getResponse();

					if (list.size() > 0) {
						MyApplication.IS_LOGINS = true;
						MyApplication.Clientuser = list.get(0);
						MyApplication.Status = "client";
						Intent mintent = new Intent(SplashActivity.this,
								ConsumerMainActivity.class);
						startActivity(mintent);
						finish();
					} else {
						Toast.makeText(SplashActivity.this, "登录失败！",
								Toast.LENGTH_LONG).show();
						Intent mintent = new Intent(SplashActivity.this,
								LoginsActivity.class);
						startActivity(mintent);
						finish();
					}
				} else if (types.equals("2")) {
					mLoginListObject = JSON.parseObject(v,
							LoginListObject.class);
					list = mLoginListObject.getResponse();
					if (list.size() > 0) {
						if (list.get(0).del_flag.equals("2")) {
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						} else if (list.get(0).del_flag.equals("1")) {
							Toast.makeText(SplashActivity.this, "您登录的账号不存在！",
									Toast.LENGTH_LONG).show();
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						} else if (list.get(0).del_flag.equals("0")) {
							MyApplication.Storeuser = list.get(0);
							MyApplication.Status = "store";
							MyApplication.isstaff = true;
							Intent mintent = new Intent(SplashActivity.this,
									HomeStoreActivity.class);
							startActivity(mintent);
							finish();
						} else {
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						}

					} else {
						Toast.makeText(SplashActivity.this, "登录失败！",
								Toast.LENGTH_LONG).show();
						Intent mintent = new Intent(SplashActivity.this,
								LoginsActivity.class);
						startActivity(mintent);
						finish();
					}
				} else if (types.equals("3")) {
					mLoginListObject = JSON.parseObject(v,
							LoginListObject.class);
					list = mLoginListObject.getResponse();
					if (list.size() > 0) {
						if (list.get(0).del_flag.equals("2")) {
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						} else if (list.get(0).del_flag.equals("1")) {
							Toast.makeText(SplashActivity.this, "您登录的账号不存在！", Toast.LENGTH_LONG).show();
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						} else if (list.get(0).del_flag.equals("0")) {
							MyApplication.Staffuser = list.get(0);
							MyApplication.Status = "staff";
							Intent mintent = new Intent(SplashActivity.this,
									MainStaffActivity.class);
							startActivity(mintent);
							finish();
						} else {
							Intent mintent = new Intent(SplashActivity.this,
									LoginsActivity.class);
							startActivity(mintent);
							finish();
						}
					} else {
						Toast.makeText(SplashActivity.this, "登录失败！",
								Toast.LENGTH_LONG).show();
						Intent mintent = new Intent(SplashActivity.this,
								LoginsActivity.class);
						startActivity(mintent);
						finish();
					}
				}
			} else {
				Toast.makeText(SplashActivity.this, "登录失败！", Toast.LENGTH_LONG)
						.show();
				Intent mintent = new Intent(SplashActivity.this,
						LoginsActivity.class);
				startActivity(mintent);
				finish();
			}
		}
	};

	public static String encryption(String plainText) {
		String re_md5 = new String();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(plainText.getBytes());
			byte b[] = md.digest();

			int i;

			StringBuffer buf = new StringBuffer("");
			for (int offset = 0; offset < b.length; offset++) {
				i = b[offset];
				if (i < 0)
					i += 256;
				if (i < 16)
					buf.append("0");
				buf.append(Integer.toHexString(i));
			}
			re_md5 = buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return re_md5;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		isForeground = true;
		JPushInterface.onResume(this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		isForeground = false;
		JPushInterface.onPause(this);
		super.onPause();
	}

	private void InitLocation() {
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
		option.setCoorType("bd09ll");// 返回的定位结果是百度经纬度，默认值gcj02
		int span = 1000;
		option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
		option.setIsNeedAddress(true);
		mLocationClient.setLocOption(option);
	}

	private void initLoc() {
		mLocationClient = ((MyApplication) getApplication()).mLocationClient;
//		((MyApplication) getApplication()).mLocationResult = tv_city;
		InitLocation();
		mLocationClient.start();
	}

}
