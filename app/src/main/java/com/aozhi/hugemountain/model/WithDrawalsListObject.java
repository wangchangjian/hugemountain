package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class WithDrawalsListObject {
	public Meta meta;
	public ArrayList<WithDrawalsObject> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<WithDrawalsObject> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<WithDrawalsObject> response) {
		this.response = response;
	}
}
