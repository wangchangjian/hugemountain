package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.Set;
import java.util.Timer;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.AboutActivity;
import com.aozhi.hugemountain.activity.PublicActivity.FeedbackActivity;
import com.aozhi.hugemountain.activity.PublicActivity.LoginsActivity;
import com.aozhi.hugemountain.view.AccountItemView;
import com.aozhi.hugemountain.view.ApplicationTopView;
import com.aozhi.hugemountain.view.OnTopClickListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;

public class SettingActivity extends Activity implements OnClickListener {

	private AccountItemView rl_function,rl_about;
	private AccountItemView rl_feedback;
	private ApplicationTopView at_app_top;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);
		initView();
		initListener();
	}

	private void initView() {
		// TODO Auto-generated method stub

		at_app_top= (ApplicationTopView) findViewById(R.id.at_app_top);

		rl_feedback = (AccountItemView) findViewById(R.id.rl_feedback);
		rl_about= (AccountItemView) findViewById(R.id.rl_about);
	}

	private void initListener() {
		rl_feedback.setOnClickListener(this);
		rl_about.setOnClickListener(this);
		at_app_top.setOnTopClickListener(new OnTopClickListener() {
			@Override
			public void onLeftbuttonClick() {
				finish();

			}

			@Override
			public void onRightbuttonClick() {

			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rl_about:{
			Intent intent = new Intent(this, AboutActivity.class);
			startActivity(intent);
		}break;
		case R.id.rl_feedback: {
			Intent intent = new Intent(this, FeedbackActivity.class);
			startActivity(intent);
		}
			break;
		default:
			break;
		}
	}

	protected void quitDialog() { // 确认退出系统
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(getString(R.string.dialog_quit_prompt));
		builder.setTitle(getString(R.string.dialog_prompt));
		builder.setPositiveButton(getString(R.string.btn_confirm),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						SharedPreferences mySharedPreferences = getSharedPreferences(
								"client_user", Activity.MODE_PRIVATE);
						SharedPreferences.Editor editor = mySharedPreferences
								.edit();
						editor.putString("name", "");
						editor.putString("pwd", "");
						editor.putBoolean("ok", false);
						editor.commit();

						JPushInterface.setAlias(getApplicationContext(),
								"", mTagsCallback);
//						JPushInterface.stopPush(getApplicationContext());
						
						Intent mIntent = new Intent(SettingActivity.this,
								LoginsActivity.class);
//						if (MyApplication.Status.equals("staff")) {
//							SharedPreferences sharedPreferences = getSharedPreferences(
//									"staff_user", Activity.MODE_PRIVATE);
//							sharedPreferences.edit().clear().commit();
//						} else if (MyApplication.Status.equals("client")) {
//							SharedPreferences sharedPreferences = getSharedPreferences(
//									"client_user", Activity.MODE_PRIVATE);
//							sharedPreferences.edit().clear().commit();
//						}
						startActivity(mIntent);
						finish();
					}
				});
		builder.setNegativeButton(getString(R.string.btn_cancel),
				new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		builder.create().show();
	}
	
	private final TagAliasCallback mTagsCallback = new TagAliasCallback() {

        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs ;
            switch (code) {
            case 0:
                logs = "Set tag and alias success";
//                Log.i(TAG, logs);
                break;
                
            case 6002:
                logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
//                Log.i(TAG, logs);
//                if (ExampleUtil.isConnected(getApplicationContext())) {
//                	mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_TAGS, tags), 1000 * 60);
//                } else {
////                	Log.i(TAG, "No network");
//                }
                break;
            
            default:
                logs = "Failed with errorCode = " + code;
//                Log.e(TAG, logs);
            }
            
//            ExampleUtil.showToast(logs, getApplicationContext());
        }
        
    };
	
	private static Boolean isQuit = false;
	Timer timer = new Timer();
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//	        if (keyCode == KeyEvent.KEYCODE_BACK) {
//	            if (isQuit == false) {
//	                isQuit = true;
//	                Toast.makeText(getBaseContext(), "再按一次返回键退出程序", Toast.LENGTH_SHORT).show();
//	                TimerTask task = null;
//	                task = new TimerTask() {
//	                    @Override
//	                    public void run() {
//	                        isQuit = false;
//	                    }
//	                };
//	                timer.schedule(task, 2000);
//	            } else {
//	            	 MyApplication.IS_LOGINS=false;
//	                finish();
//	                System.exit(0);
//	            }
//	        }
//	        return false;
//	}
}
