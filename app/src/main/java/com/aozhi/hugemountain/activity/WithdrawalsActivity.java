package com.aozhi.hugemountain.activity;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.WithDrawalsAdapter;
import com.aozhi.hugemountain.model.AccountListObject;
import com.aozhi.hugemountain.model.AccountObject;
import com.aozhi.hugemountain.model.WithDrawalsListObject;
import com.aozhi.hugemountain.model.WithDrawalsObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class WithdrawalsActivity extends Activity implements OnClickListener {
	Button btn_back;
	TextView tv_name, tv_balance, tv_lasttime;
	ListView list_withdrawals;
	WithDrawalsObject mWithDrawalsObject;
	WithDrawalsListObject mWithDrawalsListObject;
	private ProgressDialog progressDialog=null;
	WithDrawalsAdapter adapter;
	ArrayList<WithDrawalsObject> list = new ArrayList<WithDrawalsObject>();
	public Button tv_tx;
	public EditText et_money;
	private AccountListObject mAccountListObject;
	private ArrayList<AccountObject> acclist = new ArrayList<AccountObject>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_withdrawals);
		initView();
		myclick();
//		getwithdrawals();
	}

	private void initView() {
		btn_back = (Button) findViewById(R.id.btn_back);
		tv_name = (TextView) findViewById(R.id.tv_name);
		tv_name.setText(MyApplication.Staffuser.name.toString());
		tv_balance = (TextView) findViewById(R.id.tv_balance);
		tv_balance.setText(MyApplication.Staffuser.balance);
		tv_tx = (Button) findViewById(R.id.tv_tx);
		et_money = (EditText) findViewById(R.id.et_money);
		tv_balance.setText(MyApplication.Staffuser.balance);
	}

	private void myclick() {
		btn_back.setOnClickListener(this);
		tv_tx.setOnClickListener(this);
	}

	private void getwithdrawals() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getWithdrawalsStaff" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(name1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				mWithDrawalsListObject = JSON.parseObject(v,
						WithDrawalsListObject.class);
				list = mWithDrawalsListObject.response;
				if (mWithDrawalsListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						tv_balance.setText(list.get(0).money);
					}
				}
			}
		}
	};

	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()) {
		case R.id.tv_tx:
			
			if(et_money.getText().toString().equals("")){
				Toast.makeText(WithdrawalsActivity.this, "您输入提现金额",
						Toast.LENGTH_SHORT).show();
			}else{
			if (Double.parseDouble(tv_balance.getText().toString()) >= Double
					.parseDouble(et_money.getText().toString().trim())) {
				getAccount();
			} else {
				Toast.makeText(WithdrawalsActivity.this, "您输入的提现金额不能大于可提现金额",
						Toast.LENGTH_SHORT).show();
			}
			}
			break;
		case R.id.btn_back:
			finish();
			break;
		default:
			break;
		}

	}

	private void addLog() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "addApply" };
		String[] name1 = new String[] { "staff_id", MyApplication.Staffuser.id };
		String[] name2 = new String[] { "store_id", "0" };
		String[] name3 = new String[] { "del_flag", "2" };// 0为正常，1为删除，2为审核
		String[] name4 = new String[] { "money",et_money.getText().toString().trim()};
		String[] name5=new String[]{"card",acclist.get(0).zfb_name};
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name3);
		params2.add(name4);
		params2.add(name2);
		params2.add(name5);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					list_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener list_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			// 对设备结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				
				BigDecimal a1  =new BigDecimal(MyApplication.Staffuser.balance);
				BigDecimal a2  =new BigDecimal(et_money.getText().toString().trim());
				MyApplication.Staffuser.balance=a1.subtract(a2).toString();
				
				Toast.makeText(WithdrawalsActivity.this, "提现申请提交成功，请等待审核",
						Toast.LENGTH_SHORT).show();
				finish();
			}else{
				Toast.makeText(WithdrawalsActivity.this, "提现申请提交申请",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	/**
	 * 获取支付宝账号
	 */
	
	private void getAccount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffZfb" };
		String[] idParam2 = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					upCashRecord_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener upCashRecord_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				acclist = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if(acclist.size()>0){
						addLog();
					}else{
						Intent mintent=new Intent(WithdrawalsActivity.this,AddNet.class);
						mintent.putExtra("stats", "staff");
						startActivity(mintent);
//						Toast.makeText(CashRecordActivity.this,"对不起,你没有添加支付账户请添加", Toast.LENGTH_SHORT).show();
					}
				}
			} else {
				Toast.makeText(WithdrawalsActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}
		}
	};

}
