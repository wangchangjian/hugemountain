package com.aozhi.hugemountain.model;

import java.io.Serializable;

 public class ServiceObject implements Serializable{

	  public String id;
      public String name;
      public String remark;
      public String money;
      public String store_id;
      public String staff_id;
      public String create_time;
      public String update_time;
      public String mark;
      public String del_flag;
      public String photo;
      public String click_count;
      public String service_time;
      public String store_service_id;
      public String type;
      public String remarks;
      public String src;
      
      public String pay_manoy;
      public String surchange;
      
      public String star;
	@Override
	public String toString() {
		return "服务"+ id+ "\t" + name+ "\t" + remark+ "\t" + money+ "\t" + store_id+ "\t" + staff_id+ "\t" + create_time+"\n";
	}
}
