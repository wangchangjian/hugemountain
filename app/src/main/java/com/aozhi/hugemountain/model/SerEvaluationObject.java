package com.aozhi.hugemountain.model;

import java.util.ArrayList;

public class SerEvaluationObject {
	public Meta meta;
	public ArrayList<SerEvaluationBean> response;
	public Meta getMeta() {
		return meta;
	}
	public void setMeta(Meta meta) {
		this.meta = meta;
	}
	public ArrayList<SerEvaluationBean> getResponse() {
		return response;
	}
	public void setResponse(ArrayList<SerEvaluationBean> response) {
		this.response = response;
	}
	
}
