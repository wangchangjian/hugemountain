package com.aozhi.hugemountain.activity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.LoginsActivity;
import com.aozhi.hugemountain.http.HttpConnection;
import com.aozhi.hugemountain.http.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.Utils;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FindPwd3Activity extends Activity implements OnClickListener {

	private Button btn_back, btn_login;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LoginListObject mMemberListObject;
	private ProgressDialog progressDialog = null;
	private EditText et_pwd1, et_pwd;
	private String tel = "",types="";
	private String isMemory = "";// isMemory变量用来判断SharedPreferences有没有数据，包括上面的YES和NO
	private String FILE = "saveUserNamePwd";// 用于保存SharedPreferences的文件
	private SharedPreferences sp = null;// 声明一个SharedPreferences

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_findpwd3);
		initView();
		initListnner();
	}

	private void initView() {
		// TODO Auto-generated method stub
		tel = getIntent().getStringExtra("tel");
		types = getIntent().getStringExtra("types");
		btn_back = (Button) findViewById(R.id.btn_back);
		btn_login = (Button) findViewById(R.id.btn_login);
		et_pwd1 = (EditText) findViewById(R.id.et_pwd1);
		et_pwd = (EditText) findViewById(R.id.et_pwd);
	}

	private void initListnner() {
		// TODO Auto-generated method stub
		btn_back.setOnClickListener(this);
		btn_login.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_back: {
			Intent mIntent=new Intent(FindPwd3Activity.this,CodoActivity.class);
			startActivity(mIntent);
			finish();
		}
			break;

		case R.id.btn_login: {
			if (et_pwd.getText().toString() == null
					|| et_pwd.getText().toString().equals("")) {
				Toast.makeText(FindPwd3Activity.this, "请输入密码", Toast.LENGTH_SHORT)
						.show();
			} else if (et_pwd1.getText().toString() == null
					|| et_pwd1.getText().toString().equals("")) {
				Toast.makeText(FindPwd3Activity.this, "请再次输入密码", Toast.LENGTH_SHORT)
						.show();
			} else if (!et_pwd.getText().toString()
					.equals(et_pwd1.getText().toString())) {
				Toast.makeText(FindPwd3Activity.this, "两次输入的密码不一致，请重新输入",
						Toast.LENGTH_SHORT).show();
			} else {
				GetUsername();
			}
		}
		default:
			break;
		}
	}

	private void GetUsername() {
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			ArrayList<String[]> params = new ArrayList<String[]>();
			String[] methodParam=null;
			if(types.equals("1")){
				methodParam = new String[] { "fun", "upClientPs" };
			}else if(types.equals("2")){
				methodParam = new String[] { "fun", "upStorePs" };
			}else{
				methodParam = new String[] { "fun", "upStaffPs" };
			}
			String[] pageParam = new String[] { "username", tel };
			String[] pwdParam = new String[] { "password",
					Utils.encryption(et_pwd.getText().toString()) };
			params.add(methodParam);
			params.add(pageParam);
			params.add(pwdParam);
			progressDialog = ProgressDialog.show(this, null, "正在加载", false);
			progressDialog.setCancelable(true);
			new HttpConnection().get(Constant.URL, params,
					GetInFo_callbackListener);
		} else
			Toast.makeText(this, "请检查网络连接状态", Toast.LENGTH_LONG).show();
	}

	private CallbackListener GetInFo_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String result) {
			Log.d("返回数据", result);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!result.equals("fail")) {
				mMemberListObject = JSON.parseObject(result,
						LoginListObject.class);
				list = mMemberListObject.response;
				if (mMemberListObject.meta.getMsg().equals("OK")) {
					Toast.makeText(FindPwd3Activity.this, "密码设置成功",
							Toast.LENGTH_LONG).show();
					if (sp == null) {
						sp = getSharedPreferences(FILE, MODE_PRIVATE);
					}
					Editor edit = sp.edit();
					edit.putString("password", et_pwd.getText().toString());
					edit.commit();
					Intent mIntent = new Intent(FindPwd3Activity.this,
							LoginsActivity.class);
					startActivity(mIntent);
					finish();
				} else {
					Toast.makeText(FindPwd3Activity.this, "密码设置失败",
							Toast.LENGTH_LONG).show();
				}

			}
		}
	};
}

/*
extends Activity {
	Button btn_ok;
	 Button btn_back;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_findpwd3);
		btn_ok = (Button)findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(FindPwd3Activity.this, LoginsActivity.class);
				startActivity(intent);
			}
		});
		btn_back = (Button)findViewById(R.id.btn_back);
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_pwd3, menu);
		return true;
	}
*/
