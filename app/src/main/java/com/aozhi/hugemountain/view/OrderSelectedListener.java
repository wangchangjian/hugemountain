package com.aozhi.hugemountain.view;

/**
 * Created by ${wangchangjian} on 2016/5/1.
 */
public interface OrderSelectedListener {
    void onBookPressed();

    void onRightNowPressed();
}
