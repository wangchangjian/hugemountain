package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.PublicActivity.ImagePagerActivity;
import com.aozhi.hugemountain.activity.LocationsActivity;
import com.aozhi.hugemountain.adapter.StorepicAdapter;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.list.HorizontalListView;
import com.aozhi.hugemountain.model.ServiceListObject;
import com.aozhi.hugemountain.model.ServiceObject;
import com.aozhi.hugemountain.model.StoreListObject;
import com.aozhi.hugemountain.model.StoreObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.aozhi.hugemountain.view.JumpProgressDialog;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.navi.BaiduMapAppNotSupportNaviException;
import com.baidu.mapapi.navi.BaiduMapNavigation;
import com.baidu.mapapi.navi.NaviParaOption;
import com.baidu.mapapi.utils.OpenClientUtil;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StaffStoreDetailsActivity extends Activity {

    Button btn_back, btn_staff_ok, btn_service_ok;
    private ImageView img_address, img_tel;
    private ArrayList<StoreObject> list_store = new ArrayList<StoreObject>();
    private ArrayList<StoreObject> store_list = new ArrayList<StoreObject>();
    private String store_id = "";
    private HorizontalListView horizon_listview;
    StoreListObject mStoreListObject;
    StoreObject mStoreObject;
    ImageView img_avatar;
    private LocationClient mLocationClient;
    TextView name;
    TextView title;
    TextView business_houre;
    TextView remark;
    TextView address;
    TextView phone;
    ImageView photo;
    TextView service;
    private JumpProgressDialog progressDialog ;
    private StorepicAdapter mStorepicAdapter;
    private ArrayList<ServiceObject> piclist = new ArrayList<ServiceObject>();
    private ServiceListObject mServiceListObject1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_storedetails);
        initLoc();
        initView();
        getbusinesss();
        getStorePic();

        horizon_listview.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                Intent mIntent = new Intent(StaffStoreDetailsActivity.this,
                        ImagePagerActivity.class);
                mIntent.putExtra("images", piclist.get(arg2).src);
                startActivity(mIntent);
            }
        });
    }

    private void InitLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(LocationMode.Hight_Accuracy);// 设置定位模式
        option.setCoorType("gcj02");// 返回的定位结果是百度经纬度，默认值gcj02
        int span = 1000;
        option.setScanSpan(span);// 设置发起定位请求的间隔时间为5000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    private void initLoc() {
        mLocationClient = ((MyApplication) getApplication()).mLocationClient;
        // tv_city = (TextView) findViewById(R.id.tv_city);
        // ((MyApplication) getApplication()).mLocationResult = tv_city;
        InitLocation();
        mLocationClient.start();
        // System.out.println("获取到的定位信息是：" + tv_city.getText().toString());
    }

    private void initView() {
        progressDialog = new JumpProgressDialog(this);

        store_id = getIntent().getStringExtra("store_id");
        phone = (TextView) findViewById(R.id.phone);
        img_tel = (ImageView) findViewById(R.id.img_tel);
        business_houre = (TextView) findViewById(R.id.business_houre);
        remark = (TextView) findViewById(R.id.remark);
        address = (TextView) findViewById(R.id.address_name);
        photo = (ImageView) findViewById(R.id.photo);
        service = (TextView) findViewById(R.id.service);
        name = (TextView) findViewById(R.id.name);
        img_avatar = (ImageView) findViewById(R.id.img_avatar);
        title = (TextView) findViewById(R.id.title);
        img_address = (ImageView) findViewById(R.id.img_address);
        btn_back = (Button) findViewById(R.id.btn_back);

        horizon_listview = (HorizontalListView) findViewById(R.id.horizon_listview);
        mStorepicAdapter = new StorepicAdapter(this, piclist);
        horizon_listview.setAdapter(mStorepicAdapter);
        btn_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                finish();
            }
        });
        img_address.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent mIntent = new Intent(StaffStoreDetailsActivity.this, LocationsActivity.class);
                mIntent.putExtra("lat", mStoreObject.latitude);
                mIntent.putExtra("lng", mStoreObject.longitude);
                mIntent.putExtra("address", mStoreObject.address);
                startActivity(mIntent);
            }
        });
        img_tel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                // 传入服务， parse（）解析号码
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"
                        + list_store.get(0).storephoto));
                // 通知activtity处理传入的call服务
                startActivity(intent);
            }
        });
    }

    // private void spinner(Spinner sp) {
    // ArrayList<String> time=new ArrayList<String>();
    // for(int i=0;i<24;i++){
    // if(i<10){
    // time.add("0"+i+":00");
    // time.add("0"+i+":30");
    // }else{
    // time.add(i+":00");
    // time.add(i+":30");
    // }
    // }
    // ArrayAdapter<String> spadapter=new
    // ArrayAdapter<String>(getApplicationContext(), R.layout.item_time, time);
    // sp.setAdapter(spadapter);
    // }
    public void startNavi() {
        LatLng pt1 = new LatLng(MyApplication.app_latitude,
                MyApplication.app_longitude);
        LatLng pt2 = new LatLng(Double.valueOf(list_store.get(0).latitude),
                Double.valueOf(list_store.get(0).longitude));
        // 构建 导航参数
        NaviParaOption para = new NaviParaOption().startPoint(pt1)
                .endPoint(pt2).startName(MyApplication.address)
                .endName(list_store.get(0).address);
        try {
            BaiduMapNavigation.openBaiduMapNavi(para, this);
        } catch (BaiduMapAppNotSupportNaviException e) {
            e.printStackTrace();
            showDialog();
        }
    }

    /**
     * 提示未安装百度地图app或app版本过低
     */

    private void showDialog() {
        Dialog alertDialog = new AlertDialog.Builder(this)
                .setTitle("提示")
                .setMessage("您尚未安装百度地图app或app版本过低，点击确认安装？")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        OpenClientUtil
                                .getLatestBaiduMapApp(StaffStoreDetailsActivity.this);
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                    }
                }).create();
        alertDialog.show();
    }

    private void getStorePic() {
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params = new ArrayList<String[]>();
            String[] methodParam = new String[]{"fun", "getStorePic"};
            String[] geotable_idParam = new String[]{"id", store_id};

            params.add(methodParam);
            params.add(geotable_idParam);

            new HttpConnection().get(Constant.URL, params,
                    getStorePic_callbackListener);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }

    }

    private CallbackListener getStorePic_callbackListener = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);

            if (!v.equals("fail")) {
                mServiceListObject1 = JSON.parseObject(v,
                        ServiceListObject.class);
                piclist = mServiceListObject1.response;
                if (piclist.size() > 0) {
                    mStorepicAdapter = new StorepicAdapter(
                            StaffStoreDetailsActivity.this, piclist);
                    horizon_listview.setAdapter(mStorepicAdapter);
                } else {
                    horizon_listview.setVisibility(View.GONE);
                }
            }
        }
    };

    private void getbusinesss() {
        ArrayList<String[]> params2 = new ArrayList<String[]>();
        String[] funParam2 = new String[]{"fun", "getstore"};
        String[] name1 = new String[]{"store_id", store_id};
        params2.add(funParam2);
        params2.add(name1);
        progressDialog.showDialog();
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            new HttpConnection().get(Constant.URL, params2,
                    type_callbackListener3);
        } else {
            Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
        }
    }

    private CallbackListener type_callbackListener3 = new HttpConnection.CallbackListener() {
        @Override
        public void callBack(String v) {
            // TODO Auto-generated method stub
            Log.d("返回数据", v);
            // 对结果进行解析
            if (progressDialog != null) {
                progressDialog.dimissDialog();
            }
            if (!v.equals("fail")) {// 当请求网络返回值正常
                mStoreListObject = JSON.parseObject(v, StoreListObject.class);
                list_store = mStoreListObject.response;
                if (mStoreListObject.meta.getMsg().equals("OK")) {
                    if (list_store.size() > 0) {
                        mStoreObject = list_store.get(0);
                        MyApplication.this_store = mStoreObject;
                        // phone;
                        business_houre.setText(mStoreObject.business_hours);
                        remark.setText(mStoreObject.remark);
                        name.setText(mStoreObject.name);
                        title.setText(mStoreObject.name);
                        address.setText("地址：" + mStoreObject.address);
                        phone.setText("电话：" + mStoreObject.storephoto);
                        service.setText(mStoreObject.service);

                        if (mStoreObject.phone.equals("")
                                || mStoreObject.phone == null) {

                        } else {
                            MyApplication.downloadImage.addTask(mStoreObject.phone, photo,
                                    new DownloadImage.ImageCallback() {

                                        @Override
                                        public void imageLoaded(
                                                Bitmap imageBitmap,
                                                String imageUrl) {
                                            if (imageBitmap != null) {
                                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                                // xHolder.tx_image.setTag("");
                                                Drawable drawable = new BitmapDrawable(
                                                        imageBitmap);
                                                photo.setBackgroundDrawable(drawable);
                                            }
                                        }

                                        @Override
                                        public void imageLoaded(Bitmap imageBitmap, DownloadImageMode callBackTag) {
                                            if (imageBitmap != null) {
                                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                                // xHolder.tx_image.setTag("");
                                                Drawable drawable = new BitmapDrawable(imageBitmap);
                                                photo.setBackgroundDrawable(drawable);
                                            }
                                        }

                                    });
                        }
                        MyApplication.downloadImage.doTask();
                    } else {
                        Toast.makeText(StaffStoreDetailsActivity.this, "无数据",
                                Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(StaffStoreDetailsActivity.this, "无数据",
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(StaffStoreDetailsActivity.this, "无数据",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

    private void setAvatar(String path) {
        if (path.equals("") || path == null) {

        } else {
            MyApplication.downloadImage.addTask(path, img_avatar,
                    new DownloadImage.ImageCallback() {

                        @Override
                        public void imageLoaded(Bitmap imageBitmap,
                                                String imageUrl) {
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                img_avatar.setBackgroundDrawable(drawable);
                            }
                        }

                        @Override
                        public void imageLoaded(Bitmap imageBitmap,
                                                DownloadImageMode callBackTag) {
                            // TODO 自动生成的方法存根
                            if (imageBitmap != null) {
                                // xHolder.tx_image.setImageBitmap(imageBitmap);
                                // xHolder.tx_image.setTag("");
                                Drawable drawable = new BitmapDrawable(
                                        imageBitmap);
                                img_avatar.setBackgroundDrawable(drawable);
                            }
                        }

                    });
        }
        MyApplication.downloadImage.doTask();
    }

    private void upStaffStatus(String status) {
        Constant.NET_STATUS = Utils.getCurrentNetWork(this);
        if (Constant.NET_STATUS) {
            ArrayList<String[]> params2 = new ArrayList<String[]>();
            String[] funParam2 = new String[]{"fun", "upWorkStatus"};
            String[] name1 = new String[]{"user_id",
                    MyApplication.Staffuser.id};
            String[] name2 = new String[]{"store_id", store_id};
            String[] name3 = new String[]{"status", status};
            params2.add(funParam2);
            params2.add(name1);
            params2.add(name2);
            params2.add(name3);
            progressDialog.showDialog();
            Constant.NET_STATUS = Utils.getCurrentNetWork(this);
            if (Constant.NET_STATUS) {
                new HttpConnection().get(Constant.URL, params2,
                        type_callbackListener);
            } else {
                Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
            }
        }
    }

    private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {

        @Override
        public void callBack(String v) {
            Log.d("返回数据", v);
            if (progressDialog != null) {
                progressDialog.dimissDialog();
            }
            if (!v.equals("fail")) {
                mStoreListObject = JSON.parseObject(v, StoreListObject.class);
                store_list = mStoreListObject.response;
                if (mStoreListObject.meta.getMsg().equals("OK")) {
                    if (store_list.size() > 0) {
                        Toast.makeText(StaffStoreDetailsActivity.this, "签到成功", Toast.LENGTH_SHORT)
                                .show();
                        Intent mintent = new Intent(StaffStoreDetailsActivity.this,
                                StaffStoreListActivity.class);
                        setResult(133, mintent);
                        mLocationClient.stop();
                        finish();
                    }
                } else {
                    Toast.makeText(StaffStoreDetailsActivity.this, "签到失败",
                            Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(StaffStoreDetailsActivity.this, "签到失败",
                        Toast.LENGTH_LONG).show();
            }
        }
    };

}
