package com.aozhi.hugemountain.activity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.adapter.ImageAddAdapter;
import com.aozhi.hugemountain.http.UploadImageService;
import com.aozhi.hugemountain.model.LoginBean;
import com.aozhi.hugemountain.model.LoginListObject;
import com.aozhi.hugemountain.model.ManageStaffObject;
import com.aozhi.hugemountain.model.StaffsListObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;

public class EditStaffActivity extends Activity {
	private Button btn_back;
	private TextView tv_ok;
	private Spinner tv_type;
	private EditText et_content, et_commission;
	private ArrayList<LoginBean> list = new ArrayList<LoginBean>();
	private LoginListObject mLoginListObject;
	private ProgressDialog progressDialog = null;
	private String id = "";
	private TextView et_server;
	private StaffsListObject mStaffsListObject;
	private ArrayList<ManageStaffObject> type_list = new ArrayList<ManageStaffObject>();
	private ArrayList<Integer> myChose = new ArrayList<Integer>();
	private String typeid = "";
	private TextView tv_camera, tv_photo;
	public static final int NONE = 0;
	public static final int PHOTOHRAPH = 1;// ����
	public static final int PHOTOZOOM = 2; // ����
	public static final int PHOTORESOULT = 3;// ���
	public static final String IMAGE_UNSPECIFIED = "image/*";
	private String serverFile = "";
	private GridView add_photo1;
	private List<Bitmap> lists = new ArrayList<Bitmap>();
	private ImageAddAdapter adapter;
	private ArrayList<String> pt = new ArrayList<String>();
	private TextView status1, status2, status3;
	private String status="0";
	private String paths="/picture/u11.png";
	private String serv_id="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_editstaff);
		initView();
		getType();
		myclick();
	}

	private void initView() {
		tv_type = (Spinner) findViewById(R.id.tv_type1);
		tv_ok = (TextView) findViewById(R.id.tv_ok);
		btn_back = (Button) findViewById(R.id.btn_back);
		id = getIntent().getStringExtra("id");
		et_content = (EditText) findViewById(R.id.et_content1);
		et_commission = (EditText) findViewById(R.id.et_commission1);
		et_server = (TextView) findViewById(R.id.ed_server1);
		add_photo1 = (GridView) findViewById(R.id.add_photo1);
		status1 = (TextView) findViewById(R.id.status1);
		status2 = (TextView) findViewById(R.id.status2);
		status3 = (TextView) findViewById(R.id.status3);
		Bitmap bmp = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_addpic_focused);
		lists.add(bmp);
		adapter = new ImageAddAdapter(this, lists);
		add_photo1.setAdapter(adapter);
		getStaffList();
	}

	private void myclick() {

		et_server.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getService();
			}
		});
		btn_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				setResult(403);
				finish();
			}
		});

		tv_ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				upStaffUser();
				
			}
		});
		add_photo1.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				showAtaver();
			}
		});
		status1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status="1";
				status1.setBackgroundColor(Color.parseColor("#FF7900"));
				status1.setTextColor(Color.parseColor("#ffffff"));
				status2.setBackgroundColor(Color.parseColor("#ffffff"));
				status2.setTextColor(Color.parseColor("#000000"));
				status3.setBackgroundColor(Color.parseColor("#ffffff"));
				status3.setTextColor(Color.parseColor("#000000"));
			}
		});
		status2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status="2";
				status1.setBackgroundColor(Color.parseColor("#ffffff"));
				status1.setTextColor(Color.parseColor("#000000"));
				status2.setBackgroundColor(Color.parseColor("#FF7900"));
				status2.setTextColor(Color.parseColor("#ffffff"));
				status3.setBackgroundColor(Color.parseColor("#ffffff"));
				status3.setTextColor(Color.parseColor("#000000"));
			}
		});
		status3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				status="3";
				status1.setBackgroundColor(Color.parseColor("#ffffff"));
				status1.setTextColor(Color.parseColor("#000000"));
				status2.setBackgroundColor(Color.parseColor("#ffffff"));
				status2.setTextColor(Color.parseColor("#000000"));
				status3.setBackgroundColor(Color.parseColor("#FF7900"));
				status3.setTextColor(Color.parseColor("#ffffff"));
			}
		});
	}

	private void getType() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStafftype" };
		params2.add(funParam2);

		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					type_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener type_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				type_list = mStaffsListObject.getResponse();
				if (type_list.size() > 0) {
					Spinner();
				}

			} else {
				Toast.makeText(EditStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void getStaffList() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffDetails" };
		String[] idParam2 = new String[] { "id", id };
		String[] idParam1 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam1);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (list.size() > 0) {
					Spinner();
					paths=list.get(0).avatar;
					et_content.setText(list.get(0).remark);
					et_commission.setText(list.get(0).commission);
					et_server.setText(list.get(0).service_name);
					switch (Integer.parseInt(list.get(0).workstatus)) {
					case 0:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 1:
						status1.setBackgroundColor(Color.parseColor("#FF7900"));
						status1.setTextColor(Color.parseColor("#ffffff"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 2:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#FF7900"));
						status2.setTextColor(Color.parseColor("#ffffff"));
						status3.setBackgroundColor(Color.parseColor("#ffffff"));
						status3.setTextColor(Color.parseColor("#000000"));
						break;
					case 3:
						status1.setBackgroundColor(Color.parseColor("#ffffff"));
						status1.setTextColor(Color.parseColor("#000000"));
						status2.setBackgroundColor(Color.parseColor("#ffffff"));
						status2.setTextColor(Color.parseColor("#000000"));
						status3.setBackgroundColor(Color.parseColor("#FF7900"));
						status3.setTextColor(Color.parseColor("#ffffff"));
						break;
					default:

						break;
					}
				}
			} else {
				Toast.makeText(EditStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	// 下拉框适配器
	private void Spinner() {
		ArrayList<String> timelist = new ArrayList<String>();
		// 第一步：添加一个下拉列表项的list，这里添加的项就是下拉列表的列表项
		for (int i = 0; i < type_list.size(); i++) {
			timelist.add(type_list.get(i).types);
		}
		// 第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.item_type, timelist);
		// 第三步：为适配器设置下拉列表下拉时的菜单样式。
		// adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// 第四步：将适配器添加到下拉列表上
		tv_type.setAdapter(adapter);
		// 第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		tv_type.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				typeid = type_list.get(position).id;
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				typeid = list.get(0).types;
			}
		});
		SpinnerAdapter apsAdapter = tv_type.getAdapter(); // 得到SpinnerAdapter对象
		int k = apsAdapter.getCount();
		if (list.size() > 0) {
			for (int i = 0; i < k; i++) {
				if (list.get(0).type.equals(apsAdapter.getItem(i).toString())) {
					tv_type.setSelection(i, true);// 默认选中项
					break;
				}
			}
		}
	}

	// 上传更新数据
	private void upStaffUser() {
		if(pt.size()<1){
			pt.add(paths);
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "upuserstaff" };
		String[] idParam1 = new String[] { "type_id", typeid };
		String[] idParam2=new String[]{"store_id",MyApplication.Storeuser.id};
		String[] idParam4 = new String[] { "contents",
				et_content.getText().toString().trim() };
		String[] idParam5 = new String[] { "staff_id", id };
		String[] idParam6 = new String[] {"commission",et_commission.getText().toString()};
		params2.add(funParam2);
		params2.add(idParam1);
		params2.add(idParam2);
		params2.add(idParam4);
		params2.add(idParam5);
		params2.add(idParam6);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					upStaffUser1_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener upStaffUser1_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mLoginListObject = JSON.parseObject(v, LoginListObject.class);
				list = mLoginListObject.getResponse();
				if (mLoginListObject.meta.getMsg().equals("OK")) {
					
					upStaffServer(serv_id,et_server.getText().toString());
				}
			} else {
				Toast.makeText(EditStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	private void showAtaver() {
		final AlertDialog dlg = new AlertDialog.Builder(this).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.activity_xiugai);
		tv_camera = (TextView) window.findViewById(R.id.tv_camera);
		tv_photo = (TextView) window.findViewById(R.id.tv_photo);
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
						Environment.getExternalStorageDirectory(), "temp.jpg")));
				System.out.println("============="
						+ Environment.getExternalStorageDirectory());
				startActivityForResult(intent, PHOTOHRAPH);
				dlg.cancel();
			}
		});
		tv_photo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(Intent.ACTION_PICK, null);
				intent.setDataAndType(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
				startActivityForResult(intent, PHOTOZOOM);
				dlg.cancel();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.i("OnActivityResult", "**********************coming!");
		if (resultCode == NONE)
			return;
		// ����
		if (requestCode == PHOTOHRAPH) {
			// �����ļ�����·��������ڸ�Ŀ¼��
			File picture = new File(Environment.getExternalStorageDirectory()
					+ "/temp.jpg");
			System.out.println("------------------------" + picture.getPath());
			startPhotoZoom(Uri.fromFile(picture));
		}
		if (data == null)
			return;
		// ��ȡ�������ͼƬ
		if (requestCode == PHOTOZOOM) {
			startPhotoZoom(data.getData());
		}
		// ������
		if (requestCode == PHOTORESOULT) {
			Bundle extras = data.getExtras();
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				photo.compress(Bitmap.CompressFormat.JPEG, 75, stream);// (0 -

				new DateFormat();
				String name = DateFormat.format("yyyyMMdd_hhmmss",
						Calendar.getInstance(Locale.CHINA))
						+ ".jpg";
				Bundle bundle = data.getExtras();
				Bitmap bitmap = (Bitmap) bundle.get("data");
				FileOutputStream b = null;
				
				File file = new File("/sdcard/myImage/");
				file.mkdirs();
				String fileName = "/sdcard/myImage/" + name;
				try {
					b = new FileOutputStream(fileName);
					bitmap.compress(Bitmap.CompressFormat.JPEG, 100, b);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} finally {
					try {
						b.flush();
						b.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				File file1 = new File(fileName);
				new UploadAsyncTask().execute(file1);
				if (lists.size() < 9) {
					lists.add(bitmap);
					adapter.notifyDataSetChanged();
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public void startPhotoZoom(Uri uri) {
		Intent mintent = new Intent("com.android.camera.action.CROP");
		mintent.setDataAndType(uri, IMAGE_UNSPECIFIED);
		mintent.putExtra("crop", "true");
		mintent.putExtra("aspectX", 1);
		mintent.putExtra("aspectY", 1);
		mintent.putExtra("outputX", 320);
		mintent.putExtra("outputY", 320);
		mintent.putExtra("return-data", true);
		startActivityForResult(mintent, PHOTORESOULT);
	}

	class UploadAsyncTask extends AsyncTask<File, String, String> {
		@Override
		protected String doInBackground(File... params) {
			serverFile = UploadImageService
					.uploadFile(params[0], Constant.API1);
			System.out.println("---------serverFile------------" + serverFile);
			pt.add(serverFile);
			return serverFile;
		}
	}

	private void showMultiChosDia(final ArrayList<ManageStaffObject> list) {
		final String[] mList = new String[list.size()];
		final boolean mChoseSts[] = new boolean[list.size()];
		String serverStr=this.et_server.getText().toString();
		for (int i = 0; i < list.size(); i++) {
			mList[i] = list.get(i).name;
			if(serverStr.indexOf(list.get(i).name)>-1){
				mChoseSts[i] = true;
			}else{
			mChoseSts[i] = false;}
		}
		AlertDialog.Builder multiChosDia = new AlertDialog.Builder(
				EditStaffActivity.this);
		multiChosDia.setMultiChoiceItems(mList, mChoseSts,
				new DialogInterface.OnMultiChoiceClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which,
							boolean isChecked) {
						mChoseSts[which]=isChecked;
					}
				});
		multiChosDia.setPositiveButton("确定",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String str = "";
						serv_id="";
						for (int i = 0; i <mChoseSts.length; i++) {
							if(mChoseSts[i]){
							str += mList[i] + ",";
							serv_id+=list.get(i).id+",";
							}
						}
						
						if(mChoseSts.length>0){
							if(str.length()>0){
								str=str.substring(0,str.length()-1);
							}
						
						serv_id=","+serv_id;
						et_server.setText(str);
						}
					}
				});
		multiChosDia.create().show();
	}

	// 获取店铺服务列表
	private void getService() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getservicebystore" };
		String[] nameParam = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(nameParam);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					service_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener service_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				mStaffsListObject = JSON.parseObject(v, StaffsListObject.class);
				type_list = mStaffsListObject.getResponse();
				if (type_list.size() > 0) {
					showMultiChosDia(type_list);
				}

			} else {
				Toast.makeText(EditStaffActivity.this, "无数据", Toast.LENGTH_LONG)
						.show();
			}

		}
	};

	// 上传图片功能
	private void UpStaffPhoto(String path) {
		if (path.equals("") || path.equals(null)) {
			return;
		}
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		
		String[] funParam2 = new String[] { "fun", "upStaffPhoto" };
		String[] nameParam2 = new String[] { "staff_id", id };
		String[] nameParam1 = new String[] { "path", path };
		params2.add(funParam2);
		params2.add(nameParam2);
		params2.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					AddProject_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener AddProject_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
			}
			if (!v.equals("fail")) {
				if (v.indexOf("OK") != -1) {

					Toast.makeText(getApplicationContext(), "图片上传成功",
							Toast.LENGTH_LONG).show();

				} else {
					Toast.makeText(getApplicationContext(), "图片上传失败",
							Toast.LENGTH_LONG).show();
				}

			} else {
				Toast.makeText(getApplicationContext(), "图片上传失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 删除店铺服务列表
	private void delServices() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delstaffclass" };
		String[] nameParam = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam1=new String[]{"staff_id",id};
		params2.add(funParam2);
		params2.add(nameParam);
		params2.add(nameParam1);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					delservice_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener delservice_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (!v.equals("fail")) {
				String[] ids=serv_id.split("[,]");
				for(int i=0;i<ids.length;i++){
					setServices(ids[i]);
				}
			} else {
				Toast.makeText(EditStaffActivity.this, "失败", Toast.LENGTH_LONG)
						.show();
			}
		}
	};
	
	// 更新店铺服务列表
		private void setServices(String service_id) {
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "setstaffclass" };
			String[] nameParam = new String[] { "store_id",
					MyApplication.Storeuser.id };
			String[] nameParam1=new String[]{"staff_id",id};
			String[] nameParam2=new String[]{"service_id",service_id};
			params2.add(funParam2);
			params2.add(nameParam);
			params2.add(nameParam1);
			params2.add(nameParam2);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						setservice_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener setservice_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				if (!v.equals("fail")) {
					

				} else {
					Toast.makeText(EditStaffActivity.this, "失败", Toast.LENGTH_LONG)
							.show();
				}
			}
		};
		
		
		
		private void upStaffServer(String server_id,String server_name) {
			
			ArrayList<String[]> params2 = new ArrayList<String[]>();
			String[] funParam2 = new String[] { "fun", "upStaffServer" };
			String[] nameParam2 = new String[] { "server_id", server_id };
			String[] nameParam1 = new String[] { "server_name", server_name };
			String[] nameParam3 = new String[] { "user_store_id", MyApplication.Storeuser.id };
			String[] nameParam4 = new String[] { "user_staff_id", id };
			params2.add(funParam2);
			params2.add(nameParam1);
			params2.add(nameParam2);
			params2.add(nameParam3);
			params2.add(nameParam4);
			Constant.NET_STATUS = Utils.getCurrentNetWork(this);
			if (Constant.NET_STATUS) {
				new HttpConnection().get(Constant.URL, params2,
						upStaffServer_callbackListener);
			} else {
				Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
			}
		}

		private CallbackListener upStaffServer_callbackListener = new HttpConnection.CallbackListener() {
			@Override
			public void callBack(String v) {
				Log.d("返回数据", v);
				if (progressDialog != null) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				if (!v.equals("fail")) {
					if (v.indexOf("OK") != -1) {
						
						Toast.makeText(EditStaffActivity.this, "信息更新成功",
								Toast.LENGTH_LONG).show();
						for (int i = 1; i < pt.size(); i++) {
							UpStaffPhoto(pt.get(i));
						}
						delServices();
						Intent mintent = new Intent(getApplicationContext(),
								UpdateStaffActivity.class);
						setResult(403);
						finish();
					} 
				} 
			}
		};

}
