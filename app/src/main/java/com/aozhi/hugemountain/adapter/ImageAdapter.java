package com.aozhi.hugemountain.adapter;

import java.util.ArrayList;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.http.DownloadImage;
import com.aozhi.hugemountain.http.DownloadImageMode;
import com.aozhi.hugemountain.model.*;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter  extends BaseAdapter{

	private Context mContext;
	private LayoutInflater inflater;
	private ArrayList<ImageObject> invite_list = new ArrayList<ImageObject>();
	
	public ImageAdapter(Context ctx, ArrayList<ImageObject> li) {
		mContext = ctx;
		invite_list = li;
		inflater = LayoutInflater.from(mContext);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return invite_list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return invite_list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		final Holder mHolder;
		if (convertView == null) {
			mHolder = new Holder();
			convertView = inflater.inflate(R.layout.item_uploadimg, null);
			mHolder.img_upload= (ImageView) convertView.findViewById(R.id.img_upload);
			mHolder.name=(TextView) convertView.findViewById(R.id.tv_name);
			mHolder.uploadtime=(TextView) convertView.findViewById(R.id.tv_time);
			convertView.setTag(mHolder);
		} else {
			mHolder = (Holder) convertView.getTag();
		}
		mHolder.name.setText(invite_list.get(position).name);
		mHolder.uploadtime.setText(invite_list.get(position).createtime);
		if (invite_list.get(position).pic.equals("")|| invite_list.get(position).pic==null ) {
			
		} else {
			MyApplication.downloadImage.addTask(
					invite_list.get(position).pic, mHolder.img_upload,
					new DownloadImage.ImageCallback() {

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								String imageUrl) {
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(imageBitmap);
								mHolder.img_upload.setBackgroundDrawable(drawable);
							}
						}

						@Override
						public void imageLoaded(Bitmap imageBitmap,
								DownloadImageMode callBackTag) {
							// TODO 自动生成的方法存根
							if (imageBitmap != null) {
								// xHolder.tx_image.setImageBitmap(imageBitmap);
								// xHolder.tx_image.setTag("");
								Drawable drawable = new BitmapDrawable(
										imageBitmap);
								mHolder.img_upload.setBackgroundDrawable(drawable);
							}
						}

					});
		}
		MyApplication.downloadImage.doTask();
		return convertView;
	}
	
	class Holder {
		ImageView img_upload;
		TextView name,uploadtime;
	}
	
}
