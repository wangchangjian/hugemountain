package com.aozhi.hugemountain.activity.StaffActivity;

import java.util.ArrayList;

import com.alibaba.fastjson.JSON;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.AddNet;
import com.aozhi.hugemountain.activity.Addcard;
import com.aozhi.hugemountain.model.AccountListObject;
import com.aozhi.hugemountain.model.AccountObject;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class StaffAccountActivity extends Activity implements OnClickListener {

	private Button btn_back;
	private LinearLayout cardadd, card1, card2, card3, card4;
	private LinearLayout net1, net2, net3, net4, netadd;
	private LinearLayout carditem, netitem;
	private TextView netcode1, netcode2, netcode3, netcode4,netname;
	private TextView tv_1, tv_2, cardtype1, cardtype2, cardtype3, cardtype4,
			cardnumber1, cardnumber2, cardnumber3, cardnumber4;
	private TextView cardaddr, time;
	private ProgressDialog progressDialog;
	private AccountListObject mAccountListObject;
	private ArrayList<AccountObject> list = new ArrayList<AccountObject>();
	private ArrayList<AccountObject> list1 = new ArrayList<AccountObject>();
	private int status = 0;
	private String stats;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_staffaccount);
		initView();
		initListener();
		if (stats.equals("staff")) {
			getStaffCardInfo();
		} else {
			getStoreCardInfo();
		}

	}

	private void initListener() {
		btn_back.setOnClickListener(this);
		cardadd.setOnClickListener(this);
		tv_1.setOnClickListener(this);
		tv_2.setOnClickListener(this);
		netadd.setOnClickListener(this);
		card1.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				AlertDialog.Builder builder = new Builder(
						StaffAccountActivity.this);
				builder.setMessage("确认删除吗？\n\t注意删除不可恢复！！！");
				builder.setPositiveButton("确认",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if(stats.equals("staff")){
									delStaffInfo("5");
								}else{
								delStoreInfo("5");
								}
								arg0.dismiss();
							}
						});
				builder.setNegativeButton("取消",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.dismiss();
							}
						});
				builder.create().show();
				return true;
			}
		});
		net1.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				AlertDialog.Builder builder = new Builder(
						StaffAccountActivity.this);
				builder.setMessage("确认删除吗？\n\t注意删除不可恢复！！！");
				builder.setPositiveButton("确认",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								if(stats.equals("staff")){
									delStaffInfo("6");
								}else{
								   delStoreInfo("6");
								}
								arg0.dismiss();
							}
						});
				builder.setNegativeButton("取消",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								arg0.dismiss();
							}
						});
				builder.create().show();
				return true;
			}
		});
	}

	private void initView() {
		stats = getIntent().getStringExtra("stats");
		btn_back = (Button) findViewById(R.id.btn_back);
		cardadd = (LinearLayout) findViewById(R.id.cardadd);
		tv_1 = (TextView) findViewById(R.id.tv_1);
		tv_2 = (TextView) findViewById(R.id.tv_2);
		card1 = (LinearLayout) findViewById(R.id.card1);
		card2 = (LinearLayout) findViewById(R.id.card2);
		card3 = (LinearLayout) findViewById(R.id.card3);
		card4 = (LinearLayout) findViewById(R.id.card4);
		cardtype1 = (TextView) findViewById(R.id.cardtype1);
		cardtype2 = (TextView) findViewById(R.id.cardtype2);
		cardtype3 = (TextView) findViewById(R.id.cardtype3);
		cardtype4 = (TextView) findViewById(R.id.cardtype4);
		cardnumber1 = (TextView) findViewById(R.id.cardnumber1);
		cardnumber2 = (TextView) findViewById(R.id.cardnumber2);
		cardnumber3 = (TextView) findViewById(R.id.cardnumber3);
		cardnumber4 = (TextView) findViewById(R.id.cardnumber4);
		cardaddr = (TextView) findViewById(R.id.cardaddr);
		carditem = (LinearLayout) findViewById(R.id.carditem);
		netitem = (LinearLayout) findViewById(R.id.netitem);
		net1 = (LinearLayout) findViewById(R.id.net1);
		net2 = (LinearLayout) findViewById(R.id.net2);
		net3 = (LinearLayout) findViewById(R.id.net3);
		net4 = (LinearLayout) findViewById(R.id.net4);
		netadd = (LinearLayout) findViewById(R.id.netadd);
		netcode1 = (TextView) findViewById(R.id.netcode1);
		netcode2 = (TextView) findViewById(R.id.netcode2);
		netcode3 = (TextView) findViewById(R.id.netcode3);
		netcode4 = (TextView) findViewById(R.id.netcode4);
		time = (TextView) findViewById(R.id.time);
		netname=(TextView) findViewById(R.id.netname);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_back:
			finish();
			break;
		case R.id.cardadd:
			Intent mintent = new Intent(StaffAccountActivity.this,
					Addcard.class);
			mintent.putExtra("stats",stats);
			startActivityForResult(mintent, 386);
			break;
		case R.id.netadd:
			Intent intent = new Intent(getApplicationContext(), AddNet.class);
			intent.putExtra("stats",stats);
			startActivityForResult(intent, 387);
			break;
		case R.id.tv_1:
			tv_1.setBackgroundColor(Color.parseColor("#E59105"));
			tv_1.setTextColor(Color.parseColor("#ffffff"));
			tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
			tv_2.setTextColor(Color.parseColor("#000000"));
			carditem.setVisibility(View.VISIBLE);
			netitem.setVisibility(View.GONE);
			if (stats.equals("staff")) {
				getStaffCardInfo();
			} else {
				getStoreCardInfo();
			};
			break;
		case R.id.tv_2:
			tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
			tv_1.setTextColor(Color.parseColor("#000000"));
			tv_2.setBackgroundColor(Color.parseColor("#E59105"));
			tv_2.setTextColor(Color.parseColor("#ffffff"));
			carditem.setVisibility(View.GONE);
			netitem.setVisibility(View.VISIBLE);
			if (stats.equals("staff")) {
				getStaffZFBInfo();
			} else {
				getStoreZFBInfo();
			}
			break;
		default:
			break;
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		case 386:
			tv_1.setBackgroundColor(Color.parseColor("#E59105"));
			tv_1.setTextColor(Color.parseColor("#ffffff"));
			tv_2.setBackgroundColor(Color.parseColor("#ffffff"));
			tv_2.setTextColor(Color.parseColor("#000000"));
			carditem.setVisibility(View.VISIBLE);
			netitem.setVisibility(View.GONE);
			if (stats.equals("staff")) {
				getStaffCardInfo();
			} else {
				getStoreCardInfo();
			}
			break;
		case 387:
			tv_1.setBackgroundColor(Color.parseColor("#ffffff"));
			tv_1.setTextColor(Color.parseColor("#000000"));
			tv_2.setBackgroundColor(Color.parseColor("#E59105"));
			tv_2.setTextColor(Color.parseColor("#ffffff"));
			carditem.setVisibility(View.GONE);
			netitem.setVisibility(View.VISIBLE);
			if (stats.equals("staff")) {
				getStaffZFBInfo();
			} else {
				getStoreZFBInfo();
			}
			break;
		default:
			break;
		}
	}

	// 获取店铺银行卡信息
	private void getStoreCardInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreCard" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						card1.setVisibility(View.VISIBLE);
						cardadd.setVisibility(View.GONE);
						cardtype1.setText(list.get(0).card_type);
						cardnumber1.setText(list.get(0).card_number);
						cardaddr.setText(list.get(0).card_address);
					} else {
						card1.setVisibility(View.GONE);
						cardadd.setVisibility(View.VISIBLE);
					}
				}
			} else {
				Toast.makeText(StaffAccountActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 获取店铺支付宝账号
	private void getStoreZFBInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStoreZfb" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list1 = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if (list1.size() > 0) {
						net1.setVisibility(View.VISIBLE);
						netadd.setVisibility(View.GONE);
						netcode1.setText(list1.get(0).zfb_name);
						netname.setText(list1.get(0).zfb_username);
						time.setText(list1.get(0).create_time);
					} else {
						net1.setVisibility(View.GONE);
						netadd.setVisibility(View.VISIBLE);
					}
				}
			} else {
				Toast.makeText(StaffAccountActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	private void delStoreInfo(String type) {
		status = Integer.parseInt(type);
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delStoreAccountInfo" };
		String[] idParam2 = new String[] { "store_id",
				MyApplication.Storeuser.id };
		String[] nameParam = new String[] { "del_flag", type };
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					delStore_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener delStore_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				switch (status) {
				case 5:
					card1.setVisibility(View.GONE);
					cardadd.setVisibility(View.VISIBLE);
					break;
				case 6:
					net1.setVisibility(View.GONE);
					netadd.setVisibility(View.VISIBLE);
					break;
				default:
					break;
				}
				Toast.makeText(StaffAccountActivity.this, "删除成功",
						Toast.LENGTH_LONG).show();

			} else {
				Toast.makeText(StaffAccountActivity.this, "删除失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 获取店铺银行卡信息
	private void getStaffCardInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffCard" };
		String[] idParam2 = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener11);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener11 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if (list.size() > 0) {
						card1.setVisibility(View.VISIBLE);
						cardadd.setVisibility(View.GONE);
						cardtype1.setText(list.get(0).card_type);
						cardnumber1.setText(list.get(0).card_number);
						cardaddr.setText(list.get(0).card_address);
					} else {
						card1.setVisibility(View.GONE);
						cardadd.setVisibility(View.VISIBLE);
					}
				}
			} else {
				Toast.makeText(StaffAccountActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};

	// 获取店铺支付宝账号
	private void getStaffZFBInfo() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "getStaffZfb" };
		String[] idParam2 = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		params2.add(funParam2);
		params2.add(idParam2);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStore_callbackListener12);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStore_callbackListener12 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				mAccountListObject = JSON.parseObject(v,
						AccountListObject.class);
				list1 = mAccountListObject.response;
				if (mAccountListObject.meta.getMsg().equals("OK")) {
					if (list1.size() > 0) {
						net1.setVisibility(View.VISIBLE);
						netadd.setVisibility(View.GONE);
						netcode1.setText(list1.get(0).zfb_name);
						netname.setText(list1.get(0).zfb_username);
						time.setText(list1.get(0).create_time);
					} else {
						net1.setVisibility(View.GONE);
						netadd.setVisibility(View.VISIBLE);
					}
				}
			} else {
				Toast.makeText(StaffAccountActivity.this, "无数据",
						Toast.LENGTH_LONG).show();
			}

		}
	};
	
	private void delStaffInfo(String type) {
		status = Integer.parseInt(type);
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "delStaffAccountInfo" };
		String[] idParam2 = new String[] { "staff_id",
				MyApplication.Staffuser.id };
		String[] nameParam = new String[] { "del_flag", type };
		params2.add(funParam2);
		params2.add(idParam2);
		params2.add(nameParam);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					delStore_callbackListener1);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener delStore_callbackListener1 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				switch (status) {
				case 5:
					card1.setVisibility(View.GONE);
					cardadd.setVisibility(View.VISIBLE);
					break;
				case 6:
					net1.setVisibility(View.GONE);
					netadd.setVisibility(View.VISIBLE);
					break;
				default:
					break;
				}
				Toast.makeText(StaffAccountActivity.this, "删除成功",
						Toast.LENGTH_LONG).show();

			} else {
				Toast.makeText(StaffAccountActivity.this, "删除失败",
						Toast.LENGTH_LONG).show();
			}

		}
	};
}
