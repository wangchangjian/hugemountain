package com.aozhi.hugemountain.activity;

import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.activity.ConsumerActivity.ConsumerAccountActivity;
import com.aozhi.hugemountain.activity.ConsumerActivity.ConsumerOrderActivity;
import com.aozhi.hugemountain.activity.StaffActivity.OrderStaffActivity;
import com.aozhi.hugemountain.activity.StaffActivity.SettingActivity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TabHost;

public class MainActivity extends TabActivity {

	TabHost tabhost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		initView();
		initListener();
	}

	private void initView() {
		// TODO Auto-generated method stub
		tabhost = this.getTabHost();
		TabHost.TabSpec spec;
		Intent intent;

		intent = new Intent().setClass(this, HomeActivity.class);
		spec = tabhost.newTabSpec("home").setIndicator("home")
				.setContent(intent);
		tabhost.addTab(spec);
		if(MyApplication.Status.equals("staff")){
			intent = new Intent().setClass(this, OrderStaffActivity.class);
		}else if(MyApplication.Status.equals("client")){
			intent = new Intent().setClass(this, ConsumerOrderActivity.class);
		}
		spec = tabhost.newTabSpec("order").setIndicator("order")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, ConsumerAccountActivity.class);
		spec = tabhost.newTabSpec("account").setIndicator("account")
				.setContent(intent);
		tabhost.addTab(spec);

		intent = new Intent().setClass(this, SettingActivity.class);
		spec = tabhost.newTabSpec("setting").setIndicator("setting")
				.setContent(intent);
		tabhost.addTab(spec);
		tabhost.setCurrentTab(0);

		RadioGroup radioGroup = (RadioGroup) this
				.findViewById(R.id.main_tab_group);
		radioGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(RadioGroup group, int checkedId) {
						// TODO Auto-generated method stub
						switch (checkedId) {
						case R.id.staff_order:
							tabhost.setCurrentTabByTag("home");
							break;
						case R.id.staff_store:
							tabhost.setCurrentTabByTag("order");
							break;
						case R.id.staff_account:
							tabhost.setCurrentTabByTag("account");
							break;
						case R.id.staff_state:
							tabhost.setCurrentTabByTag("setting");
							break;
						default:
							;
							break;
						}
					}
				});
	}

	private void initListener() {
		// TODO Auto-generated method stub

	}
}
