package com.aozhi.hugemountain.http;

import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;

import android.graphics.Bitmap;
import android.text.TextUtils;

public class ImageMemoryCache {
	/** 锟斤拷锟斤拷锟斤拷锟斤拷 **/
	private static final int HARD_CACHE_CAPACITY = 40;
	private HashMap<String, Bitmap> hashMap;
	private final static ConcurrentHashMap<String, SoftReference<Bitmap>> mSoftBitmapCache = new ConcurrentHashMap<String, SoftReference<Bitmap>>(
			HARD_CACHE_CAPACITY / 2);
	
	public ImageMemoryCache(){
		
		hashMap = new LinkedHashMap<String, Bitmap>(HARD_CACHE_CAPACITY / 2, 0.75f, true){
			
			@Override
			protected boolean removeEldestEntry(LinkedHashMap.Entry<String, Bitmap> eldest) {
				// TODO Auto-generated method stub
				
				//锟斤拷map锟斤拷size()锟斤拷锟斤拷40时锟斤拷锟斤拷锟斤拷锟斤拷玫锟絢ey锟脚碉拷mSoftBitmapCache锟叫ｏ拷锟接讹拷证hashMap锟斤拷效锟斤拷
				if(size() > HARD_CACHE_CAPACITY){
					mSoftBitmapCache.put(eldest.getKey(), new SoftReference<Bitmap>(eldest.getValue()));
					return true;
				}else {
					return false;
				}
			}
		};
		
	}
	
	/**
	 * 锟接伙拷锟斤拷锟叫伙拷取图片
	 */
	public Bitmap getBitmapFromCache(String url) {
		// 锟饺达拷hashMap锟斤拷锟斤拷锟叫伙拷取
		if (hashMap != null && hashMap.size() > 0) {
			synchronized (hashMap) {
				try {
					final Bitmap bitmap = hashMap.get(url);
					if (bitmap != null) {
						// 锟斤拷锟斤拷业锟斤拷幕锟斤拷锟斤拷锟皆拷锟斤拷频锟絣inkedhashmap锟斤拷锟斤拷前锟芥，锟接讹拷证锟斤拷LRU锟姐法锟斤拷锟斤拷锟斤拷锟缴撅拷锟�
						hashMap.remove(url);
						hashMap.put(url, bitmap);
						return bitmap;
					}
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				}
			}
		}

		if (mSoftBitmapCache != null && !TextUtils.isEmpty(url)) {
			// 锟斤拷锟絟ashMap锟斤拷锟揭诧拷锟斤拷锟斤拷锟斤拷mSoftBitmapCache锟斤拷锟斤拷
			final SoftReference<Bitmap> bitmapReference = mSoftBitmapCache.get(url);
					
			if (bitmapReference != null) {
				try {
					final Bitmap bitmap = bitmapReference.get();
					if (bitmap != null) {
						// 锟斤拷图片锟狡伙拷硬锟斤拷锟斤拷
						hashMap.put(url, bitmap);
						mSoftBitmapCache.remove(url);
						return bitmap;
					} else {
						mSoftBitmapCache.remove(url);
					}
				} catch (OutOfMemoryError e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	/**
	 * 锟斤拷锟酵计拷锟斤拷锟斤拷锟�
	 * 
	 * @param url
	 * @param bitmap
	 */
	public void addBitmapToCache(String url, Bitmap bitmap) {
		if (bitmap != null) {
			synchronized (hashMap) {
				hashMap.put(url, bitmap);
			}
		}
	}
	
	/**
     * 删锟斤拷锟节存缓锟斤拷锟叫碉拷图片
     * @param url
     */
    public void deleteBitmapFromCache(String url) {
        synchronized (hashMap) {
            if (hashMap.containsKey(url)) {
            	hashMap.remove(url);
            }
        }
        
        if (mSoftBitmapCache.containsKey(url)) {
            mSoftBitmapCache.remove(url);
        }
    }
}
