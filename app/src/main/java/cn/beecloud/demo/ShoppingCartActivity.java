package cn.beecloud.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.aozhi.hugemountain.MyApplication;
import com.aozhi.hugemountain.R;
import com.aozhi.hugemountain.utils.Constant;
import com.aozhi.hugemountain.utils.HttpConnection;
import com.aozhi.hugemountain.utils.Utils;
import com.aozhi.hugemountain.utils.HttpConnection.CallbackListener;
import com.unionpay.UPPayAssistEx;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import cn.beecloud.BCPay;
import cn.beecloud.BeeCloud;
import cn.beecloud.async.BCCallback;
import cn.beecloud.async.BCResult;
import cn.beecloud.entity.BCPayResult;

public class ShoppingCartActivity extends Activity {

	Button btnQueryOrders;
	private String tag = "hunout";
	private String price, name, servive, order_id, login_id;
	private ProgressDialog loadingDialog;
	private ListView payMethod;
	private TextView cash, shopname, orderid, back;
	private ProgressDialog progressDialog = null;
	private String balance, percent;
	private String store_id = "";
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
			"yyyyMMddHHmmssSSS", Locale.CHINA);
	private String paytype = "";
	public String yue = "";
	private boolean weixing =false;
	private boolean zhifubao =false;
	// 支付结果返回入口
	BCCallback bcCallback = new BCCallback() {
		@Override
		public void done(final BCResult bcResult) {
			final BCPayResult bcPayResult = (BCPayResult) bcResult;
			// 此处关闭loading界面
			loadingDialog.dismiss();
			// 如果想通过Toast通知用户结果，请使用如下方式，
			// 直接makeText有可能会造成java.lang.RuntimeException: Can't create handler
			// inside thread that has not called Looper.prepare()
			ShoppingCartActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					String result = bcPayResult.getResult();
					/*
					 * 注意！ 所有支付渠道建议以服务端的状态为准，此处返回的RESULT_SUCCESS仅仅代表手机端支付成功，
					 * 该操作并非强制性要求，虽然属于小概率事件，但渠道官方推荐以防止欺诈手段
					 * 
					 * 对于PayPal的每一次支付，sdk会自动帮你与服务端同步，
					 * 如果与服务端同步失败，记录会被自动保存，此时你可以调用batchSyncPayPalPayment方法手动同步
					 * 虽然这种情况比较少
					 * ，但是建议参考PayPalUnSyncedListActivity做好同步，否则服务器将无法查阅到订单
					 */
					if (result.equals(BCPayResult.RESULT_SUCCESS)) {
						if (servive.equals("充值")) {
							setClientRecharge();
						} else {
							// 微信和支付宝支付时
							if(weixing){
								paytype = "2";
							}
							
							if(zhifubao){
								paytype = "1";
							}
							
							setOrderStatuspaytype("20");
						}

						Toast.makeText(ShoppingCartActivity.this, "用户支付成功",
								Toast.LENGTH_LONG).show();
					}
					if (result.equals(BCPayResult.RESULT_CANCEL)) {
						Toast.makeText(ShoppingCartActivity.this, "用户取消支付",
								Toast.LENGTH_LONG).show();
					}

					if (result.equals(BCPayResult.RESULT_FAIL)) {
						Toast.makeText(
								ShoppingCartActivity.this,
								"支付失败, 原因: " + bcPayResult.getErrMsg() + ", "
										+ bcPayResult.getDetailInfo(),
								Toast.LENGTH_LONG).show();
						Log.v(tag,
								bcPayResult.getErrMsg() + ", "
										+ bcPayResult.getDetailInfo());
						if (bcPayResult.getErrMsg().equals(
								BCPayResult.FAIL_PLUGIN_NOT_INSTALLED)
								|| bcPayResult.getErrMsg().equals(
										BCPayResult.FAIL_PLUGIN_NEED_UPGRADE)) {
							// 银联需要重新安装控件
							Message msg = mHandler.obtainMessage();
							msg.what = 1;
							mHandler.sendMessage(msg);
						}
					}
					if (result.equals(BCPayResult.RESULT_UNKNOWN)) {
						// 可能出现在支付宝8000返回状态
						Toast.makeText(ShoppingCartActivity.this, "订单状态未知",
								Toast.LENGTH_LONG).show();
					} else {
						Toast.makeText(ShoppingCartActivity.this,
								"invalid return", Toast.LENGTH_LONG).show();
					}
				}
			});
		}
	};

	// Defines a Handler object that's attached to the UI thread.
	// 通过Handler.Callback()可消除内存泄漏警告
	private Handler mHandler = new Handler(new Handler.Callback() {
		/**
		 * Callback interface you can use when instantiating a Handler to avoid
		 * having to implement your own subclass of Handler.
		 * 
		 * handleMessage() defines the operations to perform when the Handler
		 * receives a new Message to process.
		 */
		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == 1) {
				// 如果用户手机没有安装银联支付控件,则会提示用户安装
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ShoppingCartActivity.this);
				builder.setTitle("提示");
				builder.setMessage("完成支付需要安装或者升级银联支付控件，是否安装？");

				builder.setNegativeButton("确定",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								UPPayAssistEx
										.installUPPayPlugin(ShoppingCartActivity.this);
								dialog.dismiss();
							}
						});

				builder.setPositiveButton("取消",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}
						});
				builder.create().show();
			}
			return true;
		}
	});

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shopping_cart);
		price = getIntent().getStringExtra("cash");
		yue = getIntent().getStringExtra("yue");
		name = getIntent().getStringExtra("storename");
		servive = getIntent().getStringExtra("services");
		order_id = getIntent().getStringExtra("orderid");
		login_id = getIntent().getStringExtra("login_id");
		balance = getIntent().getStringExtra("balance");
		percent = getIntent().getStringExtra("percent");
		store_id = getIntent().getStringExtra("store_id");
		cash = (TextView) findViewById(R.id.cash);
		back = (TextView) findViewById(R.id.back);
		shopname = (TextView) findViewById(R.id.shopname);
		orderid = (TextView) findViewById(R.id.orderid);
		cash.setText(price);
		
		shopname.setText(name + "＞" + servive);
		orderid.setText(order_id);
		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		// 推荐在主Activity里的onCreate函数中初始化BeeCloud.
		BeeCloud.setAppIdAndSecret("3ac746c9-335a-4ece-820d-46029ae47a65",
				"b8962091-226e-4540-929a-1b6a0c3e8dc6");

		// 如果用到微信支付，在用到微信支付的Activity的onCreate函数里调用以下函数.
		// 第二个参数需要换成你自己的微信AppID.
		BCPay.initWechatPay(ShoppingCartActivity.this, "wx51a3100b95f2d46b");

		// 如果使用PayPal需要在支付之前设置client id和应用secret
		// BCPay.PAYPAL_PAY_TYPE.SANDBOX用于测试，BCPay.PAYPAL_PAY_TYPE.LIVE用于生产环境
		BCPay.initPayPal(
				"AVT1Ch18aTIlUJIeeCxvC7ZKQYHczGwiWm8jOwhrREc4a5FnbdwlqEB4evlHPXXUA67RAAZqZM0H8TCR",
				"EL-fkjkEUyxrwZAmrfn46awFXlX-h2nRkyCVhhpeVdlSRuhPJKXx3ZvUTTJqPQuAeomXA8PZ2MkX24vF",
				BCPay.PAYPAL_PAY_TYPE.SANDBOX, Boolean.FALSE);

		payMethod = (ListView) this.findViewById(R.id.payMethod);

		String text = "";
		Integer[] payIcons = null;
		String[] payNames = null;
		String[] payDescs = null;

		// 用户支付
		double cbd = 0.0;
		if (yue != null) {
			cbd = Double.valueOf(yue);
			if (cbd > 0.0) {
				text = "余额支付   (可用余额" + cbd + ")";
				payIcons = new Integer[] { R.drawable.cash, R.drawable.wechat,
						R.drawable.alipay, R.drawable.cash };
				payNames = new String[] { "现金支付", "微信支付", "支付宝支付", text };
				payDescs = new String[] { "使用现金支付，以人民币CNY计费",
						"使用微信支付，以人民币CNY计费", "使用支付宝支付，以人民币CNY计费",
						"使用余额支付，以人民币CNY计费" };
			}
			if (cbd <= 0.0) {
				payIcons = new Integer[] { R.drawable.cash, R.drawable.wechat,
						R.drawable.alipay };
				payNames = new String[] { "现金支付", "微信支付", "支付宝支付" };
				payDescs = new String[] { "使用现金支付，以人民币CNY计费",
						"使用微信支付，以人民币CNY计费", "使用支付宝支付，以人民币CNY计费" };
			}
		}

		// 会员充值
		if (servive != null) {
			if (servive.equals("充值")) {
				payIcons = new Integer[] { R.drawable.cash, R.drawable.wechat,
						R.drawable.alipay };
				payNames = new String[] { "现金支付", "微信支付", "支付宝支付" };
				payDescs = new String[] { "使用现金支付，以人民币CNY计费",
						"使用微信支付，以人民币CNY计费", "使用支付宝支付，以人民币CNY计费" };

			}
		}
		PayMethodListItem adapter = new PayMethodListItem(this, payIcons,
				payNames, payDescs);
		payMethod.setAdapter(adapter);

		// 如果调起支付太慢, 可以在这里开启动画, 以progressdialog为例
		loadingDialog = new ProgressDialog(ShoppingCartActivity.this);
		loadingDialog.setMessage("启动第三方支付，请稍候...");
		loadingDialog.setIndeterminate(true);
		loadingDialog.setCancelable(true);
		payMethod.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position) {
				case 1: // 微信
					weixing = true;
					loadingDialog.show();
					// 对于微信支付, 手机内存太小会有OutOfResourcesException造成的卡顿, 以致无法完成支付
					// 这个是微信自身存在的问题
					Map<String, String> mapOptional = new HashMap<String, String>();
					mapOptional.put("testkey1", "测试value值1"); // map的key暂时不支持中文
					int sum = (int) (Double.parseDouble(price) * 100);
					// mapOptional.put("paymentid", order_id);
					// mapOptional.put("consumptioncode", "consumptionCode");
					// mapOptional.put("money", String.valueOf(sum));
					if (BCPay.isWXAppInstalledAndSupported()
							&& BCPay.isWXPaySupported()) {
						sum = (int) (Double.parseDouble(price) * 100);
						BCPay.getInstance(ShoppingCartActivity.this)
								.reqWXPaymentAsync("微信支付", // 订单标题
										sum, // 订单金额(分)
										order_id, // 订单流水号
										null, // 扩展参数(可以null)
										bcCallback); // 支付完成后回调入口
					}
					break;

				case 2: // 支付宝支付
					zhifubao = true;
					loadingDialog.show();
					sum = (int) (Double.parseDouble(price) * 100);
					mapOptional = new HashMap<String, String>();
					mapOptional.put("paymentid", order_id);
					mapOptional.put("consumptioncode", "consumptionCode");
					mapOptional.put("money", String.valueOf(sum));
					BCPay.getInstance(ShoppingCartActivity.this)
							.reqAliPaymentAsync("支付宝支付", sum, order_id, null,
									bcCallback);
					break;
				case 0: // 现金支付
					if (servive.equals("充值")) {
						AlertDialog.Builder builder = new Builder(
								ShoppingCartActivity.this);
						builder.setMessage("确认充值吗？");
						builder.setPositiveButton("确定",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										// 充值
										setClientRecharge();
										arg0.dismiss();
									}
								});
						builder.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										arg0.dismiss();
									}
								});
						builder.create().show();
					} else {
						AlertDialog.Builder builder = new Builder(
								ShoppingCartActivity.this);
						builder.setMessage("确认现金支付吗？");
						builder.setPositiveButton("确定",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										Toast.makeText(getApplicationContext(),
												"请稍等,服务员马上过来",
												Toast.LENGTH_SHORT).show();
										paytype = "现金支付";
										setOrderStatuspaytype("10");
										// 40
										setOrderStatus1();
										arg0.dismiss();
									}
								});
						builder.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										arg0.dismiss();
									}
								});
						builder.create().show();
					}
					break;

				case 3: // 余额支付
					AlertDialog.Builder builder = new Builder(
							ShoppingCartActivity.this);
					builder.setMessage("确认余额支付吗？");
					builder.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									paytype = "0";
									if (Double.valueOf(price) > Double
											.valueOf(yue)) {
										Toast.makeText(
												ShoppingCartActivity.this,
												"余额不足,请充值", Toast.LENGTH_LONG)
												.show();
									} else {
										// 修改用户余额
										UpClientAmount();
										// 改变支付状态和支付方式 并执行存储过程
										setOrderStatuspaytype("20");
									}
									arg0.dismiss();
								}
							});
					builder.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
								}
							});
					builder.create().show();
					break;
				default:

					// startActivity(intent);
				}
			}
		});

		btnQueryOrders = (Button) findViewById(R.id.btnQueryOrders);
		btnQueryOrders.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(ShoppingCartActivity.this,
						OrdersEntryActivity.class);
				startActivity(intent);
			}
		});
	}

	String genBillNum() {
		return simpleDateFormat.format(new Date());
	}

	// 余额支付时
	private void UpClientAmount() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "UpClientAmount" };
		String[] name1 = new String[] { "client_id",
				MyApplication.Clientuser.id };
		String[] name2 = new String[] { "store_id", store_id };
		String[] name3 = new String[] { "balance", price };
		params2.add(funParam2);
		params2.add(name1);
		params2.add(name2);
		params2.add(name3);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					UpClientAmount_callbackListener2);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener UpClientAmount_callbackListener2 = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			// TODO Auto-generated method stub
			Log.d("返回数据", v);
			// 对结果进行解析
			if (!v.equals("fail")) {// 当请求网络返回值正常
				// 修改用户余额
				BigDecimal b1 = new BigDecimal(MyApplication.Clientuser.balance);
				BigDecimal b2 = new BigDecimal(price);
				MyApplication.Clientuser.balance = String.valueOf(b1
						.subtract(b2));
			} else {
				Toast.makeText(ShoppingCartActivity.this, "支付失败",
						Toast.LENGTH_LONG).show();
			}
		}
	};

	/*
	 * //修改订单状态并执行存储过程 private void setOrderStatus1(String states) {
	 * ArrayList<String[]> params2 = new ArrayList<String[]>(); String[]
	 * funParam2 = new String[] { "fun", "setOrderStatus" }; String[] nameParam1
	 * = new String[] { "order_id", order_id }; String[] nameParam2 = new
	 * String[] { "orderstatus", states }; params2.add(funParam2);
	 * params2.add(nameParam1); params2.add(nameParam2); Constant.NET_STATUS =
	 * Utils.getCurrentNetWork(this); if (Constant.NET_STATUS) { new
	 * HttpConnection().get(Constant.URL, params2,
	 * setOrderStatus1_callbackListener); } else { Toast.makeText(this,
	 * "请检查网络连接", Toast.LENGTH_LONG).show(); } }
	 * 
	 * private CallbackListener setOrderStatus1_callbackListener = new
	 * HttpConnection.CallbackListener() {
	 * 
	 * @Override public void callBack(String v) { Log.d("返回数据", v);
	 * 
	 * if (!v.equals("fail")) { Toast.makeText(ShoppingCartActivity.this,
	 * "支付成功", Toast.LENGTH_LONG).show(); finish(); }else{
	 * Toast.makeText(ShoppingCartActivity.this, "支付失败",
	 * Toast.LENGTH_LONG).show(); } } };
	 */

	// 充值
	private void setClientRecharge() {
		Double recharge = Double.valueOf(price)
				* (100 + Double.valueOf(percent)) / 100;
		Double sum = recharge + Double.valueOf(balance);
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setClientReCharge" };
		String[] nameParam1 = new String[] { "client_login_id", login_id };
		String[] nameParam2 = new String[] { "balance", String.valueOf(sum) };
		String[] nameParam3 = new String[] { "money", price };
		String[] nameParam4 = new String[] { "recharge_money",
				String.valueOf(recharge) };
		String[] nameParam5 = new String[] { "store_id", store_id };
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		params2.add(nameParam4);
		params2.add(nameParam5);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getStaffList_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getStaffList_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				setResult(23);
				Toast.makeText(ShoppingCartActivity.this, "充值成功",
						Toast.LENGTH_SHORT).show();
				finish();
			} else {
				Toast.makeText(ShoppingCartActivity.this, "充值失败",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	// 修改订单状态和支付方式 并执行存储过程
	private void setOrderStatuspaytype(String states) {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setOrderStatuspaytype" };
		String[] nameParam1 = new String[] { "order_id", order_id };
		// 订单状态 10生成订单 20用户付款完成 30完成订单
		String[] nameParam2 = new String[] { "orderstatus", states }; // 订单付款完成
		// 支付方式 0 余额 1 支付宝 2 微信 现金支付
		String[] nameParam3 = new String[] { "paytype", paytype };
		params2.add(funParam2);
		params2.add(nameParam1);
		params2.add(nameParam2);
		params2.add(nameParam3);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					getOrder_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener getOrder_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				weixing=false;
				zhifubao=false;
				finish();
			}
		}
	};

	// xianjinzhifu
	private void setOrderStatus1() {
		ArrayList<String[]> params2 = new ArrayList<String[]>();
		String[] funParam2 = new String[] { "fun", "setOrderStatus1" };
		String[] nameParam1 = new String[] { "order_id", order_id };
		params2.add(funParam2);
		params2.add(nameParam1);
		progressDialog = ProgressDialog.show(this,
				getString(R.string.app_name),
				getString(R.string.tv_dialog_context), false);
		Constant.NET_STATUS = Utils.getCurrentNetWork(this);
		if (Constant.NET_STATUS) {
			new HttpConnection().get(Constant.URL, params2,
					setOrderStatus1_callbackListener);
		} else {
			Toast.makeText(this, "请检查网络连接", Toast.LENGTH_LONG).show();
		}
	}

	private CallbackListener setOrderStatus1_callbackListener = new HttpConnection.CallbackListener() {
		@Override
		public void callBack(String v) {
			Log.d("返回数据", v);
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			if (!v.equals("fail")) {
				finish();
			}
		}
	};

}
